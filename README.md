# SMASH : études et modèles - langage R

## Introduction 

SMASH est un outil de micro-Simulation de la MAsse Salariale Hospitalière 
développé à la DREES/Bureau des Professions de Santé.

Ce dépôt rassemble les scripts d'**études** et mises au points des 
**modèles** utilisés par l'outil de micro-simulation SMASH. 

## Configuration du projet

L'emplacement d'accès aux données est configurable depuis le fichier `config.yml`
(package R `config`) : 

- Créer une copie du fichier `config-template.yml`, nommer la `config.yml`
- Si vous utilisez des modèles en local, créez une section avec votre `prenom.nom`,
voir les explications dans le template. 

**Remarque** :  
Le fichier `config.yml` n'est pas suivi dans le dépôt GIT pour ne pas 
exposer les chemins personnels de l'application. Il faut donc laisser le fichier 
`config-template.yml` dans le suivi de version et y ajouter les variables utiliser par les applications. 


## Organisation du code / Dossiers

- **etudes** : 
  + **analyses** : scripts d'études des données.
  + **evaluations** : scripts pour l'évaluation de paramètres et des tests de cohérence.
  + **modeles** : scripts pour la génération des modèles. Ces scripts prendront
  le plus souvent la forme de notebooks R qui permettent de documenter 
  directement les tests effectués (avec knitR).
- **preparationDonnees** : scripts SAS et R pour la mise en forme des données 
et conversions au format R (RDS) à partir des différentes sources, notamment 
les données SIASP. À chaque évolution des données sélectionnés, ces scripts 
ne sont à faire tourner qu'une seul fois. 
- **simulations** : scripts de simulations qu'on ne souhaite pas intégrer au package.
- **Tests** : extra-tests

### Dossier du micro-simulateur

Les fonctions pour lancer et paramétrer les micro-simulations SMASH sont 
développées dans un projet séparé sous forme de package
R : https://gitlab.com/gilles-gonon-drees/smash-r-pkg.

Pour utiliser les fonctions du package du simulateur SMASH, le plus simple est de :

1. Cloner le dépôt en local au même niveau que ce dossier :
```bash
git clone https://gitlab.com/gilles-gonon-drees/smash-r-pkg.git .
```
2. Charger le package dans R, adapter le chemin relatif à votre config. : 
```r
devtools::load_all('../smash_r_pkg')
```

### Dossier `etudes`

Les scripts de ce dossier rassemblent les études réalisées pour la mise en place et l'apprentissage des modèles. 
Ils doivent pouvoir être relancés facilement, par tous les membres du projet. 
Pour cela, il faut donc bien s'assurer de n'utiliser des chemins personnels que 
via le fichier de configuration.

### Dossier `simulations`

Ce dossier rassemble différents scénarios de micro-simulation utilisant 
les fonctions du package smash. 

> ⚠️ À ce stade, le package est encore en phase 
intensive de développement, et il est fortement conseillé d'utiliser les 
simulations du dossier `inst/exemples` du package. 

### Dossier `tests`

Ce dossier rassemble des tests pour la mise au point des différentes 
fonctionnalités développées. Les fonctions de ce dossier sont à utiliser avec 
précaution. 

## Données utilisées

- Les données utilisées sont situées sur le disque T de la DREES
- **Les données ne doivent pas être incluses dans ce dépôt**.
- Les données pouvant être rendues disponibles (grilles indiciaires, 
infos réglementaires, modèles issus des données, ...) seront mises à 
disposition sur une autre source. 

## Prétraitement des données

Les données SIASP sont historiquement disponibles au format SAS. 
Nous effectuons un prétraitement pour les convertir au format FST dont le 
chargement est plus rapide dans R. Pour cela on isole


