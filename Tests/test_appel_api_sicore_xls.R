# Exemple d'appel de l'API SICORE depuis R ----
# Ce programme lire un fichier XLS contenant des libellés et boucle sur une 
# liste de libellés en faisant N requêtes par secondes
# 
library(httr)
library(xlsx)


# Chargement du fichier de libellés à tester (Colonne : LIBELLE)
df_libelles <- xlsx::read.xlsx("~/data/fichier_libelles.xlsx", 
                               sheetName = "Sheet1")

# Modèle de requête (libellé sera changé à chaque fois)
mdl_requete <- list(libelle = libelle, 
                    environnement = 'PCSDADS') 
#, variables = list(NAF ="8610Z", VS1 = "Q", VS2 = "U", VS3 = "S27", VS4 = "433A"))

# Nb de requêtes par seconde
nb_requetes_secondes <- 10

# API Appelée
url <- "http://api.sicore.insee.fr/sicorews/codif"

# JSON encoded
drees_config <- use_proxy("100.78.56.201", 8080)

# Initialisation des résultats
df_results <- data.frame(LIBELLE = v_libelles_test, 
                         PCS = rep("", length(v_libelles_test)),
                         CODE_SICORE = rep("", length(v_libelles_test)),
                         FORME = rep("", length(v_libelles_test)),
                         ENVIRONNEMENT = rep("", length(v_libelles_test)),
                         ERREUR = rep("", length(v_libelles_test)))

# browser()

for (idx in seq(nrow(df_libelles))) {
  
  # Données POST intègre le libelle et éventuellement d'autres variables
  l_requete <- mdl_requete
  l_requete$libelle <- df_libelles$LIBELLE[idx]
  
  # Appel API
  r <- POST(url, drees_config, body = l_requete, encode = "json") #, verbose())
  
  # Analyse réponse
  if (r$status_code == 200) {
    l_reponse <- content(r)
    # Recommence en PCS 2003 si code ne commence pas par "RC" (RC* ou RCS)
    if (! startsWith(l_reponse$sicoreCodeRetour,"RC")) {
      # Appel API
      l_requete$environnement <- "PCS2003R"
      r <- POST(url, drees_config, body = l_requete, encode = "json")
      if (r$status_code == 200) {
        l_reponse <- content(r)
      } else {
        # Problème de requête HTTP
        df_results$ERREUR[idx] = paste("ERREUR HTTP:", r$status_code)
        next        
      }
    }
    
    # Stockage des résultats dans data.frame
    df_results$PCS[idx] = l_reponse$code
    df_results$CODE_SICORE[idx] = l_reponse$sicoreCodeRetour
    df_results$FORME[idx] = l_reponse$forme
    df_results$ENVIRONNEMENT[idx] = l_requete$environnement
    df_results$ERREUR[idx] = l_reponse$sicorevariablesErreur
    
  } else {
    
    # Problème de requête HTTP
    df_results$ERREUR[idx] = paste("ERREUR HTTP:", r$status_code)
  } 
  
  # Pause pour ne pas surcharger l'API
  
  # 1s de pause toute les 100 requêtes
  pause_add <- ifelse((idx %% 100), 0, 1)
  if (pause_add) {
    cat(paste("Requête", idx, "sur", length(v_libelles_test), "\n"))
  }
  
  Sys.sleep(1/nb_requetes_secondes + pause_add)
  
}

# Sauvegarde du résultat en Excel
write.xlsx(df_results, 
           paste("~/data/Libelles-PCS-sicore-", 
                 strftime(Sys.time(), "%Y%m%d-%H%M"),".xlsx"), 
           row.names = FALSE)

