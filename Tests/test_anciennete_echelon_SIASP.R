# Tests sur l'ancienneté dans les données SIASP ----
# On regarde en détail sur N-1, N et N+1 les indices des personnes qui 
# ont changés de grade avec SMASH mais pas dans la réalité. 


## Imports ----
library(dplyr)
library(plotly)
devtools::load_all("../smash-r-pkg")

## Chargement des données ----
champ <- "hop"


l_pop <- list()
for (an in seq(2015,2020)) {
  strAn <- as.character(an)
  l_pop[[strAn]] <- chargerFST(champ, an)
}

v_annees <- names(l_pop)
strAnDeb <- v_annees[1]
strAnFin <- v_annees[length(v_annees)]


## Sélection de la population pour comparaison ----
anGVT <- 2018
strAn <- as.character(anGVT)

anPrec <- anGVT-1
strAnPrec <- as.character(anPrec)

anSuiv <- anGVT+1
strAnSuiv <- as.character(anSuiv)

# An 
popTITH_an <- l_pop[[strAn]] %>% filter(
  STATUT_CONTRAT=="TITH" & POSTE_PRINC_AN=="P" & DUREE_POSTE==365 & !is.na(IM))
popTITH_an$GRADE <- popTITH_an$GRADE_ECH
popTITH_an$ANCIENNETE_ECH <- popTITH_an$ANC_ECHELON / 365

# An précédent
popTITH_anPrec <- l_pop[[strAnPrec]] %>% filter(
  STATUT_CONTRAT=="TITH" & POSTE_PRINC_AN=="P" & DUREE_POSTE==365 & !is.na(IM))
popTITH_anPrec$GRADE <- popTITH_anPrec$GRADE_ECH
popTITH_anPrec$ANCIENNETE_ECH <- popTITH_anPrec$ANC_ECHELON / 365

# An Suivant
popTITH_anSuiv <- l_pop[[strAnSuiv]] %>% filter(
  STATUT_CONTRAT=="TITH" & POSTE_PRINC_AN=="P" & DUREE_POSTE==365 & !is.na(IM))
popTITH_anSuiv$GRADE <- popTITH_anSuiv$GRADE_ECH
popTITH_anSuiv$ANCIENNETE_ECH <- popTITH_anSuiv$ANC_ECHELON / 365


# Sélection des personnes présentes les deux années (Intersection)
v_ids <- intersect(popTITH_an$ID_NIR20, popTITH_anPrec$ID_NIR20)

popTITH_an <- popTITH_an %>% filter(ID_NIR20 %in% v_ids) %>% arrange(ID_NIR20)
popTITH_anPrec <- popTITH_anPrec %>% filter(ID_NIR20 %in% v_ids) %>%
  arrange(ID_NIR20)
popTITH_anSuiv <- popTITH_anSuiv %>% filter(ID_NIR20 %in% v_ids) %>%
  arrange(ID_NIR20)

cat(sprintf(" - %d personnes après sélection des présents %s-%s\n",
            nrow(popTITH_an), strAn, strAnPrec))

# On filtre aussi ceux qui ont le même EQTP
is_etqp_equal <- abs(popTITH_an$EQTP - popTITH_anPrec$EQTP)<0.01
popTITH_an <- popTITH_an[is_etqp_equal, ]
popTITH_anPrec <- popTITH_anPrec[is_etqp_equal, ]

cat(sprintf(" - %d personnes après sélection EQTP identiques %s-%s\n",
            nrow(popTITH_an), strAn, strAnPrec))


# On garde ceux qui sont dans le même établissement
is_same_etab <- popTITH_an$SIREN == popTITH_anPrec$SIREN
popTITH_an <- popTITH_an[is_same_etab, ]
popTITH_anPrec <- popTITH_anPrec[is_same_etab, ]

cat(sprintf(" - %d personnes après sélection même établissement %s-%s (SIREN identiques)\n",
            nrow(popTITH_an), strAn, strAnPrec))

## Création de la population contre-factuelle ----
#.................................................
# Évolution de l'IM avec SMASH (sans évolution de la réglementation)

# Préparation des variables

popTITH_an_contrefact <- popTITH_anPrec %>% arrange(ID_NIR20)
popTITH_an_contrefact$STATUT <- "FPH"
popTITH_an_contrefact$ANCIENNETE_ECH <- popTITH_an_contrefact$ANC_ECHELON / 365

# On fait vieillir les effectifs
popTITH_an_contrefact$AGE <- popTITH_an_contrefact$AGE + 1
popTITH_an_contrefact$ANCIENNETE_ECH <- popTITH_an_contrefact$ANCIENNETE_ECH + 1

# Ajout d'un TAG pour suivre le type d'évolution
popTITH_an_contrefact$TAG <- ""

# 1: On tague ceux qui n'ont pas changé d'IM entre N-1 et N
is_same_im <- popTITH_an$IM == popTITH_anPrec$IM
is_same_grade <- popTITH_an$GRADE == popTITH_anPrec$GRADE
popTITH_an_contrefact[is_same_im, ]$TAG <- 'PAS_CHGMT'

# 2: On isole ceux qui n'ont pas changé de grade (mais qui ont changé d'IM)
popTITH_an_contrefact[is_same_grade & !is_same_im, ]$TAG <- 'CHGMT_ECH_GRILLE'

# 3: On les fait avancer sur leur grille avec SMASH
popTITH_an_contrefact[is_same_grade & !is_same_im, ] <-
  majIndiceMajore(popTITH_an_contrefact[is_same_grade & !is_same_im, ],
                  anPrec, objBDD = NULL)

# 4: Vérification qu'on n'a pas trop augmenté les IM contrefactuels
# sans doute un problème d'ancienneté
is_im_superieur <- is_same_grade & !is_same_im & (popTITH_an_contrefact$IM > popTITH_an$IM)
if (any(is_im_superieur)) {
  cat(" -", sum(is_im_superieur), "recalages d'augmentations intempestives",
      "d'IM (contrefactuel>observés)\n   somme de l'augmentation corrigée en indice =",
      sum(popTITH_an_contrefact[is_im_superieur, ]$IM - popTITH_an[is_im_superieur, ]$IM),
      "\n")
  
 # popTITH_an_contrefact[is_im_superieur, ]$IM <- popTITH_an[is_im_superieur, ]$IM
}

# Merge des gens qui ont un échelon plus grand en contrefactuel
v_ids_sup <- popTITH_an_contrefact[is_im_superieur, ]$ID_NIR20

# On vérifie que tout va bien en comparant GRADE Et IM.
cat(" - création d'un datra.frame de comparaison des IM sur N-1, N, N+1\n")

# 1 : merge anPrec et an contrefactuel
df_merge <- merge.data.frame(
  popTITH_anPrec[,c('ID_NIR20','GRADE','IM', 'ECH_RANK', 'ANCIENNETE_ECH')] %>% filter(ID_NIR20 %in% v_ids_sup),
  popTITH_an_contrefact[,c('ID_NIR20','GRADE','IM', 'ECH_RANK', 'ANCIENNETE_ECH')] %>% filter(ID_NIR20 %in% v_ids_sup),
  by="ID_NIR20", suffixes=c('_N-1','_Contrefact'))

# 2 : merge + an
df_merge <- merge(df_merge, 
                  popTITH_an[,c('ID_NIR20','GRADE','IM', 
                                'ECH_RANK', 'ANCIENNETE_ECH')] %>% filter(ID_NIR20 %in% v_ids_sup),
                  by="ID_NIR20") %>% rename(GRADE_N = GRADE,
                                            IM_N = IM,
                                            ECH_RANK_N = ECH_RANK,
                                            ANCIENNETE_ECH_N = ANCIENNETE_ECH)

df_merge <- merge.data.frame(df_merge, 
                  popTITH_anSuiv[,c('ID_NIR20','GRADE','IM', 
                                    'ECH_RANK', 'ANCIENNETE_ECH')] %>% 
                    filter(ID_NIR20 %in% v_ids_sup),
                  by="ID_NIR20", all.x= T)  %>% rename(`GRADE_N+1` = GRADE,
                                                       `IM_N+1` = IM,
                                                       `ECH_RANK_N+1` = ECH_RANK,
                                                       `ANCIENNETE_ECH_N+1` = ANCIENNETE_ECH)


# Ajout de la durée de l'échelon
cat(" - ajout de la durée de chaque grille/échelon pour info (long)\n")

o <- GrillesIndiciaires$new()
date_s <- sprintf("%d-12-31", anGVT-1)
v_id_grilles <- o$getGrillesId(df_merge$`GRADE_N-1`, date_s, fill_NA = T)$ID_GRILLE
# getDureeEch <- function(x) { 
#   return(o$getGrilleEchRank(id_grille_s = x[1], ech_rk_s = x[2], date_s)$DUREE) 
# }
# df_merge$`DUREE_ECH_N-1` <- apply(
#   X = data.frame(ID = v_id_grilles, ECH_RANK = df_merge$`ECH_RANK_N-1`), 
#   1, getDureeEch )

df_merge$`DUREE_ECH_N-1` <- NA

for (id_g in unique(v_id_grilles)) {
  is_g <- v_id_grilles==id_g
  df_grille <- o$getGrilleById(id_g)
  v_idx_echrk <- match(df_merge[is_g, ]$`ECH_RANK_N-1`, df_grille$ECHELON_RANK)
  df_merge[is_g, ]$`DUREE_ECH_N-1` <- df_grille$DUREE[v_idx_echrk]
}


### Sauvegarde de la table pour analyse ----

cat("Affichage des 10 premières lignes sur", nrow(df_merge),
    "du merge \n des 3 échantillons popAN, popAnPrec, popAnContrefact, popAnSuiv\n")

print(head(df_merge, 10))
write.table(df_merge, "~/Analyse_Pb_Anciennete_SIASP_2017-2019.csv")
