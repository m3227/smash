# Test rapide pour quantifier les changements de contrats ----

devtools::load_all("../smash-r-pkg/")
# Chargement des données
annee <- 2018
df_an <- getEchantillonSIASP("hop", annee, 0, b_agrege = T)
df_anp1 <- getEchantillonSIASP("hop", annee+1, 0, b_agrege = T)

# ID des présents / présents
v_ids_pp <- intersect(df_an$ID, df_anp1$ID)

# Jointure des présents/présents (pp)
df_pp <- left_join(df_an %>% filter(ID %in% v_ids_pp), 
                   df_anp1 %>% filter(ID %in% v_ids_pp) %>% 
                     select(ID, STATUT_CONTRAT, DUREE_CONTRAT, ANCIENNETE_FPH, 
                            GRADE, IM, ECH, EQTP, IS_ENTRANT, IS_SORTANT),
                   by = "ID", suffix = c(".an", ".anp1"))

df_chgt <- df_pp %>% count(DUREE_CONTRAT.an, STATUT_CONTRAT.an, 
                           DUREE_CONTRAT.anp1, STATUT_CONTRAT.anp1, 
                           sort = T, name = 'Total') %>% 
  mutate(PCT = round(100*Total/length(v_ids_pp), digits = 2)) %>% 
  mutate(CUM_PCT <- cumsum(PCT))


nb_tot <- length(v_ids_pp)
nb_meme_cont <- sum((df_chgt %>% 
                       filter(DUREE_CONTRAT.an==DUREE_CONTRAT.anp1 & 
                                STATUT_CONTRAT.an==STATUT_CONTRAT.anp1))$Total)

pct_chgt_cont <- round(100*(nb_tot-nb_meme_cont) / nb_tot, digits = 2)

cat("\nPourcentage des présents/présents",annee, "/", annee+1," ayant changé de contrat :", 
    pct_chgt_cont, "%\n\n")

nb_cont_cdd <- nrow(df_pp %>% filter(STATUT_CONTRAT.an=="CONT" & DUREE_CONTRAT.an=="CDD"))
nb_cont_cdd_tith <- nrow(
  df_pp %>% filter(STATUT_CONTRAT.an=="CONT" & DUREE_CONTRAT.an=="CDD" & 
                        STATUT_CONTRAT.anp1=="TITH") )

nb_cont_cdd_cdi <- nrow(
  df_pp %>% filter(STATUT_CONTRAT.an=="CONT" & DUREE_CONTRAT.an=="CDD" & 
                        STATUT_CONTRAT.anp1=="CONT" & DUREE_CONTRAT.anp1=="CDI") )

cat("Pour les CDD présents/présents",annee, "/", annee+1,":\n")
cat("- Titularisations :", round(100*nb_cont_cdd_tith/nb_cont_cdd, digits = 2), "%\n")
cat("- Passages en CDI :", round(100*nb_cont_cdd_cdi/nb_cont_cdd, digits = 2), "%\n")


# Passages INTE -> MEDI 
an <- 2020
df_anPrec <- getEchantillonSIASP(champ = "hop", an-1, b_agrege = T)
df_an <- getEchantillonSIASP(champ = "hop", an, b_agrege = T)
v_ids <- intersect(df_anPrec$ID, df_an$ID)
df_statut <- inner_join(df_anPrec %>% select(ID, STATUT_CONTRAT, ANCIENNETE_FPH) %>% 
                          rename(STATUT_ANPREC = STATUT_CONTRAT,
                                 ANC_ANPREC = ANCIENNETE_FPH), 
                        df_an %>% select(ID, STATUT_CONTRAT) %>% 
                          rename(STATUT_AN = STATUT_CONTRAT), by="ID")
df_c <- df_statut %>% count(STATUT_ANPREC, STATUT_AN) %>% arrange(desc(n))
View(df_c %>% filter(STATUT_ANPREC == "INTE"))
df_c_anc <- df_statut %>% mutate(ANC_ANPREC = round(ANC_ANPREC)) %>% 
  count(STATUT_ANPREC, STATUT_AN, ANC_ANPREC) %>% 
  arrange(desc(n))
View(df_c_anc %>% filter(STATUT_ANPREC == "INTE" & 
                           STATUT_AN %in% c("INTE", "MEDI")))
