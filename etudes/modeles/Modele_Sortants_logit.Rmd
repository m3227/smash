---
title: "Caracteristiques des sortants"
author: "BPS DREES - Maud Galametz"
date: "17/12/2021"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r librairies}
library(fBasics)
library(plyr)
library(pROC)
## Chargement des données 
Sys.setenv(R_CONFIG_ACTIVE = Sys.getenv('username'))
config <- config::get()
chemin <- config$path_siasp

# Chargement package Smash dev
devtools::load_all(config$path_smash)

Tx_err <- function(y, ypred) {
    mc <- table(y, ypred)
    error <- (mc[1, 2] + mc[2, 1])/sum(mc)
    print(error)
}

```


## Présentation 

Dans ce fichier, on va générer le modèle de nos sortants.


 
## Chargement des données
 
On charge les données SIASP de 2017 à 2020. On charge également les tables
des entrants afin de caractériser les actifs stables.

```{r chargement}

dfEnt2020_t <- chargerFST("hop", 2020, b_entrants = TRUE, chemin = chemin, verbose = 0)
dfEnt2019_t <- chargerFST("hop", 2019, b_entrants = TRUE, chemin = chemin, verbose = 0)
dfEnt2018_t <- chargerFST("hop", 2018, b_entrants = TRUE, chemin = chemin, verbose = 0)
dfEnt2017_t <- chargerFST("hop", 2017, b_entrants = TRUE, chemin = chemin, verbose = 0)
dfEnt2016_t <- chargerFST("hop", 2016, b_entrants = TRUE, chemin = chemin, verbose = 0)

dfSor2019_t <- readRDS(file.path(chemin, "hop_sortants2019_selection.rds"))
dfSor2018_t <- readRDS(file.path(chemin, "hop_sortants2018_selection.rds"))
dfSor2017_t <- readRDS(file.path(chemin, "hop_sortants2017_selection.rds"))
dfSor2016_t <- readRDS(file.path(chemin, "hop_sortants2016_selection.rds"))
dfSor2015_t <- readRDS(file.path(chemin, "hop_sortants2015_selection.rds"))

dfEff2020_t <- readRDS(file.path(chemin, "hop2020_selection.rds"))
dfEff2019_t <- readRDS(file.path(chemin, "hop2019_selection.rds"))
dfEff2018_t <- readRDS(file.path(chemin, "hop2018_selection.rds"))
dfEff2017_t <- readRDS(file.path(chemin, "hop2017_selection.rds"))
dfEff2016_t <- readRDS(file.path(chemin, "hop2016_selection.rds"))
dfEff2015_t <- readRDS(file.path(chemin, "hop2015_selection.rds"))

dfEnt2020_t <- dfEnt2020_t[dfEnt2020_t$EQTP > 0.2,]
dfEnt2019_t <- dfEnt2019_t[dfEnt2019_t$EQTP > 0.2,]
dfEnt2018_t <- dfEnt2018_t[dfEnt2018_t$EQTP > 0.2,]
dfEnt2017_t <- dfEnt2017_t[dfEnt2017_t$EQTP > 0.2,]
dfSor2019_t <- dfSor2019_t[dfSor2019_t$EQTP > 0.2,]
dfSor2018_t <- dfSor2018_t[dfSor2018_t$EQTP > 0.2,]
dfSor2017_t <- dfSor2017_t[dfSor2017_t$EQTP > 0.2,]
dfSor2016_t <- dfSor2016_t[dfSor2016_t$EQTP > 0.2,]
dfEff2020_t <- dfEff2020_t[dfEff2020_t$EQTP > 0.2,]
dfEff2019_t <- dfEff2019_t[dfEff2019_t$EQTP > 0.2,]
dfEff2018_t <- dfEff2018_t[dfEff2018_t$EQTP > 0.2,]
dfEff2017_t <- dfEff2017_t[dfEff2017_t$EQTP > 0.2,]

```


On sélectionne les Variables d’intérêt pour alléger les tables. 
On retire les agents décédés.
Si on ne croit pas MOTIF_SORTIE == 'RETR' comme tag (car beaucoup d'agents 
ayant ce motif de sortie reste en poste l'année suivante), le tag 
MOTIF_SORTIE == 'DECE' a l'air correct.


```{r Selection Variables}
l_cols_sel <-  c('ID_NIR20', 'SEXE', 'AGE', 'CORPS', 'GRADE', 'QUOTITE',
                 'STATUT_CONTRAT', 'S_BRUT', 'DUREE_POSTE','MOTIF_SORTIE',
                 'DUREE_CONTRAT', 'EQTP')

id_DECEDES2015 <- unique(dfEff2015_t[dfEff2015_t$MOTIF_SORTIE == 'DECE', c('ID_NIR20')])

dfEnt2016 <- dfEnt2016_t[dfEnt2016_t$MOTIF_SORTIE != 'DECE', l_cols_sel]
dfSor2016 <- dfSor2016_t[dfSor2016_t$MOTIF_SORTIE != 'DECE', l_cols_sel]
dfEff2016 <- dfEff2016_t[(dfEff2016_t$MOTIF_SORTIE != 'DECE') & 
                           !(dfEff2016_t$ID_NIR20 %in% id_DECEDES2015), 
                         l_cols_sel]

dfEffFixe2016 <- dfEff2016[!(dfEff2016$ID_NIR20 %in% dfEnt2016$ID_NIR20),]
dfEffFixe2016 <- dfEffFixe2016[!(dfEffFixe2016$ID_NIR20 %in% dfSor2016$ID_NIR20),]
id_DECEDES2016 <- unique(dfEff2016_t[dfEff2016_t$MOTIF_SORTIE == 'DECE', c('ID_NIR20')])

dfEnt2017 <- dfEnt2017_t[dfEnt2017_t$MOTIF_SORTIE != 'DECE', l_cols_sel]
dfSor2017 <- dfSor2017_t[dfSor2017_t$MOTIF_SORTIE != 'DECE', l_cols_sel]
dfEff2017 <- dfEff2017_t[(dfEff2017_t$MOTIF_SORTIE != 'DECE') & !(dfEff2017_t$ID_NIR20 %in% id_DECEDES2016), l_cols_sel]
dfEffFixe2017 <- dfEff2017[!(dfEff2017$ID_NIR20 %in% dfEnt2017$ID_NIR20),]
dfEffFixe2017 <- dfEffFixe2017[!(dfEffFixe2017$ID_NIR20 %in% dfSor2017$ID_NIR20),]
id_DECEDES2017 <- unique(dfEff2017_t[dfEff2017_t$MOTIF_SORTIE != 'DECE', c('ID_NIR20')])

dfEnt2018 <- dfEnt2018_t[dfEnt2018_t$MOTIF_SORTIE != 'DECE', l_cols_sel]
dfSor2018 <- dfSor2018_t[dfSor2018_t$MOTIF_SORTIE != 'DECE', l_cols_sel]
dfEff2018 <- dfEff2018_t[(dfEff2018_t$MOTIF_SORTIE != 'DECE') &  
                           !(dfEff2018_t$ID_NIR20 %in% id_DECEDES2017), l_cols_sel]
dfEffFixe2018 <- dfEff2018[!(dfEff2018$ID_NIR20 %in% dfEnt2018$ID_NIR20),]
dfEffFixe2018 <- dfEffFixe2018[!(dfEffFixe2018$ID_NIR20 %in% dfSor2018$ID_NIR20),]
id_DECEDES2018 <- unique(dfEff2018_t[dfEff2018_t$MOTIF_SORTIE != 'DECE', c('ID_NIR20')])

dfEnt2019 <- dfEnt2019_t[dfEnt2019_t$MOTIF_SORTIE != 'DECE', l_cols_sel]
dfSor2019 <- dfSor2019_t[dfSor2019_t$MOTIF_SORTIE != 'DECE', l_cols_sel]
dfEff2019 <- dfEff2019_t[(dfEff2019_t$MOTIF_SORTIE != 'DECE') &  !(dfEff2019_t$ID_NIR20 %in% id_DECEDES2018), l_cols_sel]
dfEffFixe2019 <- dfEff2019[!(dfEff2019$ID_NIR20 %in% dfEnt2019$ID_NIR20),]
dfEffFixe2019 <- dfEffFixe2019[!(dfEffFixe2019$ID_NIR20 %in% dfSor2019$ID_NIR20),]

# fonction de maj des grades
Update_Grade <- function(df){
  Tablepassage  <- as.data.frame(
    read.csv(paste0("I:/BPS/_Emplois et revenus/Projet SMASH/",
                    "04_Modele/GrillesIndiciaires/",
                    "TablePassageGrade_modifseules.csv", header=TRUE, sep = ";"))
    )
  Old_grade <-  Tablepassage$grade_final
  New_grade <-  Tablepassage$grade2020                                 
  for (i in 1:length(Old_grade)){
    df[df$GRADE == Old_grade[i], c('GRADE')] <- 
      rep(New_grade[i], nrow(df[df$GRADE == Old_grade[i],]))
  }
  return(df)
}

# maj des grades
dfEnt2019 <- Update_Grade(dfEnt2019)
dfSor2019 <- Update_Grade(dfSor2019)
dfEff2019 <- Update_Grade(dfEff2019)
dfEffFixe2019 <- Update_Grade(dfEffFixe2019)
dfEnt2018 <- Update_Grade(dfEnt2018)
dfSor2018 <- Update_Grade(dfSor2018)
dfEff2018 <- Update_Grade(dfEff2018)
dfEffFixe2018 <- Update_Grade(dfEffFixe2018)
dfEnt2017 <- Update_Grade(dfEnt2017)
dfSor2017 <- Update_Grade(dfSor2017)
dfEff2017 <- Update_Grade(dfEff2017)
dfEffFixe2017 <- Update_Grade(dfEffFixe2017)
dfEnt2016 <- Update_Grade(dfEnt2016)
dfSor2016 <- Update_Grade(dfSor2016)
dfEff2016 <- Update_Grade(dfEff2016)
dfEffFixe2016 <- Update_Grade(dfEffFixe2016)

```



## Modélisation des sortants

On ajoute une colonne flaguant les hop_entrants / sortants dans les populations avant de concaténer les effectifs pour l'apprentissage.

```{r Ajout colonne}

dfEffFixe2019$SORTANT = rep(0, NROW(dfEffFixe2019))
dfEffFixe2018$SORTANT = rep(0, NROW(dfEffFixe2018))
dfEffFixe2017$SORTANT = rep(0, NROW(dfEffFixe2017))
dfEffFixe2016$SORTANT = rep(0, NROW(dfEffFixe2016))

dfEnt2017$SORTANT = rep(0, NROW(dfEnt2017))
dfEnt2018$SORTANT = rep(0, NROW(dfEnt2018))
dfEnt2019$SORTANT = rep(0, NROW(dfEnt2019))
dfEnt2020$SORTANT = rep(0, NROW(dfEnt2020))

dfSor2016$SORTANT = rep(1, NROW(dfSor2016))
dfSor2017$SORTANT = rep(1, NROW(dfSor2017))
dfSor2018$SORTANT = rep(1, NROW(dfSor2018))
dfSor2019$SORTANT = rep(1, NROW(dfSor2019))

```



On va retirer les retraites de notre échantillon d'apprentissage

```{r Fonction estimation retraites}

estimationProbabiliteRetraite <- function(df, fic_retraites = fic_retraites, loi = ""){
  
  #%% Charger le fichier modele de retraites par grade
  load(fic_retraites)

  #%% Determiner les probabilités de départ à la retraite et le nombre de retraités total attendu 
  NbreRetraites = 0
  
  df$id_retraite <- rep(F, nrow(df))
    
  for (grade_ in unique(df$GRADE)){

    if (grade_ %in% l_grade_modelise){
      
      dftemp <- df[df$GRADE == grade_,]
      NbreRetraites <- floor(Taux[[grade_]] * NROW(dftemp))
      
      if (nrow(dftemp) > 0){

       # On choisit qui part en retraite
        modele <- loi_retraites[[grade_]]
        Shape <- modele[[1]][1]
        Scale <- modele[[1]][2]
      
        if (loi == 'weibull'){
          probas <- pweibull(dftemp$AGE, Shape, Scale)
        }
        if (loi == 'norm'){
          probas <- pnorm(dftemp$AGE, Shape, Scale)
        }      
        if (NbreRetraites > 0){
        retr = sample(dftemp$ID_NIR20, NbreRetraites, replace = FALSE, prob=probas)
        df[df$ID_NIR20 %in% retr, c('id_retraite')] <- rep(T, length(df[df$ID_NIR20 %in% retr, c('id_retraite')]))
        }
      }
    } else{
            df[(df$GRADE == grade_) & (df$AGE > 60), c('id_retraite')] <- rep(T, length(df[(df$GRADE == grade_) & (df$AGE > 60), c('id_retraite')]))
      }
  
  }
  return(df)
} 
```


```{r Ajout colonne}

fic_retraites <- file.path("T:/BPS/SMASH/modeles/modele_retraites_all_grade_weibull.Rdata")
dfEff2016_retr <- estimationProbabiliteRetraite(dfEff2016, fic_retraites = fic_retraites, loi = 'weibull')
dfEff2017_retr <- estimationProbabiliteRetraite(dfEff2017, fic_retraites = fic_retraites, loi = 'weibull')
dfEff2018_retr <- estimationProbabiliteRetraite(dfEff2018, fic_retraites = fic_retraites, loi = 'weibull')
dfEff2019_retr <- estimationProbabiliteRetraite(dfEff2019, fic_retraites = fic_retraites, loi = 'weibull')

# dfEff2016_retr <- dfEff2016_retr[dfEff2016_retr$id_retraite == FALSE, ]
# dfEff2017_retr <- dfEff2017_retr[dfEff2017_retr$id_retraite == FALSE, ]
# dfEff2018_retr <- dfEff2018_retr[dfEff2018_retr$id_retraite == FALSE, ]
# dfEff2019_retr <- dfEff2019_retr[dfEff2019_retr$id_retraite == FALSE, ]
  
```


```{r Test de robustesse TITH}

p1 <- hist(dfEff2019[, c('AGE')], breaks=c(5:95), 
           main = " ", xlim=c(0, 80), xlab='Repartition Age', ylim = c(0,2000))
p2 <- hist(dfSor2019[, c('AGE')], breaks=c(5:95), 
           main = " ", xlim=c(0, 80), xlab='Repartition Age', ylim = c(0,2000))
p3 <- hist(dfEff2019_retr[dfEff2019_retr$id_retraite == 'TRUE', c('AGE')], breaks=c(5:95),
          main = " ", xlim=c(0, 80), xlab='Repartition Age', ylim = c(0,2000))

plot(p1, col=rgb(1,0,0,8/9), main=" ", xlab='Repartition Age', ylab='Nombre')
plot(p2, col=rgb(0,0,1,8/9), main=" ", xlab='Repartition Age', ylab='Nombre', add=TRUE)
plot(p3, col=rgb(0,1,0,8/9), add=TRUE)
legend("topright", legend=c("All", "Sortants", "Retraites"), col=c("red", "blue", "green"), lty=1:1)

```



On crée tout d'abord un échantillon d'apprentissage et un échantillon test. 
On veille à ne garder qu'une ligne par agent. 
L'Apprentissage semble très lourd pour R sur les données totales: on réduit l'echantillon
d'apprentissage dans un premier temps.


```{r Echantillon Apprentissage et test}

# On apprend sur les agents 2017-2018, on appliquera sur les agents 2019
dftrain <- rbind(dfSor2017, dfEffFixe2018, dfSor2018)
#dftrain[dftrain$EQTP > 0.2,]
# Apprentissage sur tout apparemment trop lourd pour R : on réduit l'echantillon
NEch = 100000
TrainSet <- dftrain[dftrain$ID_NIR20 %in% sample(dftrain$ID_NIR20, NEch, replace=F),]
TrainSet <- as.data.frame(TrainSet %>% group_by(ID_NIR20) %>% slice_max(order_by = DUREE_POSTE))
TrainSet <- as.data.frame(TrainSet %>% group_by(ID_NIR20) %>% slice_max(order_by = QUOTITE))
TrainSet <- as.data.frame(TrainSet %>% group_by(ID_NIR20) %>% slice_max(order_by = S_BRUT))

# Echantillon de test
dftest <- rbind(dfEffFixe2019, dfEnt2019, dfSor2019)   
Ntest = 10000
TestSet <- dftest[dftest$ID_NIR20 %in% sample(dftest$ID_NIR20, Ntest, replace=F),]
TestSet <- as.data.frame(TestSet %>% group_by(ID_NIR20) %>% slice_max(order_by = DUREE_POSTE))
TestSet <- as.data.frame(TestSet %>% group_by(ID_NIR20) %>% slice_max(order_by = QUOTITE))
TestSet <- as.data.frame(TestSet %>% group_by(ID_NIR20) %>% slice_max(order_by = S_BRUT))

```

Et maintenant la modélisation, par regression logistique (modèle binomial).
La dépendence en age précis n'étant pas forcément pertinente, on regroupe les individus par catégorie d'age.


```{r Modelisation}

logit = function(formula, lien = "logit", data = NULL) {
    glm(formula, family = binomial(link = lien), data)
}
TrainSet$TAGE<- floor((TrainSet$AGE+5)/10.)*10
TestSet$TAGE<- floor((TestSet$AGE+5)/10.)*10

l_grade <- unique(TrainSet$GRADE)
count <- count(TrainSet$GRADE)
#TrainSet <- TrainSet[TrainSet$GRADE %in% count[count$freq >100,]$x,]
m.logit <- logit(SORTANT ~ TAGE + GRADE + STATUT_CONTRAT + DUREE_CONTRAT, data = TrainSet)#QUOTITE? CORPS? 

#Autre modélisation disponible avec glm:
# binomial	(link = "logit")
# gaussian	(link = "identity")
# Gamma	(link = "inverse")
# inverse.gaussian	(link = "1/mu^2")
# poisson	(link = "log")
# quasi	(link = "identity", variance = "constant")
# quasibinomial	(link = "logit")
# quasipoisson	(link = "log")

```


### Validation du modèle sur les échantillons


On cherche à valider le modèle sur les données d'apprentissage tout d'abord:

```{r Validation - Set Apprentissage}

TrainSet.p <- cbind(TrainSet, predict(m.logit, newdata = TrainSet, type = "link", se = TRUE))
# Pour obtenir les probabilités prédites pour chaque agent  : probas stockées dans la variable TrainSet.p$PredictedProb
TrainSet.p <- within(TrainSet.p, {
            PredictedProb <- plogis(fit)
            LL <- plogis(fit - (1.96 * se.fit))
            UL <- plogis(fit + (1.96 * se.fit))
})

test_prob = predict(m.logit, newdata = TrainSet, type = "response")
test_roc = roc(TrainSet$SORTANT ~ test_prob, plot = TRUE, print.auc = TRUE)

TrainSet.p <- cbind(TrainSet.p, pred.SORTANT = factor(ifelse(TrainSet.p$PredictedProb > 0.5, 1, 0)))
(m.confusion <- as.matrix(table(TrainSet.p$pred.SORTANT, TrainSet.p$SORTANT)))
m.confusion <- unclass(m.confusion)
#Tx_err(TrainSet.p$pred.SORTANT, TrainSet.p$SORTANT)

```
La matrice de confusion n'est pas franchement rassurante et le taux d'erreur équivaut à notre taux de sortants:
il est donc difficile de flagger un individu en particulier car les comportements diffèrent beaucoup pour un même profil ! 

Décision: 

On va appliquer le modèle sur les données de test et calculer les probabilités de départ pour chaque agent. On va alors tirer nos 12% de sortants en fonction de ces probabilités.




```{r Application du modèle sur le set de test}

#On ne peut predire que les corps qui ont été modélisés - voir comment gérer les autres dans le modèle
#corpstestes <- unique(TrainSet$CORPS)
#TestSet <- TestSet[TestSet$CORPS %in% corpstestes,]
gradetestes <- unique(TrainSet$GRADE)
TestSet <- TestSet[TestSet$GRADE %in% gradetestes,]
statuttestes <- unique(TrainSet$STATUT_CONTRAT)
TestSet <- TestSet[TestSet$STATUT_CONTRAT %in% statuttestes,]

TestSet.p <- cbind(TestSet, predict(m.logit, newdata = TestSet, type = "link", se = TRUE))
TestSet.p <- within(TestSet.p, {
            PredictedProb <- plogis(fit)
            LL <- plogis(fit - (1.96 * se.fit))
            UL <- plogis(fit + (1.96 * se.fit))
})

# On tire les agents sortants suivant ces probabilités
Nsortants = floor(NROW(TestSet)*0.12)
SortantsPredits <- TestSet.p[TestSet.p$ID_NIR20 %in% sample(TestSet.p$ID_NIR20, Nsortants, replace = FALSE, prob=TestSet.p$PredictedProb),]

# moyenne des probabilités pour les agents jeunes et agés
#mean(TrainSet.p[(TrainSet.p$AGE < 30) & (TrainSet.p$STATUT_CONTRAT == 'TITH'), c('PredictedProb')])
#mean(TrainSet.p[(TrainSet.p$AGE > 60) & (TrainSet.p$STATUT_CONTRAT == 'TITH'), c('PredictedProb')])
```


Le modèle est appliqué. 
Verifions maintenant si les sortants prédits ont les mêmes caractéristiques que les 'vrais' sortants.
On produit un échantillon de vrais sortants à comparer avec notre échantillon de sortants produits
(agents en place en 2019 et sortants en 2020, comme pour notre échantillon test).

```{r Donnees de comparaison}
dfSor2020_unique <- as.data.frame(dfSor2020 %>% group_by(ID_NIR20) %>% slice_max(order_by = DUREE_POSTE))
dfSor2020_unique <- as.data.frame(dfSor2020 %>% group_by(ID_NIR20) %>% slice_max(order_by = QUOTITE))
dfSor2020_unique <- as.data.frame(dfSor2020 %>% group_by(ID_NIR20) %>% slice_max(order_by = S_BRUT))
dfSor2020_sub = dfSor2020_unique[dfSor2020_unique$ID_NIR20 %in% sample(dfSor2020_unique$ID_NIR20, Nsortants, replace=F), ]
```


On compare les taux de sortants par type de contrat:

```{r Visualisation des resultats - Type Contrat}
NROW(SortantsPredits[SortantsPredits$STATUT_CONTRAT == 'TITH',]) / NROW(SortantsPredits)
NROW(dfSor2020_sub[dfSor2020_sub$STATUT_CONTRAT == 'TITH',])/ NROW(dfSor2020_sub)
```
Relativement bon recouvrement du taux de titulaires / contractuels 



```{r Visualisation des resultats - Type Contrat}
NROW(SortantsPredits[SortantsPredits$CORPS == 'Aide-soignant et ashq 0226 C',]) / NROW(SortantsPredits)
NROW(dfSor2020_sub[dfSor2020_sub$CORPS == 'Aide-soignant et ashq 0226 C',])/ NROW(dfSor2020_sub)

NROW(SortantsPredits[SortantsPredits$GRADE == '3021',]) / NROW(SortantsPredits)
NROW(dfSor2020_sub[dfSor2020_sub$GRADE == '3021',])/ NROW(dfSor2020_sub)

```
Relativement bon recouvrement du taux d'AS sortants. 


```{r Visualisation des resultats - SEXE}

NROW(SortantsPredits[SortantsPredits$SEXE == 'F',]) / NROW(SortantsPredits)
NROW(dfSor2020_sub[dfSor2020_sub$SEXE == 'F',])/ NROW(dfSor2020_sub)

```
Bon recouvrement du taux hommes : femmes. 



```{r Visualisation des resultats - par corps}
count <- count(SortantsPredits$CORPS)
for (l_corps in count[count$freq >150,]$x) {
  print(c(l_corps, 'Salaire Median des sortants prédits', median(SortantsPredits[SortantsPredits$CORPS == l_corps, c('S_BRUT')])))
  print(c(l_corps, 'Salaire Median des vrais sortants', median(dfSor2020_sub[dfSor2020_sub$CORPS == l_corps, c('S_BRUT')])))
  
}

ggplot(SortantsPredits[SortantsPredits$CORPS %in% count[count$freq >150,]$x,], aes(CORPS, S_BRUT)) + geom_boxplot() + ylim(0, 60000) + ggtitle('Sortants prédits') +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
ggplot(dfSor2020_sub[dfSor2020_sub$CORPS %in% count[count$freq >150,]$x,], aes(CORPS, S_BRUT)) + geom_boxplot() + ylim(0, 60000) + ggtitle('Effectivement sortis') +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))

```



Le salaire median semble en revanche sur-estimé par le modèle pour la plupart des corps.

On plot les histogrammes en age par type de contrat pour comprendre pourquoi:


```{r Visualisation des resultats - AGE}

lim = Ntest*0.12/10
p1 <- hist(SortantsPredits[SortantsPredits$STATUT_CONTRAT == 'TITH', c('AGE')], 
          breaks=30, plot = FALSE)
p2 <- hist(dfSor2020_sub[dfSor2020_sub$STATUT_CONTRAT == 'TITH', c('AGE')], 
          breaks=30, plot = FALSE)
plot(p1, col = c1, main="Histogramme des Ages (TITH))", xlab='Repartition Age', 
     xlim=c(0, 80), ylim = c(0, lim))
plot(p2, col = c2, add=T)
legend("topright",
       legend = c('Prédits', 'Réels'),
       fill = c(c1,c2),       # Color of the squares
       border = "black")


lim = Ntest*0.12/10
p3 <- hist(SortantsPredits[SortantsPredits$STATUT_CONTRAT != 'TITH', c('AGE')], 
          breaks=30, plot = FALSE)
p4 <- hist(dfSor2020_sub[dfSor2020_sub$STATUT_CONTRAT != 'TITH', c('AGE')], 
          breaks=30, plot = FALSE)
plot(p3, col = c1, main="Histogramme des Ages (CONT)", xlab='Repartition Age', 
     xlim=c(0, 80), ylim = c(0, lim))
plot(p4, col = c2, add=T)
legend("topright",
       legend = c('Prédits', 'Réels'),
       fill = c(c1,c2),       # Color of the squares
       border = "black")



```


Il semble que chez les titulaires, il va nous manquer pas mal de personnes de plus de 60 ans, notre distribution en age étant plus homogène que celle des sortants dans la réalité.

Et les salaires dans tout ça:

```{r Visualisation des resultats - S_BRUT}

lim = 100

p1 <- hist(SortantsPredits[SortantsPredits$STATUT_CONTRAT == 'TITH', c('S_BRUT')], 
          breaks=50, plot = FALSE)

p2 <- hist(dfSor2020_sub[dfSor2020_sub$STATUT_CONTRAT == 'TITH', c('S_BRUT')], 
          breaks=50, plot = FALSE)

plot(p1, col = c1, main="Histogramme des Salaires (TITH)", xlab='Repartition Salaire', 
     xlim=c(1000, 50000), ylim = c(0, lim))
plot(p2, col = c2, xlim=c(1000, 50000), add=T)

legend("topright",
       legend = c('Prédits', 'Réels'),
       fill = c(c1,c2),       # Color of the squares
       border = "black")

```
Les sortants TITH prédits ont aussi un meilleur salaire que ceux effectivement sortis.



### Sauvegarde du modèle

```{r Save}
fic_sortants = "modele_sortants_glm.Rdata"
l_vars = c("TAGE",  "CORPS", "STATUT_CONTRAT") 
l_corps = unique(TrainSet$CORPS)
l_statut = unique(TrainSet$STATUT_CONTRAT)
save(list = c("m.logit", "l_vars", "l_statut", "l_corps"), file = fic_sortants)
cat("Sauvegarde du fichier modèle sortants :\n ", fic_sortants, "\n")
```

