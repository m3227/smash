---
title: "Modèle de transition de grade par statut en fonction de l'échelon"
subtitle: "`r params$anneeDeb` - `r params$anneeFin`"
author: "DREES OSAM/BPS - `r stringr::str_to_title(stringr::str_replace(Sys.getenv('username'), '\\.', ' '))`"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  html_document:
    toc: true
    toc_float: true
    number_sections: true
    theme: united
    highlight: tango
    
  rmdformats::downcute:
    toc_depth: 5
    number_sections: true
    theme: united
    highlight: tango
    
  pdf_document:
    toc: true
    number_sections: true

params:
  champ:
    label: "champ des données"
    value: "hop"
    input: select
    choices: ["hop", "fph"]
  anneeDeb:
    label: "Année de début"
    value: 2013
    input: slider
    min: 2009
    max: 2020
    step: 1
    sep: ""
  anneeFin:
    label: "Année de fin"
    value: 2020
    input: slider
    min: 2010
    max: 2021
    step: 1
    sep: ""
  rep_mdl:
    label: "Dossier pour sauver les modèles"
    value: "~/data/modeles"

# indent: false 
# mainfont: Arial
# monofont: Arial
# fontsize: 12pt
geometry: "left=2cm,right=2cm,top=2cm,bottom=2cm"

keyword: "SIASP"

---

```{r options, echo=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```


```{r generate_models, eval=FALSE, include=FALSE}
# Exécuter cette cellule pour générer lemodèles+rapports

rmd_file <- "Modele_MatriceTransitionGradeEchelon"

rep_mdl <- "~/data/modeles/2023-05-22/"
champ <- "hop"
anDeb <- 2012
anFin <- 2021
out_local <- sprintf("%s.html", rmd_file)
rmarkdown::render(paste(rmd_file,".Rmd", sep = ""),
                  output_dir = rep_mdl,
                  output_format = "rmdformats::robobook", # "html_document", # 
                  output_file = out_local,
                  params = list(champ = champ,
                                rep_mdl = rep_mdl, 
                                anneeDeb = anDeb,
                                anneeFin = anFin)
                  )

cat("\nModèle créé dans le dossier", rep_mdl, "\n")
utils::browseURL(rep_mdl)

```


# Introduction 

Ce script calcule des taux de transition des grades observés entre 2 années
pour les `STATUT_CONTRAT` TITH, CDD et MEDI selon l'échelon dans le grade de 
départ.

Les différents types transitions analysées sont : 

- "ID" : pas de transition, les grades de départ et d'arrivée sont identiques
- "PR" : promotions, en première approximation les grades de départ et 
d'arrivée est le supérieur appartiennent au même corps
- "EP" : études promotionnelles, les grades de départ et d'arrivée appartiennent 
à des corps différents
- "RE" : reclassement, la nomenclature des grades de départ évolue vers les grades 
d'arrivée

```{r imports_et_options, include=FALSE}
library(dplyr)
library(plotly)
# Chargement package SMASH, dev local (vérifier le chemin)
devtools::load_all("../../../smash-r-pkg/")

# Récupération des paramètres
anneeDeb <- params$anneeDeb
anneeFin <- params$anneeFin
champ <- params$champ

# Chemin pour sauver le modèle en local
chemin_local = params$rep_mdl

```

## Données étudiées

Dans cette étude nous travaillerons sur les personnels des années `r anneeDeb`
à `r anneeFin`, en créant un modèle de transition par année de 
`r anneeDeb+1` à `r anneeFin`, basé sur les transitions entre l'année N-1 et
l'année N.

Afin de n'observer que les transitions _principales_, une sélection des 
individus est effectuée selon les critères suivants : 

- poste principal de chaque individu
- durée de poste est 365 jours 
- EQTP supérieur à 0.5

```{r chargement_donnees, message=FALSE, include=FALSE}
l_pop <- list() # Tout le monde par année

v_annees <- seq(anneeDeb, anneeFin)

# Nom du modèle des entrants à créer
chemin_dist <- "T:/BPS/SMASH/modeles"
fic_mdl = sprintf("mat_transition_grade_im_%s_%s-%s.rds", 
                  champ, v_annees[1], v_annees[length(v_annees)])

for (an in v_annees) {
  strAn <- as.character(an)
  # Chargement ensemble des effectifs
  l_pop[[strAn]] <- chargerFST(champ, an, b_agrege = TRUE)
  # Sélection des individus
  l_pop[[strAn]] <- l_pop[[strAn]] %>% filter(POSTE_PRINC_AN=="P" &
                                               DUREE_POSTE==365 &
                                               EQTP>0.5)
  # Ajout de la colonne N3 : "T":TITH, "M":MEDI, "NT":Les autres
  l_pop[[strAn]]$N3 <- "NT"
  l_pop[[strAn]][l_pop[[strAn]]$STATUT_CONTRAT=="TITH",]$N3 <- "T"
  l_pop[[strAn]][l_pop[[strAn]]$STATUT_CONTRAT=="MEDI",]$N3 <- "M"
  
}

v_annees <- names(l_pop)
strAnDeb <- v_annees[1]
strAnFin <- v_annees[length(v_annees)]

# Chargment des données réglementaires
objBDD <- GrillesIndiciaires$new()

```

# Création des modèles

## Transition entre grades

Parmi les transitions observées pour l'année, les changements de 
réglementation sont récupérés pour indiquer lesquelles sont dues à 
un changement de nomenclature du grade ou une évolution de la grille. 

## Calcul des matrices

Calcul des taux de transition par Grade/IM pour chaque année sous la forme 
d'un grand `data.frame` contenant l'ensemble des années.

Les transitions sont filtrées pour enlever les cas anecdotiques (<= 10 personnes)
et les cas dus aux erreurs de codage de grade : 

- Grade `XXXX` pour les cas non trouvés
- Grade `270A`/`270B` (Clinicien hospitalier à temp partiel/complet) : la grille 
`A_MIN_MAX` est associée au grade mais n'est pas définie pas ils sont 
contractuels dans le sens où ils négocient leur contrat de travail avec le 
directeur de l'hôpital. La loi autorise à une rémunération comprise entre un 
plancher et un plafond 40% de plus que la rémunération du dernier échelon d'un 
PH à temps plein. On ne sait pas les faire évoluer dans les versions actuelles 
de SMASH.


```{r calcul_matrice, echo=FALSE}

l_statut <- c("TITH", "CONT", "MEDI")
l_N3 <- list(TITH = "T", CONT = "NT", MEDI = "M")

v_grades_exclus <- c("XXXX", "270A", "270B", "1000")

# Initialisation du data.frame qui va recevoir les transitions 
mdl_transition <- NULL

# Parcours des années, les taux de chaque grade pour chaque année
# sont concaténés dans mdl_transition
for (an in v_annees[-1]) {
  strAn <- an
  strAnPrec <- as.character(as.numeric(an)-1)
  
  # Récupération du tableau des changements de nomenclature pour l'année choisie
  df_chgmtnomen <- objBDD$getChangementNomenclatureGrade(as.numeric(strAn))

  if (length(df_chgmtnomen$GRADE_NEW)==0) {
    cat("En", strAn,", Pas de retraitement de l'impact mesures catégorielles statutaires \n")
  } else {
    cat("En", strAn, ":", length(unique(df_chgmtnomen$GRADE_OLD)) ,
        "changements catégoriels statutaires, dont", 
        sum(df_chgmtnomen$EVOL_GRILLE_SEULE), "sont des évolutions de grille uniquement\n")
  }
  
  # Un modèle par statut (converti en N3) et par grade
  for (statut in l_statut) {
    # Filtre des individus pour le statut présent sur les 2 ans
    # + Suppression des grades XXXX
    
    df_an <- l_pop[[strAn]] %>% 
      filter(STATUT_CONTRAT==statut & !(GRADE_ECH %in% v_grades_exclus))
    df_anPrec <- l_pop[[strAnPrec]] %>% 
      filter(STATUT_CONTRAT==statut & !(GRADE_ECH %in% v_grades_exclus))
    
    v_ids <- intersect(df_an$ID_NIR20, df_anPrec$ID_NIR20)
    df_an <- df_an %>% filter(ID_NIR20 %in% v_ids)
    df_anPrec <- df_anPrec %>% filter(ID_NIR20 %in% v_ids)
    
    l_grades <- unique(df_anPrec$GRADE_ECH)
    
    for (g in l_grades) {
      
      if (g=="XXXX") {
        # On saute le grade fourre-tout
        next
      }
      
      # if (g=="3021") { browser() }
      
      df_g_src <- df_anPrec %>% filter(GRADE_ECH==g)
      # IDs des personnes dans le gradSrc
      v_ids_src <- unique(df_g_src$ID_NIR20)
      
      # Sélection des individus toujours présents l'an suivant
      df_g_dest <- df_an %>% filter(ID_NIR20 %in% v_ids_src)
      
      cols_merge <- c("ID_NIR20", "GRADE_ECH", "IM") # , "S_BRUT")
      df_g_merge <- merge.data.frame(df_g_src[, cols_merge], df_g_dest[, cols_merge], 
                                     by = "ID_NIR20", 
                                     suffixes = c("_SRC", "_DEST"))
      
      
      # Taux d'évolution pour le grade source en deux étapes pour avoir
      # l'échelon le plus représenté et le S_brut moyen en destination
      freq_evol <- df_g_merge %>% 
        group_by(GRADE_ECH_SRC, IM_SRC, GRADE_ECH_DEST) %>% #, IM_DEST, S_BRUT_DEST) %>% 
        summarise(TOTAL = n(), 
                  IM_DEST_FREQUENT = as.numeric(names(which.max(table(IM_DEST)))), 
                  IM_DEST_FREQUENT_COUNT = max(table(IM_DEST)), 
                  .groups = 'drop') %>% 
        arrange(GRADE_ECH_SRC, desc(TOTAL)) # IM_SRC, GRADE_ECH_DEST)
      
      # Filtre des cas anecdotiques <= 10 personnes 
      freq_evol <- freq_evol %>% filter(TOTAL > 10)
      
      if (nrow(freq_evol)==0) { 
        # Pas d'évolution significative 
        next 
      }
      
      freq_evol$TAUX <- freq_evol$TOTAL / sum(freq_evol$TOTAL)
      
      
      # Ajout des échelon_rank à partir de la grille réglementaire
      # Initialisation des échelons
      freq_evol$ECHELON <- "01"
      freq_evol$ECHELON_RANK <- 1
      freq_evol$ECHELON_DEST <- "01"
      freq_evol$ECHELON_DEST_RANK <- 1
      
      
      # Ancienne grille
      df_grille <- objBDD$getGrilleByGrade(g, sprintf("%s-12-31", strAnPrec))
      if (!is.null(df_grille) && nrow(df_grille)>0) {
        for (idx in 1:nrow(freq_evol)) {
          idx_ech_proche <- which.min(abs(df_grille$IM - freq_evol$IM_SRC[idx]))
          freq_evol$ECHELON[idx] <- df_grille$ECHELON[idx_ech_proche]
          freq_evol$ECHELON_RANK[idx] <- df_grille$ECHELON_RANK[idx_ech_proche]
        }
      }
      
      # Nouvelles Grilles de destination
      for (idx in 1:nrow(freq_evol)) {
        df_grille <- objBDD$getGrilleByGrade(freq_evol$GRADE_ECH_DEST[idx], 
                                             sprintf("%s-12-31", strAn))
        
        if (!is.null(df_grille) && nrow(df_grille)>0) {
          idx_ech_proche <- which.min(abs(df_grille$IM -
                                            freq_evol$IM_DEST_FREQUENT[idx]))
          freq_evol$ECHELON_DEST[idx] <- df_grille$ECHELON[idx_ech_proche]
          freq_evol$ECHELON_DEST_RANK[idx] <- df_grille$ECHELON_RANK[idx_ech_proche]
        } 
      }
        
      
      freq_evol <- dplyr::rename(freq_evol, 
                                 GRADE_SRC = GRADE_ECH_SRC, 
                                 GRADE_DEST = GRADE_ECH_DEST)
      
      # On agrège les lignes de transition du grade vers lui-même
      # 1: récupère les lignes à agréger
      freq_non_evol <- freq_evol[freq_evol$GRADE_SRC == freq_evol$GRADE_DEST,]
        
      if (nrow(freq_non_evol)>0) {
        # 2: on les supprime de la matrice
        freq_evol <- freq_evol[freq_evol$GRADE_SRC != freq_evol$GRADE_DEST,]
        # 3: on rajoute l'agrégat
        freq_evol <- rbind(
          data.frame(
            GRADE_SRC = g,
            IM_SRC = NA,
            GRADE_DEST = g,
            TOTAL = sum(freq_non_evol$TOTAL),
            IM_DEST_FREQUENT = NA,
            IM_DEST_FREQUENT_COUNT = NA,
            TAUX = sum(freq_non_evol$TAUX),
            ECHELON = NA,
            ECHELON_RANK = NA,
            ECHELON_DEST = NA,
            ECHELON_DEST_RANK = NA),
          freq_evol
        )
      }
      
      # Pour le grade, promotion, EP ou reclassement ?
      # Ajout du type de transition PR:Promotion, (EP:étude promotionnelle ?), 
      # RE:reclassement
      
      
      # "PR" par défaut
      freq_evol$TYPE <- "PR"
      
      # "RE":reclassement si le grade de départ est dans la liste des grades changeants de nomenclature
      df_chg_grade <- df_chgmtnomen %>% 
        filter(EVOL_GRILLE_SEULE==FALSE & GRADE_OLD==g)
      if (nrow(df_chg_grade)>0) {
        # Reclassement pour les cas GRADE_NEW = GRADE_DEST
        freq_evol$TYPE[freq_evol$GRADE_DEST %in% df_chg_grade$GRADE_NEW] <- "RE"
      }
      
      # "EP" si les corps de départ et d'arrivée sont différents
      freq_evol$CORPS_SRC <- objBDD$getInfoGrades(g)$CORPS
      if (any(is.na(freq_evol$CORPS_SRC))) {
        freq_evol[is.na(freq_evol$CORPS_SRC), ]$CORPS_SRC <- "Corps non trouvé"
      }
      
      freq_evol$CORPS_DEST <- objBDD$getInfoGrades(freq_evol$GRADE_DEST, fill_NA = T)$CORPS
      if (any(is.na(freq_evol$CORPS_DEST))) {
        freq_evol[is.na(freq_evol$CORPS_DEST), ]$CORPS_DEST <- "Corps non trouvé"
      }
      
      if (any(freq_evol$CORPS_SRC!=freq_evol$CORPS_DEST)) {
        freq_evol[freq_evol$CORPS_SRC!=freq_evol$CORPS_DEST,]$TYPE <- "EP"
      }
      
      # Ajout à mdl_transition
      n_trans <- nrow(freq_evol)
      mdl_transition <- rbind( 
        mdl_transition, 
        data.frame(AN = rep(as.numeric(strAn), n_trans),
                   N3 = rep(l_N3[[statut]], n_trans), 
                   GRADE_SRC = freq_evol$GRADE_SRC, 
                   ECHELON_SRC = freq_evol$ECHELON, 
                   ECHELON_RK_SRC = freq_evol$ECHELON_RANK, 
                   GRADE_DEST = freq_evol$GRADE_DEST, 
                   ECHELON_DEST = freq_evol$ECHELON_DEST,
                   ECHELON_RK_DEST = freq_evol$ECHELON_DEST_RANK,
                   ECHELON_DEST_COUNT = freq_evol$IM_DEST_FREQUENT_COUNT,
                   TYPE = freq_evol$TYPE, 
                   PROBA = freq_evol$TAUX, 
                   TOTAL = freq_evol$TOTAL)
      )
    
    } # Grade
    
  } # Statut
  
  ## Affectation des Types de transition ----
  # Pas de changement
  mdl_transition[mdl_transition$GRADE_SRC==mdl_transition$GRADE_DEST,]$TYPE <- "ID"

}


# Affichage 
knitr::kable(head(mdl_transition, 10), 
  caption = "Aperçu de la matrice de transition des grades")

```



## Sauvegarde des modèles

```{r save_model, echo=FALSE}
# Sauvegarde des modèles en local et en distant
cat(paste("Sauvegarde du fichier modèle des transitions :\n",
          fic_mdl, "\n"))

tryCatch( {
  cat("Sauvegarde en local, dossier :", chemin_local, "\n")
  saveRDS(mdl_transition, file = file.path(chemin_local, fic_mdl))
  },
  error = function(e) {
    message( paste("sauvegarde en local impossible, dossier :", chemin_local) )
  }
)

# tryCatch( {
#   cat("Sauvegarde distante, dossier :", chemin_dist, "\n")
#   saveRDS(mdl_transition, file = file.path(chemin_dist, fic_mdl))
#   },
#   error = function(e) {
#     message( paste("sauvegarde distante impossible, dossier :", chemin_dist) )
#   }
# )


```

# Exemples d'utilisation du modèle

Après chargement du modèle, voici différents filtres illustrant les éléments 
contenus dans la matrice de transition : 

- Taux des personnes restées sur un grade spécifique (e.g. 3021)
- Taux de transition spécifique d'un grade vers un autre pour une année donnée 
(e.g. de 3021 vers 3022)
- Taux de promotions annuel d'un grade vers un autre (e.g. de 3021 vers 3022)

```{r test_model, echo=TRUE}
mdl <- readRDS(file.path(chemin_local, fic_mdl))

# Affichage des transitions pour les AS à partir de 2018
# On transforme PROBA en POURCENT pour l'affichage
mdl <- mdl %>% mutate(PROBA = round(100*PROBA, digits = 2)) %>% 
  rename(POURCENT = PROBA)

```

## Promotions grade 3021

```{r ex_2, echo=TRUE}
knitr::kable(
  mdl %>% filter(GRADE_SRC=="3021" & GRADE_DEST=="3021" & AN >= 2018) %>%
    select(AN, N3, GRADE_SRC, POURCENT, TOTAL) %>% arrange(desc(N3)),
  caption = "Taux des 3021 restés 3021 à partir de 2018, en %")
```

```{r ex_3, echo=TRUE}
knitr::kable(
  mdl %>% filter(GRADE_SRC=="3021" & GRADE_DEST=="3022" & AN == 2020),
  caption = "Taux des 3021 passés 3022 en 2020")
```

```{r ex_4, echo=TRUE}
g_src <- "3021"
g_dest <- "3022"
knitr::kable(
  mdl %>% filter(GRADE_SRC==g_src & GRADE_DEST==g_dest) %>%
    group_by(AN) %>% summarise(Taux = sum(POURCENT)),
  caption = paste("Taux de promotion", g_src, "vers", g_dest, "par année, en %")
  )
```

## Promotions grade 2154

Nous reprenons les mêmes analyses sur le grade des infirmiers

```{r ex_5}
g_src <- "2154"
g_dest <- "2164"
knitr::kable(
  mdl %>% filter(GRADE_SRC==g_src & GRADE_DEST==g_dest) %>%
    group_by(AN) %>% summarise(Taux = sum(POURCENT)),
  caption = paste("Taux de promotion", g_src, "vers", g_dest, "par année, en %")
  )
```


## Promotions comparaison décrets

```{r}

df_grades <- data.frame(
  src = c("3021", "2154", "3259", "2432"),
  dest = c("3022", "2164", "3258", "2753"))

knitr::kable(
  mdl %>% filter(GRADE_SRC %in% df_grades$src & 
                   GRADE_DEST %in% df_grades$dest) %>%
    group_by(AN, GRADE_SRC, GRADE_DEST) %>% summarise(Pourcent = sum(POURCENT), 
                                                      .groups = 'drop'),
  caption = paste("Taux de promotion par année, en %")
)

```



# Analyse des échelons de destinations 

Enfin, nous créons une nouvelle matrice de transition, plus complexe qui inclut les 
échelons de destination. L'objectif, n'est pas de l'utiliser comme modèle
mais de pouvoir analyser l'échelon de destination en cas de promotion.


```{r calcul_matrice_im_dest, echo=FALSE, warning=FALSE}

l_statut <- c("TITH", "CONT", "MEDI")
l_N3 <- list(TITH = "T", CONT = "NT", MEDI = "M")

# Initialisation du data.frame qui va recevoir les transitions 
mdl_transition_2 <- data.frame(AN = NULL, N3 = NULL, 
                             GRADE_SRC = NULL, ECHELON_SRC = NULL, 
                             GRADE_DEST = NULL, ECHELON_DEST = NULL, 
                             TYPE = NULL, 
                             PROBA = NULL, 
                             TOTAL = NULL)

# Parcours des années, les taux de chaque grade pour chaque année
# sont concaténés dans mdl_transition_2
for (an in v_annees[-1]) {
  strAn <- an
  strAnPrec <- as.character(as.numeric(an)-1)
  
  # Récupération du tableau des changements de nomenclature pour l'année choisie
  df_chgmtnomen <- objBDD$getChangementNomenclatureGrade(as.numeric(strAn))
  
  if (length(df_chgmtnomen$GRADE_NEW)==0) {
    cat("En", strAn,", Pas de retraitement de l'impact mesures catégorielles statutaires \n")
  } else {
    cat("En", strAn, ":", length(unique(df_chgmtnomen$GRADE_OLD)) ,
        "changements catégoriels statutaires, dont", 
        sum(df_chgmtnomen$EVOL_GRILLE_SEULE), "sont des évolutions de grille uniquement\n")
  }

  
  
  # Un modèle par statut (converti en N3) et par grade
  for (statut in l_statut) {
    # Filtre des individus pour le statut présent sur les 2 ans
    df_an <- l_pop[[strAn]] %>% filter(STATUT_CONTRAT==statut)
    df_anPrec <- l_pop[[strAnPrec]] %>% filter(STATUT_CONTRAT==statut)
    v_ids <- intersect(df_an$ID_NIR20, df_anPrec$ID_NIR20)
    df_an <- df_an %>% filter(ID_NIR20 %in% v_ids)
    df_anPrec <- df_anPrec %>% filter(ID_NIR20 %in% v_ids)
    
    l_grades <- unique(df_anPrec$GRADE_ECH)
    
    for (g in l_grades) {
      
      if (g=="XXXX") {
        # On saute le grade fourre-tout
        next
      }
      
      # if (g=="3021") { browser() }
      
      df_g_src <- df_anPrec %>% filter(GRADE_ECH==g)
      # IDs des personnes dans le gradSrc
      v_ids_src <- unique(df_g_src$ID_NIR20)
      
      # Sélection des individus toujours présents l'an suivant
      df_g_dest <- df_an %>% filter(ID_NIR20 %in% v_ids_src)
      
      cols_merge <- c("ID_NIR20", "GRADE_ECH", "IM")
      df_g_merge <- merge.data.frame(df_g_src[, cols_merge], df_g_dest[, cols_merge], 
                                     by = "ID_NIR20", 
                                     suffixes = c("_SRC", "_DEST"))
      
      # Taux d'évolution pour le grade source
      freq_evol <- df_g_merge %>% group_by(GRADE_ECH_SRC, IM_SRC, GRADE_ECH_DEST, IM_DEST) %>% 
        summarise(TOTAL = n(), .groups = 'drop') %>% 
        arrange(GRADE_ECH_SRC, IM_SRC, GRADE_ECH_DEST, IM_DEST)
      
      freq_evol$TAUX <- freq_evol$TOTAL / sum(freq_evol$TOTAL)
      
      
      # Ajout de l'échelon_rank à partir de la grille réglementaire
      # Initialisation du # d'échelon
      freq_evol$ECHELON_RANK <- 1
      freq_evol$ECHELON_RANK_DEST <- 1
      
      if (statut != "MEDI") {
        # Pas encore de grille pour les MEDI
        # Ancienne grille
        df_grille <- objBDD$getGrilleByGrade(g, sprintf("%s-12-31", strAnPrec))
        if (!is.null(df_grille) && nrow(df_grille)>0) {
          for (idx in 1:nrow(freq_evol)) {
            idx_ech_proche <- which.min(abs(df_grille$IM - freq_evol$IM_SRC[idx]))
            freq_evol$ECHELON_RANK[idx] <- df_grille$ECHELON_RANK[idx_ech_proche]
          }
        }
        # TODO : check vers quel grade on transite dans la nouvelle grille
        for (g_dest in unique(df_g_dest$GRADE_ECH)) {
          v_idx_freq_g <- which(freq_evol$GRADE_ECH_DEST == g_dest)
          df_grille_new <- objBDD$getGrilleByGrade(g_dest, sprintf("%s-12-31", strAn))
          if (!is.null(df_grille_new) && nrow(df_grille_new)>0) {
            for (idx in v_idx_freq_g) {
              idx_ech_proche <- which.min(abs(df_grille_new$IM - freq_evol$IM_DEST[idx]))
              freq_evol$ECHELON_RANK_DEST[idx] <- df_grille_new$ECHELON_RANK[idx_ech_proche]
            }
          }
        }
      }
      
      freq_evol <- dplyr::rename(freq_evol, 
                                 GRADE_SRC = GRADE_ECH_SRC, 
                                 GRADE_DEST = GRADE_ECH_DEST)
      
      # On agrège les lignes de transition du grade vers lui-même
      # 1: récupère les lignes à agréger
      freq_non_evol <- freq_evol[freq_evol$GRADE_SRC == freq_evol$GRADE_DEST,]
        
      if (nrow(freq_non_evol)>0) {
        # 2: on les supprime de la matrice
        freq_evol <- freq_evol[freq_evol$GRADE_SRC != freq_evol$GRADE_DEST,]
        # 3: on rajoute l'agrégat
        freq_evol <- rbind(
          data.frame(
            GRADE_SRC = g, IM_SRC = NA, 
            GRADE_DEST = g, IM_DEST = NA, 
            TOTAL = sum(freq_non_evol$TOTAL),
            TAUX = sum(freq_non_evol$TAUX),
            ECHELON_RANK = NA, 
            ECHELON_RANK_DEST = NA) , 
          freq_evol
        )
      }
  
      # Ajout à mdl_transition_2
      n_trans <- nrow(freq_evol)
      mdl_transition_2 <- rbind( 
        mdl_transition_2, 
        data.frame(AN = rep(as.numeric(strAn), n_trans),
                   N3 = rep(l_N3[[statut]], n_trans), 
                   GRADE_SRC = freq_evol$GRADE_SRC, 
                   ECHELON_SRC = freq_evol$ECHELON_RANK, 
                   GRADE_DEST = freq_evol$GRADE_DEST, 
                   ECHELON_DEST = freq_evol$ECHELON_RANK_DEST, 
                   TYPE = "PR", 
                   PROBA = freq_evol$TAUX, 
                   TOTAL = freq_evol$TOTAL)
      )
    
    } # Grade
  } # Statut
  
  # Affectation des Types de transition
  mdl_transition_2[mdl_transition_2$GRADE_SRC==mdl_transition_2$GRADE_DEST,]$TYPE <- "ID"
  
}


# Affichage 
knitr::kable(head(mdl_transition_2, 10), 
  caption = "Aperçu de la matrice de transition des grades")

```

## Promotion aide-soignant

```{r ex_60}
an <- 2020
g_src <- "3021"
g_dest <- "3022"

df_g_src <- objBDD$getGrilleByGrade(g_src, sprintf("%d-12-31", an-1))
df_g_dest <- objBDD$getGrilleByGrade(g_dest, sprintf("%d-12-31", an))
```

On observe ici les transitions de `r g_src` vers `r g_dest` en `r an`. 
Il en ressort que l'échelon majoritaire dans le grade de destination n'est pas 
l'échelon avec l'IM supérieur le plus proche dans la nouvelle grille mais 
plutôt l'échelon encore supérieur. À confirmer sur d'autres analyses.


```{r ex_61}
knitr::kable(df_g_src,
  caption = paste("Grille", g_src, "en", an-1)
  )
```

```{r ex_62}
knitr::kable(df_g_dest,
  caption = paste("Grille", g_dest, "en", an)
  )
```

```{r ex_63}
df_im_promo <- mdl_transition_2 %>% 
  filter(GRADE_SRC==g_src & GRADE_DEST==g_dest & AN==an) %>%
  arrange(desc(PROBA))

knitr::kable(df_im_promo,
  caption = paste("Taux de promotion", g_src, "vers", g_dest, "en", an)
  )

```


```{r ex_64}
library(ggplot2)

v_ech_src <- sort(unique(df_im_promo$ECHELON_SRC))
v_ech_dest <- sort(unique(df_im_promo$ECHELON_DEST))

ggplot(data =  df_im_promo %>% filter(N3=="T") %>% mutate(PROBA_PCT = 100*PROBA), 
       mapping = aes(x = ECHELON_SRC, y = ECHELON_DEST)) +
  geom_tile(aes(fill = PROBA_PCT)) + # , colour = "white") +
  geom_text(aes(label = sprintf("%.2f", PROBA_PCT)), vjust = 1) +
  scale_fill_gradient(low = "#FFEEEE", high = "red") +
  scale_x_continuous(breaks = v_ech_src, minor_breaks = NULL, 
                     labels=as.character(v_ech_src)) +
  scale_y_continuous(breaks = v_ech_dest, minor_breaks = NULL, 
                     labels=as.character(v_ech_dest)) + 
  theme(legend.position = "none") + theme_bw() + 
  labs(title = paste("Taux de promotion du grade", g_src, 
                                      "vers", g_dest, "en", an, "total", 
                                      round(100*sum(df_im_promo$PROBA), 
                                            digits = 1), "%") )
```

Dans la figure ci-dessous nous attribuons comme échelon de destination l'échelon
le plus probable, en lui associant la somme des probabilités de l'échelon source.


```{r ech_dest_AS_ML}
# Agrégat vers l'échelon de destination le plus probable
mdl_transition_2_max <- mdl_transition_2 %>% 
  group_by(AN, N3, GRADE_SRC, ECHELON_SRC, GRADE_DEST, TYPE) %>%
  arrange(desc(TOTAL)) %>% 
  summarise(ECHELON_DEST = first(ECHELON_DEST), 
            PROBA = sum(PROBA), 
            TOTAL = sum(TOTAL), .groups = 'drop')

ggplot(data = mdl_transition_2_max %>% 
         filter(GRADE_SRC==g_src & GRADE_DEST==g_dest & 
                  AN==an & N3=="T") %>% mutate(PROBA_PCT = 100*PROBA), 
       mapping = aes(x = ECHELON_SRC, y = ECHELON_DEST)) +
  geom_tile(aes(fill = PROBA_PCT)) + # , colour = "white") +
  geom_text(aes(label = sprintf("%.2f", PROBA_PCT)), vjust = 1) +
  scale_fill_gradient(low = "#FFEEEE", high = "red") +
  scale_x_continuous(breaks = v_ech_src, minor_breaks = NULL, 
                     labels=as.character(v_ech_src)) +
  scale_y_continuous(breaks = v_ech_dest, minor_breaks = NULL, 
                     labels=as.character(v_ech_dest)) + 
  theme(legend.position = "none") + theme_bw() + 
  labs(title = paste("Taux de promotion du grade", g_src, 
                                      "vers", g_dest, "en", an, "total", 
                                      round(100*sum(df_im_promo$PROBA), 
                                            digits = 1), "%"),
       subtitle = "Échelon de destination le plus probable")

```


## Promotion Infirmier

```{r ex_70}
an <- 2020
g_src <- "2154"
g_dest <- "2164"

df_g_src <- objBDD$getGrilleByGrade(g_src, sprintf("%d-12-31", an-1))
df_g_dest <- objBDD$getGrilleByGrade(g_dest, sprintf("%d-12-31", an))
```

On observe ici les transitions de `r g_src` vers `r g_dest` en `r an`. 
Il en ressort que l'échelon majoritaire dans le grade de destination n'est pas 
l'échelon avec l'IM supérieur le plus proche dans la nouvelle grille mais 
plutôt l'échelon encore supérieur. À confirmer sur d'autres analyses.


```{r ex_71}
knitr::kable(df_g_src,
  caption = paste("Grille", g_src, "en", an-1)
  )
```

```{r ex_72}
knitr::kable(df_g_dest,
  caption = paste("Grille", g_dest, "en", an)
  )
```

```{r ex_73}
df_im_promo <- mdl_transition_2 %>% 
  filter(GRADE_SRC==g_src & GRADE_DEST==g_dest & AN==an) %>%
  arrange(desc(PROBA))

knitr::kable(df_im_promo,
  caption = paste("Taux de promotion", g_src, "vers", g_dest, "en", an)
  )

```


```{r ex_74}
library(ggplot2)

v_ech_src <- sort(unique(df_im_promo$ECHELON_SRC))
v_ech_dest <- sort(unique(df_im_promo$ECHELON_DEST))

ggplot(data =  df_im_promo %>% filter(N3=="T") %>% mutate(PROBA_PCT = 100*PROBA), 
       mapping = aes(x = ECHELON_SRC, y = ECHELON_DEST)) +
  geom_tile(aes(fill = PROBA_PCT)) + # , colour = "white") +
  geom_text(aes(label = sprintf("%.2f", PROBA_PCT)), vjust = 1) +
  scale_fill_gradient(low = "#FFEEEE", high = "red") +
  scale_x_continuous(breaks = v_ech_src, minor_breaks = NULL, 
                     labels=as.character(v_ech_src)) +
  scale_y_continuous(breaks = v_ech_dest, minor_breaks = NULL, 
                     labels=as.character(v_ech_dest)) + 
  theme(legend.position = "none") + theme_bw() + 
  labs(title = paste("Taux de promotion du grade", g_src, 
                                      "vers", g_dest, "en", an, "total", 
                                      round(100*sum(df_im_promo$PROBA), 
                                            digits = 1), "%") )
```

Dans la figure ci-dessous nous attribuons comme échelon de destination l'échelon
le plus probable, en lui associant la somme des probabilités de l'échelon source.


```{r ech_dest_ML_inf}
# Agrégat vers l'échelon de destination le plus probable
mdl_transition_2_max <- mdl_transition_2 %>% 
  group_by(AN, N3, GRADE_SRC, ECHELON_SRC, GRADE_DEST, TYPE) %>%
  arrange(desc(TOTAL)) %>% 
  summarise(ECHELON_DEST = first(ECHELON_DEST), 
            PROBA = sum(PROBA), 
            TOTAL = sum(TOTAL), .groups = 'drop')

ggplot(data = mdl_transition_2_max %>% 
         filter(GRADE_SRC==g_src & GRADE_DEST==g_dest & 
                  AN==an & N3=="T") %>% mutate(PROBA_PCT = 100*PROBA), 
       mapping = aes(x = ECHELON_SRC, y = ECHELON_DEST)) +
  geom_tile(aes(fill = PROBA_PCT)) + # , colour = "white") +
  geom_text(aes(label = sprintf("%.2f", PROBA_PCT)), vjust = 1) +
  scale_fill_gradient(low = "#FFEEEE", high = "red") +
  scale_x_continuous(breaks = v_ech_src, minor_breaks = NULL, 
                     labels=as.character(v_ech_src)) +
  scale_y_continuous(breaks = v_ech_dest, minor_breaks = NULL, 
                     labels=as.character(v_ech_dest)) + 
  theme(legend.position = "none") + theme_bw() + 
  labs(title = paste("Taux de promotion du grade", g_src, 
                                      "vers", g_dest, "en", an, "total", 
                                      round(100*sum(df_im_promo$PROBA), 
                                            digits = 1), "%"),
       subtitle = "Échelon de destination le plus probable")

```

