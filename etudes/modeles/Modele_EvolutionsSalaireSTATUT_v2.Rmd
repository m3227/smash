---
title: "Modélisation des évolutions moyennes de salaires `r params$statut`"
author: "BPS/OSAM/DREES - Équipe SMASH"
date: "`r format(Sys.time(), '%d/%m/%Y-%H%M%S')`"
output: 
  pdf_document:
    toc: true
    number_sections: true
  html_document:
    toc: true
    toc_float: true
    number_sections: true
    theme: united
    highlight: tango

params:
  anneeDeb:
    label: "Année de début"
    value: 2010
    input: slider
    min: 2010
    max: 2020
    step: 1
    sep: ""
  anneeFin:
    label: "Année de fin"
    value: 2021
    input: slider
    min: 2011
    max: 2021
    step: 1
    sep: ""
  champ:
    label: "Champ des données"
    value: "hop"
    input: select
    choices: ["hop", "fph"]
  statut:
    label: "Statut de contrat pour le modèle"
    value: "TITH"
    input: select
    choices: ["MEDI", "TITH", "CONT", "INTE", "ELEV", "AUTR", "CAID"]
  rep_mdl:
    label: "Dossier pour sauver les modèles"
    value: "~/data/modeles/temp"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```


```{r librairies, include=FALSE}
library(ggplot2)
library(glue)
library(dplyr)
library(viridis)
devtools::load_all("../../../smash-r-pkg")

# Récupération des paramètres
anneeDeb <- params$anneeDeb
anneeFin <- params$anneeFin
champ <- params$champ
statut <- params$statut
# Choix d'agrégat des données
b_agrege = TRUE

# Chemin pour sauver le modèle en local
chemin_local = params$rep_mdl

# Nombre de personnes minimum dans 1 grade pour faire une régression
nb_min_grade <- 11

fic_evol_salaire = sprintf("tx_evolution_salaires_grade_%s_%s_%d-%d.rds", 
                           champ, statut, anneeDeb, anneeFin)


```


# Présentation 

Ce document propose une modélisation des évolutions moyennes de salaires
bruts des `r statut` à partir de taux d'augmentation moyen par personne pour
chaque grade pour utilisation dans SMASH.

De plus dans ce modèle, on part du S_BRUT hors primes, car les primes sont 
modélisées séparément par le modèle de primes. 

# Chargement des données SIASP

À partir des données SIASP chaînées suivant l'ID de `r anneeDeb` à 
`r anneeFin`, on calcule le taux d'évolution du salaire brut hors prime par année 
pour les agents `r statut` n'ayant pas changé d'EQTP entre 2 années successives.

`EVOL_S_BRUT_HP = (df_an$S_BRUT_HP - df_anPrec$S_BRUT_HP) / df_anPrec$S_BRUT_HP`

```{r chargement}

df_infos <- NULL

df_evol_s_brut_hp <- NULL

for (an in seq(anneeDeb, anneeFin)) {
  strAn <- as.character(an)
  
  # Chargement / sélection des présents/présents pour le statut
  df_anPrec <- getEchantillonSIASP(champ, an-1, 0, b_agrege = T, verbose = 0) %>% 
    filter(STATUT_CONTRAT == statut) %>% mutate(S_BRUT_HP = S_BRUT - PRIMES) %>% 
    filter(S_BRUT_HP>0)
  
  df_an <- getEchantillonSIASP(champ, an, 0, b_agrege = T, verbose = 0) %>% 
    filter(STATUT_CONTRAT == statut) %>% mutate(S_BRUT_HP = S_BRUT - PRIMES) %>% 
    filter(S_BRUT_HP>0)

  # Nombre statut avant sélection
  nb <- nrow(df_an)
  
  v_ids <- intersect(df_anPrec$ID, df_an$ID)
  df_anPrec <- df_anPrec %>% filter(ID %in% v_ids)
  df_an <- df_an %>% filter(ID %in% v_ids)

  # Sélection des personnes dont l'EQTP n'a pas évolué
  is_eqtp_ok <- df_an$EQTP>0.99 & df_anPrec$EQTP>0.99 # & abs(df_anPrec$EQTP - df_an$EQTP)<0.01
  df_anPrec <- df_anPrec[is_eqtp_ok, ]
  df_an <- df_an[is_eqtp_ok, ]
  
    
  # Mise à jour des grades sur la nomenclature anneeFin
  for (an_maj in seq(an, anneeFin)) {
    df_an$GRADE = majGrades(df_an$GRADE, an_maj)
  }
  
  df_an$EVOL_S_BRUT_HP <- (df_an$S_BRUT_HP - df_anPrec$S_BRUT_HP) / df_anPrec$S_BRUT_HP
  df_an$AN <- an
  df_evol_s_brut_hp <- rbind(
    df_evol_s_brut_hp, 
    df_an %>% select(ID, AN, AGE, SEXE, STATUT_CONTRAT, DUREE_CONTRAT, 
                     EQTP, GRADE, IM, ECH, S_BRUT_HP, PRIMES, EVOL_S_BRUT_HP))
  
  # Infos globale sélection par année
  df_infos <- rbind(df_infos, 
                    data.frame(An = an, 
                               Nb = nb, 
                               Nb_selection = nrow(df_an), 
                               Evol_moy_pct = round(100*mean(df_an$EVOL_S_BRUT_HP), 
                                  digits = 2)
                               )
                    )
  
}

# Ajout d'une ligne avec le total
df_infos <- rbind(
  df_infos, 
  data.frame(An = "Total", 
             Nb = sum(df_infos$Nb), 
             Nb_selection = nrow(df_evol_s_brut_hp),
             Evol_moy_pct = round(100*mean(df_evol_s_brut_hp$EVOL_S_BRUT_HP), 
                                  digits = 2)
             )
  )

knitr::kable(df_infos, 
             caption = paste("Évolution moyenne de salaire des", statut, ", sélection",
                             "des présents N-1/N à temps complet.") )

```



## Synthèse des évolutions de salaire

Quelques statistiques sur les évolutions de salaire brut hors prime : 

- Évolution pour les principaux grades

```{r stat_desc_evol}
# df_evol_s_brut_hp$EQTP_Q <- 
#   cut(df_evol_s_brut_hp$EQTP, 
#       breaks = c(0, 0.35, 0.55, 0.75, 0.85, 0.95, 1.05))
# 
# df_annees <- df_evol_s_brut_hp %>% group_by(AN, EQTP_Q) %>% 
#   summarise(EVOL_MOY_PCT = round(100*mean(EVOL_S_BRUT_HP), digits = 2), 
#             Nb = n(),
#             .groups = "drop") 
# 
# knitr::kable(df_annees, 
#              caption = paste("Évolution moyenne de salaire des", statut, ", sélection",
#                              "des présents N-1/N, même EQTP.") )

```


```{r}

df_count_grades <- df_evol_s_brut_hp %>% count(GRADE, sort= T)

df_infos_grades <- df_evol_s_brut_hp %>% group_by(GRADE, AN) %>%
  summarise(Nb = n(),
            EVOL_MOY_PCT = round(100*mean(EVOL_S_BRUT_HP), digits = 2), 
            .groups = "drop") %>% arrange(GRADE, AN) # desc(Nb), AN)

oBDD <- GrillesIndiciaires$new()
v_dates_ext <- oBDD$getInfoGrades(df_infos_grades$GRADE)$DATE_EXTINCTION
df_infos_grades$DATE_EXTINCTION <-  ifelse(is.na(v_dates_ext), "", v_dates_ext)

knitr::kable(df_infos_grades %>% filter(GRADE %in% df_count_grades$GRADE[1:10]), 
             caption = paste("Évolution moyenne de salaire des", statut, ", sélection",
                             "des présents N-1/N, même EQTP, 10 principaux grades") )


```


```{r save_excel, eval=FALSE, include=FALSE}
fic_xls <- sprintf("~/Evol_salaires_%s_%d-%d.xlsx", champ, anneeDeb, anneeFin)
xlsx::write.xlsx(as.data.frame(df_infos_grades), fic_xls,
                 sheetName = statut, 
                 row.names = FALSE, 
                 append = file.exists(fic_xls))

```


```{r bubble_chart}

fig <- plot_ly(df_infos_grades %>% filter(GRADE %in% df_count_grades$GRADE[1:6]), 
               x = ~AN, y = ~EVOL_MOY_PCT, text = ~paste("Grade:", GRADE, "\nNb:", Nb),
               type = 'scatter', mode = 'markers', size = ~Nb, 
               color = ~GRADE, marker = list(opacity = 0.5, sizemode = 'diameter')) %>% 
  layout(title = 'Évolution moyenne des salaires par GRADE et année',
         xaxis = list(showgrid = FALSE),
         yaxis = list(showgrid = FALSE),
         showlegend = TRUE)

fig
```


# Création du modèle 

Le modèle recherche une équation de salaires en fonction de l'age par grade.

```{r creation_modele}

df_evolution <- df_evol_s_brut_hp %>% group_by(GRADE) %>%
  summarise(NB = n(),
            EVOL_MOY = mean(EVOL_S_BRUT_HP), 
            .groups = "drop") %>% 
  filter(NB > nb_min_grade)

# Sauvegarde des pentes
model <- list(
  champ = champ,
  anneeDebut = anneeDeb,
  anneeFin = anneeFin,
  statut = statut,
  tx_moy =  mean(df_evol_s_brut_hp$EVOL_S_BRUT_HP),
  df_evolution = df_evolution)

saveRDS(model, file = file.path(chemin_local, fic_evol_salaire))

cat("Sauvegarde du fichier d'évolution des salaires", statut, ":\n", 
    fic_evol_salaire, "\n")


```


# Évaluation du modèle

On compare la somme des salaires `r statut` prédits aux salaires réels.

```{r Validation}

mdl <- readRDS(file.path(chemin_local, fic_evol_salaire))

v_annees_test <- anneeFin:2021

df_estim_salaires <- NULL

for (an in v_annees_test) {
  strAn <- as.character(an)
  
  # Applique l'estimation du modèle pour tous les CONT
  # Chargement des données SIASP
  df_test <- getEchantillonSIASP(champ, an-1, 0, b_agrege = T, verbose = 0) %>% 
    filter(STATUT_CONTRAT == statut) %>% mutate(S_BRUT_HP = S_BRUT - PRIMES)
  
  # On ne garde que les présents N-1/N pour la comparaison
  df_target <- getEchantillonSIASP(champ, an, 0, b_agrege = T, verbose = 0) %>% 
    filter(STATUT_CONTRAT == statut) %>% filter(ID %in% df_test$ID & EQTP>0) %>% 
     mutate(S_BRUT_HP = S_BRUT - PRIMES)
  
  df_test <- df_test %>% filter(ID %in% df_target$ID)
  
  if (an==2021) { 
    df_test$GRADE <- majGrades(df_test$GRADE, 2020)
    df_target$GRADE <- majGrades(df_target$GRADE, 2020)
  }
  df_test$GRADE <- majGrades(df_test$GRADE, an)
  df_target$GRADE <- majGrades(df_target$GRADE, an)

  # Mise à jour des grades du modèle pour l'année
  mdl$df_evolution$GRADE <- majGrades(mdl$df_evolution$GRADE, an)
  mdl$df_evolution <- mdl$df_evolution %>% group_by(GRADE) %>% 
    summarise(EVOL_MOY = weighted.mean(EVOL_MOY, NB),
              NB = sum(NB),
              .groups = "drop")
  
  
  Total_S_BRUT_HP_anPrec <- sum(df_test$S_BRUT_HP)
  Total_S_BRUT_HP <- sum(df_target$S_BRUT_HP)
  
  is_brut_predit <- rep(FALSE, nrow(df_test))
  # Applique le modèle sur les grades présents
  for (grade_ in intersect(df_test$GRADE, mdl$df_evolution$GRADE)) {

    mdl_grade <- mdl$df_evolution %>% filter(GRADE == grade_)

    is_grade <- df_test$GRADE == grade_
    df_test$S_BRUT_HP[is_grade] <- df_test$S_BRUT_HP[is_grade] * (1+mdl_grade$EVOL_MOY)
    is_brut_predit[is_grade] <- TRUE
  }
  
  # Applique le taux moyen pour les autres 
  df_test$S_BRUT_HP[!is_brut_predit] <- df_test$S_BRUT_HP[!is_brut_predit] * (1+mdl$tx_moy)
  
  Total_S_BRUT_HP_PREDIT <- sum(df_test$S_BRUT_HP)
  df_estim_salaires <- rbind(
    df_estim_salaires,
    data.frame(An = paste(strAn, ifelse(an>=anneeDeb & an<=anneeFin, "*","")),
               S_BRUT_HP_anPrec = round(Total_S_BRUT_HP_anPrec/1e9, digits = 3),
               S_BRUT_HP = round(Total_S_BRUT_HP/1e9, digits = 3),
               S_BRUT_HP_PREDIT = round(Total_S_BRUT_HP_PREDIT/1e9, digits = 3),
               Ecart_Pourcent = 
                 round(100*(Total_S_BRUT_HP_PREDIT-Total_S_BRUT_HP)/Total_S_BRUT_HP, 
                       digits = 2)
               )
  )
}

knitr::kable(df_estim_salaires, 
             caption = paste("Estimation des salaires brut hors primes", statut, 
                              "par grade, en Md €"))


```


La prédiction est mauvaise !


