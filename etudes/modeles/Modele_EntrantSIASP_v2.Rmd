---
title: "Modèle entrants SIASP par taux"
subtitle: "Version 2"
author: "BPS DREES - `r stringr::str_to_title(stringr::str_replace(Sys.getenv('username'), '\\.', ' '))`"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output: 
  pdf_document:
    toc: true
    number_sections: true
  html_document:
    toc: true
    toc_float: true
    number_sections: true
    theme: united
    highlight: tango

params:
  annee:
    label: "Année"
    value: 2018
    input: slider
    min: 2010
    max: 2020
    step: 1
    sep: ""
  champ:
    label: "Champ des données"
    value: "hop"
    input: select
    choices: ["hop", "fph"]
  rep_mdl:
    label: "Dossier pour sauver les modèles"
    value: "~/data/modeles/"

---


```{r generate_models, eval=FALSE, include=FALSE}
# Exécuter cette cellule pour générer l'ensemble des modèles+rapports 2016-2019
for (an in seq(2010,2021)) {
  for (cha in c("hop","fph")) {
    out_local <- paste0("~/Modele_Entrants_", toupper(cha), "_", an, ".pdf")
    rmarkdown::render("Modele_EntrantSIASP_v2.Rmd", 
                      params = list(annee = an, champ = cha),
                      output_file = out_local
    )
    # Copie locale -> I:
    tryCatch( 
      {
        chemin_dist <- file.path("I:/BPS/_Emplois et revenus/Projet SMASH",
                                 "03_Comptes_rendus",
                                "EtudesComportementales/ModelesGeneresPourSMASH")
        out_dist <- file.path(chemin_dist, basename(out_local))
        cat("Sauvegarde distante, dossier :", out_dist, "\n")
        file.copy(out_local, out_dist, overwrite = TRUE)
      }, 
      error = function(e) {
        message( paste("sauvegarde distante impossible, fichier :", out_dist) )
      }
    )
  }
}
```


```{r librairies, include=FALSE}
library(dplyr)
library(fst)

# Charge le package SMASH
devtools::load_all("../../../smash-r-pkg")

# Récupération paramètre année (entête Rmd)
annee <- params$annee
champ <- params$champ

# Nom du modèle des entrants à créer
chemin_local <- params$rep_mdl
chemin_dist <- "T:/BPS/SMASH/modeles"
fic_mdl_entrants = sprintf("modele_entrants_v2_%s_%d.rds", champ, annee)

# Chargement Base de données réglementaire
oBD <- GrillesIndiciaires$new()


```

# Présentation 

Ce script propose une modélisation des entrants par corps, statut et grade 
pour l'année ``r annee``, sur les données de champ `r toupper(champ)`. 

La modélisation est issue des analyses sur les données SIASP menées dans 
le fichier `AnalysesEntrants.Rmd`.

Les étapes du modèle sont les suivantes : 

- calcul les taux d'entrants par statut et durée de contrat à partir des données SIASP
  - CONT (durée : CDD/CDI)
  - TITH (durée : SANS)
  - MEDI (durée : SANS/CDD/CDI)
  - ...
- calcul des taux de répartition par grade, pour chaque grade, on répartit 
indépendamment suivant des taux :
  - le sexe
  - l'indice
  - La quotité
  - L'EQTP
  - l'âge

# Chargement des données SIASP

```{r chargement, message=FALSE, include=FALSE}

df <- getEchantillonSIASP(champ, annee, 0, 
                          b_agrege = TRUE)

# Ajout du corps de la BDD
df$CORPS <- oBD$getCorps(df$GRADE)

df_entrants <- df %>% filter(IS_ENTRANT == TRUE)

# AGE min limité (saturation basse)
age_min <- 21
test_age_min <- df_entrants$AGE<age_min
cat( " -", sum(test_age_min), "entrants AGE<", age_min, 
     "remis à ", age_min, "ans, (sur",
     nrow(df_entrants), ")+\n" )
df_entrants$AGE[test_age_min] <- age_min


```


```{r filtrage, message=FALSE, include=FALSE}
# Calcul du salaire brut normé
# Suppression des entrants avec un EQTP trop bas
eqtp_min <- 1/52 
test_eqtp_min <- df_entrants$EQTP < eqtp_min
cat( " -", sum(test_eqtp_min), "entrants supprimés, sur ",
    nrow(df_entrants), ", EQTP <", eqtp_min, "\n" )
df_entrants <- df_entrants[!test_eqtp_min, ]
df_entrants$S_BRUT_NORM <- df_entrants$S_BRUT / df_entrants$EQTP


```

Pour la préparation des données, on ne garde que les entrants 
d'age > `r age_min` et qui ont travaillés plus de `r nbheures_min` heures. 

# Analyse des taux d'entrants

Les entrants sont obtenus sur les données `r annee`.

Le taux d'**entrants** est calculé comme le nombre d'ID_NIR entrants en 
`r annee`, divisé par le nombre total d'ID_NIR en `r annee`.

```{r calcul_taux, echo=FALSE}
# Récupération des ID_NIR pour l'année
id <- unique(df$ID)
id_entrants <- unique(df_entrants$ID)

# Entrants
tx_entrants <- length(id_entrants) / length(id)
cat(paste("Taux d'entrants", annee,":", 
          round(tx_entrants*100, digits = 1), "%\n"))
```

## Taux d'entrants par corps

Nous calculons les taux d'entrants par corps et affichons les taux des 10 corps
les plus représentés.

```{r tx_in_corps}
df_count_corps <- df %>% count(CORPS, sort = T, name = "Total")
df_count_corps_in <- df_entrants %>% 
  count(CORPS, sort = T, name = "Total_entrants")

df_count_corps_court <- df_entrants %>% filter(IS_SORTANT==TRUE) %>%
  count(CORPS, sort = T, name = "Total_contrats_courts")

df_infos_corps <- merge.data.frame(
  df_count_corps, df_count_corps_in, by = "CORPS", all.x = T) %>%
  mutate(Total_entrants = na_if(Total_entrants, 0)) %>% 
  mutate(Pct_entrants = round(100*Total_entrants/Total, digits = 2))

df_infos_corps <- merge.data.frame(
  df_infos_corps, df_count_corps_court, by = "CORPS", all.x = T) %>% 
  arrange(desc(Total_entrants))



knitr::kable(
  head(df_infos_corps,20), 
  caption = "Taux d'entrants par corps, sur les 20 corps les plus réprésentatifs"
  )


```


## Taux d'entrants par GRADE

```{r tx_in_grade}
df_count_grade <- df %>% count(GRADE, sort = T, name = "Total")
df_count_grade_in <- df_entrants %>% 
  count(GRADE, sort = T, name = "Total_entrants")

df_count_grade_court <- df_entrants %>% filter(IS_SORTANT==TRUE) %>%
  count(GRADE, sort = T, name = "Total_contrats_courts")

df_infos_grade <- merge.data.frame(
  df_count_grade, df_count_grade_in, by = "GRADE", all.x = T) %>%
  mutate(Total_entrants = na_if(Total_entrants, 0)) %>% 
  mutate(Pct_entrants = round(100*Total_entrants/Total, digits = 2))

df_infos_corps <- merge.data.frame(
  df_infos_grade, df_count_grade_court, by = "GRADE", all.x = T) %>% 
  arrange(desc(Total_entrants))



knitr::kable(
  head(df_infos_corps,20), 
  caption = "Taux d'entrants par grade, sur les 20 grades les plus réprésentatifs"
  )


```

# Modèle d'entrants, variables indépendantes

## Création d'un modèle 

Le modèle créé dépend des variables d'intérêt utilisées dans la micro-simulation. 
Dans la version actuelle de SMASH, les variables sont les suivantes :

```{r var_smash, echo=F, results='markup'}
# On génère avec la fonction existante pour récupérer les noms de variables
df <- genererEntrantsStatut(
  10, 2017, fic_model = "T:/BPS/SMASH/modeles/modele_entrants_v2_fph_2016.rds")
cat(" -", paste(names(df), collapse = "\n - "), "\n\n")

```

Il apparaît qu'il y a des variables liées, notamment :

- s_brut ~ indice * valeur_point_annuel (~58.32)

L'idée est de faire un modèle par type et durée de contrat puis pour chaque 
grade de calculer les taux :

- Des types et durée de contrats
- De répartition des entrants par grade
- De répartition des sexes dans chaque grade

On pourra ainsi générer un individu de la manière suivante :

1. tirage aléatoire du type de contrat : statut et durée selon les taux observés
dans l'effectif global
2. tirage aléatoire des variables selon les taux observés pour le type de contrat
   - GRADE
   - SEXE
   - QUOTITE
   - EQTP
   - AGE
   - DEPCOM_FONCTION
3. Pour les TITH, l'indice majoré et le salaire sont conditionnés au grade
4. Pour les autres, le salaire est conditionné au grade
5. Le salaire est pondéré par l'EQTP 


```{r modeles_entrants, echo=FALSE}
# Variable pour les modèles
mdl_entrants <- list()

# Taux de contrats courts parmis les entrants, entrants & sortants la même année
mdl_entrants$tx_contrats_courts <- sum(df_entrants$IS_SORTANT) / nrow(df_entrants)

# Homogénéisation des DUREE_CONTRAT (SANS,CDD,CDI)
df_entrants <- homogeneiser_statut_duree(df_entrants)

# Taux d'entrants par type de contrat
mdl_entrants[["STATUT_DUREE_CONTRAT"]] <- ProbaTaux2D$new(
  df_entrants, c("STATUT_CONTRAT", "DUREE_CONTRAT") )

# Choix des variables indépendantes du modèle : attention à l'indépendance
mdl_entrants$l_vars_statut <- c("GRADE", "SEXE", "QUOTITE", "EQTP", "AGE",
                                "DEPCOM_FONCTION")
# Pour chaque type de contrat
# - Tirage suivant un taux des variables dans `l_vars_statut`
# - Pour chaque grade : 
#   - Tirage suivant un taux d'un indice
df_stat_duree <- mdl_entrants$STATUT_DUREE_CONTRAT$listerValeurs()
for ( idx in seq(1,nrow(df_stat_duree)) ) {
  # Traitements relatifs au statut
  s <- df_stat_duree$STATUT_CONTRAT[idx]
  d <- df_stat_duree$DUREE_CONTRAT[idx]
  s_d <- paste(s,d,sep = "_")
  # Extraction des entrants pour le statut & durée
  df_s <- df_entrants %>% filter(STATUT_CONTRAT==s & DUREE_CONTRAT==d)
  nb <- nrow(df_s)

  # Ajout au modèle des taux calculés
  for (nom in mdl_entrants$l_vars_statut) {
    # Spécification de nbins suivant les variables
    # NA: variables discrètes, entier: nombre de bins pour quantifier la variable
    nbins_ <- switch(
      nom, 
      "GRADE" = NA, 
      "SEXE" = NA,
      "QUOTITE" = 10, 
      "EQTP" = 10, 
      "AGE" = 20,
      "DEPCOM_FONCTION" = NA)
    
    # Cas par défaut (si variable pas dans la liste): 
    if (is.null(nbins_)) {
      nbins_ = ifelse(is.double(df_s[[nom]]), 10, NA)
    }
    
    mdl_entrants[[s_d]][[nom]] <- ProbaTaux$new(df_s, nom, nbins = nbins_ )
  }
  
  # tx des indices par grade
  for (g in mdl_entrants[[s_d]]$GRADE$listerValeurs() ) {
    # Indices / salaires
    # Attention aux indices 0
    df_i <- df_s %>% filter(GRADE==g & IM>0)
    if (nrow(df_i)==0) {
      # Que des indices 0
      # Option 1 : on remet les zéros -> proba d'avoir indice 0 = 1
      message(paste("Statut_duree", s_d, "grade", g,": tous les IM sont a 0"))
      df_i <- df_s %>% filter(GRADE==g) 
      # Conditionnal debug
      # if (s=="CONT" && g=="3259") {
      #   browser()
      # }

      # Option 2 : calcul d'indice à partir de 0
      # message(paste("Statut", s, "grade", g, 
      #              ": tous les IM sont a 0, recalcul a partir de S_BRUT/EQTP"))
      # df_i$IM <- round(df_i$S_BRUT / (df_i$EQTP * 56.23224) )
      
      # Option 3 : dans la fonction de génération tirer les indices des entrants 
      # selon les taux des TITH
    }
    
    # Ajout au modèle des taux d'indices calculés et S_BRUT_NORM associé
    if (s_d=="TITH_SANS") {
      # Pour les TITH on stocke en 2D IM/S_BRUT_NORM 
      mdl_entrants[[s_d]][[g]] <- ProbaTaux2D$new(df_i, 
                                                c("IM", "S_BRUT_NORM"), 
                                                nbins.2 = 20 )
    } else {
      # Pour les autres (IM=0), on apprend le S_BRUT_NORM
      mdl_entrants[[s_d]][[g]] <- ProbaTaux$new(df_i, "S_BRUT_NORM", nbins = 20)
    }
  }
}
```

## Sauvegarde des modèles en local et en distant

Le modèle est sauvegardé en local et devra être recopié dans le dossier du 
projet SMASH sur le disque T de la DREES.

```{r save_model, echo=FALSE}

cat(paste("Sauvegarde du fichier modèle des entrants :\n",
          fic_mdl_entrants, "\n"))

tryCatch( {
  cat("Sauvegarde en local, dossier :", chemin_local, "\n")
  saveRDS(mdl_entrants, file = file.path(chemin_local, fic_mdl_entrants))
  },
  error = function(e) {
    message( paste("sauvegarde en local impossible, dossier :", chemin_local) )
  }
)

# tryCatch( {
#   cat("Sauvegarde distante, dossier :", chemin_dist, "\n")
#   saveRDS(mdl_entrants, file = file.path(chemin_dist, fic_mdl_entrants))
#   },
#   error = function(e) {
#     message( paste("sauvegarde distante impossible, dossier :", chemin_dist) )
#   }
# )

```

## Exemple de génération aléatoire d'entrants

Le code ci-dessous illustre un exemple pour générer des entrants utilisable 
dans SMASH. La fonction de génération est une fonction du package SMASH.  

```{r generation_entrants, echo=TRUE}  
devtools::load_all("../../../smash-r-pkg")
n_a_tirer = 10000

fic_mdl <- file.path(chemin_local, fic_mdl_entrants)
df_new <- genererEntrantsStatut(n_a_tirer, 2020,
                                l_statuts = c("TITH", "CONT", "MEDI"),
                                l_grades = NULL,
                                fic_model = fic_mdl,
                                use_ind_tith = FALSE,
                                objBDD = NULL)

```

Nous générons `r n_a_tirer` nouveaux profils d'entrants à partir du fichier 
modèle : `r fic_mdl_entrants`.

```{r barplot_grades, echo=FALSE, message=FALSE, warning=FALSE}
# Affichage des données générées
library(ggplot2)
plot_grades <- function(df, n_grades, statut) {
  df.statut <- df %>% filter(STATUT_CONTRAT==statut) %>% group_by(GRADE)
  df.count <- df.statut %>% summarise(total=n()) %>% arrange(desc(total))

  n_visus <- min(n_grades, nrow(df.count))
  v_grades <- df.count$GRADE[1:n_visus]
  plt <- ggplot(data=filter(df.count, GRADE %in% v_grades) %>% arrange(desc(total)),
                aes(x=1:n_visus, y=total)) + 
    geom_bar(stat="identity") +
    labs(title = sprintf("Grades (%d/%d), statut %s", n_visus,nrow(df.count), statut)) +
    scale_x_discrete(name ="GRADE", 
                     limits = 1:n_visus, 
                     labels = v_grades, 
                     guide = guide_axis(angle = 90))

  invisible(plt)
}

print(plot_grades(df_new, 10, "TITH"))

print(plot_grades(df_new, 10, "CONT"))

```

Le nouveau tirage d'entrants par statut montre des résultats intéressants.

L'idée de ce modèle est cependant de modéliser un lien réaliste entre les 3
variables. Il faudra donc bien penser à borner les valeurs sur des plages réalistes. 

```{r plot_pairs, echo=FALSE}
pairs(~ S_BRUT + QUOTITE + EQTP + AGE, df_new %>% 
        filter(STATUT_CONTRAT=="TITH"), 
      main = paste("Entrants générés", annee, "TITH"))

pairs(~ S_BRUT + QUOTITE + EQTP + AGE, df_new %>%
        filter(STATUT_CONTRAT=="CONT"), 
      main = paste("Entrants générés", annee, "CONT"))
```

