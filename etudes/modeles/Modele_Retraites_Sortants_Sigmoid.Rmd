---
title: "Modèle de loi de départ à la retraite par grade"
subtitle: "2016-2019"
author: "BPS DREES - `r stringr::str_to_title(stringr::str_replace(Sys.getenv('username'), '\\.', ' '))`"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  pdf_document:
    toc: true
    number_sections: true

# indent: false 
# mainfont: Arial
# monofont: Arial
# fontsize: 12pt
geometry: "left=2cm,right=2cm,top=2cm,bottom=2cm"

# abstract: \singlespacing BLABLABLA.
keyword: "SIASP"
params:
  annee:
    label: "Année"
    value: 2016
    input: slider
    min: 2015
    max: 2019
    step: 1
    sep: ""
  donnees:
    label: "Champs des données"
    value: "hop"
    input: select
    choices: ["hop", "fph"]
---

```{r generate_models, eval=FALSE, include=FALSE}
# Exécuter cette cellule pour générer l'ensemble des modèles+rapports
# sur les années sélectionnées
for (an in seq(2015,2019)) {
  champs <- "hop" # fph" # 
  rmarkdown::render("Modele_Retraites_Sortants_Sigmoid.Rmd", 
                    params = list(annee = an, donnees = champs),
                    output_file = paste0("Modele_Retraites_Sortants_Sigmoid-", 
                                         an, "-", donnees, ".pdf", sep="")
                    )
}
```


# Introduction 

Ce notebook propose de caler des lois de départ à la retraite en fonction 
de l'age et du grade pour les titulaires de la FPH. Les lois seront de type
fonction d'activation de type `P(x) = 1 / (1+exp(-R*(x - age_pivot)))`.


```{r imports_et_options, include=FALSE}
library(dplyr)
library(ggplot2)
library(patchwork)

# paramètres DREES globaux de graphiques 
ggplot2::theme_update(panel.background = element_rect(fill = "white"), 
                      panel.grid.major.y = element_line("grey"), 
                      panel.grid.major.x = element_line("grey"), 
                      legend.position = "bottom",
                      legend.title = ggplot2::element_blank(),
                      legend.text=element_text(size= 10), 
                      axis.text.y = element_text(size=10), 
                      axis.text.x = element_text(size=10), 
                      legend.background = element_rect(fill="white"), 
                      legend.key = element_rect(fill = "white"))

# Récupération paramètre année (entête Rmd)
annee <- params$annee
donnees <- params$donnees

# Nom du modèle des retraites à créer
chemin_local <- "~/data/modeles/temp"
fic_mdl = sprintf("mdl_retraites_sigmoid_%s_%d.rds", donnees, annee);

```

## Données étudiées

Dans cette étude nous utilisons les données SIASP des personnels 
"`r toupper(donnees)`" de l'année `r annee`. Deux jeux de données sont
chargés pour les personnels *en poste* et pour les *sortants* de l'année. 


```{r chargement_donnees, message=FALSE, include=FALSE}
# Choix de l'emplacement des données
## Chargement des données 
Sys.setenv(R_CONFIG_ACTIVE = Sys.getenv('username'))
config <- config::get()
chemin <- config$path_siasp

# Chargement package Smash dev
devtools::load_all(config$path_smash)


df_poste <- chargerFST(donnees, annee, b_selection = TRUE, verbose = 0)

df_sortants <- chargerFST(donnees, annee, b_sortant = TRUE, verbose = 0)

```

## Sortants titulaires par grade / indice / age

On cherche ici à voir s'il y a un lien grade/indice/age permettant de 
discriminer les sortants du reste du personnel pour les titulaires, notamment 
avec l'*a priori* que les sortants autour de 60 ans partent à la retraite. 

Pour cela on observe par grade, pour les grades les plus représentatifs la 
répartition des personnes en indice / age sur le grade. 

Ces graphiques illustrent notamment pour les grades supérieurs l'aspect 
dépendant de l'age et de l'indice pour les sortants. Ils ne sont cependant pas 
discriminant dans le sens où entrants et sortants n'ont pas des caractéristiques
disjointes. 

On voit également que pour les grades d'entrée de carrière, *e.g.* infirmiers 
grade 1 (2154) ou aide soignant (3021), 
l'age et l'indice majoritaires des sortants ne correspondent pas à l'âge de la
retraite.


```{r grade_ind_age, echo=FALSE}
df_poste_tith_count_grades <- df_poste %>% 
  filter(STATUT_CONTRAT=="TITH") %>%
  group_by(GRADE_ECH) %>% 
  summarise(total=n()) %>% arrange(desc(total))

df_sortants_tith_count_grades <- df_sortants %>% 
  filter(STATUT_CONTRAT=="TITH") %>% 
  group_by(GRADE_ECH) %>% 
  summarise(total=n()) %>% 
  mutate(pct=round(100*total / sum(total), digits = 1)) %>%
  arrange(desc(total))

# Base de données / info sur les grades
BDD <- GrillesIndiciaires$new()

scatter_age_indice <- function(grade, statut="TITH", use_facet = FALSE) {
  # Constitution du dataframe
  
  df_visu <- rbind(df_poste %>% 
                     filter(STATUT_CONTRAT==statut & GRADE_ECH==grade) %>% 
                     mutate(TYPE="En poste"), 
                   df_sortants %>% 
                     filter(STATUT_CONTRAT==statut & GRADE_ECH==grade) %>% 
                     mutate(TYPE="Sortant"))

  info_grade <- BDD$getInfoGrades(grade)
  # Raccourci du libellé
  str_grade <- stringr::str_replace_all(info_grade$LIBELLE, 
                                        "( de)|( en)|d\\'|\\(|\\)", "")

  fig <- ggplot(df_visu, aes(x = AGE, y = IM, shape = TYPE)) + 
    geom_point(aes(colour = TYPE), alpha = 0.1+0.1*use_facet) +
    geom_density_2d(aes(colour = TYPE), alpha = 0.5) +
    labs(title = paste("Age/Indice majoré,", statut, 
                       ", grade", grade, "\n", str_grade), 
         x="Age", y="IM") + 
    coord_cartesian(xlim = c(25,90))
  
  if (use_facet) {
    fig <- fig + facet_wrap(vars(TYPE))
  }
  
  plot(fig)
  
}


if (annee==2019) {
  v_idx <- c(1,2,3,4,5,6,7,9,10,13,16)
} else {
  v_idx <- (1:10)
}
for (grad in df_sortants_tith_count_grades$GRADE_ECH[v_idx]) {
  tryCatch( {
      # use_facet pour séparer en poste et sortants en 2 subplots
      # scatter_age_indice(grad, "TITH", use_facet = T)
      scatter_age_indice(grad, "TITH", use_facet = F)
    }, 
    error = function(e) { 
      message(paste(
        "Impossible d'afficher le graphique pour le grade", grad))
    }
  )
}

# Ferme la connexion à la BDD
rm(BDD)
```


# Création des modèles

## Approche proposée

Cette section propose d'estimer une probabilité de sortie pour retraite 
en estimant une sigmoïde sur la partie haute de distribution
cumulée des ages.

## Test fit Sigmod

La figure ci-dessous teste et illustre ce principe de calage d'une courbe sur 
l'ensemble des ages des personnels "`r toupper(donnees)`".

```{r test_fit_sigmoid, echo=FALSE}
sigmoid = function(p, x) {
  1 / (1 + exp(-p[1] * (x - p[2])))
}

breaks_x <- seq(min(df_poste$AGE),max(df_poste$AGE))
breaks_x <- seq(40,max(df_poste$AGE))
hist_age <- hist(df_poste[df_poste$AGE>=min(breaks_x), ]$AGE, breaks = breaks_x, plot = F)

x <- 0.5* (breaks_x[-length(breaks_x)] + breaks_x[-1])
y <- cumsum(hist_age$counts) / sum(hist_age$counts)

# Fit
fitmodel <- nls(y ~ 1/(1 + exp(-a * (x-b))), start=list(a=5,b=50))
    
# visualization code
# get the coefficients using the coef function
p = coef(fitmodel)

x_fit <- seq(1,80)
y_fit <- sigmoid(p,x_fit)
ggplot() + 
  geom_line(aes(x=x_fit, y=y_fit, colour="Fit")) + 
  geom_point(aes(x=x, y=y, colour="Données"), alpha=0.5) + 
  scale_colour_manual("", 
                      breaks = c("Fit", "Données"),
                      values = c("red", "blue")) +
  labs(title = "Estimation CDF Ages global HOP", x="Age", y="CDF")

```


Les figures suivantes illustrent les calages obtenues sur les données des 
**sortants** sélectionnées par : 

- ensemble des sortants
- sortant du grade Aide soignant : départ avant 64 ans
- sortants de type MEDI : départs étalés jusqu'à 80 ans

```{r retraites_sortants, echo=FALSE}

# Test 1 : sur tous les sortants
oRetraiteSortants <- FitRetraiteSigmoid$new(
  df_sortants$AGE, 
  min_fit=60,
  min_retraite=57,
  b_plot = F)

p1 <- oRetraiteSortants$plot(
  25,90,"Estimation CDF Ages sortants HOP, age min fit = 60",F)



# Test sur les TITH
oRetraiteTITH <- FitRetraiteSigmoid$new(
  (df_sortants %>% filter(STATUT_CONTRAT=="TITH"))$AGE,
  min_fit=62, 
  min_retraite=57, 
  b_plot = F )

p2 <- oRetraiteTITH$plot(
  25,90,"Estimation CDF Ages sortants TITH, age min fit = 62",F)

# Test sur les AS
grades_as <- c("3001","3002","3003","3021","3022","3121","3122","3123")
df_as <- df_sortants %>% filter(GRADE %in% grades_as)

oRetraiteSortantsAS <- FitRetraiteSigmoid$new(
  df_as$AGE, 
  min_fit=60,
  min_retraite=57,
  b_plot = F)

p3 <- oRetraiteSortantsAS$plot(
  25,90,"Estimation CDF Ages sortants Aide-soignants, age min fit = 60",F)



# Test sur les MEDI
oRetraiteMEDI <- FitRetraiteSigmoid$new(
  (df_sortants %>% filter(STATUT_CONTRAT=="MEDI"))$AGE,
  min_fit=62, 
  min_retraite=60, 
  b_plot = F)

p4 <- oRetraiteMEDI$plot(
  25,90,"Estimation CDF Ages sortants MEDI, age min fit = 62",F)

show(p1)
show(p2)
show(p3)
show(p4)

```

Les figures suivantes illustrent les calages obtenues sur les données des 
personnels **en poste** par : 

- tous
- sortant du grade Aide soignant : départ avant 64 ans
- sortants de type MEDI : départs étalés jusqu'à 80 ans


De part la distribution des ages sur les personnels en poste qui sont plus
uniformes sur l'ensemble des ages, ces figures montrent qu'il vaut mieux 
*fitter* nos modèles sur les données des sortants.

```{r retraites_postes, echo=FALSE}

# Test sur les TITH
oRetraiteAll <- FitRetraiteSigmoid$new(
  df_poste$AGE,
  min_fit=60, 
  min_retraite=57, 
  b_plot = T, titre="Estimation CDF Ages TITH, age min fit = 60" )



# Test sur les TITH
oRetraiteTITH <- FitRetraiteSigmoid$new(
  (df_poste %>% filter(STATUT_CONTRAT=="TITH"))$AGE,
  min_fit=60, 
  min_retraite=57, 
  b_plot = T, titre="Estimation CDF Ages TITH, age min fit = 60" )

# Filtre sur les aide-soignant
grades_as <- c("3001","3002","3003","3021","3022","3121","3122","3123")

oRetraite <- FitRetraiteSigmoid$new(
  (df_poste %>% filter(GRADE %in% grades_as))$AGE, 
  min_fit=60,
  min_retraite=57,
  b_plot = T,
  titre="Estimation CDF Ages Aide-soignants, age min fit = 60" )

# Test sur les MEDI
oRetraiteTITH <- FitRetraiteSigmoid$new(
  (df_poste %>% filter(STATUT_CONTRAT=="MEDI"))$AGE,
  min_fit=62, 
  min_retraite=60, 
  b_plot = T, titre="Estimation CDF Ages MEDI, age min fit = 62" )


```


## Modèle par filière

Le modèle calcule la loi de probabilité de sortie en regroupant les grades par
filières, suivant la nomenclature.


```{r modele_entrant_p1, echo=FALSE, warning=FALSE}

# Chargement BDD
BD <- GrillesIndiciaires$new()
l_filieres <- BD$listFilieres()

# Variable pour les modèles
mdl_retraite <- list()

# Ajout d'un modèle MEDI (ils partent plus tard à la retraites)
is_medi <- df_sortants$STATUT_CONTRAT =="MEDI"
mdl_retraite[["MEDI"]] <- FitRetraiteSigmoid$new(
      df_sortants[is_medi,]$AGE,
      min_fit=62, 
      min_retraite=58, 
      b_plot = F
      )


# Modèle TITH par filière
for (f in l_filieres) {
  
  # Liste des grades pour la filière
  l_grades <- BD$getGradesByFiliere(f)$GRADE
  
  # Fit du modèle sur les données filtrées
  # On sauve également les grades utilisés
  mdl_retraite[["TITH"]][[f]] <- list (
    l_grades = l_grades,
    oFit = FitRetraiteSigmoid$new(
      (df_sortants[!is_medi,] %>% filter(GRADE_ECH %in% l_grades))$AGE,
      min_fit=61, 
      min_retraite=57, 
      b_plot = F
      )
  )

}

# Ajout d'un modèle par défaut si le grade n'est pas trouvé à partir de la 
# filière
mdl_retraite[["defaut"]] <- FitRetraiteSigmoid$new(
  (df_sortants %>% filter(STATUT_CONTRAT=="TITH"))$AGE,
  min_fit=62, 
  min_retraite=57, 
  b_plot = F )

  
# Cloture BD
rm(BD)

```


```{r save_model, echo=FALSE}
# Sauvegarde des modèles en local et en distant
cat("Sauvegarde du fichier modèle des retraites :\n", fic_mdl)
cat("Sauvegarde en local, dossier :", chemin_local, "\n")
saveRDS(mdl_retraite, file = file.path(chemin_local, fic_mdl))

```

# Exemple d'utilisation du modèle

Le code ci-dessous montre l'utilisation du modèle pour choisir des sortants 
candidats à la retraite parmi les personnes en poste.


```{r test_model, echo=FALSE}
mdl <- readRDS(file.path(chemin_local, fic_mdl))

# Tirage des retraites pour la population actuelle

estimerRetraite <- function(df, mdl) {
  
  cat("estimerRetraite : calcul des probabilités d'être à la retraite\n")
  # Calcul des probas de la population MEDI
  v_probas <- rep(NA,nrow(df))
  is_medi <- df_poste$STATUT_CONTRAT=="MEDI"
  if (any(is_medi)) {
      v_probas[is_medi] <- mdl$MEDI$calculerProba(df[is_medi, ]$AGE)
  }
  cat(sprintf(" - modèle MEDI : %d\n", sum(is_medi)))
  
  
  # Calcul des probas par filière pour les autres
  for (f in names(mdl$TITH)) {
    # Indice des grades correspondants au modèle
    is_mdl <- !is_medi & (df$GRADE_ECH %in% mdl$TITH[[f]]$l_grades)
    if (any(is_mdl)) {
      v_probas[is_mdl] <- mdl$TITH[[f]]$oFit$calculerProba(df[is_mdl, ]$AGE)
    }
    cat(sprintf(" - modèle filière %s : %d \n", f, sum(is_mdl)))
  }
  
  # Calcul des probas restantes grades non trouvés avec le modèle par défaut
  is_na <- is.na(v_probas)
  if (any(is_na)) {
    v_probas[is_na] <- mdl$defaut$calculerProba(df[is_na, ]$AGE)
  }
  cat(sprintf(" - modèle par défaut : %d\n", sum(is_na)))
  
  # Tirage de valeur aléatoire pour sélection suivant les probas
  v_alea <- runif(nrow(df))
  
  is_retraite <- (v_probas / 4) > v_alea
  cat(sprintf(" -> %d personnes à la retraite (sur %d)\n", 
              sum(is_retraite), nrow(df)) )
  
  return(is_retraite)
}

df_poste$RETRAITE <- estimerRetraite(df_poste, mdl)

# Visualisation 
fig <- ggplot() + 
  geom_density(stat = "count", aes(x=df_poste$AGE), linetype = "dotted") +
  geom_density(stat = "count", 
               aes(x=df_poste$AGE, fill=df_poste$RETRAITE), 
               alpha = 0.5) +
  scale_fill_discrete(name="Statut",
                      breaks=c(FALSE, TRUE),
                      labels=c("En poste", "Retraite")) + 
  labs(title = paste("Estimation des retraites sur la population en poste", 
                     annee), 
       x="Age", y="Count")


# Chargement des sortants année suivante s'ils existent
fic_anplus1 <- file.path( chemin, paste(donnees, "_sortants", annee+1,
                         "_selection.rds", sep = "") )
if (file.exists(fic_anplus1)) {
  df_sortants_anplus1 <-  as.data.frame(readRDS(fic_anplus1))
  fig <- fig + 
    scale_size(paste("Sortants", annee+1)) + 
    geom_density(stat = "count", 
                 aes(x=df_sortants_anplus1$AGE, colour = "blue"), 
                 linetype = "dashed") +
    scale_color_manual(values = "blue", 
                       labels = paste("Sortants", annee+1), 
                       name = "Sortants") 
  
}

plot(fig)
```

