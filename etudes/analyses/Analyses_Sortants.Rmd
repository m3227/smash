---
title: "Caracteristiques des sortants"
author: "BPS DREES - Maud Galametz"
date: "26/11/2021"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r librairies}
library(dplyr)
library(ggplot2)
library(fBasics)
library(fst)

Sys.setenv(R_CONFIG_ACTIVE = Sys.getenv('username'))
config <- config::get()
chemin <- config$path_siasp

# Chargement package Smash dev
devtools::load_all(config$path_smash)

champ <- "hop"

c1 <- rgb(173, 216, 230, max = 255, alpha = 180, names = "lt.blue")
c2 <- rgb(255, 192, 203, max = 255, alpha = 180, names = "lt.red")

```


## Présentation 

Ce Rmd vise à étudier les agents sortants.


 
## Chargement des données
 
On charge les données SIASP de 2017 à 2020. On charge également les tables des entrants afin de caractériser les actifs stables.

On sélectionne les Variables d’intérêt pour alléger les tables.


```{r chargement}
## Chargement des données 
l_cols_sel <- c('ID_NIR20', 'SEXE', 'AGE', 'CORPS', 'GRADE_ECH', 'QUOTITE',
                'STATUT_CONTRAT', 'S_BRUT', 'DUREE_POSTE', 'EQTP')

dfEff2020 <- chargerFST(champ, 2020, b_selection = TRUE) %>%
  select(all_of(l_cols_sel))
dfEff2019 <- chargerFST(champ, 2019, b_selection = TRUE) %>%
  select(all_of(l_cols_sel)) 
dfEff2018 <- chargerFST(champ, 2018, b_selection = TRUE)  %>%
  select(all_of(l_cols_sel))
dfEff2017 <- chargerFST(champ, 2017, b_selection = TRUE)  %>%
  select(all_of(l_cols_sel))
dfEff2016 <- chargerFST(champ, 2016, b_selection = TRUE)  %>%
  select(all_of(l_cols_sel))
dfEff2015 <- chargerFST(champ, 2015, b_selection = TRUE)  %>%
  select(all_of(l_cols_sel))

dfSor2019 <- chargerFST(champ, 2019, b_sortant = TRUE, b_agrege = TRUE)   %>%
  select(all_of(l_cols_sel))
dfSor2018 <- chargerFST(champ, 2018, b_sortant = TRUE, b_agrege = TRUE)   %>%
  select(all_of(l_cols_sel))
dfSor2017 <- chargerFST(champ, 2017, b_sortant = TRUE, b_agrege = TRUE)   %>%
  select(all_of(l_cols_sel))
dfSor2016 <- chargerFST(champ, 2016, b_sortant = TRUE, b_agrege = TRUE)   %>%
  select(all_of(l_cols_sel))
dfSor2015 <- chargerFST(champ, 2015, b_sortant = TRUE, b_agrege = TRUE)   %>%
  select(all_of(l_cols_sel))

dfEnt2020 <- chargerFST(champ, 2020, b_entrant = TRUE, b_agrege = TRUE)  %>%
  select(all_of(l_cols_sel))
dfEnt2019 <- chargerFST(champ, 2019, b_entrant = TRUE, b_agrege = TRUE)  %>%
  select(all_of(l_cols_sel))
dfEnt2018 <- chargerFST(champ, 2018, b_entrant = TRUE, b_agrege = TRUE)  %>%
  select(all_of(l_cols_sel))
dfEnt2017 <- chargerFST(champ, 2017, b_entrant = TRUE, b_agrege = TRUE)  %>%
  select(all_of(l_cols_sel))
dfEnt2016 <- chargerFST(champ, 2016, b_entrant = TRUE, b_agrege = TRUE)  %>%
  select(all_of(l_cols_sel))

```


## Quelques statistiques globales


Tout d'abord, quelques statistiques, avec les taux d'entrants et de sortants sur ces années. On veille à ne retenir qu'une ligne par agent. On ajoute les taux entrants/sortants femmes sur ces entrants/sortants.

```{r Quelques stats}
print(c('2019', 'Entrants:', round(NROW(unique(dfEnt2019$ID_NIR20))*100 / NROW(unique(dfEff2019$ID_NIR20)), 3), 
        'Sortants:', round(NROW(unique(dfSor2019$ID_NIR20))*100 / NROW(unique(dfEff2019$ID_NIR20)), 3)))
print(c('2018', 'Entrants:', round(NROW(unique(dfEnt2018$ID_NIR20))*100 / NROW(unique(dfEff2018$ID_NIR20)), 3), 
        'Sortants:', round(NROW(unique(dfSor2018$ID_NIR20))*100 / NROW(unique(dfEff2018$ID_NIR20)), 3)))
print(c('2017', 'Entrants:', round(NROW(unique(dfEnt2017$ID_NIR20))*100 / NROW(unique(dfEff2017$ID_NIR20)), 3), 
        'Sortants:', round(NROW(unique(dfSor2017$ID_NIR20))*100 / NROW(unique(dfEff2017$ID_NIR20)), 3)))

print(c('2019', 'Taux Entrants F:', round(NROW(unique(dfEnt2019[dfEnt2019$SEXE == 'F', c('ID_NIR20')]))*100 / NROW(unique(dfEnt2019$ID_NIR20)), 3), 
        'Taux Sortants F:', round(NROW(unique(dfSor2019[dfSor2019$SEXE == 'F', c('ID_NIR20')]))*100 / NROW(unique(dfSor2019$ID_NIR20)), 3)))
print(c('2018', 'Taux Entrants F:', round(NROW(unique(dfEnt2018[dfEnt2018$SEXE == 'F', c('ID_NIR20')]))*100 / NROW(unique(dfEnt2018$ID_NIR20)), 3), 
        'Taux Sortants F:', round(NROW(unique(dfSor2018[dfSor2018$SEXE == 'F', c('ID_NIR20')]))*100 / NROW(unique(dfSor2018$ID_NIR20)), 3)))
print(c('2017', 'Taux Entrants F:', round(NROW(unique(dfEnt2017[dfEnt2017$SEXE == 'F', c('ID_NIR20')]))*100 / NROW(unique(dfEnt2017$ID_NIR20)), 3), 
        'Taux Sortants F:', round(NROW(unique(dfSor2017[dfSor2017$SEXE == 'F', c('ID_NIR20')]))*100 / NROW(unique(dfSor2017$ID_NIR20)), 3)))

```



Il y a 10.6% qui sont sortants.



## Visualisations des salaires et des ages

Quelques visualisations maintenant des salaires bruts de ces agents entrants/sortants. On ne retient pour la visualisation que les corps à haut effectif.


```{r fig.asp = 0.7, fig.width = 7 }

dfEnt2019_sub <- dfEnt2019#[dfEnt2019$CORPS %in% count[count$freq >10000,]$x,]
dfSor2019_sub <- dfSor2019#[dfSor2019$CORPS %in% count[count$freq >10000,]$x,]

ggplot(dfEnt2019_sub, aes(CORPS, S_BRUT/EQTP)) + geom_boxplot() + ylim(0, 60000) + ggtitle('Entrants') +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
ggplot(dfSor2019_sub, aes(CORPS, S_BRUT/EQTP)) + geom_boxplot() + ylim(0, 60000) + ggtitle('Sortants') +
    theme(axis.text.x = element_text(angle = 90, vjust = 0.5, hjust=1))
```

Et l'histogramme des ages les caractérisant:

```{r fig.asp = 0.8, fig.width = 5 }
p1 <- hist(dfEnt2019$AGE, main="Histogramme des Ages (Entrants)", breaks=30,
          xlim=c(0, 80), ylim = c(0, 15000),
          xlab='Repartition Age') 
p2 <- hist(dfSor2019$AGE, main="Histogramme des Ages (Sortants)", breaks=30,
          xlim=c(0, 80), ylim = c(0, 15000),
          xlab='Repartition Age')
plot(p1, col=rgb(1,0,0,8/9), main="Age des entrants/sortants en 2019", xlab='Repartition Age', ylab='Nombre')
plot(p2, col=rgb(0,0,1,8/9), add=T)
legend("topright", legend=c("Entrants", "Sortants"),
       col=c("red", "blue"), lty=1:1)
```

L'entrée dans la population de la hop se fait jeune, avec un taux décroissant en fonction de l'age.
Les sorties peuvent être caractérisées par l'addition d'une loi décroissante de départ en fonction de l'âge
+ d'une gaussienne centrée autour de l'âge de départ à la retraite.


La tendance se confirme t elle sur les fichiers SIASP d'autres années ?
On sépare les agents par type de contrat.

```{r fig.asp = 0.8, fig.width = 5 }
p1_2019 <- hist(dfEnt2019[dfEnt2019$STATUT_CONTRAT == 'TITH', c('AGE')], 
          breaks=30, plot = FALSE) 
p2_2019 <- hist(dfEnt2019[dfEnt2019$STATUT_CONTRAT != 'TITH', c('AGE')], 
          breaks=30, plot = FALSE) 
p3_2019 <- hist(dfSor2019[dfSor2019$STATUT_CONTRAT == 'TITH', c('AGE')], 
          breaks=30, plot = FALSE, )
p4_2019 <- hist(dfSor2019[dfSor2019$STATUT_CONTRAT != 'TITH', c('AGE')], 
          breaks=30, plot = FALSE)

p1_2018 <- hist(dfEnt2018[dfEnt2018$STATUT_CONTRAT == 'TITH', c('AGE')], breaks=30, plot = FALSE) 
p2_2018 <- hist(dfEnt2018[dfEnt2018$STATUT_CONTRAT != 'TITH', c('AGE')], breaks=30, plot = FALSE) 
p3_2018 <- hist(dfSor2018[dfSor2018$STATUT_CONTRAT == 'TITH', c('AGE')], breaks=30, plot = FALSE)
p4_2018 <- hist(dfSor2018[dfSor2018$STATUT_CONTRAT != 'TITH', c('AGE')], breaks=30, plot = FALSE)

p1_2017 <- hist(dfEnt2017[dfEnt2017$STATUT_CONTRAT == 'TITH', c('AGE')], breaks=30, plot = FALSE) 
p2_2017 <- hist(dfEnt2017[dfEnt2017$STATUT_CONTRAT != 'TITH', c('AGE')], breaks=30, plot = FALSE) 
p3_2017 <- hist(dfSor2017[dfSor2017$STATUT_CONTRAT == 'TITH', c('AGE')], breaks=30, plot = FALSE)
p4_2017 <- hist(dfSor2017[dfSor2017$STATUT_CONTRAT != 'TITH', c('AGE')], breaks=30, plot = FALSE)

plot(p1_2017, col='red', main="Histogramme des Ages (Entrants - TITH)", 
     xlab='Repartition Age', xlim=c(0, 80), ylim = c(0, 1000))
plot(p1_2018, col='blue', add=T)
plot(p1_2019, col='green', add=T)

plot(p2_2017, col='red', main="Histogramme des Ages (Entrants - Cont)", 
     xlab='Repartition Age', xlim=c(0, 80), ylim = c(0, 50000))
plot(p2_2018, col='blue', add=T)
plot(p2_2019, col='green', add=T)

plot(p3_2017, col='red', main="Histogramme des Ages (Sortants - TITH)", 
     xlab='Repartition Age', xlim=c(0, 80), ylim = c(0, 15000))
plot(p3_2018, col='blue', add=T)
plot(p3_2019, col='green', add=T)

plot(p4_2017, col='red', main="Histogramme des Ages (Sortants - Cont)", 
     xlab='Repartition Age', xlim=c(0, 80), ylim = c(0, 15000))
plot(p4_2018, col='blue', add=T)
plot(p4_2019, col='green', add=T)
```


Les comportements différent énormément entre contractuels et titulaires : le statut du contrat 
doit être pris en compte dans le modèle des sortants. 

Les répartitions semblent stables d'une année à l'autre: on va pouvoir concaténer les données de 2 années pour tester/construire le modèle et isoler une 3eme pour vérification.


```{r fig.asp = 0.8, fig.width = 5 }
p1_2019 <- hist(dfEnt2019[(dfEnt2019$STATUT_CONTRAT == 'TITH') & (dfEnt2019$CORPS == "Infirmier en soins généraux et spécialisés 0206 A"), c('AGE')], 
          breaks=30, plot = FALSE) 
p2_2019 <- hist(dfEnt2019[(dfEnt2019$STATUT_CONTRAT != 'TITH') & (dfEnt2019$CORPS == "Infirmier en soins généraux et spécialisés 0206 A"), c('AGE')], 
          breaks=30, plot = FALSE) 
p3_2019 <- hist(dfSor2019[(dfSor2019$STATUT_CONTRAT == 'TITH') & (dfSor2019$CORPS == "Infirmier en soins généraux et spécialisés 0206 A"), c('AGE')], 
          breaks=30, plot = FALSE, )
p4_2019 <- hist(dfSor2019[(dfSor2019$STATUT_CONTRAT != 'TITH') & (dfSor2019$CORPS == "Infirmier en soins généraux et spécialisés 0206 A"), c('AGE')], 
          breaks=30, plot = FALSE)

p1_2018 <- hist(dfEnt2018[(dfEnt2018$STATUT_CONTRAT == 'TITH') & (dfEnt2018$CORPS == "Infirmier en soins généraux et spécialisés 0206 A"), c('AGE')], 
                breaks=30, plot = FALSE) 
p2_2018 <- hist(dfEnt2018[(dfEnt2018$STATUT_CONTRAT != 'TITH') & (dfEnt2018$CORPS == "Infirmier en soins généraux et spécialisés 0206 A"), c('AGE')], 
                breaks=30, plot = FALSE) 
p3_2018 <- hist(dfSor2018[(dfSor2018$STATUT_CONTRAT == 'TITH') & (dfSor2018$CORPS == "Infirmier en soins généraux et spécialisés 0206 A"), c('AGE')], 
                breaks=30, plot = FALSE)
p4_2018 <- hist(dfSor2018[(dfSor2018$STATUT_CONTRAT != 'TITH') & (dfSor2018$CORPS == "Infirmier en soins généraux et spécialisés 0206 A"), c('AGE')], 
                breaks=30, plot = FALSE)

p1_2017 <- hist(dfEnt2017[(dfEnt2017$STATUT_CONTRAT == 'TITH') & (dfEnt2017$CORPS == "Infirmier en soins généraux et spécialisés 0206 A"), c('AGE')], 
                breaks=30, plot = FALSE) 
p2_2017 <- hist(dfEnt2017[(dfEnt2017$STATUT_CONTRAT != 'TITH') & (dfEnt2017$CORPS == "Infirmier en soins généraux et spécialisés 0206 A"), c('AGE')], 
                breaks=30, plot = FALSE) 
p3_2017 <- hist(dfSor2017[(dfSor2017$STATUT_CONTRAT == 'TITH') & (dfSor2017$CORPS == "Infirmier en soins généraux et spécialisés 0206 A"), c('AGE')], 
                breaks=30, plot = FALSE)
p4_2017 <- hist(dfSor2017[(dfSor2017$STATUT_CONTRAT != 'TITH') & (dfSor2017$CORPS == "Infirmier en soins généraux et spécialisés 0206 A"), c('AGE')], 
                breaks=30, plot = FALSE)

plot(p1_2017, col='red', main="Histogramme des Ages (Entrants - TITH)", 
     xlab='Repartition Age', xlim=c(0, 80), ylim = c(0, 1000))
plot(p1_2018, col='blue', add=T)
plot(p1_2019, col='green', add=T)

plot(p2_2017, col='red', main="Histogramme des Ages (Entrants - Cont)", 
     xlab='Repartition Age', xlim=c(0, 80), ylim = c(0, 2000))
plot(p2_2018, col='blue', add=T)
plot(p2_2019, col='green', add=T)

plot(p3_2017, col='red', main="Histogramme des Ages (Sortants - TITH)", 
     xlab='Repartition Age', xlim=c(0, 80), ylim = c(0, 2000))
plot(p3_2018, col='blue', add=T)
plot(p3_2019, col='green', add=T)

plot(p4_2017, col='red', main="Histogramme des Ages (Sortants - Cont)", 
     xlab='Repartition Age', xlim=c(0, 80), ylim = c(0, 2000))
plot(p4_2018, col='blue', add=T)
plot(p4_2019, col='green', add=T)
```


## Analyse des ratios salaires année de sortie / salaire année précédente 

```{r Ratio salaire sortie 2016 - 2017}

dfSor <- dfSor2017
dfEff_N <- dfEff2017
dfEff_Nmoins1 <- dfEff2016

id_sortants <- dfSor$ID_NIR20
SalairesNmoins1 <- dfEff_Nmoins1[dfEff_Nmoins1$ID_NIR20 %in% id_sortants, c('ID_NIR20', 'S_BRUT', 'STATUT_CONTRAT')]
SalairesN <- dfEff_N[dfEff_N$ID_NIR20 %in% id_sortants, c('ID_NIR20', 'S_BRUT', 'STATUT_CONTRAT')]
SalairesNmoins1 <- SalairesNmoins1 %>% dplyr::rename(S_BRUT_Nmoins1 = S_BRUT)
SalairesN <- SalairesN %>% dplyr::rename(S_BRUT_N = S_BRUT)
SalairesNmoins1 <- SalairesNmoins1 %>% dplyr::rename(SC_Nmoins1 = STATUT_CONTRAT)
SalairesN <- SalairesN %>% dplyr::rename(SC_N = STATUT_CONTRAT)

Data <- merge(x = SalairesNmoins1, y = SalairesN, all = TRUE)
Data$RATIO <- Data$S_BRUT_N / Data$S_BRUT_Nmoins1

median(Data[!is.na(Data$RATIO),]$RATIO)
median(Data[(!is.na(Data$RATIO)) & (Data$SC_N == 'TITH'),]$RATIO)
median(Data[(!is.na(Data$RATIO)) & (Data$SC_N == 'CONT'),]$RATIO)
median(Data[(!is.na(Data$RATIO)) & (Data$SC_N == 'MEDI'),]$RATIO)

```


```{r Ratio salaire sortie 2017 - 2018}

dfSor <- dfSor2018
dfEff_N <- dfEff2018
dfEff_Nmoins1 <- dfEff2017

id_sortants <- dfSor$ID_NIR20
SalairesNmoins1 <- dfEff_Nmoins1[dfEff_Nmoins1$ID_NIR20 %in% id_sortants, c('ID_NIR20', 'S_BRUT', 'STATUT_CONTRAT', 'EQTP')]
SalairesN <- dfEff_N[dfEff_N$ID_NIR20 %in% id_sortants, c('ID_NIR20', 'S_BRUT', 'STATUT_CONTRAT', 'EQTP')]

SalairesN <- SalairesN %>% dplyr::rename(S_BRUT_N = S_BRUT)
SalairesN <- SalairesN %>% dplyr::rename(EQTP_N = EQTP)
SalairesN <- SalairesN %>% dplyr::rename(SC_N = STATUT_CONTRAT)

SalairesNmoins1 <- SalairesNmoins1 %>% dplyr::rename(S_BRUT_Nmoins1 = S_BRUT)
SalairesNmoins1 <- SalairesNmoins1 %>% dplyr::rename(EQTP_Nmoins1 = EQTP)
SalairesNmoins1 <- SalairesNmoins1 %>% dplyr::rename(SC_Nmoins1 = STATUT_CONTRAT)

Data <- merge(x = SalairesNmoins1, y = SalairesN, all = TRUE)
Data$RATIO <- (Data$S_BRUT_N/Data$EQTP_N) / (Data$S_BRUT_Nmoins1/Data$EQTP_Nmoins1)

median(Data[!is.na(Data$RATIO),]$RATIO)
median(Data[(!is.na(Data$RATIO)) & (Data$SC_N == 'TITH'),]$RATIO)
median(Data[(!is.na(Data$RATIO)) & (Data$SC_N == 'CONT'),]$RATIO)
median(Data[(!is.na(Data$RATIO)) & (Data$SC_N == 'MEDI'),]$RATIO)
```



## Analyse des dates de sorties des sortants 

Voir notebook `Analyses_LiensEntrantsSortants.Rmd`.




