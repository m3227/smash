---
title: "Visualisation sur les données SIASP `r params$annee`, champ `r toupper(params$champ)`"
author: "DREES/OSAM/BPS - Maud Galametz / Gilles Gonon"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  pdf_document:
    toc: true
    number_sections: true
  html_document:
    toc: true
    toc_float: true
    number_sections: true
    theme: united
    highlight: tango

params:
  annee:
    label: "Année"
    value: 2020
    input: slider
    min: 2016
    max: 2020
    step: 1
    sep: ""
  champ:
    label: "champ des données"
    value: "fph"
    input: select
    choices: ["hop", "fph"]

geometry: "left=2cm,right=2cm,top=2cm,bottom=2cm"
---


```{r generate_page, eval=FALSE, include=FALSE}
# Exécuter cette cellule pour générer le document PDF (accès à I: optionnel)

for (ch in c("fph")) {
  for (an in seq(2018, 2020)) { # c(2020)) {  # 
    rmd_file <- "Analyses_Population_SIASP"
    out_format <- "html" # "pdf"
    out_local <-  sprintf("~/%s-%s-%d.%s", rmd_file, ch, an, out_format)
    rmarkdown::render(paste(rmd_file,".Rmd", sep = ""),
                      output_format = paste(out_format, "_document", sep=""),
                      output_file = out_local, 
                      params = list(annee = an))
    
    tryCatch( {
      chemin_dist <- file.path("I:/BPS/_Emplois et revenus",
                               "Projet SMASH/03_Comptes_rendus",
                               "EtudesComportementales/AnalysesSurSIASP")
      out_dist <- file.path(chemin_dist, basename(out_local))
      cat("Sauvegarde distante, dossier :", out_dist, "\n")
      file.copy(out_local, out_dist, overwrite = TRUE)
      },
      error = function(e) {
        message( paste("sauvegarde distante impossible, fichier :", out_dist) )
      }
    )
  }
}

``` 


```{r librairies, include=FALSE}
library(data.table)
library(ggplot2)
library(plotly)
annee <- params$annee
champ <- params$champ

## Chargement des données 
Sys.setenv(R_CONFIG_ACTIVE = Sys.getenv('username'))
config <- config::get()
data_path <- config$path_siasp

# Chargement package Smash dev
devtools::load_all(config$path_smash)

```

```{r chargement, echo=FALSE, include=FALSE}
## Chargement des données
dt_salaires <- chargerFST(champ, annee, b_selection = FALSE)
l_cols_utf_8 <- c("REGION", "GRADE_FIL", "CORPS", "NATURE_B", "CSREG4_BIS")
for (col in l_cols_utf_8) { Encoding(dt_salaires[[col]]) <- "UTF-8" }
dt_salaires <- as.data.table(dt_salaires)

# Garde la ligne d'EQTP max pour les doublons d'ID
# Très long et pas ce qu'on veut : si 2 EQTP égaux les 2 lignes sont gardées
# dt_effectifs <- dt_salaires[, .SD[EQTP==max(EQTP)], by=ID_NIR20] # très long
# Ok mais un peu long
# dt_effectifs <- dt_salaires[dt_salaires[, .I[which.max(EQTP)], by = ID_NIR20]$V1]
# Ok et plus rapide
dt_effectifs <- dt_salaires[order(-EQTP)][dt_salaires[, .I[1], by = ID_NIR20]$V1]

cat("Effectifs :", nrow(dt_effectifs), "personnes\n")

```

# Chargement des données

Les données utilisées sont les SIASP de l'année `r annee``, pour le champ 
`r toupper(champ)`.

Pour l'étude des effectifs, on agrège l'ensemble des données par l'identifiant 
de chaque individu (ID_NIR20) en gardant le poste d'EQTP maximum 
(si 2 postes ont le même EQTP, le premier dans la table est gardé).

La masse salariale est calculée comme `COUT_EMP = S_BRUT + TOT_COT_EMP`, où 

`S_BRUT` est le salaire brut incluant : 

- le traitement indiciaire `TRMT_PRINC` 
- les `PRIMES` 
- les heures supplémentaires `HEURES_SUP` 
- les autres frais comprennent  :
  - indemnités de résidence `INDEMN_RESID`,
  - frais de transport `VSMT_TRANSP`, 
  - avantage en nature `AV_NAT`, 
  - supplément familial de traitement `SFT`.

`TOT_COT_EMP` est le total des charges salariales de l'employeur. 

```{r cout_emp, echo=FALSE, include=FALSE}
# Ajout d'une colonne COUT_EMP
dt_salaires$COUT_EMP <- dt_salaires$S_BRUT + dt_salaires$TOT_COT_EMP

# Calcul de la masse salariale totale
MasseSalarialeTotale <- sum(dt_salaires$COUT_EMP)
```

# Effectifs et salaires par filière

Le montant total de la masse salariale est de 
 `r round(MasseSalarialeTotale/1e9, digits=2)` milliards d'€ .

Nous visualisons ci-dessous les répartitions par filière des 
effectifs d'une part et de la masse salariale d'autre part.

```{r stats_filiere, echo=FALSE}
raccourcir_filiere <- function(v_filieres) {
  v_filieres <- stringr::str_replace(v_filieres, 
                                     "Personnel (de |des )*", "")
  v_filieres <- sub("(.)","\\U\\1", v_filieres, perl = T)
  v_filieres <- stringr::str_replace_all(v_filieres, "(\\s|-)", "\\1<br>")
}

# Effectifs
dt_stats_FILIERE <- dt_effectifs[, .(.N), by = .(GRADE_FIL)][order(-N)]
# Raccourcissement des termes
dt_stats_FILIERE$GRADE_FIL <- raccourcir_filiere(dt_stats_FILIERE$GRADE_FIL)

# Couleurs des filières (pour homogénéité sur les différents graph)
dt_stats_FILIERE$COLOR <- hcl.colors(nrow(dt_stats_FILIERE))

# Salaires
brut_FILIERE <- dt_salaires[, .(COUT_EMP=round(sum(COUT_EMP)/1e9, digits = 2)), 
                            by = .(GRADE_FIL)][order(-COUT_EMP)]
# Raccourcissement des termes
brut_FILIERE$GRADE_FIL <- raccourcir_filiere(brut_FILIERE$GRADE_FIL)
# Utilise les mêmes couleurs pour chaque filière
brut_FILIERE$COLOR <- dt_stats_FILIERE$COLOR[match(brut_FILIERE$GRADE_FIL,
                                                   dt_stats_FILIERE$GRADE_FIL)]

# EQTP
eqtp_FILIERE <- dt_salaires[, .(EQTP=round(sum(EQTP), digits = 1)), 
                            by = .(GRADE_FIL)][order(-EQTP)]
# Raccourcissement des termes
eqtp_FILIERE$GRADE_FIL <- raccourcir_filiere(eqtp_FILIERE$GRADE_FIL)
# Utilise les mêmes couleurs pour chaque filière
eqtp_FILIERE$COLOR <- dt_stats_FILIERE$COLOR[match(eqtp_FILIERE$GRADE_FIL,
                                                   dt_stats_FILIERE$GRADE_FIL)]

# Visu Effectifs
l_figs <- list()
l_figs$effectifs <- plot_ly(dt_stats_FILIERE, labels = ~GRADE_FIL, 
                            values = ~N, type = 'pie',
                            textposition = 'inside',
                            textinfo = 'label+value+percent', 
                            hoverinfo = 'text',
                            text = ~paste(GRADE_FIL, '\n', N, ' personnes'),
                            insidetextorientation='horizontal',
                            marker = list(colors = ~COLOR,
                                          line = list(color = '#FFFFFF', width = 1)),
                            showlegend = FALSE)

l_figs$effectifs <- l_figs$effectifs %>% layout(
  title = paste('Répartition des effectifs FPH par filière', annee),
  xaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE),
  yaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE))


# Salaires
l_figs$brut <- plot_ly(brut_FILIERE, labels = ~GRADE_FIL, 
                       values = ~COUT_EMP, type = 'pie',
               textposition = 'inside',
               textinfo = 'label+value+percent', 
               hoverinfo = 'text',
               text = ~paste(GRADE_FIL, '\n', COUT_EMP, ' Milliards €'),
               insidetextorientation='horizontal',
               marker = list(colors = ~COLOR,
                             line = list(color = '#FFFFFF', width = 1)),
               showlegend = FALSE)

l_figs$brut <- l_figs$brut %>% layout(
  title = paste('Masse salariale par filière FPH',annee,
                'en milliards €, total', 
                sprintf("%.1f", MasseSalarialeTotale/1e9)),
  xaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE),
  yaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE))


# en EQTP
l_figs$eqtp <- plot_ly(eqtp_FILIERE, labels = ~GRADE_FIL, 
                       values = ~EQTP, type = 'pie',
               textposition = 'inside',
               textinfo = 'label+value+percent', 
               hoverinfo = 'text',
               text = ~paste(GRADE_FIL, '\nSomme EQTP =', EQTP),
               insidetextorientation='horizontal',
               marker = list(colors = ~COLOR,
                             line = list(color = '#FFFFFF', width = 1)),
               showlegend = FALSE)

l_figs$eqtp <- l_figs$eqtp %>% layout(
  title = paste('Effectifs en EQTP par filière FPH',annee),
  xaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE),
  yaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE))


# Rendu HTML des figures créées précédemment
htmltools::tagList(l_figs)

```

# Agrégation par CORPS de la FPH

## Masse salariale


```{r cout_metier, echo=FALSE}

# Salaires
count_CORPS <- dt_salaires[, .(COUT_EMP=round(sum(COUT_EMP)/1e9, digits = 2)), 
                              by = .(CORPS)][order(-COUT_EMP)]
count_CORPS$CORPS <- sub("(.)","\\U\\1", 
                              count_CORPS$CORPS, perl = T)
count_CORPS$CORPS_LAB <- stringr::str_replace_all(count_CORPS$CORPS, 
                                                   "(\\s|-)", "\\1<br>")

fig <- plot_ly(count_CORPS, labels = ~CORPS_LAB, values = ~COUT_EMP, type = 'pie',
               textposition = 'inside',
               textinfo = 'label+value+percent', 
               hoverinfo = 'text',
               text = ~paste(stringr::str_replace_all(CORPS, '<br>', ' '), 
                             '\n', COUT_EMP, ' milliards €'),
               insidetextorientation='horizontal',
               marker = list(colors = colors,
                             line = list(color = '#FFFFFF', width = 1)),
               showlegend = FALSE)

fig <- fig %>% layout(
  title = paste('Masse salariale par CORPS', 
                toupper(champ), annee, 'en milliards d\'€'), 
  xaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE),
  yaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE))

htmltools::tagList(list(fig))


```

## Effectifs

```{r eff, echo=FALSE}

o <- GrillesIndiciaires$new()
dt_effectifs$CORPS <- o$getCorps(dt_effectifs$GRADE_ECH)
dt_salaires$CORPS <- o$getCorps(dt_salaires$GRADE_ECH)

count_CORPS <- dt_effectifs[, .(.N), by = .(CORPS)][order(-N)]
count_CORPS <- dt_effectifs[, .(.N), by = .(CORPS)][order(-N)]
# count_CORPS$CORPS <- sub("(.)","\\U\\1", count_CORPS$CORPS, perl = T)
count_CORPS$CORPS_LAB <- stringr::str_replace_all(count_CORPS$CORPS, 
                                                   "(\\s|-)", "\\1<br>")


fig <- plot_ly(count_CORPS, labels = ~CORPS_LAB, values = ~N, type = 'pie',
               textposition = 'inside',
               textinfo = 'label+value+percent', 
               hoverinfo = 'text',
               text = ~paste(CORPS, '\n', N, ' personnes'),
               insidetextorientation='horizontal',
               marker = list(colors = colors,
                             line = list(color = '#FFFFFF', width = 1)),
               showlegend = FALSE)

fig <- fig %>% layout(
  title = paste('Répartition des effectifs FPH par CORPS', annee),
  xaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE),
  yaxis = list(showgrid = FALSE, zeroline = FALSE, showticklabels = FALSE))

htmltools::tagList(list(fig))


# total_CORPS <- sum(count_CORPS$N)
# plot(cumsum(count_CORPS[order(-N)]$N) / total_CORPS,
#     main = "CDF des Corps", ylab = "% Effectifs cumulés")


```



# Groupement sur 2 critères

## Histogramme sur des tranches d'ages définies

### Groupement sur 2 critères : Corps & Age 

```{r AS, echo=FALSE}
count_CORPS_age <- dt_salaires[, .(nb = .N), by = .(CORPS, AGE)]

# Création de groupes d'age
tranche_age <- c(0, 20, 30, 40, 50, 57, Inf)
nom_tranche_age <- c("0-20", "20-30", "30-40", "40-50", "50-57", ">57")
dt_effectifs[, "tranche_age"] <- cut(dt_effectifs$AGE, tranche_age,
                                    nom_tranche_age, include.lowest = TRUE)

# Affichage de l'histogramme
fig <- plot_ly(dt_effectifs[CORPS == "Aides-soignants et ashq", ], 
               x = ~AGE, 
               type="histogram", 
               xbins = list(size = 5)) %>% 
  layout(
  title = paste('Répartition des Aides-soignantes par age,', annee))
fig

```

et zoom sur les AS de plus de 50 ans

```{r AS50, echo=FALSE}
dt_agees <- dt_effectifs[CORPS=="Aides-soignants et ashq" & AGE>=50,]

fig <- plot_ly(dt_agees, 
               x = ~AGE, 
               type="histogram", 
               xbins = list(size = 2)) %>% 
  layout(
  title = paste('Histogramme des ages des Aides-soignantes, >= 50 ans,', annee))
fig

```

# Répartition des ages pour les principaux corps

Les tables suivantes donnent la répartition des effectifs par tranche 
d'age pour les principaux corps en terme d'effectifs. 

```{r count_CORPS_tranche_age, results='asis', echo=FALSE}
count_CORPS_tranche_age <- dt_effectifs[, .(n_tranche = .N), by = .(CORPS, tranche_age)]
count_CORPS_tranche_age <- count_CORPS_tranche_age[order(-n_tranche, tranche_age)]

# Transformation des comptes N en pourcentage par rapport au total de chaque CORPS
# Jointure à gauche en éliminant les lignes non trouvées
count_CORPS_tranche_age <- count_CORPS_tranche_age[count_CORPS, on = .(CORPS), nomatch = 0]
count_CORPS_tranche_age <- count_CORPS_tranche_age[order(-n_tranche, tranche_age)]
count_CORPS_tranche_age[, "% tranche"] <- round(100*count_CORPS_tranche_age[, .(n_tranche)] /
  count_CORPS_tranche_age[, .(N)], digits = 1)

## Affichage des répartitions par tranche d'age
# Rq : Curieusement il faut retrier count_CORPS ...
count_CORPS <- count_CORPS[order(-N)]

# La boucle ne marche pas :( , rien ne s'affiche dans le résultat
# for (i in 1:3) {
#   cat('\n\n')
#   strTitre <- sprintf("%s (%d personnes) par tranche d'age", 
#                       count_CORPS[i, CORPS], 
#                       count_CORPS_tranche_age[CORPS == count_CORPS[i, CORPS]][1,N])
#   knitr::kable(
#     count_CORPS_tranche_age[CORPS == count_CORPS[i, CORPS], !c("CORPS", "N")], 
#     caption =  strTitre)
#   
#   cat('\n\n')
# }

i <- 1
strTitre <- sprintf("%s (%d personnes) par tranche d'age", 
                      count_CORPS[i, CORPS], 
                      count_CORPS_tranche_age[CORPS == count_CORPS[i, CORPS]][1,N])
knitr::kable(
    count_CORPS_tranche_age[CORPS == count_CORPS[i, CORPS], !c("CORPS", "N")], 
    caption =  strTitre)

i <- 2
strTitre <- sprintf("%s (%d personnes) par tranche d'age", 
                      count_CORPS[i, CORPS], 
                      count_CORPS_tranche_age[CORPS == count_CORPS[i, CORPS]][1,N])
knitr::kable(
    count_CORPS_tranche_age[CORPS == count_CORPS[i, CORPS], !c("CORPS", "N")], 
    caption =  strTitre)

i <- 3
strTitre <- sprintf("%s (%d personnes) par tranche d'age", 
                      count_CORPS[i, CORPS], 
                      count_CORPS_tranche_age[CORPS == count_CORPS[i, CORPS]][1,N])
knitr::kable(
    count_CORPS_tranche_age[CORPS == count_CORPS[i, CORPS], !c("CORPS", "N")], 
    caption =  strTitre)




```

