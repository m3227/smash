---
title: "Appariemment entre les grades et grille Aide-soignant"
output: pdf_notebook
---

Pour l'ensemble des grades d'aide-soignants, ce script recherche la grille indiciaire la plus proche parmi les 3 grilles disponibles.

Une table d'association est proposée pour pouvoir ensuite facilement 
affecter un grade à une grille indiciaire pour les simulations. 


```{r chargement_siasp}
if (Sys.getenv('username')== "gilles.gonon") {
  fic_rds <- "C:\\Users\\gilles.gonon\\Documents\\data\\Salaires\\SIASP2020_c20_Salaires.rds"
} else {
  # Serveur de données T
  fic_rds <- "T:\\BPS\\SMASH\\Salaires\\Tables_RDS\\SIASP2020_c20_Salaires.rds"
}

dt_siasp <- readRDS(fic_rds)
```


```{r chargement_grilles}
chemin = "T:\\BPS\\SMASH\\\\grilles\\"
fichiers = c("Aide-soignant_C1.txt", 
             "Aide-soignant_C2.txt", 
             "Aide-soignant_C3.txt")

# Chargement des fichiers dans une liste de données
grilles <- list()
for (i in 1:length(fichiers)) {
  new_donnees <- read.csv(file.path(chemin, fichiers[i]), encoding = "UTF-8")
  new_donnees <- new_donnees[order(new_donnees$Échelon),]
  # Suppression des accents dans les noms de colonnes (non nécessaire)
  # names(new_donnees) <- iconv(colnames(new_donnees),to="ASCII//TRANSLIT")
  grilles[[i]] <- new_donnees
}
```


## Analyse du corps Aide-soignant

On récupère les données dont le corps est de type "aide-soignants", ainsi que la liste des grades contenus dans ces appellations. 

```{r filtre_as}
list_corps <- c("Aide-soignant 0225 C", 
               "Aide-soignant et ashq 0226 C", 
               "Aide-soignant et ashq 0227 C")

dt_as <- dt_siasp[dt_siasp$corps %in% list_corps, ]

df_grades <- data.frame(grade=sort(unique(dt_as$GRADE)))
```


Pour chaque grade, on réalise l'association avec la procédure suivante : 

- on récupère les indices des personnes du grade
- on compare cette liste d'indice à chaque grille indiciaire chargée
- on calcule une "distance" comme la somme des différences des indices du grade avec l'échelon le plus proche dans la grille étudiée
- on associe à la grille la plus proche

```{r association_grade_grille}

# Ajout d'une colonne avec le nom de la grille la plus proche
df_grades$grille <- rep("", nrow(df_grades))

# Parcours des grades
for (idx in 1:nrow(df_grades)) {
  grade <- df_grades$grade[idx]
  l_indices <- sort( unique( dt_as[ dt_as$STATUT_CONTRAT=="TITH"
                                    & dt_as$GRADE==grade 
                                    & dt_as$INDICE>=300 , ]$INDICE ) )
  # cat( paste("Grade", grade, ": indices (", 
  #           paste(l_indices, collapse = ","), ")\n" ) )
  
  dist_grille <- rep(0, length(fichiers))
  for (idxi in 1:length(l_indices)) {
    indice <- as.numeric(l_indices[idxi])
    for (idxg in 1:length(grilles)) {
      dist_grille[idxg] <- dist_grille[idxg] + min(abs(grilles[[idxg]]$IB-indice))
    }
  }
  
  idx_min <- which.min(dist_grille)
  df_grades$grille[idx_min] <-  fichiers[idx_min]
  df_grades$dist <-  dist_grille[idx_min]
  cat( paste("Grade", grade, ":", 
             df_grades$grille[idx_min], 
             "distances (", 
             paste(dist_grille, collapse = ","), ")\n" ) )
}

nom_csv <- "association_grades_siasp_grilles.csv"
write.csv(df_grades, nom_csv)
cat("\n -> Sauvegarde en CSV, fichier", nom_csv, "\n")
```


