# Quelques Tableaux pour le rapport méthodo de SMASH ----
# Paramètres propres à chaque figure, voir sommaire --->
# - Comptage du nombres de poste par agent FPH/HOP
# - Taux d'entrants / sortants / contrats courts
# - Quotients de mortalité modèle Vs. simulé

devtools::load_all("../smash-r-pkg/")
library(ggplot2)

## Comptage du nombres de poste par agent FPH/HOP ----
an <- 2020

# Chargement des données non agrégées pour comptge des postes
df_fph <- chargerFST("fph", an, b_agrege = F)
df_hop <- chargerFST("hop", an, b_agrege = F)

compter_nb_postes_par_agent <- function(df) {
  # Comptage du nombres de poste = count du count des ID_NIR20
  df_nb_contrat <- df %>% 
    count(ID_NIR20, name = "nb_postes", sort = T) %>% 
    count(nb_postes, name = "Effectifs", sort = T)
  # Création de tranches, pour limiter à 5 postes et plus
  df_nb_contrat$nb_postes_cut <- 
    cut(df_nb_contrat$nb_postes, breaks = c(0,1,2,3,4,Inf), 
        labels = c("1 poste", "2 postes", "3 postes", "4 postes", "5 postes et +"), 
        right = TRUE )
  
  # Synthèse 
  df_nb_postes <- df_nb_contrat %>% 
    select(nb_postes_cut, Effectifs) %>% 
    group_by(nb_postes_cut) %>% 
    summarize(Effectifs = sum(Effectifs)) %>% 
    mutate(Effectifs_pct = Effectifs / sum(Effectifs))

  return(df_nb_postes)  
}


fic_xl <- sprintf("~/Comptage_postes_par_agents_FPH_HOP_%s.xlsx", an)


df_nb_postes_fph <- compter_nb_postes_par_agent(df_fph)
df_nb_postes_hop <- compter_nb_postes_par_agent(df_hop)

xlsx::write.xlsx(df_nb_postes_fph, fic_xl, sheetName = "FPH", 
                 row.names = FALSE, append = FALSE)
xlsx::write.xlsx(df_nb_postes_hop, fic_xl, sheetName = "HOP", 
                 row.names = FALSE, append = TRUE)

utils::browseURL(fic_xl)

## Taux d'entrants / sortants / contrats courts ----
l_taux_in_out <- list()

fic_xl <- "~/Taux_entrants_sortants_cc.xlsx"


for (champ in c("fph","hop")) {
  
  l_taux_in_out[[champ]] <- NULL
  n_tot_prec <- NA # pour évolution effectif
  
  for (an in seq(2010, 2021)) {
    # Chargement des données agrégés pour les effectifs
    df <- getEchantillonSIASP(champ, an, b_agrege = T)

    # Calcul des indicateurs
    n_tot <- nrow(df)
    n_in_hors_cc <- sum(df$IS_ENTRANT & !df$IS_SORTANT)
    n_out_hors_cc <- sum(!df$IS_ENTRANT & df$IS_SORTANT)
    n_cc <- sum(df$IS_ENTRANT & df$IS_SORTANT)
    
    tx_in  <- n_in_hors_cc / n_tot
    tx_out <- n_out_hors_cc / n_tot
    tx_cc  <- n_cc / n_tot
    tx_evol <- (n_tot-n_tot_prec) / n_tot_prec
    # Calcul des taux pour SMASH
    # Les sortants sont tirés avant que les entrants de l'année soient ajoutés
    n_in <- n_in_hors_cc + n_cc
    tx_out_smash <- n_out_hors_cc / (n_tot - n_in)
    # Les entrants sont tirés en incluant les CC à partir (fraction)
    tx_in_smash <- (n_in_hors_cc+n_cc) / (n_tot - n_in)
    tx_in_cc_smash =  n_cc / n_in
                                    
    n_tot_prec <- n_tot
    # Ajout au tableau
    l_taux_in_out[[champ]] <- rbind(
      l_taux_in_out[[champ]],
      data.frame(Annee = an,
                 Effectif = n_tot,
                 Pct_evol_effectif = tx_evol,
                 N_entrants_hors_cc = n_in_hors_cc,
                 N_sortants_hors_cc = n_out_hors_cc,
                 N_cc = n_cc,
                 Pct_entrants = tx_in,
                 Pct_sortants = tx_out,
                 Pct_cc = tx_cc,
                 Pct_in_smash = tx_in_smash,
                 Pct_out_smash = tx_out_smash,
                 Pct_cc_in_smash = tx_in_cc_smash
      )
    )
  }
  
  # Sauvegarde Excel 
  names(l_taux_in_out[[champ]]) <- c("Année", 
                                     "Effectif",
                                     "Évolution effectif, %",
                                     "Entrants hors CC",
                                     "Sortants hors CC",
                                     "Contrats courts",
                                     "Taux entrants, %",
                                     "Taux sortants, %",
                                     "Taux contrats courts, %",
                                     "Taux entrants SMASH, %",
                                     "Taux sortants SMASH, %", 
                                     "Fraction CC ds les entrants, %")
  
  xlsx::write.xlsx(l_taux_in_out[[champ]], 
                   fic_xl, sheetName = toupper(champ), 
                   row.names = FALSE, append = (champ=="hop") )
  
}

utils::browseURL(fic_xl)

## Entrants par STATUT ----
df_contrats <- NULL
l_statuts <- c("TITH", "CONT", "MEDI", "CAID", "ELEV", "INTE")
anDeb <- 2013
anFin <- 2020
for (an in seq(anDeb,anFin)) {
  df_an <- getEchantillonSIASP("hop", an, 0, b_agrege = TRUE) %>% 
    filter(STATUT_CONTRAT %in% l_statuts & IS_ENTRANT == TRUE)
  
  # Séparation CDD/CDI
  is_cont <- df_an$STATUT_CONTRAT=="CONT"
  df_an$STATUT_CONTRAT[is_cont] <- 
    stringi::stri_c(df_an$STATUT_CONTRAT[is_cont], "_", df_an$DUREE_CONTRAT[is_cont])
  
  df_contrats <- rbind(
    df_contrats, 
    df_an %>% group_by(STATUT_CONTRAT) %>% 
      summarise(total=n()) %>% arrange(desc(total)) %>%
      mutate(AN = an)
  )
}

df_contrats$AN <- factor(df_contrats$AN)
df_contrats$STATUT_CONTRAT <- factor(df_contrats$STATUT_CONTRAT)


# Création d'une table pour avoir les valeurs
# df.count.contrats <- merge.data.frame(
#   df.2017.contrats, df.2018.contrats, all = T,
#   by = "STATUT_CONTRAT", suffixes = c(".2017",".2018") 
# )
# df.count.contrats <- merge.data.frame(
#   df.count.contrats, df.2019.contrats, all = T, by = "STATUT_CONTRAT") %>% 
#   rename(total.2019 = total)
# df.count.contrats <- merge.data.frame(
#   df.count.contrats, df.2020.contrats, all = T, by = "STATUT_CONTRAT") %>% 
#   rename(total.2020 = total) %>% arrange(desc(total.2020))
# 
# df.count.contrats[is.na(df.count.contrats)] <- 0
# # Ajout d'une colonne pour les taux de représentativité en 2017 & 2020
# df.count.contrats$pct.2017 <- round(100*df.count.contrats$total.2017 / sum(df.count.contrats$total.2017), digits=1)
# df.count.contrats$pct.2020 <- round(100*df.count.contrats$total.2020 / sum(df.count.contrats$total.2020), digits=1)
# # Affichage console
# print(df.count.contrats)

ggplot(data=df_contrats, aes(x=STATUT_CONTRAT, y=total, fill=AN)) + 
  geom_bar(stat="identity", position="dodge") +
  labs(title = paste("Effectifs entrants par type de contrat, de", anDeb, "à", anFin),
       y     = "Effectifs",
       x     = "") +
  theme_minimal() +
  theme(axis.text.x=element_text(angle=45, hjust=1)) 



## Comparaison Taux de morts générés par SMASH et modèle INSEE ----
an <- 2021
df_an <- getEchantillonSIASP("fph", an, b_agrege = T)
df_plot <- full_join(data.frame(AGE = seq(20,80)),
                     data.frame(SEXE = c("F", "M")), 
                     by = character())
fic_mortalite <- "T:/BPS/SMASH/modeles/mortalite_Ensemble.rds"
for (test in seq(10)) {
  df_dead <- majMort(df_an, 2022, 
                     fic_mortalite = fic_mortalite)
  df_dead$IS_MORT <- stringi::stri_detect_fixed(df_dead$INFO, "Decede")

  df_c <- df_dead %>% group_by(AGE, SEXE) %>% 
    summarise(TOTAL = n(), NB_MORT = sum(IS_MORT), 
              .groups = "drop") %>%
    mutate(TX_MORT_100000 = 100000 * NB_MORT / TOTAL)
  
  df_plot <- left_join(df_plot, df_c[, c("AGE", "SEXE", "TX_MORT_100000")], 
                       by = c("AGE", "SEXE"), suffix=c("", paste0("_",test)))
  
}

# Calcul de la moyenne des taux pour chaque AGE (en ligne)
l_cols <- names(df_plot)
df_plot$TX_MOY <- rowMeans(df_plot[, l_cols[startsWith(l_cols, "TX_MORT")]])

# Chargement du modèle
mdl <- readRDS(fic_mortalite)
mdl$SEXE <- stringi::stri_c(mdl$SEXE, " modèle")
df_plot$SEXE <- stringi::stri_c(df_plot$SEXE, " simu.")

ggplot(df_plot, aes(x=AGE, y=TX_MOY, color = SEXE)) +
  geom_line() + 
  geom_line(data = mdl %>% filter(AGE >19 & AGE<81), linetype = 2,
            aes(x=AGE, y=QUOTIENT, color=SEXE)) +
  labs(
    title    = "Quotients de mortalité simulé et modèlisé",
    subtitle = "Moyenne sur 10 simulations",
    y        = "Nombre de morts pour 100000",
    x        = "Age", 
    caption  = paste("Données FPH SIASP", an)) + 
  scale_y_log10() + theme_minimal()


## Structure des effectifs en cumulés par filière / corps ----

an <- 2021
df_an <- getEchantillonSIASP("fph", an, b_agrege = T)
oBDD <- GrillesIndiciaires$new()

df_an$CORPS <- oBDD$getCorps(df_an$GRADE)
df_an$FILIERE <- oBDD$getFiliere(df_an$GRADE)
df_an$FILIERE[is.na(df_an$FILIERE)] <- "Personnel hors filière"

### Contribution des CORPS ----
df_corps <- df_an %>% group_by(CORPS) %>% 
  summarise(Effectifs = n(), S_BRUT = sum(S_BRUT)) %>% 
  mutate(PCT_Effectifs = round(100* Effectifs/nrow(df_an), digits = 2),
         PCT_BRUT = round(100* S_BRUT/sum(df_an$S_BRUT), digits = 2)) %>% 
  arrange(desc(PCT_BRUT))

# On coupe ceux qui contribuent à moins de xx% 
seuil = 0.5
df_corps_inf_pct <- df_corps %>% filter(PCT_Effectifs < seuil)

df_corps_tbl <- rbind(
  df_corps %>% filter(PCT_Effectifs >= seuil),
  data.frame(CORPS = paste0("Autres corps (", 
                            nrow(df_corps_inf_pct), "), ", 
                            sum(df_corps_inf_pct$PCT_Effectifs), "%"), 
             Effectifs = sum(df_corps_inf_pct$Effectifs),
             S_BRUT = sum(df_corps_inf_pct$S_BRUT),
             PCT_Effectifs = sum(df_corps_inf_pct$PCT_Effectifs),
             PCT_BRUT = sum(df_corps_inf_pct$PCT_BRUT)
  ) ) %>% mutate(
    CUM_PCT_Effectifs = cumsum(PCT_Effectifs),
    CUM_PCT_BRUT = cumsum(PCT_BRUT)
  )


df_plot <- rbind(df_corps_tbl %>% select(CORPS, PCT_Effectifs, CUM_PCT_Effectifs) %>%
                   mutate(TYPE = "Effectifs", IDX = row_number()) %>% 
                   rename(PCT = PCT_Effectifs, CUM_PCT = CUM_PCT_Effectifs), 
                 df_corps_tbl %>% select(CORPS, PCT_BRUT, CUM_PCT_BRUT) %>%
                   mutate(TYPE = "Salaires Bruts", IDX = row_number()) %>% 
                   rename(PCT = PCT_BRUT, CUM_PCT = CUM_PCT_BRUT))

v_corps <- raccourcir_corps(df_corps_tbl$CORPS)

ggplot(df_plot, aes(x = IDX, y = CUM_PCT, fill=TYPE)) +
  geom_bar(stat="identity", position=position_dodge() ) +
  labs(
    title = "Contribution cumulée des corps FPH, effectifs et salaires",
    y     = "%",
    x     = "") + 
  theme_minimal() +
  scale_x_discrete(name ="", 
                   limits = factor(1:length(v_corps)), 
                   labels = v_corps, 
                   guide = guide_axis(angle = 90))
  

### Contribution des FILIERE ----

df_filiere <- df_an %>% group_by(FILIERE) %>% 
  summarise(Effectifs = n(), S_BRUT = sum(S_BRUT)) %>% 
  mutate(PCT_Effectifs = round(100* Effectifs/nrow(df_an), digits = 2),
         PCT_BRUT = round(100* S_BRUT/sum(df_an$S_BRUT), digits = 2)) %>% 
  arrange(desc(PCT_BRUT))


df_filiere_tbl <- df_filiere %>% 
  mutate(CUM_PCT_Effectifs = cumsum(PCT_Effectifs),
         CUM_PCT_BRUT = cumsum(PCT_BRUT))


df_plot <- rbind(df_filiere_tbl %>% 
                   select(FILIERE, PCT_Effectifs, CUM_PCT_Effectifs) %>%
                   mutate(TYPE = "Effectifs", IDX = row_number()) %>% 
                   rename(PCT = PCT_Effectifs, CUM_PCT = CUM_PCT_Effectifs), 
                 df_filiere_tbl %>% select(FILIERE, PCT_BRUT, CUM_PCT_BRUT) %>%
                   mutate(TYPE = "Salaires Bruts", IDX = row_number()) %>% 
                   rename(PCT = PCT_BRUT, CUM_PCT = CUM_PCT_BRUT))

v_filiere <- raccourcir_filiere(df_filiere_tbl$FILIERE, b_html = FALSE)

ggplot(df_plot, aes(x = IDX, y = CUM_PCT, fill=TYPE)) +
  geom_bar(stat="identity", position=position_dodge() ) +
  labs(
    title = "Contribution cumulée des filières FPH, effectifs et salaires",
    y     = "%",
    x     = "") + 
  theme_minimal() +
  scale_x_discrete(name ="", 
                   limits = factor(1:length(v_filiere)), 
                   labels = v_filiere, 
                   guide = guide_axis(angle = 90))


## Répartit° effectifs > 55ans par statut/age retraites ----

an <- 2019
df_an <- getEchantillonSIASP("hop", an, 0, b_agrege=T)

# Liste des tranches d'âges souhaitées
v_breaks <- c(55, 61, 66, Inf)

df_count_age_statut <- NULL

for (idx in seq(length(v_breaks)-1)) {
  df_c_age <- df_an %>% filter(AGE>v_breaks[idx] & AGE<=v_breaks[idx+1]) %>% 
    count(STATUT_CONTRAT, sort = T, 
          name = sprintf("]%.0f,%.0f]", v_breaks[idx], v_breaks[idx+1]))
  
  if (idx==1) {
    df_count_age_statut <- df_c_age
  } else {
    df_count_age_statut <- left_join(df_count_age_statut, df_c_age, by = "STATUT_CONTRAT")
  }
  
}

fic_xl <- sprintf("~/Effectifs_Age_statut_Sup_55ans_HOP_%s.xlsx", an)
xlsx::write.xlsx(df_count_age_statut, fic_xl, sheetName = "HOP", 
                 row.names = FALSE, append = FALSE)
utils::browseURL(fic_xl)


calculer_repartition_age(df_an,
                         v_tranches = c(56, 61, 66, Inf),
                         v_labels = c("De 56 à 60 ans","De 61 à 65 ans",
                                      "66 ans et plus")
                         )
  

# Comparaison promotions décret / SIASP ----
oBDD <- GrillesIndiciaires$new()

calculer_tx_promo <- function(an, grade_src, grade_dest, 
                              ech_rk_min, anc_ech_min, anc_fph_min) {
  # Chargement des 2 années
  df_anPrec <- getEchantillonSIASP("fph", an-1, 0, b_agrege=T)
  df_an <- getEchantillonSIASP("fph", an, 0, b_agrege=T)
  
  # Sélection des individus TITH du grade
  df_grade <- df_anPrec %>% filter(STATUT_CONTRAT=="TITH" & GRADE==grade_src)
  
  # Récupération de l'échelon
  df_grilles <- oBDD$getGrillesId(df_grade$GRADE, "2019-12-31")
  df_echs <- oBDD$getGrilleEch(df_grilles$ID_GRILLE, ech_s = df_grade$ECH)
  df_grade$ECH_RANK <- df_echs$ECHELON_RANK
  
  # Sélection des promouvables (an précédent)
  df_ech_anPrec <- df_grade %>%  filter(ECH_RANK >= ech_rk_min & 
                                      ANCIENNETE_ECH>(anc_ech_min-1))
  
  df_promouvables <- df_ech_anPrec %>% filter(ANCIENNETE_FPH>(anc_fph_min-1))
  
  # Sélection de ceux qui sont toujours là année considérée
  df_echelon_ok <- inner_join(df_ech_anPrec %>% select(ID, STATUT_CONTRAT,
                                                   GRADE, ECH, ECH_RANK), 
                              df_an %>% select(ID, STATUT_CONTRAT, 
                                               GRADE, ECH), 
                              by= "ID", suffix = c(".anPrec", ".an"))
  
  df_promotions <- inner_join(df_promouvables %>% select(ID, STATUT_CONTRAT, 
                                                         GRADE, ECH, ECH_RANK), 
                              df_an %>% select(ID, STATUT_CONTRAT, 
                                               GRADE, ECH), 
                              by= "ID", suffix = c(".anPrec", ".an"))
  
  df_grades_an <- inner_join(df_grade %>% select(ID, STATUT_CONTRAT, 
                                                 GRADE, ECH, ECH_RANK), 
                             df_an %>% select(ID, STATUT_CONTRAT, 
                                              GRADE, ECH), 
                             by= "ID", suffix = c(".anPrec", ".an"))
  
  # Comptage des promus dans les 2 cas 
  nb_prom_promus <- nrow(df_promotions %>% filter(GRADE.an==grade_dest))
  nb_promus <- nrow(df_grades_an %>% filter(GRADE.an==grade_dest))
  
  # Comptage même grade
  nb_prom_meme_grade <- nrow(df_promotions %>% filter(GRADE.an==grade_src))
  nb_meme_grade <- nrow(df_grades_an %>% filter(GRADE.an==grade_src))
  
  # Résultat
  df_result <- data.frame(
    AN = an, 
    GRADE_SRC = grade_src,
    GRADE_DEST = grade_dest,
    NB_GRADES_ANPREC = nrow(df_grade),
    NB_ECHELON_OK_ANPREC = nrow(df_ech_anPrec),
    NB_PROMOUVABLES_ANPREC = nrow(df_promouvables),
    NB_GRADES_AN = nrow(df_grades_an),
    NB_ECHELON_OK_AN = nrow(df_echelon_ok),
    NB_PROMOUVABLES_AN = nrow(df_promotions),
    NB_PROMOUVABLES_PROMUS = nb_prom_promus,
    TX_PROMOUVABLES_PROMUS = nb_prom_promus/nrow(df_promotions),
    NB_PROMOUVABLES_MEME_GRADE = nb_prom_meme_grade,
    NB_PROMUS = nb_promus,
    NB_MEME_GRADE = nb_meme_grade,
    NB_AUTRE = nrow(df_grades_an) - nb_promus - nb_meme_grade,
    TX_PROMUS = nb_promus/nrow(df_grades_an)
    )
    

  return(df_result)
}

noms_cols <- 
  c("An", "Grade Src", "Grade Dest", 
    "Présents Grade src An-1", 
    "Ancienneté échelon OK An-1",
    "Promouvables An-1", 
    "Présents Grade src An", 
    "Ancienneté échelon OK An",
    "Promouvables An", "Promouvables Promus", 
    "Promouvables Promus, %", "Promouvables restés grade src", "Promus, total",
    "Nb restés grade src", "Autres cas", "Tx promus, total")

anc_min_max <- 9
df_promos <- rbind(
  calculer_tx_promo(2019, "3259", "3258", 
                    ech_rk_min = 5, anc_ech_min = 0, anc_fph_min = 5),
  calculer_tx_promo(2020, "3259", "3258", 
                    ech_rk_min = 5, anc_ech_min = 0, anc_fph_min = 5),
  calculer_tx_promo(2019, "3021", "3022", 4, 1, 5),
  calculer_tx_promo(2020, "3021", "3022", 4, 1, 5),
  calculer_tx_promo(2019, "2432", "2753", 4, 2, anc_min_max), # 7 au lieu de 10 (limite SIASP)
  calculer_tx_promo(2020, "2432", "2753", 4, 2, anc_min_max),
  calculer_tx_promo(2019, "2154", "2164", 
                    ech_rk_min = 4, anc_ech_min = 2, anc_fph_min = anc_min_max), 
  calculer_tx_promo(2019, "2155", "2165", 
                    ech_rk_min = 4, anc_ech_min = 2, anc_fph_min = anc_min_max), 
  calculer_tx_promo(2020, "2154", "2164", 
                    ech_rk_min = 4, anc_ech_min = 2, anc_fph_min = anc_min_max), 
  calculer_tx_promo(2020, "2155", "2165", 
                    ech_rk_min = 4, anc_ech_min = 2, anc_fph_min = anc_min_max)
  )

names(df_promos) <- noms_cols

fic_xl <- "~/Taux_promouvables_promus.xlsx"
xlsx::write.xlsx(df_promos, fic_xl, 
                 sheetName = paste0("2019-2020_anc_",anc_min_max), 
                 row.names = FALSE, append = TRUE)
utils::browseURL(fic_xl)
