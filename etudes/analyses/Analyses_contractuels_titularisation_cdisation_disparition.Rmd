---
title: "Titularisation et CDIsation des contractuels"
author: "Vincent Attia (LabSante)"
date: "18/10/2022"
output:
  word_document: default
  pdf_document: default
  html_document:
    df_print: paged
---


```{r setup, echo=FALSE}
#library(officer)

#install.packages("officer")

knitr::opts_chunk$set(echo = FALSE)
                      # tab.id=NULL,tab.cap=NULL,tab.topcaption=TRUE,tab.lp="tab:",
                      # tab.cap.style=NULL,tab.cap.pre="Table",tab.cap.sep=" :",tab.cap.tnd=0,tab.cap.tns="-",
                      # tab.style=NULL,tab.layout="autofit",tab.width=1)

#tab.cap.fp_text=officer::fp_text_lite(bold = TRUE)

```

```{r librairies, include=F}
library(dplyr)
library(data.table)
library(MASS)
library(fitdistrplus)
library(Rcpp)
library(fst)
library(readxl)
library(ggplot2)
library(plotly)

library(rpart)
library(caret)

library(flextable)
```


## Présentation 

Ce document étudie la titularisation et cdisation des contractuels (STATUT_CONTRAT=="CONT"),
 basée sur les taux empiriques observés dans les SIASP. Les taux peuvent être déclinés par grade/échelon, ancienneté... et serviront à modéliser ces phénomènes.
 
Comme les "sorties" sont gérées par une autre brique du modèle, elles ne seront pas prises en compte dans les modalités possibles de statut de l'année d'après, mais cette alternative est considérée pour les statistiques descriptives suivantes. De même que la transition vers d'autres statuts. Les taux de cette brique de travail devront être calculés sans ces modalités, sur la base des individus présents plusieurs années de suite.
 
Cette même méthode des taux empiriques sera utilisée pour la modélisation des parcours des MEDI (ELEV/INTE/MEDI).
 
```{r render, eval=FALSE, echo=FALSE}
# Exécuter cette cellule pour générer un pdf du rmd
# out_local <- paste0("~/Analyses nouveau contrat.pdf")
# rmarkdown::render("Analyses_contractuels_titularisation_cdisation_disparition.Rmd", 
#                       output_file = out_local)

```


## Chargement des données 

On recupere les données des années 2012 à 2021 avec une ligne par individu: "hopAAAA_agrege.fst".

Avant 2012, la base n'était pas encore correctement remplie.

```{r chargement, include=F}

chemin <- "T:\\BPS\\SMASH\\DonneesSIASP\\Tables_FST\\"

to_keep=c('ID_NIR20','STATUT_CONTRAT','DUREE_CONTRAT','GRADE_ECH','CORPS',
          'QUOTITE','EQTP','AGE','SEXE','ANC_FPH','DEPCOM_FONCTION','REGION','S_BRUT')

# salaire but dans les variables explicatives?
# Regarder plus tard si interet de les incorporer, car pas dans smash a priori:
#   'NB_HEURES_REMUN','DUREE_POSTE','DAT_DEBUT', 'NB_POSTES'

#de quoi charger les filières correspondantes:
#table_grade - idée abandonnée: utiliser le package
# conversion_grades<-as.data.table(read_excel("T:\\BPS\\SMASH\\grilles\\20211217_neh_grilles_sas.xlsx",
#                                    sheet="Table grade",col_names=T))
devtools::load_all("../../../smash-r-pkg") # adapter le chemin relatif Rmd <-> package
o <- GrillesIndiciaires$new() #o$getFiliere("3001")


# chargement des bases
years=as.character(2012:2021)

#première année faite à part à cause des diffèrentes variables crées
assign(paste0("dfEff",years[1]),
         read.fst(file.path(chemin, paste0("hop",years[1],"_agrege.fst")), #selection_
                  as.data.table=T,columns=to_keep)[,`:=`(nb_mois=12*(100*EQTP/QUOTITE),
                                                         DEP=substr(DEPCOM_FONCTION,1,2),
                                                         FILIERE=o$getFiliere(GRADE_ECH))]
)

for (year in years[2:length(years)]){
  assign(paste0("dfEff",year),
         read.fst(file.path(chemin, paste0("hop",year,"_agrege.fst")), #selection_
                  as.data.table=T,columns=to_keep)[,`:=`(nb_mois=12*(100*EQTP/QUOTITE),
                                                         DEP=substr(DEPCOM_FONCTION,1,2),
                                                         FILIERE=o$getFiliere(GRADE_ECH),
                                                         FILIERE_NP1=o$getFiliere(GRADE_ECH),
                                                         STATUT_CONTRAT_NP1=STATUT_CONTRAT,
                                                         DUREE_CONTRAT_NP1=DUREE_CONTRAT,
                                                         DEP_NP1=substr(DEPCOM_FONCTION,1,2),
                                                         S_BRUT_NP1=S_BRUT,
                                                         EQTP_NP1=EQTP)]
  )
} 

```

L'objectif de cette brique consiste à retrouver les profils suivants conjointement aux entrées et aux sorties:

```{r objectif, include=F,echo=F}


count <- rbindlist(list(dfEff2013[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2013"],
                        dfEff2014[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2014"],
                        dfEff2015[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2015"],
                        dfEff2016[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2016"],
                        dfEff2017[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2017"],
                        dfEff2018[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2018"],
                        dfEff2019[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2019"],
                        dfEff2020[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2020"],
                        dfEff2021[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2021"]))

count[STATUT_CONTRAT=="CONT" & DUREE_CONTRAT %in% c("AUTR","SANS"),DUREE_CONTRAT:="CDD"]
count[STATUT_CONTRAT=="TITH",DUREE_CONTRAT:= "SANS"]

count[,type:=ifelse(STATUT_CONTRAT=="TITH","TITH",DUREE_CONTRAT)]

count_plot <- count[,.N,by=c("ANNEE","type")]

ggplot(count_plot) +
  aes(x = ANNEE, y = N, colour = type) +
  geom_point(shape = "circle", size = 1.5) +
  scale_color_hue(direction = 1) +
  theme_minimal() +
  ylim(0, 801000)

count_plot_wide <- data.table::dcast(count_plot, ANNEE ~ type, value.var="N")[order(ANNEE)]

```


```{r chainage, include=F}

statuts_n=c("CONT")

statuts_np1=append(statuts_n,c("TITH","SORTANT")) 

#liste des variables conservées dans la base N+1
liste_N <- append(to_keep,c("nb_mois","DEP","FILIERE"))
liste_NP1 = c("ID_NIR20","STATUT_CONTRAT_NP1","DUREE_CONTRAT_NP1","DEP_NP1","S_BRUT_NP1","EQTP_NP1")

dfEff2012_y = merge(dfEff2012[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2013[,..liste_NP1],all.x=T)[,ANNEE:=2012]
dfEff2013_y = merge(dfEff2013[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2014[,..liste_NP1],all.x=T)[,ANNEE:=2013]
dfEff2014_y = merge(dfEff2014[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2015[,..liste_NP1],all.x=T)[,ANNEE:=2014]
dfEff2015_y = merge(dfEff2015[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2016[,..liste_NP1],all.x=T)[,ANNEE:=2015]
dfEff2016_y = merge(dfEff2016[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2017[,..liste_NP1],all.x=T)[,ANNEE:=2016]
dfEff2017_y = merge(dfEff2017[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2018[,..liste_NP1],all.x=T)[,ANNEE:=2017]
dfEff2018_y = merge(dfEff2018[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2019[,..liste_NP1],all.x=T)[,ANNEE:=2018]
dfEff2019_y = merge(dfEff2019[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2020[,..liste_NP1],all.x=T)[,ANNEE:=2019]
dfEff2020_y = merge(dfEff2020[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2021[,..liste_NP1],all.x=T)[,ANNEE:=2020]

rm(list=paste0("dfEff",years))

# nrow(dfEff2020[is.na(STATUT_CONTRAT)]) nul, pour chauqe année

dfEffAAAA_y=rbind(rbind(rbind(rbind(rbind(rbind(rbind(
  rbind(dfEff2012_y,
        dfEff2013_y),
        dfEff2014_y),
        dfEff2015_y),
        dfEff2016_y),
        dfEff2017_y),
        dfEff2018_y),
        dfEff2019_y),
        dfEff2020_y)
dfEffAAAA_y[,one:=1]
dfEffAAAA_y[,INDIV_ANNEE:=sum(one),by="ANNEE"]
dfEffAAAA_y[,MS_ANNEE:=sum(S_BRUT),by="ANNEE"]

dfEffAAAA_y[is.na(STATUT_CONTRAT_NP1),STATUT_CONTRAT_NP1:="SORTANT"]
dfEffAAAA_y[!(STATUT_CONTRAT_NP1 %in% statuts_np1),STATUT_CONTRAT_NP1:="AUTRE"]

#Il y a peu de CONT AUTR, on les supprime
#nrow(dfEffAAAA_y[DUREE_CONTRAT=="AUTR"]) #4
#nrow(dfEffAAAA_y[(STATUT_CONTRAT_NP1=="CONT" & DUREE_CONTRAT_NP1=="AUTR")]) #5
dfEffAAAA_y <- data.table::copy(dfEffAAAA_y[DUREE_CONTRAT!="AUTR"])
dfEffAAAA_y <- data.table::copy(dfEffAAAA_y[!(STATUT_CONTRAT_NP1=="CONT" & DUREE_CONTRAT_NP1=="AUTR")])

dfEffAAAA_y[DUREE_CONTRAT=="SANS",DUREE_CONTRAT:="CDD"]
dfEffAAAA_y[DUREE_CONTRAT_NP1=="SANS",DUREE_CONTRAT_NP1:="CDD"]

dfEffAAAA_y[,CONTRAT_NP1:=ifelse(STATUT_CONTRAT_NP1 %in% c("SORTANT","TITH","AUTRE"),STATUT_CONTRAT_NP1,
                                 DUREE_CONTRAT_NP1)]

dfEffAAAA_y[,`:=`(GRADE_ECH=as.factor(GRADE_ECH),SEXE=as.factor(SEXE),DEP=as.factor(DEP))]
dfEffAAAA_y[,`:=`(ANNEE=as.character(ANNEE))]

dfEffAAAA_y[,tr_age:=fcase(AGE<=20,"-20",
                           AGE>20 & AGE<=30,"20-30",
                           AGE>30 & AGE<=40,"30-40",
                           AGE>40 & AGE<=50,"40-50",
                           AGE>50 & AGE<=60,"50-60",
                           AGE>60,"60-")]

```

## Introduction : poids des grades

A partir de la base de 2018, voici les grades les plus representés parmi les contractuels, en termes de masse salariale:

```{r rpz_grade,echo=F,warning=F}

RPZ_GRADE <- dfEffAAAA_y[ANNEE=="2018",.(nb=sum(one),MS=sum(S_BRUT)),by=c("ANNEE","GRADE_ECH","INDIV_ANNEE","MS_ANNEE")]

RPZ_GRADE[,`:=`(libelle=o$getInfoGrades(GRADE_ECH)$LIBELLE,
                `part des effectifs`=round(100*nb/INDIV_ANNEE,1),
                `part de la masse salariale`=round(100*MS/MS_ANNEE,1))]
setorder(RPZ_GRADE,-`part de la masse salariale`)

ft_RPZ_GRADE=flextable(head(RPZ_GRADE[,-c("ANNEE","nb","MS","INDIV_ANNEE","MS_ANNEE")],20)) %>%
  width(width=1.7)
ft_RPZ_GRADE
#autofit(ft_RPZ_GRADE)

```

Les grades changent d'années en années, parfois d'une manière non bijective, mais cela concerne la brique des promotions d'un grade à l'autre. 

## Taux de transition

Comme justifié en annexe, les contrats "SANS" durée ont été considérés comme des CDD.

```{r Stats, echo=FALSE}

stats=dfEffAAAA_y[,.(nb=sum(one),MS=sum(S_BRUT)),by=c("ANNEE","DUREE_CONTRAT","CONTRAT_NP1","INDIV_ANNEE","MS_ANNEE")]
stats[,`:=`(pc_indiv=round(100*nb/INDIV_ANNEE,2),
            pc_MS=round(100*MS/MS_ANNEE,2))]
setorder(stats,ANNEE,DUREE_CONTRAT,CONTRAT_NP1)

stats[,`:=`(cas=paste0(DUREE_CONTRAT,"_",CONTRAT_NP1))]
stats[,`:=`(Effectifs=sum(nb)),by=c("ANNEE","DUREE_CONTRAT")]
stats[,`:=`(pc_NP1=round(100*nb/Effectifs,2))]

#stats

# esquisse::esquisser()

```


```{r matrice_share, echo=F,fig.width=6,fig.height=8}

#share_statut<- 
ggplot(stats) +
  aes(x = cas, weight = pc_NP1) +
  geom_bar(fill = "#112446") +
  theme_minimal() + theme(axis.text.x = element_text(angle = 40,vjust = 1, hjust=1)) +
  labs(x= "N/N+1", y = "Pourcentage", title = "Part des statuts de contrats, année N/N+1") +
  facet_wrap(vars(ANNEE), ncol = 1L)

```

Voici les chiffres précis (les effectifs sont en milliers):

```{r matrice_share_table, echo=F}

stats_wide=data.table::dcast(stats, ANNEE + DUREE_CONTRAT + Effectifs ~ CONTRAT_NP1, value.var="pc_NP1")[order(DUREE_CONTRAT,ANNEE)]

stats_wide %>% flextable() %>% width(width=.8)

```

## Transitions par grade

Les transitions observées auparavant peuvent être déclinées par grade. 

Par exemple, pour le grade 3021 des Aide-soignant.e.s :

```{r stats_GRADE_ECH, echo=FALSE}

stats_GRADE_ECH=dfEffAAAA_y[,.(nb=sum(one),MS=sum(S_BRUT)),
                            by=c("ANNEE","DUREE_CONTRAT","CONTRAT_NP1","INDIV_ANNEE","MS_ANNEE","GRADE_ECH")]

stats_GRADE_ECH[,pc_indiv:=round(100*nb/INDIV_ANNEE,2)]
stats_GRADE_ECH[,pc_MS:=round(100*nb/MS_ANNEE,2)]

#setorder(stats,ANNEE,DUREE_CONTRAT,CONTRAT_NP1)

stats_GRADE_ECH[,`:=`(cas=paste0(DUREE_CONTRAT,"_",CONTRAT_NP1,"_",GRADE_ECH))]
stats_GRADE_ECH[,`:=`(Effectifs=sum(nb),MS=sum(MS)),by=c("ANNEE","DUREE_CONTRAT","GRADE_ECH")]
stats_GRADE_ECH[,`:=`(pc_NP1=round(100*nb/Effectifs,2))]


stats_GRADE_ECH_wide=data.table::dcast(stats_GRADE_ECH, ANNEE + DUREE_CONTRAT+ GRADE_ECH + Effectifs + MS ~ CONTRAT_NP1, value.var="pc_NP1")[order(DUREE_CONTRAT,GRADE_ECH,ANNEE)]

stats_GRADE_ECH_wide[GRADE_ECH=="3021"] %>% flextable() %>% width(width=.6)

```


## Ancienneté FPH

Après avoir étudié l'âge dans un premier temps (Annexe), l'ancienneté FPH a semblé plus adéquate. On distingue en effet des probabilités accrues à certaines échéances:

```{r etude_ancFPH_CDI, echo=FALSE,warning=F}

CDI<-copy(dfEffAAAA_y[DUREE_CONTRAT=="CDD" & CONTRAT_NP1=="CDI"])
CDI[,ANNEE_CDISATION:=paste0(as.character(ANNEE)," - ",as.character(as.numeric(ANNEE)+1))]

CDI %>%
 ggplot() +
  aes(x = ANC_FPH/365) +
  geom_histogram(bins = 100L, fill = "#112446") +
  labs(title = "CDIsations annuelles selon la variable ANC_FPH") +
  xlab("Ancienneté en année") + ylab("nombre") +
  theme_minimal() +
  theme(plot.title = element_text(size = 16L, face = "bold")) +
  facet_wrap(vars(ANNEE_CDISATION),scales = "free_y")

```

```{r etude_ancFPH_TITH, echo=FALSE}

TITH<-copy(dfEffAAAA_y[CONTRAT_NP1=="TITH"])
TITH[,ANNEE_TITH:=paste0(as.character(ANNEE)," - ",as.character(as.numeric(ANNEE)+1))]

TITH %>%
 ggplot() +
  aes(x = ANC_FPH/365) +
  geom_histogram(bins = 100L, fill = "#112446") +
  labs(title = "Titularisations annuelles selon la variable ANC_FPH") +
  xlab("Ancienneté en année") + ylab("nombre") +
  theme_minimal() +
  theme(plot.title = element_text(size = 16L, face = "bold")) +
  facet_wrap(vars(ANNEE_TITH),scales = "free_y")

```

L'ancienneté FPH décompte le nombre de jours de présence depuis le premier janvier 2009. Elle provient de la somme de la variable DUREE_POSTE dans la base originelle, soit le nombre de jours effectifs d'emploi de l’année, compris entre 0 et 365.
On ne comptabilise que les jours d’activité en excluant de la somme les périodes inactives du poste. Ainsi, tous les jours compris entre la date de début et la date de fin (comprises) d’une des périodes actives du poste sont comptabilisés, une et une seule fois. 

On distingue des vagues de cdisations/titularisations après des périodes bien précises de contrats. Cette variable, bien qu'imparfaite dans sa construction, permet la prise en compte des seuils légaux où la hiérarchie est amenée à étudier l'opportunité d'une transition, et approxime l'expérience acquise.

Voici la répartition de cette variable par année:

```{r etude_ancFPH_ALL, echo=FALSE}

dfEffAAAA_y %>%
  ggplot() +
  aes(x = ANC_FPH/365) +
  geom_histogram(bins = 100L, fill = "#112446") +
  labs(title = " ") +
  xlab("Ancienneté (en année)") + ylab("Effectifs") +
  theme_minimal() +
  theme(plot.title = element_text(size = 16L, face = "bold")) +
  facet_wrap(vars(ANNEE),scales = "free_y")


```

La variable est donc censurée: pour les individus présents avant le 1er janvier 2009 inclus, l'ancienneté est arbitrairement fixée au nombre d'année depuis 2009 (=ANNEE - 2009). Avant 2016, nous excluons ces individus des probabilités de CDIsation/titularisation pour ne pas les fausser. Après 2016, leur ancienneté est bien supérieure à 6,25 ans, dernier seuil considéré.

Nous discretisons en effet la durée d'ancienneté autour des "seuils" probables de durée de CDD. Voici le tableau de transition agrégé obtenu, montrant notamment les périodes de temps choisies ensemble.

```{r anc_FPH_conversion, echo=F}

#check=reshape(data.table(table(dfEffAAAA_y$ANC_FPH,dfEffAAAA_y$ANNEE)),idvar="V1",timevar = "V2",direction="wide")

etude_ANC=copy(dfEffAAAA_y)
etude_ANC[,max_ANC_FPH:=max(ANC_FPH),by="ANNEE"]
# table(etude_ANC$ANNEE,etude_ANC$max_ANC_FPH)
etude_ANC=etude_ANC[ANC_FPH<max_ANC_FPH | ANNEE %in% as.character(2016:2021)] # exclusion des personnes présentes depuis plus longtemps que 2009, sauf après 2016 ("plus de 6,25 ans" réel)

etude_ANC[,ANC_FPH_AN:=ANC_FPH/365] #conversion en année

etude_ANC[,ANC_FPH_CASE:=fcase(ANC_FPH_AN<=.75,"-.75",
                               ANC_FPH_AN>.75 & ANC_FPH_AN<=1.25,".75 - 1.25",
                               ANC_FPH_AN>1.25 & ANC_FPH_AN<=1.75,"1.25 - 1.75",
                               ANC_FPH_AN>1.75 & ANC_FPH_AN<=2.25,"1.75 - 2.25",
                               ANC_FPH_AN>2.25 & ANC_FPH_AN<=2.75,"2.25 - 2.75",
                               ANC_FPH_AN>2.75 & ANC_FPH_AN<=3.25,"2.75 - 3.25",
                               ANC_FPH_AN>3.25 & ANC_FPH_AN<=4.25,"3.25 - 4.25",
                               ANC_FPH_AN>4.25 & ANC_FPH_AN<=5.25,"4.25 - 5.25",
                               ANC_FPH_AN>5.25 & ANC_FPH_AN<=6.25,"5.25 - 6,25",
                               ANC_FPH_AN>6.25,"6.25-")]

# etude_ANC[,INDIV_ANC:=sum(one),by="ANC_FPH_CASE"]
# etude_ANC[,MS_ANC:=sum(S_BRUT),by="ANC_FPH_CASE"]

```

```{r stats_ANC,echo=F}

stats_ANC=etude_ANC[,.(nb=sum(one),MS=sum(S_BRUT)),by=c("DUREE_CONTRAT","CONTRAT_NP1","ANC_FPH_CASE")]#,"INDIV_ANC","MS_ANC"
setorder(stats_ANC,ANC_FPH_CASE,DUREE_CONTRAT,CONTRAT_NP1)

stats_ANC[,`:=`(cas=paste0(DUREE_CONTRAT,"_",CONTRAT_NP1))]
stats_ANC[,`:=`(Effectifs=sum(nb)),by=c("ANC_FPH_CASE","DUREE_CONTRAT")]
stats_ANC[,`:=`(pc_NP1=round(100*nb/Effectifs,2))]


```

```{r stats_ANC2,echo=F}

stats_ANC_wide=data.table::dcast(stats_ANC, DUREE_CONTRAT + ANC_FPH_CASE + Effectifs ~ CONTRAT_NP1, value.var="pc_NP1")[order(DUREE_CONTRAT,ANC_FPH_CASE)]

stats_ANC_wide %>% flextable() %>% width(width=.8)

# parmi les 5,75-6,25, il ya de tout:
# boxplot(etude_ANC[ANC_FPH_CASE=="5.75 - 6,25" & DUREE_CONTRAT=="CDD" & CONTRAT_NP1=="CDI"]$ANC_FPH_AN)


``` 

Le but est d'utiliser ces chiffres déclinés pour chaque grade. Par exemple, pour les aide-soignant.e.s:

```{r stats_ANC_grade,echo=F}

stats_ANC_GRADE=etude_ANC[,.(nb=sum(one),MS=sum(S_BRUT)),by=c("DUREE_CONTRAT","CONTRAT_NP1","GRADE_ECH","ANC_FPH_CASE")]
setorder(stats_ANC_GRADE,DUREE_CONTRAT,CONTRAT_NP1,GRADE_ECH,ANC_FPH_CASE)

stats_ANC_GRADE[,`:=`(cas=paste0(DUREE_CONTRAT,"_",CONTRAT_NP1))]
stats_ANC_GRADE[,`:=`(Effectifs=sum(nb)),by=c("ANC_FPH_CASE","DUREE_CONTRAT","GRADE_ECH")]
stats_ANC_GRADE[,`:=`(pc_NP1=round(100*nb/Effectifs,2))]

stats_ANC_GRADE_wide=data.table::dcast(stats_ANC_GRADE, GRADE_ECH + DUREE_CONTRAT + ANC_FPH_CASE + Effectifs ~ CONTRAT_NP1, value.var="pc_NP1")[order(GRADE_ECH,DUREE_CONTRAT,ANC_FPH_CASE)]

stats_ANC_GRADE_wide[GRADE_ECH=="3021"] %>% flextable() %>% width(width=.8)

# parmi les 5,75-6,25, il ya de tout:
# boxplot(etude_ANC[ANC_FPH_CASE=="5.75 - 6,25" & DUREE_CONTRAT=="CDD" & CONTRAT_NP1=="CDI"]$ANC_FPH_AN)


``` 

Cependant, ces chiffres ne tiennent pas compte d'une possible évolution dans le temps sur la période 2012-2020 où ils sont calculés. Peut-être dissimulent-ils des tendances ?

Regardons les variations de chaque catégorie d'ancienneté dans le temps, pour les grades les plus nombreux : "3021","2154","3259","1314","4200","6012".

```{r stats_ANC_grade2,echo=F}

big_grades <- c("3021","2154","3259","1314","4200","6012")

stats_ANC_GRADE_EVOL=etude_ANC[GRADE_ECH %in% big_grades,
                               .(nb=sum(one),MS=sum(S_BRUT)),
                               by=c("ANNEE","DUREE_CONTRAT","CONTRAT_NP1","GRADE_ECH","ANC_FPH_CASE")]

setorder(stats_ANC_GRADE_EVOL,DUREE_CONTRAT,CONTRAT_NP1,GRADE_ECH,ANC_FPH_CASE,ANNEE)

stats_ANC_GRADE_EVOL[,`:=`(cas=paste0(DUREE_CONTRAT,"_",CONTRAT_NP1))]
stats_ANC_GRADE_EVOL[,`:=`(Effectifs=sum(nb)),by=c("ANC_FPH_CASE","DUREE_CONTRAT","GRADE_ECH","ANNEE")]
stats_ANC_GRADE_EVOL[,`:=`(pc_NP1=round(100*nb/Effectifs,2))]

stats_ANC_GRADE_EVOL<- stats_ANC_GRADE_EVOL[CONTRAT_NP1 %in% c("CDD","CDI","TITH","SORTANT") & DUREE_CONTRAT %in% c("CDD","CDI")]
stats_ANC_GRADE_EVOL[,ANNEE:=as.numeric(ANNEE)]

for(grade in big_grades){
  assign(paste0("big_grade_plot_",grade),
         stats_ANC_GRADE_EVOL %>%
           filter(GRADE_ECH %in% grade) %>%
           ggplot() +
           aes(x = ANNEE, y = pc_NP1, colour = ANC_FPH_CASE) +
           geom_step(size = 1L) +
           scale_color_viridis_d(option = "inferno", direction = 1) +
           labs(
             x = "année",
             y = "probabilité",
             title = ifelse(grade=="3021","Probabilité de transition à un contrat par an et selon l'ancienneté",""),
             subtitle = paste0("grade ",grade," - ",o$getInfoGrades(grade)$LIBELLE),
             color = "ancienneté en année"
           ) +
           theme_minimal() +
           theme(
             legend.position = "right",
             plot.title = element_text(
               size = 16L,
               face = "bold"
             ),
             plot.subtitle = element_text(
               size = 14L,
               face = "bold"
             )
           ) +
           facet_wrap(vars(cas))
  )
  
}

``` 

```{r stats_ANC_grade_plot,echo=F, fig.height=5, fig.width=9}

big_grade_plot_3021
big_grade_plot_2154
big_grade_plot_3259
big_grade_plot_1314
big_grade_plot_4200
big_grade_plot_6012


``` 

La progression des CDD-CDI des 2154 pose question mais en général, il ne semble pas y avoir de tendances importantes à modéliser sur ces grades.

En outre, il est prévu d'ajouter une variable à étudier dans les bases : l'ancienneté *dans l'établissement*.

### Utilisation

L'inclusion de ces chiffres dans le modèle permettra de tenir compte des titularisations et cdisations. Ils seront recalculés sans l'alternative de sortie et "AUTRE", gérées en amont. 

Gilles: Utilisation = Exemple d’utilisation du modèle dans SMASH. L’étude amène à la création d’un modèle (stocké dans un fichier RDS). L’utilisation c’est une illustration de l’utilisation du code SMASH pur appliquer le modèle : comment on utilise les taux appris, dans le fichier pour modifier les statut/durée contrant dans les données. On peut utiliser la fonction du package ou juste montrer comment s’applique les taux du modèle RDS aux données (par grade, tranche d’âge, ancienneté, … )

Partie à compléter en parallèle de l'inclusion dans le package donc.

Ajouter la méthode de validation de la brique dans le package.

## Annexes 

### Annexe 1 : âge 

Avant de s'appuyer sur l'ancienneté, nous avions dans un premier temps étudié la dimension de l'âge.
Certains modèles déjà implémentés se basent sur les ages précis, mais manquent de robustesse pour certains grades peu nombreux. 

En utilisant des tranches d'âge sur la transition 2018-2019, pour le grade 3259, voici ce qu'on obtient:

```{r stats_GRADE_ECH_age, echo=FALSE}

stats_GRADE_ECH_age=dfEffAAAA_y[ANNEE=="2018",.(nb=sum(one),MS=sum(S_BRUT)),
                            by=c("DUREE_CONTRAT","CONTRAT_NP1","GRADE_ECH","tr_age")]

# stats_GRADE_ECH_age[,pc_indiv:=round(100*nb/INDIV_ANNEE,2)]
# stats_GRADE_ECH_age[,pc_MS:=round(100*nb/MS_ANNEE,2)]

#setorder(stats,ANNEE,DUREE_CONTRAT,CONTRAT_NP1)

stats_GRADE_ECH_age[,`:=`(cas=paste0(DUREE_CONTRAT,"_",CONTRAT_NP1,"_",GRADE_ECH,"_",tr_age))]

stats_GRADE_ECH_age[,`:=`(Effectifs=sum(nb)),by=c("DUREE_CONTRAT","GRADE_ECH","tr_age")]
stats_GRADE_ECH_age[,`:=`(pc_NP1=round(100*nb/Effectifs,2))]


stats_GRADE_ECH_age_wide=data.table::dcast(stats_GRADE_ECH_age,DUREE_CONTRAT+ GRADE_ECH + tr_age + Effectifs ~ CONTRAT_NP1, value.var="pc_NP1")[order(DUREE_CONTRAT,GRADE_ECH,tr_age)]

all_combi=expand.grid(DUREE_CONTRAT=unique(stats_GRADE_ECH_age_wide$DUREE_CONTRAT),
                      GRADE_ECH=unique(stats_GRADE_ECH_age_wide$GRADE_ECH),
                      tr_age=unique(stats_GRADE_ECH_age_wide$tr_age))

stats_GRADE_ECH_age_wide=data.table(merge(all_combi,stats_GRADE_ECH_age_wide,all.x=T))
stats_GRADE_ECH_age_wide[is.na(Effectifs),Effectifs:=0]

stats_GRADE_ECH_age_wide[GRADE_ECH=="3259"] %>% flextable() %>% width(width=.8)

```

Cette piste a été écartée à la faveur de l'ancienneté dans la FPH, approximant l'expérience acquise par l'individu et tenant compte de la durée habituelle des CDD.

### Annexe 2: cas particuliers

#### "AUTRES" types de contrat de destination

Il convient de vérifier que les fitres de contrats de destination étudiés sont pertinents. Autrement dit, que nous ne passons pas à côté de parcours importants. Les contrats "SANS" sont ici ditingués.

```{r AUTRES,echo=FALSE}

dfEffAAAA_AUTRE=rbind(rbind(rbind(rbind(rbind(rbind(rbind(rbind(dfEff2012_y,dfEff2013_y),dfEff2014_y),dfEff2015_y),dfEff2016_y),dfEff2017_y),dfEff2018_y),dfEff2019_y),dfEff2020_y)
dfEffAAAA_AUTRE[,one:=1]
dfEffAAAA_AUTRE[,INDIV_ANNEE:=sum(one),by="ANNEE"]
dfEffAAAA_AUTRE[,MS_ANNEE:=sum(S_BRUT),by="ANNEE"]

dfEffAAAA_AUTRE[is.na(STATUT_CONTRAT_NP1),STATUT_CONTRAT_NP1:="SORTANT"]


```

```{r Stats_AUTRE, echo=FALSE}

stats_AUTRE=dfEffAAAA_AUTRE[,.(nb=sum(one),MS=sum(S_BRUT)),by=c("ANNEE","DUREE_CONTRAT","STATUT_CONTRAT_NP1","INDIV_ANNEE","MS_ANNEE")]
stats_AUTRE[,`:=`(pc_indiv=round(100*nb/INDIV_ANNEE,2),
            pc_MS=round(100*MS/MS_ANNEE,2))]
setorder(stats_AUTRE,ANNEE,DUREE_CONTRAT,STATUT_CONTRAT_NP1)

stats_AUTRE[,ANNEE:=as.character(ANNEE)]

stats_AUTRE[,`:=`(cas=paste0(DUREE_CONTRAT,"_",STATUT_CONTRAT_NP1))]
stats_AUTRE[,`:=`(Effectifs=sum(nb)),by=c("ANNEE","DUREE_CONTRAT")]
stats_AUTRE[,`:=`(pc_NP1=round(100*nb/Effectifs,2))]

stats_AUTRE<-stats_AUTRE[!(STATUT_CONTRAT_NP1 %in% c("CIRM","SCIV","STAT","TITE","TITT","CA","CAE","APPR","ASMA"))]

stats_AUTRE_wide=data.table::dcast(stats_AUTRE, ANNEE + DUREE_CONTRAT+ Effectifs ~ STATUT_CONTRAT_NP1, value.var="pc_NP1")[order(DUREE_CONTRAT,ANNEE)]


stats_AUTRE_wide  %>% flextable %>% width(width=.55)

# esquisse::esquisser()

```

Pour des raisons de lisibilité, les CIRM, SCIV, STAT, TITE, TITT, CA, CAE, APPR et ASMA ont été exclus des colonnes mais les calculs en tiennent compte.

On apprend que les CONT AUTR sont très peu nombreux (en ligne); ce qui justifie leur exclusion.

Les colonnes ACTE, AUTR, CAID, ELEV et INTE ne semblent pas conséquentes. Il convient probablement de ne pas tenir compte de ces parcours dans les probabilités à extraire. 

Les probabilités attenantes aux contrats SANS semblent plus variables dans le temps.

Par exemple, en 2018, `r round(stats_AUTRE_wide[ANNEE=="2018" & DUREE_CONTRAT=="SANS","MEDI"],2)` % des `r as.numeric(stats_AUTRE_wide[ANNEE=="2018" & DUREE_CONTRAT=="SANS","Effectifs"])` CONT SANS sont passés en MEDI, soit `r stats_AUTRE_wide[ANNEE=="2018" & DUREE_CONTRAT=="SANS","Effectifs"]*stats_AUTRE_wide[ANNEE=="2018" & DUREE_CONTRAT=="SANS","MEDI"]/100` personnes. Mais cela semble être une exception: ce phénomène ne se produit pas les autres années où une centaine de SANS deviennent MEDI environ. 

#### Contrats "sans" durée


```{r SANS_CDD,echo=FALSE}

hop2019_agg=read.fst(file.path(chemin, paste0("hop2019_agrege.fst")), #selection_
         as.data.table=T)[,`:=`(nb_mois=12*(100*EQTP/QUOTITE),
                                DEP=substr(DEPCOM_FONCTION,1,2),
                                FILIERE=o$getFiliere(GRADE_ECH),
                                one=1)]

stats<-hop2019_agg[STATUT_CONTRAT=="CONT" & DUREE_CONTRAT %in% c("CDD","SANS"),
                   .(nb=sum(one),
                    EQTP=mean(EQTP,na.rm=T),
                    QUOTITE=mean(QUOTITE,na.rm=T),
                    S_BRUT=round(mean(S_BRUT,na.rm=T),0),
                    nb_mois=mean(nb_mois,na.rm=T),
                    NB_POSTES=mean(NB_POSTES,na.rm=T),
                    ANC_FPH=round(mean(ANC_FPH,na.rm=T)/365,1)),by=c("GRADE_ECH","DUREE_CONTRAT")]

stats_wide <- data.table::dcast(stats,GRADE_ECH~DUREE_CONTRAT, value.var = c("nb","EQTP","QUOTITE","S_BRUT","nb_mois","NB_POSTES","ANC_FPH"))
stats_wide[is.na(nb_CDD),nb_CDD:=0]
stats_wide[is.na(nb_SANS),nb_SANS:=0]
setorder(stats_wide,-nb_CDD)
stats_wide[,pc_SANS:=round(100*nb_SANS/(nb_SANS+nb_CDD),1)]
stats_wide[,nb_GRADE:=nb_SANS+nb_CDD]

stats_wide[,libelle:=o$getInfoGrades(GRADE_ECH)$LIBELLE]



```

Ces contractuels ont été assimilés à des CDD dans nos calculs. 
De manière générale, les contrats SANS représentent `r paste0(as.character(round(100*stats_wide[,sum(nb_SANS)]/(stats_wide[,sum(nb_SANS)]+stats_wide[,sum(nb_CDD)]),1)),
       " %")` des contractuels non CDI.
       
Ils sont très souvent minoritaires, comme le montre leur pourcentage en ce qui concerne les 10 grades importants suivants:

```{r SANS_CDD_top,echo=FALSE}

setorder(stats_wide,-nb_GRADE)
stats_wide[1:10,
           c("GRADE_ECH","libelle","nb_GRADE","pc_SANS","S_BRUT_CDD","S_BRUT_SANS","ANC_FPH_CDD","ANC_FPH_SANS")] %>% flextable() %>% width(width=.8)


```

Les taux de cdisation/titularisation observés sont parfois inférieurs à ceux des CDD mais calculés sur de moins nombreux individus. Il arrive aussi qu'ils soient très majoritaires. Le cas problématique correspond en fait aux grades où la part des "SANS" est comparable.

Dans ce cas, les caractéristiques des contrats "SANS" semblent tout de même ressembler à celle des CDD: 

```{r SANS_CDD_rare,echo=FALSE}


stats_wide[pc_SANS>20 & pc_SANS<80 & nb_GRADE>40,
           c("GRADE_ECH","libelle","nb_GRADE","pc_SANS","S_BRUT_CDD","S_BRUT_SANS","ANC_FPH_CDD","ANC_FPH_SANS")] %>% flextable() %>% width(width=.8)


```

Cette modalité peut aussi être le fait du remplissage d'un établissement. Il convient de faire la même adaptation dans le package.

#### Annexe 3: Passage des CDIs au CDD

Cas non intuitifs. Comment les expliquer?

Cette partie est très détaillée pour comprendre le phénomène et le décrire mais les conclusions ne sont pas prises en compte dans cette brique dans le cadre de la V1.

En revanche, on laisse l'alternative que des CDIs passent en CDD dans le calcul des taux empiriques pour que le nombre de CDI et CDD soit estimé au mieux.

```{r etude_CDI_CDD, echo=FALSE}

CDI_CDD=dfEffAAAA_y[DUREE_CONTRAT=="CDI" & CONTRAT_NP1=="CDD"] #8274 cas
CDI_CDD[,new_dep:=(DEP==DEP_NP1)]
CDI_CDD[!is.na(CONTRAT_NP1),S_BRUT_diff:=S_BRUT_NP1-S_BRUT]
CDI_CDD[!is.na(CONTRAT_NP1),S_BRUT_EQTP_diff:=S_BRUT_NP1/EQTP_NP1-S_BRUT/EQTP]
CDI_CDD[!is.na(CONTRAT_NP1),S_BRUT_diff_pos:=as.integer((S_BRUT_diff>=0))]
CDI_CDD[!is.na(CONTRAT_NP1) & S_BRUT>0,S_BRUT_diff_pc:=100*(S_BRUT_NP1-S_BRUT)/S_BRUT]

# mean(CDI_CDD$S_BRUT_diff_pc,na.rm=T)
# hist(CDI_CDD$S_BRUT_diff_pc,breaks=50)
# plot(density(CDI_CDD[new_dep==0& S_BRUT_diff_pc<500]$S_BRUT_diff_pc,na.rm=T))

```

Dans 75 % des cas, le département de fonction a changé.

```{r CDI_CDD_dep, eval=FALSE,echo=F}

table(CDI_CDD$new_dep,useNA = "ifany")

```

On suppose donc que c'est la raison pour laquelle leur contrat a changé. Nous excluons ces individus de l'échantillon de CDI passés CDD étudiés pour les calculs suivants.

L'étude des déméngaments n'est pas prévue dans la V1 (pas mal d’implications si on ajoute la géographie dans tous les modèles, notamment les entrants/sortants par géographie/établissement).

On remarque que la distribution des filières diffère du reste de la base, que les individus sont plus jeunes et que le phénomène concerne majoritairement certains grades.

```{r CDI_CDD_filières, echo=FALSE}

tot=nrow(dfEffAAAA_y)
tot_CDI_CDD_same_dep=CDI_CDD[new_dep==F,.N]

merge(dfEffAAAA_y[,.(`Pourcentage dans la base`=round(100*sum(one)/tot,1)),by="FILIERE"],CDI_CDD[new_dep==F][,.(`Pourcentage parmi les CDI passés CDD sans changer de département`=100*sum(one)/tot_CDI_CDD_same_dep),by="FILIERE"])%>%
  flextable() %>% width(width=1.8)

```

```{r CDI_CDD_ages,include=T, fig.height=4,fig.width=7}
#cat("CDI passés en CDD")
hist(CDI_CDD[new_dep==F]$AGE, main="Répartition de l'âge parmi les CDI->CDD sans changement de département",
     xlab = "Age",ylab = "Fréquence")
#cat("2015-2020")
hist(dfEffAAAA_y$AGE,main="Répartition de l'âge dans l'ensemble de la base",
     xlab = "Age",ylab = "Fréquence")

# boxplot(CDI_CDD[new_dep==F]$AGE)
# boxplot(dfEffAAAA_y$AGE)


```

Le salaire des CDI passés CDD dans le même département n'a pas forcément augmenté. Regardons l'évolution du salaire en pourcentage:

```{r CDI_CDD_S_BRUT, warning=FALSE, eval=FALSE, include=F}

table(CDI_CDD[new_dep==F]$S_BRUT_diff_pos)

```

```{r CDI_CDD_S_BRUT_plot, warning=FALSE, echo=FALSE}
CDI_CDD[new_dep==F] %>%
 filter(S_BRUT_diff_pc >= -250L & S_BRUT_diff_pc <= 250L ) %>% #| is.na(S_BRUT_diff_pc)
 ggplot() +
  aes(x = S_BRUT_diff_pc) + geom_density(adjust = 1L, fill = "#112446") + theme_minimal()



```

L'ampleur de certaines baisses de salaire est peut-être liée à un nouveau mi-temps. Regardons la différence de salaire brut par EQTP:

```{r CDI_CDD_S_BRUT_EQTP_plot, warning=FALSE, echo=FALSE}

CDI_CDD %>%
 filter(S_BRUT_EQTP_diff >= -1500L & S_BRUT_EQTP_diff <= 1500L &!is.na(S_BRUT_EQTP_diff)) %>%  
 ggplot() +
  aes(x = S_BRUT_EQTP_diff) + geom_density(adjust = 1L, fill = "#112446") + theme_minimal()

```

En termes de genre, les femmes sont aussi représentée dans l'échantillon que dans le reste de la base. 

Voici les grades les plus concernés par un passage de CDI à CDD dans un même département, avec leur part et la part dans la base originale.

```{r CDI_CDD_GRADE_ECH, warning=F,echo=FALSE}

fusion_CDI_CDD_GRADE=merge(dfEffAAAA_y[,.(`Pourcentage dans la base`=round(100*sum(one)/tot,1)),by="GRADE_ECH"],
      CDI_CDD[new_dep==F][,.(`Pourcentage parmi les CDI passés CDD sans changer de département`=round(100*sum(one)/tot_CDI_CDD_same_dep,1)),by="GRADE_ECH"])

head(fusion_CDI_CDD_GRADE[order(-`Pourcentage parmi les CDI passés CDD sans changer de département`)][,libelle:=o$getInfoGrades(GRADE_ECH)$LIBELLE],10) %>%
  flextable() %>% width(width=1.8)

```

### Idées modèles de prédiction 

à voir.

```{r arbre_CDD, echo=FALSE,eval=F}

#https://thinkr.fr/premiers-pas-en-machine-learning-avec-r-volume-4-random-forest/
#https://thinkr.fr/premiers-pas-en-machine-learning-avec-r-volume-2-la-classification/

set.seed(2811)
train <- sample_frac(dfEffAAAA_y[DUREE_CONTRAT=="CDD"],0.7)
test <- anti_join(dfEffAAAA_y[DUREE_CONTRAT=="CDD"], train)


tree <- rpart(CONTRAT_NP1 ~ nb_mois+AGE+SEXE, data = train, method = "class") #GRADE_ECH+DEP QUOTITE+
#regarder l'aide de rpart
test$prediction <- predict(tree, test, type = "class")
conf <- confusionMatrix(data = test$prediction, reference = test$CONTRAT_NP1)

```



