---
title: "Analyses des MEDI"
author: "BPS DREES - Maud Galametz"
date: "21/02/2022"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r librairies}
library(dplyr)
# library(ggExtra)
library(ggplot2)
library(cowplot)
library(viridis)
library(fBasics)

## Chargement des données 
Sys.setenv(R_CONFIG_ACTIVE = Sys.getenv('username'))
config <- config::get()
chemin <- config$path_siasp

# Chemin local vers le Package Smash
devtools::load_all(config$path_smash)

```

## Présentation 

Cette fonction vise à analyser les profils des agents flaggués MEDI dans les SIASP.


## Chargement des données SIASP

```{r chargement, include=FALSE}

dfEff2019 <- chargerFST("hop", 2019, chemin = chemin, verbose = 0)
dfEff2018 <- chargerFST("hop", 2018, chemin = chemin, verbose = 0)
dfEff2017 <- chargerFST("hop", 2017, chemin = chemin, verbose = 0)
dfEff2016 <- chargerFST("hop", 2016, chemin = chemin, verbose = 0)
dfEff2015 <- chargerFST("hop", 2015, chemin = chemin, verbose = 0)

```


### Variables d'interet

Pour cette étude, nous ne conserverons que certaines variables 

```{r calcul_taux}
l_cols_sel <- c('ID_NIR20', 'SEXE', 'AGE', 'CORPS', 'GRADE_ECH', 'QUOTITE', 
                'STATUT_CONTRAT', 'S_BRUT', 'PR_TOT_CAL', 'EQTP')
dfEff2015_cut <- dfEff2015[, l_cols_sel]
dfEff2016_cut <- dfEff2016[, l_cols_sel]
dfEff2017_cut <- dfEff2017[, l_cols_sel]
dfEff2018_cut <- dfEff2018[, l_cols_sel]
dfEff2019_cut <- dfEff2019[, l_cols_sel]

dfEff2015_cut$TIME <- 2015
dfEff2016_cut$TIME <- 2016
dfEff2017_cut$TIME <- 2017
dfEff2018_cut$TIME <- 2018
dfEff2019_cut$TIME <- 2019

DataAll <- rbind(dfEff2015_cut, dfEff2016_cut, dfEff2017_cut, dfEff2018_cut, dfEff2019_cut)
DataAll <- DataAll[!duplicated(DataAll),]  %>% rename(GRADE = GRADE_ECH)

rm(dfEff2015_cut, dfEff2016_cut, dfEff2017_cut, dfEff2018_cut, dfEff2019_cut,
   dfEff2015, dfEff2016, dfEff2017, dfEff2018, dfEff2019)

```



### Quelques visualisations des salaires et de leur évolution

Dans le but de caractériser les parcours et salaires des agents MEDI, 
on commence par faire quelques visualisations.

Sur les données agrégées, on plot les salaires moyens par sexe, grade et age. 
On ramène les salaires bruts à un salaire en équivalent temps plein pour la 
comparaison.


```{r fig.asp = 0.8, fig.width = 10 }

p1 <- ggplot(DataAll, aes(SEXE, S_BRUT/EQTP)) + geom_boxplot() 
p2 <- ggplot(DataAll, aes(GRADE, S_BRUT/EQTP)) + geom_boxplot() 
p3 <- ggplot(DataAll, aes(AGE, S_BRUT/EQTP)) + geom_point(aes(colour = GRADE)) + 
  scale_colour_viridis_d()

plot_grid(p1, p2, p3, ncol = 1) 

```


Sur les données agrégées, on plot le salaire moyen et son évolution en fonction 
de l'âge des agents.

On ne retient que les EQTP = 1 pour cette visualisation.

```{r Plot des evolutions de salaire}

df_count <- DataAll %>% count(GRADE, sort = TRUE)
DataC <- DataAll %>% filter(GRADE %in% (df_count %>% filter(n > 10000))$GRADE & 
                              EQTP >.99)

# On ne visualise que qq agents ici
ID = sample(DataC$ID_NIR20, 1000, replace=F)
DataC_cut <- DataC[DataC$ID_NIR %in% ID,]
DataC_cut[is.na(DataC_cut)] = 0

# On plot
ggplot(data = DataC_cut, aes(x=AGE, y=S_BRUT, group=ID_NIR20, color=GRADE)) +
    geom_line() +
    geom_point() +
    scale_color_viridis(discrete = TRUE) +
    ggtitle("Salaires de MEDI chainés sur 6 ans") +
    theme_minimal() +
    xlab("Age") +
    ylab("Salaire brut")

cor.test(DataC$AGE, DataC$S_BRUT, na.action = na.omit, method = c("pearson"))
```

Il semble que chaque grade suive une tendance que nous pourrons modéliser par 
une droite comme nous l'avons fait pour les contractuels et aussi pour les 
titulaires dans un premier temps.

Les équations de salaires seront dérivées dans le Rmd
`Modele_EquationsDeSalaireMEDI.Rmd`

### Quelques visualisations pour leurs PRIMES (`PR_TOT_CAL`)

Et ça donne quoi les PRIMES chez les medi??

```{r fig.asp = 0.8, fig.width = 10 }

df_PR_TOT_CAL <- DataAll[DataAll$GRADE == '2400',]

p1 <- ggplot(df_PR_TOT_CAL, aes(SEXE, PR_TOT_CAL)) + geom_boxplot() 
p2 <- ggplot(df_PR_TOT_CAL, aes(GRADE, PR_TOT_CAL)) + geom_boxplot() 
p3 <- ggplot(df_PR_TOT_CAL, aes(AGE, PR_TOT_CAL)) + geom_point(aes(colour = GRADE)) + scale_colour_viridis_d()
p4 <- ggplot(df_PR_TOT_CAL, aes(S_BRUT, PR_TOT_CAL)) + geom_point(aes(colour = GRADE)) + scale_colour_viridis_d()

plot_grid(p1, p2, p3, p4, ncol = 2) 

```

OK, c'est négligeable. On ignorera donc ce paramètre.
