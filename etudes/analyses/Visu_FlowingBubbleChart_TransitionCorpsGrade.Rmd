---
  title: "Construction d'un flowing bubble chart pour visualiser les transitions entre grades"
author: "BPS DREES - Maud Galametz"
date: "09/12/2021"
output: pdf_document
---

  
```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

Chargement des libraries

```{r librairies, include=FALSE}
library(ggplot2)
library(dplyr)
library(glue)
library(gapminder)
library(gganimate)
library(RColorBrewer)



Update_Grade <- function(df){
  Tablepassage  <- as.data.frame(
    read.csv("T:/BPS/SMASH/grilles/TablePassageGrade_modifseules.csv", 
             header=TRUE, sep = ";"))
  Old_grade <-  Tablepassage$grade_final
  New_grade <-  Tablepassage$grade2020                                 
  for (i in 1:length(Old_grade)){
    df[df$GRADE == Old_grade[i], c('GRADE')] <- 
      rep(New_grade[i], nrow(df[df$GRADE == Old_grade[i],]))
  }
  return(df)
}

Reduce_to_1lineperagent <- function(df){
  id <- df[df$POSTE_PRINC_AN == 'P', c('ID_NIR20')]
  dfP <- df[df$POSTE_PRINC_AN == 'P',]
  dfS <- df[!(df$ID_NIR20 %in% id),]
  dfS <- as.data.frame(dfS %>% group_by(ID_NIR20) %>% slice_max(order_by = DUREE_POSTE))
  dfS <- as.data.frame(dfS %>% group_by(ID_NIR20) %>% slice_max(order_by = QUOTITE))
  dfS <- as.data.frame(dfS %>% group_by(ID_NIR20) %>% slice_max(order_by = S_BRUT))
  dfS <- dfS[!duplicated(dfS),] 
  dfS <- as.data.frame(dfS %>% group_by(ID_NIR20) %>% slice(1))
  df <- rbind(dfP,dfS)
  return(df)
}
```


## Présentation 

Ce Rmd vise à construire un plot animé visant à visualiser la complexité des mobilités entre grade des agents de l'hopital public.



## Chargement des tables

On charge les données SIASP avec les données chainées de 2015 à 2020.
L'ID_NIR20 commun permet de faire ce chainage.

```{r chargement, include=FALSE}
## Chargement des données 
Sys.setenv(R_CONFIG_ACTIVE = Sys.getenv('username'))
config <- config::get()
chemin <- config$path_siasp

# Chargement package Smash dev
devtools::load_all(config$path_smash)

dfEff2020_t <- readRDS(file.path(chemin, "hop2020_selection.rds"))
dfEff2019_t <- readRDS(file.path(chemin, "hop2019_selection.rds"))
dfEff2018_t <- readRDS(file.path(chemin, "hop2018_selection.rds"))
dfEff2017_t <- readRDS(file.path(chemin, "hop2017_selection.rds"))
dfEff2016_t <- readRDS(file.path(chemin, "hop2016_selection.rds"))
dfEff2015_t <- readRDS(file.path(chemin, "hop2015_selection.rds"))

```


### Variables d'interet

On ne retient que les variables qui nous seront utiles pour le reste de l'étude.
On ne va visualiser pour simplifier que les titulaires.

Pour comparer les agents dont la nomenclature de grade a changé sous les 6 années de nos données, on recale tous les grades à leur équivalent en 2020.

```{r variables, include=FALSE}
dfEff2020 <- dfEff2020_t[, c('ID_NIR20', 'POSTE_PRINC_AN', 'STATUT_CONTRAT', 
                             'DUREE_CONTRAT', 'GRADE', 'CORPS', 'DUREE_POSTE', 'QUOTITE', 'S_BRUT')]
dfEff2019 <- dfEff2019_t[, c('ID_NIR20', 'POSTE_PRINC_AN', 'STATUT_CONTRAT', 
                             'DUREE_CONTRAT', 'GRADE', 'CORPS', 'DUREE_POSTE', 'QUOTITE', 'S_BRUT')]
dfEff2018 <- dfEff2018_t[, c('ID_NIR20', 'POSTE_PRINC_AN', 'STATUT_CONTRAT', 
                             'DUREE_CONTRAT', 'GRADE', 'CORPS', 'DUREE_POSTE', 'QUOTITE', 'S_BRUT')]
dfEff2017 <- dfEff2017_t[, c('ID_NIR20', 'POSTE_PRINC_AN', 'STATUT_CONTRAT', 
                             'DUREE_CONTRAT', 'GRADE', 'CORPS', 'DUREE_POSTE', 'QUOTITE', 'S_BRUT')]
dfEff2016 <- dfEff2016_t[, c('ID_NIR20', 'POSTE_PRINC_AN', 'STATUT_CONTRAT', 
                             'DUREE_CONTRAT', 'GRADE', 'CORPS', 'DUREE_POSTE', 'QUOTITE', 'S_BRUT')]
dfEff2015 <- dfEff2015_t[, c('ID_NIR20', 'POSTE_PRINC_AN', 'STATUT_CONTRAT', 
                             'DUREE_CONTRAT', 'GRADE', 'CORPS', 'DUREE_POSTE', 'QUOTITE', 'S_BRUT')]

# Ugrade the grades
dfEff2015 <- Update_Grade(dfEff2015)
dfEff2016 <- Update_Grade(dfEff2016)
dfEff2017 <- Update_Grade(dfEff2017)
dfEff2018 <- Update_Grade(dfEff2018)
dfEff2019 <- Update_Grade(dfEff2019)
dfEff2020 <- Update_Grade(dfEff2020)

# Reduce to 1 line per agent
dfEff2015 <- Reduce_to_1lineperagent(dfEff2015)
dfEff2016 <- Reduce_to_1lineperagent(dfEff2016)
dfEff2017 <- Reduce_to_1lineperagent(dfEff2017)
dfEff2018 <- Reduce_to_1lineperagent(dfEff2018)
dfEff2019 <- Reduce_to_1lineperagent(dfEff2019)
dfEff2020 <- Reduce_to_1lineperagent(dfEff2020)

# Separer les TITH
dfEff2020_TITH <- dfEff2020[(dfEff2020$STATUT_CONTRAT == 'TITH'),]
dfEff2019_TITH <- dfEff2019[(dfEff2019$STATUT_CONTRAT == 'TITH'),]
dfEff2018_TITH <- dfEff2018[(dfEff2018$STATUT_CONTRAT == 'TITH'),]
dfEff2017_TITH <- dfEff2017[(dfEff2017$STATUT_CONTRAT == 'TITH'),]
dfEff2016_TITH <- dfEff2016[(dfEff2016$STATUT_CONTRAT == 'TITH'),]
dfEff2015_TITH <- dfEff2015[(dfEff2015$STATUT_CONTRAT == 'TITH'),]
```




## Création de la table de passage chainee grade / corps des titulaires

Dans cette section , on va créer une table qui concatenera tous les grades d'un même agent sur 6 ans (ou moins si l'agent entre ou sort de la FPH durant la période).

Cette table sera utilisée dans un autre rmd, pour calculer la matrice de transition entre grade.
Sauver la table permet également de ne pas avoir à recharger les SIASP si on veut ajouter une nouvelle visualisation ci dessous.


```{r Table Passage}

dfEff2015_TITH <- dfEff2015_TITH %>% dplyr::rename(GRADE_C15 = GRADE)
dfEff2016_TITH <- dfEff2016_TITH %>% dplyr::rename(GRADE_C16 = GRADE)
dfEff2017_TITH <- dfEff2017_TITH %>% dplyr::rename(GRADE_C17 = GRADE)
dfEff2018_TITH <- dfEff2018_TITH %>% dplyr::rename(GRADE_C18 = GRADE)
dfEff2019_TITH <- dfEff2019_TITH %>% dplyr::rename(GRADE_C19 = GRADE)
dfEff2020_TITH <- dfEff2020_TITH %>% dplyr::rename(GRADE_C20 = GRADE)

dfEff2015_TITH <- dfEff2015_TITH %>% dplyr::rename(CORPS_C15 = CORPS)
dfEff2016_TITH <- dfEff2016_TITH %>% dplyr::rename(CORPS_C16 = CORPS)
dfEff2017_TITH <- dfEff2017_TITH %>% dplyr::rename(CORPS_C17 = CORPS)
dfEff2018_TITH <- dfEff2018_TITH %>% dplyr::rename(CORPS_C18 = CORPS)
dfEff2019_TITH <- dfEff2019_TITH %>% dplyr::rename(CORPS_C19 = CORPS)
dfEff2020_TITH <- dfEff2020_TITH %>% dplyr::rename(CORPS_C20 = CORPS)

dfEff2015_TITH <- dfEff2015_TITH[, c('ID_NIR20','GRADE_C15','CORPS_C15')] 
dfEff2016_TITH <- dfEff2016_TITH[, c('ID_NIR20','GRADE_C16','CORPS_C16')] 
dfEff2017_TITH <- dfEff2017_TITH[, c('ID_NIR20','GRADE_C17','CORPS_C17')] 
dfEff2018_TITH <- dfEff2018_TITH[, c('ID_NIR20','GRADE_C18','CORPS_C18')] 
dfEff2019_TITH <- dfEff2019_TITH[, c('ID_NIR20','GRADE_C19','CORPS_C19')] 
dfEff2020_TITH <- dfEff2020_TITH[, c('ID_NIR20','GRADE_C20','CORPS_C20')] 

Data_TITH <- merge(x = dfEff2015_TITH, y = dfEff2016_TITH, all = TRUE)
Data_TITH <- merge(x = Data_TITH, y = dfEff2017_TITH, all = TRUE)
Data_TITH <- merge(x = Data_TITH, y = dfEff2018_TITH, all = TRUE)
Data_TITH <- merge(x = Data_TITH, y = dfEff2019_TITH, all = TRUE)
Data_TITH <- merge(x = Data_TITH, y = dfEff2020_TITH, all = TRUE)
Data_TITH <- Data_TITH[is.na(Data_TITH$GRADE_C15) + is.na(Data_TITH$GRADE_C17) + is.na(Data_TITH$GRADE_C16) + is.na(Data_TITH$GRADE_C18) + is.na(Data_TITH$GRADE_C19) + is.na(Data_TITH$GRADE_C20) < 3,]
                          
fic_dest <- file.path(chemin, 'TableChaineeGradeCorps_Titulaires.rds' )
saveRDS(Data_TITH, file = fic_dest)
```





## Visualisation en flowing bubble chart


Pour ce type de visualisation, il est nécessaire d'avoir les agents enregistrés 
en colonne avec une ligne par année.
On réorganise la table précédemment enregistrée.


```{r Preparation Table}

chemin <- "C:\\Users\\maud.galametz\\Documents\\ProjetSMASH\\TablesSIASP\\"
Data <- readRDS(file.path(chemin, 'TableChaineeGradeCorps_Titulaires.rds'))

dfEff2016 <- Data[, c('ID_NIR20','GRADE_C16','CORPS_C16')]
dfEff2016$TIME <- 2016
dfEff2016 <- dfEff2016 %>% dplyr::rename(GRADE = GRADE_C16)
dfEff2016 <- dfEff2016 %>% dplyr::rename(CORPS = CORPS_C16)

dfEff2017 <- Data[, c('ID_NIR20','GRADE_C17','CORPS_C17')]
dfEff2017$TIME <- 2017
dfEff2017 <- dfEff2017 %>% dplyr::rename(GRADE = GRADE_C17)
dfEff2017 <- dfEff2017 %>% dplyr::rename(CORPS = CORPS_C17)

dfEff2018 <- Data[, c('ID_NIR20','GRADE_C18','CORPS_C18')]
dfEff2018$TIME <- 2018
dfEff2018 <- dfEff2018 %>% dplyr::rename(GRADE = GRADE_C18)
dfEff2018 <- dfEff2018 %>% dplyr::rename(CORPS = CORPS_C18)

dfEff2019 <- Data[, c('ID_NIR20','GRADE_C19','CORPS_C19')]
dfEff2019$TIME <- 2019
dfEff2019 <- dfEff2019 %>% dplyr::rename(GRADE = GRADE_C19)
dfEff2019 <- dfEff2019 %>% dplyr::rename(CORPS = CORPS_C19)

dfEff2020 <- Data[, c('ID_NIR20','GRADE_C20','CORPS_C20')]
dfEff2020$TIME <- 2020
dfEff2020 <- dfEff2020 %>% dplyr::rename(GRADE = GRADE_C20)
dfEff2020 <- dfEff2020 %>% dplyr::rename(CORPS = CORPS_C20)

Data <- rbind(dfEff2016,dfEff2017,dfEff2018,dfEff2019,dfEff2020)

```


Première animation, on visualise les changements de corps de quelques agents. 
On enregistre le gganimate plot dans le répertoire courant.


```{r Animation}

# On sélectionne les agents, on ne visualisera que des agents de corps assez nombreux en effectifs ici
NewData <- Data
count <- plyr::count(NewData$CORPS)
NewData <- NewData[NewData$CORPS %in% count[count$freq >50000,]$x,]
ID = sample(NewData$ID_NIR20, 100, replace=F)
NewData_cut <- NewData[NewData$ID_NIR20 %in% ID,]
NewData_cut[is.na(NewData_cut)] = 0
l_corps <- unique(NewData_cut$CORPS)
l_corps <- sort(l_corps[l_corps != "0"])

# On ajoute une colonne de coordonnées. Les agents seront répartis sur une ellipse, les entrants/sortants au centre.
NewData_cut$Abs <- 0
NewData_cut$Ord <- 0
Abs_save <- rep(0, length(l_corps))
Ord_save <- rep(0, length(l_corps))
for (i in 1:length(l_corps)){
  Temp <- NewData_cut[NewData_cut$CORPS == l_corps[i],]
  if (nrow(Temp) > 0){  
    NewData_cut[NewData_cut$CORPS == l_corps[i], c('Abs')] <- rep(5*cos(as.numeric(2*pi * i / length(l_corps))), nrow(Temp))
    Abs_save[i] = 5*cos(as.numeric(2*pi * i / length(l_corps)))
    NewData_cut[NewData_cut$CORPS == l_corps[i], c('Ord')] <- rep(20*round(sin(as.numeric(2*pi * i / length(l_corps))), digits=2), nrow(Temp))
    Ord_save[i] = 20*round(sin(as.numeric(2*pi * i / length(l_corps))), digits=2)
  print(c('replacing', l_corps[i], 'by', 2*pi / i))
  }
}
Out <- data.frame(Abs = 0, Ord = 0, tag= "Out")

# On définit les labels et les couleurs pour les labels des agents
qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
col_vector = unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))
labels <- data.frame(Abs_save=Abs_save, Ord_save=Ord_save, tags=l_corps, col=sample(col_vector, length(l_corps)))

# On définit le plot et on enregistre
anim <- ggplot() + 
  geom_jitter(data = NewData_cut, 
       aes(x = Abs, y = Ord, group = ID_NIR20), alpha = 0.5, width = 1E-4) + 
  labs(y = "", x = " ", title = "Changement de corps - Année: {as.integer(frame_time)}", size = 12, face = "bold") +
  geom_text(data = labels, aes(x = Abs_save, y = Ord_save, label = l_corps, color  = col), show.legend = FALSE) +
  geom_text(data = Out, aes(x = 0, y = 0, label = tag)) +
  theme_minimal() +  # apply minimal theme
  transition_time(TIME)
animate(anim)
anim_save("./ChangementCorps.gif", anim)

```



Deuxième animation, les changements de grade de quelques agents. On enregistre le gganimate plot
dans le répertoire courant.

```{r Animation 2 grade}

# On sélectionne les agents, on ne visualisera que des agents de corps assez nombreux en effectifs ici
NewData <- Data
count <- plyr::count(NewData$CORPS)
NewData <- NewData[NewData$CORPS %in% count[count$freq >10000,]$x,]
ID = sample(NewData$ID_NIR20, 2000, replace=F)
NewData_cut <- NewData[NewData$ID_NIR20 %in% ID,]
NewData_cut[is.na(NewData_cut)] = 0
l_grade <- unique(NewData_cut$GRADE)
l_grade <- sort(l_grade[l_grade != "0"])

# On ajoute une colonne de coordonnées. Les agents seront répartis sur une ellipse, les entrants/sortants au centre.
NewData_cut$Abs <- 0
NewData_cut$Ord <- 0
Abs_save <- rep(0, length(l_grade))
Ord_save <- rep(0, length(l_grade))
for (i in 1:length(l_grade)){
  Temp <- NewData_cut[NewData_cut$GRADE == l_grade[i],]
  if (nrow(Temp) > 0){  
    NewData_cut[NewData_cut$GRADE == l_grade[i], c('Abs')] <- rep(5*cos(as.numeric(2*pi * i / length(l_grade))), nrow(Temp))
    Abs_save[i] = 5*cos(as.numeric(2*pi * i / length(l_grade)))
    NewData_cut[NewData_cut$GRADE == l_grade[i], c('Ord')] <- rep(20*round(sin(as.numeric(2*pi * i / length(l_grade))), digits=2), nrow(Temp))
    Ord_save[i] = 20*round(sin(as.numeric(2*pi * i / length(l_grade))), digits=2)
  print(c('replacing', l_grade[i], 'by', 2*pi / i))
  }
}
Out <- data.frame(Abs = 0, Ord = 0, tag= "Out")

# On définit les labels et les couleurs pour les labels des agents
qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
col_vector = unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))
labels <- data.frame(Abs_save=Abs_save, Ord_save=Ord_save, tags=l_grade) #, col=sample(col_vector, length(l_grade)))

# On définit le plot et on enregistre
anim <- ggplot() + 
  geom_jitter(data = NewData_cut, 
       aes(x = Abs, y = Ord, group = ID_NIR20), alpha = 0.5, width = 1E-4) + 
  labs(y = "", x = " ", title = "Changement de grade - Année: {as.integer(frame_time)}", size = 12, face = "bold") +
  geom_text(data = labels, aes(x = Abs_save, y = Ord_save, label = l_grade), show.legend = FALSE) + 
  #, color  = col), show.legend = FALSE) +
  geom_text(data = Out, aes(x = 0, y = 0, label = tag)) +
  theme_minimal() +  # apply minimal theme
  transition_time(TIME)
animate(anim)
anim_save("./ChangementGrade.gif", anim)

```

Troisième animation, on va se concentrer sur les grades d'un même corps, on ne représente pas les entrants sortants.

```{r Animation_3_grade_Infirmiers}

BD <- GrillesIndiciaires$new()
l_filieres <- BD$listFilieres()
l_corps_modelise <- BD$listCorps()

corps = l_corps_modelise[14]
l_grade = BD$getGradesByCorps(corps)$GRADE

NewData <- Data[Data$GRADE %in% l_grade,]
ID = sample(NewData$ID_NIR20, 2000, replace=F)
NewData_cut <- NewData[NewData$ID_NIR20 %in% ID,]
NewData_cut[is.na(NewData_cut)] = 0

# On ajoute une colonne de coordonnées. Les agents seront répartis sur une ellipse
NewData_cut$Abs <- 0
NewData_cut$Ord <- 0
Abs_save <- rep(0, length(l_grade))
Ord_save <- rep(0, length(l_grade))
for (i in 1:length(l_grade)){
  Temp <- NewData_cut[NewData_cut$GRADE == l_grade[i],]
  if (nrow(Temp) > 0){  
    NewData_cut[NewData_cut$GRADE == l_grade[i], c('Abs')] <- rep(5*cos(as.numeric(2*pi * i / length(l_grade))), nrow(Temp))
    Abs_save[i] = 5*cos(as.numeric(2*pi * i / length(l_grade)))
    NewData_cut[NewData_cut$GRADE == l_grade[i], c('Ord')] <- rep(20*round(sin(as.numeric(2*pi * i / length(l_grade))), digits=2), nrow(Temp))
    Ord_save[i] = 20*round(sin(as.numeric(2*pi * i / length(l_grade))), digits=2)
  print(c('replacing', l_grade[i], 'by', 2*pi / i))
  }
}

# On définit les labels et les couleurs pour les labels des agents
qual_col_pals = brewer.pal.info[brewer.pal.info$category == 'qual',]
col_vector = unlist(mapply(brewer.pal, qual_col_pals$maxcolors, rownames(qual_col_pals)))
labels <- data.frame(Abs_save=Abs_save, Ord_save=Ord_save, tags=l_grade, col=sample(col_vector, length(l_grade)))

# On définit le plot et on enregistre
anim <- ggplot() + 
  geom_jitter(data = NewData_cut, 
       aes(x = Abs, y = Ord, group = ID_NIR20), alpha = 0.5, width = 1E-4) + 
  labs(y = "", x = " ", title = "Changement de corps - Année: {as.integer(frame_time)}", size = 12, face = "bold") +
  geom_text(data = labels, aes(x = Abs_save, y = Ord_save, label = l_grade, color  = col), show.legend = FALSE) +
  theme_minimal() +  # apply minimal theme
  transition_time(TIME)
animate(anim)
anim_save("./ChangementGrade_Infirmiers.gif", anim)

```




