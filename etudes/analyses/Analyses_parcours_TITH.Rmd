---
title: "Parcours des Titularisés"
author: "Vincent Attia (LabSante)"
date: "23/01/2023"
output:
  word_document: default
pdf_document: default
html_document:
  df_print: paged
---
  
  
```{r setup, echo=FALSE}
#library(officer)

#install.packages("officer")

knitr::opts_chunk$set(echo = FALSE)
# tab.id=NULL,tab.cap=NULL,tab.topcaption=TRUE,tab.lp="tab:",
# tab.cap.style=NULL,tab.cap.pre="Table",tab.cap.sep=" :",tab.cap.tnd=0,tab.cap.tns="-",
# tab.style=NULL,tab.layout="autofit",tab.width=1)

#tab.cap.fp_text=officer::fp_text_lite(bold = TRUE)

```

```{r librairies, include=F}
library(dplyr)
library(data.table)
library(glue)
library(MASS)
library(fitdistrplus)
library(Rcpp)
library(fst)
library(readxl)
library(ggplot2)
library(plotly)

library(rpart)
library(caret)

library(flextable)

fic_mdl <- "T:/BPS/SMASH/modeles"

```


## Présentation 

Ce document étudie l'éventuelle transition de titulaires ("TITH") vers d'autres statuts. Bien que contre-intuitif, ce phénomène a été remarqué à l'occasion des analyses sur le parcours des contractuels ("CONT") et [semble de plus en plus fréquent](https://urldefense.com/v3/__https:/www.lagazettedescommunes.com/844898/ces-fonctionnaires-qui-preferent-etre-contractuels/__;!!FiWPmuqhD5aF3oDTQnc!36mB_8Y_38MxoNeri90gBXTJpqLsW1XS7skR2iBcsou9kA-LlrP9tVCe-A14Yrf4fnxi4Mt83dM$).
A l'échelle des TITH, ce phénomène est rare mais il semble cohérent de s'en préoccuper car il contribue à l'augmentation des CDD autant que les CDI.

Comme les "sorties" sont gérées par une autre brique du modèle, elles ne seront pas prises en compte dans les modalités possibles de statut de l'année d'après, mais cette alternative est considérée pour les statistiques descriptives suivantes. Les taux de cette brique de travail devront être exprimés sans cette modalité, sur la base des individus présents plusieurs années de suite.
 

## Chargement des données 

On recupere les données des années 2012 à 2021 avec une ligne par individu: "hopAAAA_agrege.fst".

Avant 2012, la base n'était pas encore correctement remplie.

```{r chargement, include=F}

chemin <- "T:\\BPS\\SMASH\\DonneesSIASP\\Tables_FST\\"

to_keep=c('ID_NIR20','STATUT_CONTRAT','DUREE_CONTRAT','GRADE_ECH','CORPS',
          'QUOTITE','EQTP','AGE','SEXE','ANC_FPH','DEPCOM_FONCTION','REGION','S_BRUT')

# salaire but dans les variables explicatives?
# Regarder plus tard si interet de les incorporer, car pas dans smash a priori:
#   'NB_HEURES_REMUN','DUREE_POSTE','DAT_DEBUT', 'NB_POSTES'

#de quoi charger les filières correspondantes:
#table_grade - idée abandonnée: utiliser le package
# conversion_grades<-as.data.table(read_excel("T:\\BPS\\SMASH\\grilles\\20211217_neh_grilles_sas.xlsx",
#                                    sheet="Table grade",col_names=T))
devtools::load_all("../../../smash-r-pkg") # adapter le chemin relatif Rmd <-> package
o <- GrillesIndiciaires$new() #o$getFiliere("3001")


# chargement des bases
years=as.character(2012:2021)

#première année faite à part à cause des diffèrentes variables crées
assign(paste0("dfEff",years[1]),
       read.fst(file.path(chemin, paste0("hop",years[1],"_agrege.fst")), #selection_
                as.data.table=T,columns=to_keep)[,`:=`(nb_mois=12*(100*EQTP/QUOTITE),
                                                       DEP=substr(DEPCOM_FONCTION,1,2),
                                                       FILIERE=o$getFiliere(GRADE_ECH))]
)

for (year in years[2:length(years)]){
  assign(paste0("dfEff",year),
         read.fst(file.path(chemin, paste0("hop",year,"_agrege.fst")), #selection_
                  as.data.table=T,columns=to_keep)[,`:=`(nb_mois=12*(100*EQTP/QUOTITE),
                                                         DEP=substr(DEPCOM_FONCTION,1,2),
                                                         FILIERE=o$getFiliere(GRADE_ECH),
                                                         FILIERE_NP1=o$getFiliere(GRADE_ECH),
                                                         STATUT_CONTRAT_NP1=STATUT_CONTRAT,
                                                         DUREE_CONTRAT_NP1=DUREE_CONTRAT,
                                                         DEP_NP1=substr(DEPCOM_FONCTION,1,2),
                                                         S_BRUT_NP1=S_BRUT,
                                                         EQTP_NP1=EQTP)]
  )
} 

```

L'objectif de la brique de transition des contrats consiste à retrouver les profils suivants conjointement aux entrées et aux sorties:

```{r objectif, echo=F}


count <- rbindlist(list(dfEff2013[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2013"],
                        dfEff2014[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2014"],
                        dfEff2015[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2015"],
                        dfEff2016[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2016"],
                        dfEff2017[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2017"],
                        dfEff2018[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2018"],
                        dfEff2019[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2019"],
                        dfEff2020[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2020"],
                        dfEff2021[STATUT_CONTRAT %in% c("CONT","TITH")][,ANNEE:="2021"]))

count[STATUT_CONTRAT=="CONT" & DUREE_CONTRAT %in% c("AUTR","SANS"),DUREE_CONTRAT:="CDD"]
count[STATUT_CONTRAT=="TITH",DUREE_CONTRAT:= "SANS"]

count[,type:=ifelse(STATUT_CONTRAT=="TITH","TITH",DUREE_CONTRAT)]

count_plot <- count[,.N,by=c("ANNEE","type")]

ggplot(count_plot) +
  aes(x = ANNEE, y = N, colour = type) +
  geom_point(shape = "circle", size = 1.5) +
  scale_color_hue(direction = 1) +
  theme_minimal() +
  ylim(0, 801000)

count_plot_wide <- data.table::dcast(count_plot, ANNEE ~ type, value.var="N")[order(ANNEE)]

```


```{r chainage}

statuts_n=c("TITH")

#statuts_np1=append(statuts_n,c("TITH","SORTANT")) 

#liste des variables conservées dans la base N+1
liste_N <- append(to_keep,c("nb_mois","DEP","FILIERE"))
liste_NP1 = c("ID_NIR20","STATUT_CONTRAT_NP1","DUREE_CONTRAT_NP1","DEP_NP1","S_BRUT_NP1","EQTP_NP1")

dfEff2012_y = merge(dfEff2012[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2013[,..liste_NP1],all.x=T)[,ANNEE:=2012]
dfEff2013_y = merge(dfEff2013[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2014[,..liste_NP1],all.x=T)[,ANNEE:=2013]
dfEff2014_y = merge(dfEff2014[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2015[,..liste_NP1],all.x=T)[,ANNEE:=2014]
dfEff2015_y = merge(dfEff2015[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2016[,..liste_NP1],all.x=T)[,ANNEE:=2015]
dfEff2016_y = merge(dfEff2016[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2017[,..liste_NP1],all.x=T)[,ANNEE:=2016]
dfEff2017_y = merge(dfEff2017[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2018[,..liste_NP1],all.x=T)[,ANNEE:=2017]
dfEff2018_y = merge(dfEff2018[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2019[,..liste_NP1],all.x=T)[,ANNEE:=2018]
dfEff2019_y = merge(dfEff2019[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2020[,..liste_NP1],all.x=T)[,ANNEE:=2019]
dfEff2020_y = merge(dfEff2020[STATUT_CONTRAT %in% statuts_n,..liste_N],
                    dfEff2021[,..liste_NP1],all.x=T)[,ANNEE:=2020]

#rm(list=paste0("dfEff",years))

# nrow(dfEff2020[is.na(STATUT_CONTRAT)]) nul, pour chauqe année

dfEffAAAA_y=rbind(rbind(rbind(rbind(rbind(rbind(rbind(
  rbind(dfEff2012_y,
        dfEff2013_y),
        dfEff2014_y),
        dfEff2015_y),
        dfEff2016_y),
        dfEff2017_y),
        dfEff2018_y),
        dfEff2019_y),
        dfEff2020_y)
dfEffAAAA_y[,one:=1]
dfEffAAAA_y[,INDIV_ANNEE:=sum(one),by="ANNEE"]
dfEffAAAA_y[,MS_ANNEE:=sum(S_BRUT),by="ANNEE"]

dfEffAAAA_y[is.na(STATUT_CONTRAT_NP1),STATUT_CONTRAT_NP1:="SORTANT"]

dfEffAAAA_y[STATUT_CONTRAT_NP1=="CONT" & DUREE_CONTRAT_NP1 %in% c("SANS","AUTR"),DUREE_CONTRAT_NP1:="CDD"]

dfEffAAAA_y[,STATUT_CONTRAT_NP1:=ifelse(STATUT_CONTRAT_NP1 %in% c("CONT"),DUREE_CONTRAT_NP1,STATUT_CONTRAT_NP1)]

# dfEffAAAA_y[,CONTRAT_NP1:=ifelse(STATUT_CONTRAT_NP1 %in% c("SORTANT","TITH","AUTRE"),STATUT_CONTRAT_NP1,
#                                  DUREE_CONTRAT_NP1)]

dfEffAAAA_y[,`:=`(GRADE_ECH=as.factor(GRADE_ECH),SEXE=as.factor(SEXE),DEP=as.factor(DEP))]
dfEffAAAA_y[,`:=`(ANNEE=as.character(ANNEE))]

dfEffAAAA_y[,tr_age:=fcase(AGE<=20,"-20",
                           AGE>20 & AGE<=30,"20-30",
                           AGE>30 & AGE<=40,"30-40",
                           AGE>40 & AGE<=50,"40-50",
                           AGE>50 & AGE<=60,"50-60",
                           AGE>60,"60-")]

```

## Transitions possibles et déterminants

Etudions l'envergure du phénomène:

```{r stats_basiques, echo=F}

check=dfEffAAAA_y[,.N,by=c("STATUT_CONTRAT_NP1","ANNEE")][,TOTAL:=sum(N),by="ANNEE"][,pc:=round(100*N/TOTAL,2)]

check %>%
 filter(!(STATUT_CONTRAT_NP1 %in% "TITH")) %>%
 ggplot() +
  aes(x = ANNEE, fill = STATUT_CONTRAT_NP1, weight = pc) +
  geom_bar(position = "dodge") +
  scale_fill_hue(direction = 1) +
  labs(
    x = "Année N",
    y = "Pourcentage",
    title = "Pourcentage de transitions des TITH",
    subtitle = "Exclusion des TITH restés TITH",
    fill = "Statut N+1"
  )+
  theme_minimal()

check %>%
 filter(!(STATUT_CONTRAT_NP1 %in% c("TITH", "SORTANT"))) %>%
 ggplot() +
  aes(x = ANNEE, fill = STATUT_CONTRAT_NP1, weight = N) +
  geom_bar(position = "dodge") +
  scale_fill_hue(direction = 1) +
  labs(
    x = "Année N",
    y = "Nombre",
    title = "Nombre de transitions des TITH",
    subtitle = "Exclusion des TITH restés TITH ou sortis",
    fill = "Statut N+1"
  ) +
  theme_minimal() +
  theme(
    plot.title = element_text(size = 18L,
    face = "bold"),
    plot.subtitle = element_text(size = 16L)
  )


```

La transition la plus fréquente, en dehors de rester titulaire ou sortir, est celle de devenir contractuels en CDD.
Quelle est leur profil par rapport à ceux restés titulaires?

```{r boxplots, echo=F}

# TITH_TITH=dfEffAAAA_y[STATUT_CONTRAT=="TITH" & STATUT_CONTRAT_NP1=="TITH"]
# TITH_CDD=dfEffAAAA_y[STATUT_CONTRAT=="TITH" & STATUT_CONTRAT_NP1=="CDD"]

# GRADE_ECH
# QUOTITE
# EQTP
# AGE
# SEXE
# ANC_FPH
# S_BRUT

INTEREST = dfEffAAAA_y[ANNEE=="2019" & STATUT_CONTRAT=="TITH" & STATUT_CONTRAT_NP1 %in% c("CDD","TITH")]

ggplot(INTEREST, aes(y=AGE, fill=STATUT_CONTRAT_NP1)) + geom_boxplot()

ggplot(INTEREST, aes(y=ANC_FPH/365, fill=STATUT_CONTRAT_NP1)) + geom_boxplot()

ggplot(INTEREST, aes(y=S_BRUT/1000, fill=STATUT_CONTRAT_NP1)) + geom_boxplot()


table(INTEREST$STATUT_CONTRAT_NP1,INTEREST$SEXE)



```

Le sexe ne change rien. Une combinaison des autres variables a-t-elle un sens?

Etant donné la rareté du phénomène à l'échelle des titulaires, un logit cherchant à expliquer la transition vers un CDD entre 2019 et 2020, à partir de l'ancienneté, de l'âge et du salaire ne fonctionne pas. 


```{r logit}

INTEREST[,CDD:=as.numeric(STATUT_CONTRAT_NP1=="CDD")]
INTEREST[,S_BRUT_K:=S_BRUT/1000]
INTEREST[,ANC_FPH_ANS:=ANC_FPH/365]



logit=glm(data=INTEREST, CDD~AGE+S_BRUT_K+ANC_FPH_ANS, family=binomial(link=logit))

INTEREST$fitted=round(logit$fitted.values,0) # transfo inverse de logit

# plot(INTEREST$AGE,INTEREST$CDD)
# points(INTEREST$AGE[order(INTEREST$AGE)],INTEREST$fitted[order(INTEREST$AGE)], col="red",type="l", lwd=2)
# 
# plot(INTEREST$S_BRUT_K,INTEREST$CDD)
# points(INTEREST$S_BRUT_K[order(INTEREST$S_BRUT_K)],INTEREST$fitted[order(INTEREST$AGE)], col="red",type="l", lwd=2)



```

En lignes, l'indicatrice désignant le passage à un CDD, en colonne la prédiction.

```{r logit_res}

table(INTEREST$CDD,INTEREST$fitted)


```

Au niveau économétrique, Il conviendrait alors de mobiliser un modèle plus complexe adapté aux évènements rares, mais cela contrevient au caractère manipulable de SMASH.

De plus, cette tendance est nouvelle et risque de voir ses déterminants changer.

Une solution pour intégrer tout de même ce phénomène au package consisterait à simplement utiliser les pourcentages obtenus plus haut.

## Taux simples

Bien que plus rares, rien ne contrevient à la prise en compte des transitions TITH/CDI.

On obtient, les transitions suivantes, en excluant les sorties et en considérant les transitions vers d'autres statuts que TITH et CONT comme TITH:

```{r taux_simples, echo=F}

Chiffres_finaux = copy(dfEffAAAA_y[STATUT_CONTRAT_NP1!="SORTANT"])
Chiffres_finaux[!(STATUT_CONTRAT_NP1 %in% c("CDD","CDI","TITH")),STATUT_CONTRAT_NP1:="TITH"]
Chiffres_finaux[,CONTRAT_NP1:=STATUT_CONTRAT_NP1]

decompte=Chiffres_finaux[,.N,by=c("CONTRAT_NP1","ANNEE")][,TOTAL:=sum(N),by="ANNEE"][,pc_NP1:=round(100*N/TOTAL,2)]

decompte= data.table::dcast(decompte, ANNEE ~ CONTRAT_NP1, value.var="pc_NP1")[order(ANNEE)] 

decompte %>% flextable() %>% width(width=.8)

```

L'idée serait de tirer une probabilité pour chaque titulaire et de lui attribuer son statut de l'année suivante, sur la base de ces chiffres pour les années avant 2021. Ensuite, les pourcentages de 2020, respectivement 0,60 et 0,04 % pour les CDD et CDI peuvent être retenus faute de recul sur ce phénoème limité mais bien réel. 

Ce modèle a été implémenté à la suite de celui des ceontractuels.


