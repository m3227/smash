---
title: "Test mortalite"
author: "BPS DREES - Maud Galametz"
date: "02/11/2022"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
```

```{r librairies}
library(plyr)
library(ggplot2)

# Charge le package SMASH
devtools::load_all("../../../smash-r-pkg")
```


## Présentation 

Ce document présente un exemple d'utilisation des probabilités de décès de 
l'INSEE pour tirer aléatoirement des individus qui vont mourir dans une population. Les probabilités sont estimées suivant leur age et leur sexe au vue des 
prédictions de l'INSEE

Les tables sont récupérées à partir de:
https://www.insee.fr/fr/statistiques/3311422?sommaire=3311425#consulter-sommaire

Leur documentation est disponible ici:
https://www.insee.fr/fr/statistiques/3311422?sommaire=3311425#documentation-sommaire


## Chargement des données 


```{r DefinitionChemin}
chemin_tableINSEE <- file.path("I:/BPS/_Emplois et revenus/Projet SMASH/",
                               "04_Modele/DemographieTablesInsee/",
                               "MortaliteAgeSexeNiveauVie")
```


```{r Chargement}
df_initial <- chargerFST("hop", 2020)
```



## Probabilité de mortalité par age et par sexe

Cette fonction retourne la probabilité de mortalité de chacun des entrants ainsi que le nombre total
de morts attendu pour cette population.

```{r ProbaMortalite}
ProbaMortalite<- function(df, agemin, agemax){
  
  #%% Lire les tables de mortalite
  TM_data <- file.path(chemin_tableINSEE, "Mortalite_Ensemble.csv")
  TM = read.csv(TM_data, sep=';')

  #%% Déterminer le quotient de mortalite par age et le nombre de morts total attendu 
  probas = array(1, NROW(df))
  NbreMortsTotal = 0
  for (i in 0:agemax-agemin-1){
    # Quotient pour cet age
    temp = TM[(TM$SEXE == 'F') & (TM$AGE == agemin + i), ]
    if (NROW(temp) != 0){ 
      QF = as.numeric(temp$QUOTIENT) / 100000.
    }
    else{
      QF = 0
    }
    temp = TM[(TM$SEXE == 'H') & (TM$AGE == agemin + i), ]  
    if (NROW(temp) != 0){   
      QH = as.numeric(temp$QUOTIENT) / 100000.
    }
    else{
      QH = 0  
    }
    
    # Sauver cette proba pour chaque individu ayant cet age
    probas[(df$AGE == agemin + i) & (df$SEXE == 'F')] = QF
    probas[(df$AGE == agemin + i) & (df$SEXE == 'M')] = QH
    
    # Nombre de morts total attendu pour l'echantillon
    FemmesParAge = NROW(df[(df$AGE == agemin + i) & (df$SEXE == 'F'),])
    HommesParAge = NROW(df[(df$AGE == agemin + i) & (df$SEXE == 'M'),])
    NbreMortsFemmes = QF * FemmesParAge 
    NbreMortsHommes = QH * HommesParAge 
    NbreMortsTotal = NbreMortsTotal + NbreMortsFemmes + NbreMortsHommes
  }
  Resultats <- list("probas" = probas, "NbreMortsTotal" = NbreMortsTotal)
  return(Resultats)
} 
```



### Sur les as ou ashq de l'échantillon statique

On applique ce taux sur les as et ashq de l'échantillon.


```{r Appliquer}

# Borne min et max d'ages considérés
agemin = 15 
agemax = 95

# Ne garder que les as ou ashq
df <- df_initial[df_initial$CORPS == 'Aide-soignant et ashq 0227 C' | df_initial$CORPS == 'Aide-soignant et ashq 0226 C',]

# Ne garder qu'une fois les individus et les colonnes d'intérêt
df <- df %>% group_by(ID_NIR20) %>% slice(1)
df <- df[, c('ID_NIR20', 'AGE', 'SEXE')]

# Gérer les individus dont l'age est mal reporté (typiquement < agemin) en leur attribuant une valeur stat. représentative
df[df$AGE < agemin, c('AGE')] = median(df$AGE)

# Ajout d'une colonne d'état (1 pour salariés, 0 pour decedes ou retraités)
df$STATUS <- 1
df[df$AGE > agemax, c('STATUS')] <- 0

### Appliquer le taux de mortalite standard a cette population pour la duree de la simulation

Duree_Simulation = 10         #en annee

for (t in 1:Duree_Simulation){ 

  Resultats = ProbaMortalite(df[df$STATUS == 1,], agemin, agemax)
  probas = Resultats$probas 
  NbreMortsTotal = Resultats$NbreMortsTotal 
  
  goodbye = sample(df[df$STATUS == 1,]$ID_NIR20, signif(NbreMortsTotal), replace = FALSE, prob = probas)

  if (NROW(goodbye) != 0){
    df[df$ID_NIR20 %in% goodbye, c("STATUS")] = 0 
  }
  print('Nbre de morts total: ')
  print(NROW(df[df$STATUS == 0,]))
  df[df$STATUS == 1, c('AGE')] = df[df$STATUS == 1, c('AGE')] + 1

}
```


### Prédictions INSEE versus la loi de mortalité de micsim

Ici, on compare les prédictions INSEE avec la loi de mortalité paramétriques 
utilisées dans micsim.

```{r Appliquer}
TM_data <- file.path(chemin_tableINSEE, "Mortalite_Ensemble.csv")
TM = read.csv(TM_data, sep=';')

AGE = 0:100
EstimeAv2020 = .00003*exp(0.1*0:100)
EstimeAp2020 = .00003*exp(0.097*0:100)
PredINSEE_F <- vector(mode="numeric", length = 101) 
PredINSEE_H <- vector(mode="numeric", length = 101)

for (t in 0:101){
  tempF = TM[(TM$SEXE == 'F') & (TM$AGE == t), ]
  tempH = TM[(TM$SEXE == 'H') & (TM$AGE == t), ]
  PredINSEE_F[t] = as.numeric(tempF$QUOTIENT) / 100000.
  PredINSEE_H[t] = as.numeric(tempH$QUOTIENT) / 100000.
}

df <- data.frame(AGE, EstimeAv2020, EstimeAp2020, PredINSEE_F, PredINSEE_H)
ggplot(df) + 
  geom_line(aes(x = AGE, y= EstimeAv2020, color = "MicSim <2020"), alpha = 0.8) +
  geom_line(aes(x = AGE, y= EstimeAp2020, color = "MicSim >2020"), alpha = 0.8) +
  geom_point(aes(x = AGE, y= PredINSEE_F, color = "Taux INSEE F"), 
             size = 3, alpha = 0.3) +
  geom_point(aes(x = AGE, y= PredINSEE_H, color = "Taux INSEE H"), 
             size = 3, alpha = 0.3) +
  xlab("Age (ans)") + 
  ylab("Probabilité") + 
  labs (title = "Loi de Mortalité") +
  scale_colour_manual("Méthode", 
                      values = c("MicSim <2020" = "red", 
                                 "MicSim >2020" = "orange", 
                                 "Taux INSEE F" = "darkgreen", 
                                 "Taux INSEE H" = "navyblue")) +
  theme (legend.position = "bottom") 

```



### Annexe : Appliquer le taux de mortalité sur un échantillon généré aléatoirement

Idem, mais cette fois appliqué sur un échantillon de personnes aléatoire.

```{r Version aléatoire}

### Fonction aleatoire
aleatoire <- function(n){
  alea = runif(n, 0, 1)
  aleas = array(' ', dim=n)
  for (i in 1:n){
    if(alea[i] < 0.5){
      aleas[i] = 'F'}
    else{
      aleas[i] = 'M'
    }
  }
  return(aleas)
}
  
### Appliquer le tx de mortalite
NbreIndiv = 1000
ID = 1:NbreIndiv
SEXE = aleatoire(NbreIndiv)
agemin = 15 
agemax = 95
AGE = floor(runif(NbreIndiv, agemin, agemax))
STATUS = array(1, dim=NbreIndiv)
df <- data.frame(ID, SEXE, AGE, STATUS)
 
annees = 10
plot(0, type='n', xlim=c(0,NbreIndiv), ylim=c(0,annees+1))


for (t in 1:annees){ 

  Resultats = ProbaMortalite(df[df$STATUS == 1,], agemin, agemax)
  probas = Resultats$probas 
  NbreMortsTotal = Resultats$NbreMortsTotal 

  goodbye = sample(df[df$STATUS == 1, ]$ID, signif(NbreMortsTotal), replace = FALSE, prob = probas)

  if (NROW(goodbye) != 0){
    df[df$ID %in% goodbye, c("STATUS")] = 0 
  }

  print('Nbre de morts total: ')
  print(NROW(df[df$STATUS == 0,]))
  df[df$STATUS == 1, c('AGE')] = df[df$STATUS == 1, c('AGE')] + 1
  temp = df[df$STATUS == 1, c('ID')]
  points(temp, array(t,  NROW(temp)), col='black', type='p', pch = 19)
  temp = df[df$STATUS == 0, c('ID')]
  points(temp, array(t,  NROW(temp)), col='yellow', type='p', pch = 19)
}
```
