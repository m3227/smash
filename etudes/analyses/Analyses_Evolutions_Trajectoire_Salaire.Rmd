---
title: "Analyse des évolutions de salaires 2010-2021"
author: "BPS/OSAM/DREES - Équipe SMASH"
date: "`r format(Sys.time(), '%d/%m/%Y-%H%M%S')`"
output: 
  pdf_document:
    toc: true
    number_sections: true
  html_document:
    toc: true
    toc_float: true
    number_sections: true
    theme: united
    highlight: tango

params:
  anneeDeb:
    label: "Année de début"
    value: 2009
    input: slider
    min: 2009
    max: 2020
    step: 1
    sep: ""
  anneeFin:
    label: "Année de fin"
    value: 2021
    input: slider
    min: 2019
    max: 2021
    step: 1
    sep: ""
  champ:
    label: "Champ des données"
    value: "hop"
    input: select
    choices: ["hop", "fph"]
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

```{r librairies, include=FALSE}
library(ggplot2)
library(glue)
library(dplyr)
library(viridis)
devtools::load_all("../../../smash-r-pkg")

# Récupération des paramètres
anneeDeb <- params$anneeDeb
anneeFin <- params$anneeFin
champ <- params$champ

# Nombre de personnes minimum dans 1 grade pour faire une régression
nb_min_grade <- 11


# Choix d'agrégat des données
b_agrege = TRUE

# Chemin pour sauver le modèle en local
chemin_local = params$rep_mdl

```

```{r generate_page, eval=FALSE, include=FALSE}
# Exécuter cette cellule pour générer le document
# Vérifier également le chemin relatif vers le package SMASH, ci-dessus
rmd_file <- "Analyses_Evolutions_Trajectoire_Salaire"
rep_out <- "~/"
out_format <- "html"

ch <- "hop"
anDeb <- 2014
anFin <- 2019

out_local <-  sprintf("%s-%s-%d-%d.%s", rmd_file, ch, anDeb, anFin, out_format)

# Génération au format demandé
rmarkdown::render(sprintf("%s.Rmd", rmd_file),
                  output_format = ifelse(out_format=="html", 
                                     "rmdformats::robobook", 
                                     paste(out_format, "_document", sep="")), 
                  output_dir = rep_out,
                  output_file = out_local, 
                  params = list(anneeDeb = anDeb, 
                                anneeFin = anFin, champ = ch))

# Ouverture du fichier : 
cat("Pour ouvrir le document créé : browseURL(\"", rep_out, "\")\n", sep ="")

```


# Présentation 

Ce document étudie les trajectoires de salaires bruts hors primes 
pour différents statuts afin de préparer une modélisation plus fiable
des salaires pour utilisation dans SMASH.

# Chargement des données SIASP

On charge les données SIASP chaînées suivant l'`ID` de `r anneeDeb` à 
`r anneeFin`.

```{r chargement}

cols_select <- c("ID", "STATUT_CONTRAT", "DUREE_CONTRAT", "AGE", "GRADE", "IM",
                 "ECH", "DEPCOM_FONCTION", "EQTP", "S_BRUT", "PRIMES",
                 "IS_ENTRANT", "IS_SORTANT")

df_DataAll <- NULL
df_infos <- NULL

for (an in seq(anneeDeb, anneeFin)) {
  df_an <- getEchantillonSIASP(champ, an, 0, b_agrege = b_agrege, 
                               verbose = 0) %>% 
    select(all_of(cols_select)) %>% filter(abs(EQTP-1)<0.01) %>%
    mutate(AN = an,
           S_BRUT_HP = S_BRUT - PRIMES,
           PRIMES = round(PRIMES),
           STATUT_DUREE = sub("_SANS", "", stringi::stri_c(STATUT_CONTRAT, 
                                                           "_", DUREE_CONTRAT))
           )
  
  # Mise à jour des grades sur la nomenclature anneeFin
  for (an_maj in seq(an, anneeFin)) {
    df_an$GRADE = majGrades(df_an$GRADE, an_maj)
  }

  df_count <- df_an %>% count(STATUT_DUREE, name = as.character(an), sort = T)
  if (is.null(df_infos)) {
    df_infos <- df_count
  } else {
    df_infos <- left_join(df_infos, df_count, by = "STATUT_DUREE")
  }

  df_DataAll <- rbind(df_DataAll, df_an)
}

df_infos[is.na(df_infos)] <- 0

knitr::kable(df_infos, 
             caption = paste("Effectifs des agents à temps plein par",
                             "statut et année") )

```



```{r prepar_donnees}
# Ajoute l'info du nombre d'année de présence par individus et par GRADE

# Count par ID°GRADE pour compter le nombre d'année passées par GRADE et 
# filtrer les cas de GRADEs partiels sur la période
df_count_ids <- df_DataAll %>% count(ID, GRADE, STATUT_DUREE, name = "NB_AN")
df_DataAll <- left_join(df_DataAll, df_count_ids, by = c("ID", "GRADE",
                                                      "STATUT_DUREE"))

# Code département pour couleur
df_DataAll$DEPARTEMENT <- factor(substr(df_DataAll$DEPCOM_FONCTION,1,2))
df_DataAll$REGION <- getRegionFromDepartement(df_DataAll$DEPARTEMENT,
                                              b_noms_courts = T)

```

On regarde la fréquence du nombre d'année des trajectoires observées par statut.
Ce tableau à vocation à nous indiquer les durées représentatives des trajectoires
salariales par statut.


```{r count_nb_an}
df_count_statut <- df_DataAll %>% count(STATUT_DUREE, sort = T)

df_count_nb_an <- data.frame(NB_AN = seq(max(df_DataAll$NB_AN)))

for (statut in df_count_statut$STATUT_DUREE) {
  
  df_nb <- df_DataAll %>% filter(STATUT_DUREE==statut) %>% 
    count(NB_AN, name = statut, sort=T)
  
  df_count_nb_an <- left_join(df_count_nb_an, df_nb, by = "NB_AN")
}

df_count_nb_an[is.na(df_count_nb_an)] <- 0

knitr::kable(df_count_nb_an, 
             caption = paste("Effectifs par nombre d'année (trajectoire salariale par statut") )

```


```{r fcn_plot_trajectoires_salaires}

#' Affichage pour les principaux GRADE en effectifs de personnes tirées au hasard
#'
#' @param df_All data.frame de tous les agents concaténés par année
#' @param statut statut de contrat pour filtrer les agents, "CONT", "MEDI", ...
#' @param nb_grades Nombres de grades à afficher pour le statut. Les grades sont 
#' triés par effectif décroissant et donc les grades les plus représentés sont 
#' affichés en premier, défaut 5
#' @param nb_traj nombre maximal de trajectoires affichées, dans la limite du 
#' nombre de personnes avec une chronique complète sur la période, défaut 50
#' @param x variable d'affichage en abscisse : "AGE" ou "AN" (défaut)
#' @param b_salaire_centre booléen pour indiqué de centrer chaque chronique de 
#' salaires bruts par rapport à sa moyenne sur les années observées
#'
#' @return liste de visualisations plotly, une par grade représenté
#' @export
#'
#' @examples
voir_grades_statut <- function(df_All, statut, nb_grades = 5, 
                               nb_traj = 50, x = "AN",
                               b_salaire_centre = F) {
  
  # Filtre sur le statut 
  df_AllStatut <- df_All %>% filter(STATUT_CONTRAT == statut)
  df_count_grades <- df_AllStatut %>% count(GRADE, sort = T)

  nb_an_max <- max(df_AllStatut$NB_AN)
  nb_an_min <- switch (statut,
    TITH = nb_an_max,
    CONT = min(nb_an_max,6),
    MEDI = min(nb_an_max,8),
    ELEV = min(nb_an_max,3),
    INTE = min(nb_an_max,6),
    min(nb_an_max,10)
  )

  
  l_figs <- list(
    "titre" = htmltools::h3(paste(
      "Trajectoires des salaires Bruts Hors Prime",
      ifelse(b_salaire_centre, "centrées", ""), statut)
      )
  )
  
  for (grade in df_count_grades$GRADE[1:5]) {
    # Tirage de 100 personnes présentes les 3 ans
    df_DataGrade <- df_AllStatut %>% 
      filter(GRADE==grade & NB_AN>=nb_an_min)  %>% 
        group_by(ID) %>% 
      mutate(S_BRUT_HP_CENT = S_BRUT_HP - mean(S_BRUT_HP)) %>%
        ungroup()
    
    if (nrow(df_DataGrade)==0) { 
      cat(statut, ", grade", grade,
          "pas d'agent ayant une trajectoire longue (>", nb_an_min, "ans)\n")
      next }
    
    v_ids_ans <- unique(df_DataGrade$ID)
    if (length(v_ids_ans) > nb_traj) {
        v_ids_test <- sample(v_ids_ans, nb_traj)
    } else {
      v_ids_test <- v_ids_ans
    }
  
    
    # Visu des trajectoires
    if (x=="AGE") {
      if (b_salaire_centre) {
        fig <- ggplot(df_DataGrade %>% filter(ID %in% v_ids_test), 
             aes(AGE, S_BRUT_HP_CENT, group = ID, colour=REGION))
      } else {
        fig <- ggplot(df_DataGrade %>% filter(ID %in% v_ids_test), 
             aes(AGE, S_BRUT_HP, group = ID, colour=REGION))
      }
       
    } else {
      if (b_salaire_centre) {
        fig <- ggplot(df_DataGrade %>% filter(ID %in% v_ids_test), 
             aes(AN, S_BRUT_HP_CENT, group = ID, colour=REGION))
      } else {
        fig <- ggplot(df_DataGrade %>% filter(ID %in% v_ids_test), 
             aes(AN, S_BRUT_HP, group = ID, colour=REGION))
      }    } 

    fig <- fig + geom_line() + geom_point() + 
      labs(x= x, 
           title = paste0(length(v_ids_test), 
                          " Trajectoires salariales aléatoires ", 
                          statut," grade ", 
                          grade, ", >", nb_an_min, " ans")) +
      theme_minimal()
    
    l_figs[[grade]] <- ggplotly(fig)
  }
  
  htmltools::tagList(l_figs)
  
}

```



# Contractuels

## Évolutions de salaires en fonction de l'age

Les premiers modèles d'évolution de salaires ont été calculés sur les 
données en fonction de l'âge. Les graphiques ci-dessous montrent la 
dispersion des salaires en fonction de l'âge. 

```{r traj_cont_salaires_age}

voir_grades_statut(df_DataAll, "CONT", 5, x = "AGE")

```

## Évolutions de salaires en fonction de l'anéee

```{r traj_cont_salaires_an}

voir_grades_statut(df_DataAll, "CONT", 5, x = "AN")

```

## Évolutions de salaires centrés en fonction de l'anéee

Afin d'essayer de réduire encore la variance des données observées,
nous proposons de centrer les salaires brut hors primes par rapport à la 
valeur moyenne sur la chronique observée.

```{r traj_cont_centre}

voir_grades_statut(df_DataAll, "CONT", 5, x = "AN", b_salaire_centre = T)

```

## Premières conclusions

On observe mieux les tendances d'évolution des salaires en s'affranchissant 
de l'âge des agents ainsi que la valeur initiale du salaire. 

Cela ne va donc pas dans le sens d'une évolution proportionnelle 
du salaire mais plutôt dans le sens d'une évolution fixe annuelle 
par grade. 

# Autres statuts 

Nous regardons de la même manière les tendances pour les autres statuts de contrat. Cette fois-ci on se concentre sur les évolutions annuelles et les trajectoires centrées

## Médecin


On observe les trajectoires par année des salaires brut hors prime (non centrés
et centrés) pour les principaux grades en terme d'effectifs.

```{r traj_salaires_medi}

voir_grades_statut(df_DataAll, "MEDI", nb_grades = 4, nb_traj = 50,
                   x = "AN", b_salaire_centre = F)

voir_grades_statut(df_DataAll, "MEDI", nb_grades = 4, nb_traj = 50,
                   x = "AN", b_salaire_centre = T)

```

## Internes

On observe les trajectoires par année des salaires brut hors prime (non centrés
et centrés) pour les principaux grades en terme d'effectifs.


```{r traj_salaires_inte}

voir_grades_statut(df_DataAll, "INTE", nb_grades = 4, nb_traj = 100,
                   x = "AN", b_salaire_centre = F)
voir_grades_statut(df_DataAll, "INTE", nb_grades = 4, nb_traj = 100,
                   x = "AN", b_salaire_centre = T)

```

## Titulaires

On observe les trajectoires par année des salaires brut hors prime (non centrés
et centrés) pour les principaux grades en terme d'effectifs.

```{r traj_salaires_tith}

voir_grades_statut(df_DataAll, "TITH", nb_grades = 5, nb_traj = 100, 
                   x = "AN", b_salaire_centre = F)
voir_grades_statut(df_DataAll, "TITH", nb_grades = 5, nb_traj = 100, 
                   x = "AN", b_salaire_centre = T)

```

# Analyse individuelle

Le bloc ci-dessous permet d'analyser différents cas spéciaux identifiés ci-dessus, 
en affichant leur données principales, filtrées sur l'ID. L'ID est obtenue à 
l'aide des infos-bulles sur les courbes de salaires. 

```{r analyse_ID}
library(flextable) 
set_flextable_defaults(big.mark = "")


l_ids <- list("MEDI salaire parabolique" = 953701,
              "TITH avec pic de salaire" = 10928556, 
              "CONT hors norme" = 1718757)

voir_detail_ID <- function(id_test) {
  df_ID <- df_DataAll %>% filter(ID == id_test)
  
  colourer <- scales::col_numeric(
    palette = c("transparent", "orange"),
    domain = c(min(df_ID$S_BRUT_HP),max(df_ID$S_BRUT_HP)))
  
  statut <- df_ID$STATUT_CONTRAT[1]
  
  l_cols <- switch(
    statut, 
    MEDI = c("AN", "GRADE", "IM", "ECH",  
             "S_BRUT_HP", "PRIMES",  "DEPCOM_FONCTION", "IS_SORTANT", "IS_ENTRANT"),
    CONT = c("AN", "DUREE_CONTRAT", "GRADE",
             "S_BRUT_HP", "PRIMES",  "DEPCOM_FONCTION", "IS_SORTANT", "IS_ENTRANT"),
    TITH = c("AN", "GRADE", "IM", "ECH",
             "S_BRUT_HP", "PRIMES",  "DEPCOM_FONCTION", "IS_SORTANT", "IS_ENTRANT"),
    INTE = c("AN", "GRADE",
             "S_BRUT_HP", "PRIMES",  "DEPCOM_FONCTION", "IS_SORTANT", "IS_ENTRANT"),
    c("AN", "GRADE", "IM", "ECH", 
             "S_BRUT_HP", "PRIMES",  "DEPCOM_FONCTION", "IS_SORTANT", "IS_ENTRANT")
  )
    
  
  ft <- flextable(df_ID %>% select(all_of(l_cols))) %>% 
    set_table_properties(layout = "autofit", width = .8) %>% 
    bg(bg = colourer,
       j = ~S_BRUT_HP,
       part = "body") %>% 
    hline() %>% 
    set_caption(caption = paste("ID", id_test, "statut", statut))

  return(ft)
}

l_tags <- list()
idx <- 1
for (test in names(l_ids)) {
  l_tags[[paste0("h2_",idx)]] <- htmltools::h2(paste(test, ", ID ", l_ids[[test]]))
  l_tags[[paste0("ft_",idx)]] <- htmltools_value(voir_detail_ID(l_ids[[test]]))
  idx <- idx + 1
}
rm(idx)

htmltools::tagList(l_tags)

```



# Comparaison salaire brut CONT et TITH

## Trajectoires multiples

On compare cette fois-ci les salaires bruts (dont primes) de 2 cohortes d'agents 
titulaires et contractuels sur les principaux GRADE.


```{r compar_tith_cont}


# Filtre sur le statut 
df_AllStatut <- df_DataAll %>% filter(STATUT_CONTRAT == "CONT" | 
                                    STATUT_CONTRAT == "TITH")

df_count_grades <- df_AllStatut %>% count(GRADE, sort = T)

nb_an_max <- max(df_AllStatut$NB_AN)
nb_an_min <- min(nb_an_max, 8)
nb_traj <- 50

l_figs <- list()
  
for (grade in df_count_grades$GRADE[1:5]) {
  # Tirage de 100 personnes présentes les 3 ans
  df_DataGrade <- df_AllStatut %>% 
    filter(GRADE==grade & NB_AN>=nb_an_min)  %>% 
      group_by(ID) %>% 
    mutate(S_BRUT_HP_CENT = S_BRUT_HP - mean(S_BRUT_HP)) %>%
      ungroup()
  
  if (nrow(df_DataGrade)==0) { 
    cat(" Grade", grade,
        "pas d'agent ayant une trajectoire longue (>", nb_an_max, "ans)\n")
    next }
  
  v_ids_cont <- unique(df_DataGrade$ID[df_DataGrade$STATUT_CONTRAT=="CONT"])
  v_ids_tith <- unique(df_DataGrade$ID[df_DataGrade$STATUT_CONTRAT=="TITH"])
  
  # Tirage des ID CONT
  if (length(v_ids_cont) > nb_traj) {
    v_ids_test <- sample(v_ids_cont, nb_traj)
  } else {
    v_ids_test <- v_ids_cont
  }
  # Concatène les ID des TITH
  if (length(v_ids_tith) > nb_traj) {
    v_ids_test <- c(v_ids_test, sample(v_ids_tith, nb_traj))
  } else {
    v_ids_test <- c(v_ids_test, v_ids_tith)
  }
  

  
  # Visu des trajectoires
  fig <- ggplot(df_DataGrade %>% filter(ID %in% v_ids_test), 
           aes(AN, S_BRUT, group = ID, colour=STATUT_CONTRAT))
  
  fig <- fig + geom_line() + geom_point() + 
    labs(x= "Année", 
         title = paste0(length(v_ids_test), 
                        " Trajectoires salariales aléatoires, grade ", 
                        grade, ", >", nb_an_min, " ans")) +
    theme_minimal()
  
  l_figs[[paste0("titre",grade)]] <- htmltools::h2(paste(
    "Comparaison TITH/CONT des salaires Bruts", grade))
  
  l_figs[[grade]] <- ggplotly(fig)
}
  
htmltools::tagList(l_figs)
  


```


## Salaires moyens

```{r compar_moy_median}
l_figs <- list()
  
for (grade in df_count_grades$GRADE[1:5]) {
  # Sélection des personnes présentes la période min
  df_DataGrade <- df_AllStatut %>% 
    filter(GRADE==grade & NB_AN>=nb_an_min)  %>% 
      group_by(ID) %>% 
    mutate(S_BRUT_HP_CENT = S_BRUT_HP - mean(S_BRUT_HP)) %>%
      ungroup()
  
  if (nrow(df_DataGrade)==0) { 
    cat(" Grade", grade,
        "pas d'agent ayant une trajectoire longue (>", nb_an_max, "ans)\n")
    next }
  
  # Calcul des stats et réorganisation du df
  df_stats <- df_DataGrade %>% group_by(STATUT_CONTRAT, AN) %>%
    summarise(S_BRUT_MOY = mean(S_BRUT), 
              S_BRUT_MEDIAN = median(S_BRUT),
              S_BRUT_SD = sd(S_BRUT),
              .groups = "drop")
  
  df_visu <- rbind(df_stats %>% mutate(S_BRUT = S_BRUT_MOY,
                                       STAT = "Moyenne"), 
                   df_stats %>% mutate(S_BRUT = S_BRUT_MEDIAN,
                                       STAT = "Médiane"))
  
  # The errorbars overlapped, so use position_dodge to move them horizontally
  pd <- position_dodge(0.1) # move them .05 to the left and right

  # ggplot(tgc, aes(x=dose, y=len, colour=supp)) + 
  # geom_errorbar(aes(ymin=len-se, ymax=len+se), width=.1, position=pd) +
  # geom_line(position=pd) +
  # geom_point(position=pd)
  
  # Visu des trajectoires
  fig <- ggplot(df_visu, aes(AN, S_BRUT, colour=STATUT_CONTRAT, linetype=STAT)) + 
    geom_line(position = pd) +
    geom_point(position = pd) +
    geom_errorbar(data = df_visu %>% filter(STAT=="Moyenne"), 
                  aes(ymin=S_BRUT-0.5*S_BRUT_SD, 
                      ymax=S_BRUT+0.5*S_BRUT_SD, linetype=STAT),
                  position = pd,
                  width=.1) + 
    scale_linetype_manual(values = c("dotted", "solid")) +
    labs(x= "Année", 
         title = paste0("Trajectoires salariales moyennes, grade ", 
                        grade, ", >", nb_an_min, " ans\nTITH:", 
                        sum(df_DataGrade$STATUT_CONTRAT=="TITH"), 
                        ", CONT:", sum(df_DataGrade$STATUT_CONTRAT=="CONT"), 
                        ", écarts-type sur moyenne")) +
    theme_minimal() + 
    theme(plot.title = element_text(size=12))
  
  l_figs[[paste0("titre",grade)]] <- htmltools::h2(paste(
    "Comparaison TITH/CONT des salaires Bruts moyen et médian", grade))
  
  l_figs[[grade]] <- ggplotly(fig)
}
  
htmltools::tagList(l_figs)
  


```
```

