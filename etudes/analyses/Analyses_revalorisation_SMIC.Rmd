---
title: "Analyses revalorisation SMIC"
author: "DREES/OSAM/BPS - Équipe SMASH"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  pdf_document:
    toc: true
    number_sections: true
  html_document:
    toc: true
    toc_float: true
    number_sections: true
    theme: united
    highlight: tango
  rmdformats::downcute:
    toc_depth: 2
    number_sections: true
    theme: united
    highlight: tango

params:
  im_smic_old:
    label: "IM du SMIC en cours, pour remplacer les indices inférieurs"
    value: 329
  im_smic:
    label: "Nouvel IM du SMIC"
    value: 361
  date_smic:
    label: "date pour la revalorisation du SMIC, format 2023-01-01"
    value: "2023-01-01"
  annee:
    label: "Année de réfèrence pour analyser les effectifs impactés"
    value: 2020
  rep_local:
    label: "Répertoire local ou sauver la revalorisation"
    value: "~"
  b_pre_reclass:
    label: "Flag pour pre-réclasser les données 2021"
    value: FALSE
    
---


```{r generate_page, eval=FALSE, include=FALSE}
# Exécuter cette cellule pour générer le document HTML/PDF
# Vérifier également le chemin relatif vers le package SMASH (vers la ligne 72)

# Paramètres de simulation ---- 
new_im_smic = 361
annee = 2021
b_pre_reclass = annee==2021 && TRUE #  FALSE # 


old_im_smic <- dplyr::case_when(annee==2020 ~ 329, 
                                annee==2021 ~ 343, 
                                TRUE  ~ 352)

rep_local = paste0("~/", strftime(Sys.time(),"%y%m%d-%Hh%M-Revalo-SMIC"))
date_smic = "2023-01-01"

# Noms des sorties
rmd_file <- "Analyses_revalorisation_SMIC"
out_format <- "html" # "pdf"
out_local <-  sprintf("%s_im_%d-%d%s.%s", rmd_file, new_im_smic, annee, 
                      ifelse(b_pre_reclass, "_pre_reclass_2021", ""), out_format)

# Génération Markdown
rmarkdown::render(paste(rmd_file,".Rmd", sep = ""),
                  output_format = ifelse(out_format=="html", 
                                         "rmdformats::robobook", 
                                         paste(out_format, "_document", sep="")), 
                  output_dir = rep_local, 
                  output_file = out_local, 
                  params = list(im_smic_old = old_im_smic,
                                im_smic = new_im_smic, 
                                date_smic = date_smic,
                                annee = annee, 
                                rep_local = rep_local,
                                b_pre_reclass = b_pre_reclass) 
                  )

```


```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```

```{r imports_et_options, include=FALSE}
library(dplyr)
library(plotly)
library(xlsx)
# Chargement package SMASH, dev local (vérifier le chemin)
devtools::load_all("../../../smash-r-pkg/")

```


# Impact de la revalorisation du SMIC

## Chargement des données 

> Champs des données : SIASP `r params$annee` FPH, hors DOM. Personnels Titulaires uniquement.

Dans un premier temps nous chargeons les données SIASP les personnels dont 
l'indice est inférieur au nouvel indice de revalorisation du SMIC : 
`r params$im_smic`.

Pour les agents sélectionnés, l'indice minimum est également recalé sur l'IM 
du SMIC en cours, à savoir `r params$im_smic_old` pour l'année `r params$annee`.

## Sélections des agents et effectifs Aides-soignants 



```{r chargement_donnees, echo=FALSE}
# Récupération des paramètres
annee <- params$annee
im_smic <- params$im_smic
im_smic_old <- params$im_smic_old
rep_local <- params$rep_local
date_smic <- params$date_smic
b_pre_reclass <- params$b_pre_reclass

date_avant_evol <- strftime(as.Date(date_smic)-1, "%Y-%m-%d")
annee_avant_evol <- as.numeric(substr(date_avant_evol,1,4))

# Chargement des données au format SMASH
df <- smash::getEchantillonSIASP("fph", annee, 0, b_agrege = T)

# Sélection des personnes ayant un indice < im_smic

df_select <- df %>% filter(IM<im_smic) %>% arrange(ID) %>% 
  filter(STATUT_CONTRAT=="TITH")
nb_all <- nrow(df_select)

# Filtre sur les DOM
df_select <- df_select %>% 
  filter(!(substr(DEPCOM_FONCTION,1,3) %in% c("975", "976", "977", "978")) &
             substr(DEPCOM_FONCTION,1,2)  != "99")

df_infos_IM <- df_select %>% count(IM, name='Total') %>% arrange(IM)

# Recalage des indices inférieurs sur l'ancienne valeur du SMIC
is_inf_old <- df_select$IM < im_smic_old
df_select$IM[is_inf_old] <- im_smic_old

df_infos_effectifs <- data.frame("Nb_personnes_à_revaloriser" = nrow(df_select),
                        "Nb_IM_inf_SMIC_actuel_recalés" = sum(is_inf_old), 
                        "Nb_DOM_exclus" = nb_all - nrow(df_select))

knitr::kable(df_infos_effectifs, 
             caption = paste("Nombre de titulaires impactés par la revalorisation, données", annee),
             col.names = str_replace_all(names(df_infos_effectifs), "_", " "), 
             format.args = list(big.mark = " "))

# Point d'indice
objBDD <- GrillesIndiciaires$new()
point_indice <- objBDD$getPointIndice(date_smic)$VALEUR_ANNUELLE

```

```{r comptage_as_ap}
df_count_corps_all <- df %>% 
  filter(startsWith(CORPS, "Aide-soignant") & STATUT_CONTRAT %in% 
           c("TITH", "CONT")) %>% count( CORPS, STATUT_CONTRAT,sort = T, name = "Total")

df_count_corps_all <- rbind(df_count_corps_all,
                            data.frame(CORPS = "Total", 
                                       STATUT_CONTRAT = "",
                                       Total = sum(df_count_corps_all$Total)) )

df_count_corps_select <- df_select %>% 
  filter(startsWith(CORPS, "Aide-soignant") & STATUT_CONTRAT %in% 
           c("TITH", "CONT")) %>% count(CORPS, STATUT_CONTRAT, sort = T, name = "Total")

df_count_corps_select <- rbind(df_count_corps_select,
                               data.frame(CORPS = "Total", 
                                          STATUT_CONTRAT = "",
                                          Total = sum(df_count_corps_select$Total)) )

knitr::kable(rbind(data.frame(CORPS="Tous effectifs", 
                              STATUT_CONTRAT = "", Total = ""),
                   df_count_corps_all, 
                   data.frame(CORPS="Impactés par la revalorisation", 
                              STATUT_CONTRAT = "", Total = ""),
                   df_count_corps_select), 
             caption = paste("Nombre de d'aide-soignant titulaires", annee), 
             # col.names = str_replace_all(names(df_infos_effectifs), "_", " "), 
             format.args = list(big.mark = " "))

```


# Calage des grades `r annee` au `r date_avant_evol`

Nous utilisons les fonctions SMASH pour recaler les IM et GRADE des effectifs 
sur la nomenclature du jour d'avant l'évolution souhaitée. 

Pour cela, on met à jour Grilles et Grades selon les évolutions de 
réglementation contenues dans la BDD SMASH.

```{r calage_grade, echo=TRUE}

# Recopie des agents pour mise en forme
l_revalo <- list()
if (b_pre_reclass==TRUE) {
  # Test en reclassant les agents 2021 selon les évolutions 2021 ...
  cat("Reclassement préalable des données 2021 sur la nomemclature de 2021")
  l_revalo[[as.character(annee)]] <- 
    smash::majReclassement(df_select, 2021, majsbrut = TRUE)
} else {
  # Recopie des Données SIASP année initiale
  l_revalo[[as.character(annee)]] <- df_select
}

# Reclassement 
for (an in seq(annee+1, annee_avant_evol)) {
  strAn <- as.character(an)
  strAnPrec <- as.character(an-1)
  calTime <- ifelse(an==annee_avant_evol, date_avant_evol, an)
  
  l_revalo[[strAn]]  <- smash::majReclassement(l_revalo[[strAnPrec]], 
                                                 calTime, majsbrut = TRUE)
  
}


# Ajoute les colonnes des nouveaux indices en 2022
df_revalo <- df_select

df_revalo$IM_1 <- l_revalo[[strAnPrec]]$IM
df_revalo$ECH_1 <- l_revalo[[strAnPrec]]$ECH
df_revalo$GRADE_1 <- l_revalo[[strAnPrec]]$GRADE

df_revalo$IM_NEW <- l_revalo[[strAn]]$IM
df_revalo$ECH_NEW <- l_revalo[[strAn]]$ECH
df_revalo$GRADE_NEW <- l_revalo[[strAn]]$GRADE

# Ajoute les colonnes des libellés/filières/corps 2022
df_infos_grades_new <- objBDD$getInfoGrades(df_revalo$GRADE_NEW, fill_NA = T)
df_revalo$FILIERE_NEW <- df_infos_grades_new$FILIERE
df_revalo$CORPS_NEW <- df_infos_grades_new$CORPS
df_revalo$LIBELLE_NEW <- df_infos_grades_new$LIBELLE
# NA -> Hors filière
df_revalo$FILIERE_NEW[is.na(df_revalo$FILIERE_NEW)] <- "Personnel hors filière"
df_revalo$CORPS_NEW[is.na(df_revalo$CORPS_NEW)] <- "Personnel hors filière"

# On fait évoluer le salaire proportionnellement à l'évolution d'indice
df_revalo$S_BRUT_NEW <- df_revalo$S_BRUT * df_revalo$IM_NEW / df_revalo$IM

# Ajout des grilles au début et avant revalo
df_revalo$GRILLE <- objBDD$getInfoGrilles(df_revalo$GRADE, 
                                             sprintf("%d-12-31", annee))$GRILLE
df_revalo$GRILLE_NEW <- objBDD$getInfoGrilles(df_revalo$GRADE_NEW,
                                                 date_avant_evol)$GRILLE

nb_all <- nrow(df_revalo)

df_revalo_all <- df_revalo # Stockage avant suppression des IM>SMIC
df_revalo <- df_revalo %>% filter(IM_NEW < im_smic)

cat("\nAprès calage des grades à", date_avant_evol,",",  
    nb_all - nrow(df_revalo), "personnes supprimés", 
    "de l'analyse car leur IM revalorisé est supérieur à", im_smic,"\n")

```


# Calcul des revalorisations

Le coût de la revalorisation du traitement indiciaire est obtenue en multipliant 
la différence d'IM par le point d'indice pondéré par l'EQTP de la personne, soit :

`COUT_REVALO_IND = (im_smic - IM) * point_indice * EQTP`

Sur le salaire BRUT, une estimation est donnée à titre indicatif du coût de la 
revalorisation à partir du ratio du nouvel IM du SMIC sur l'IM de 
la personne. Dans ce cas, on fait l'approximation que le S_BRUT augmente 
proportionnellement à l'augmentation d'indice, soit :

`COUT_S_BRUT_REVALO = S_BRUT * (IM_SMIC/IM - 1)`


```{r calcul_revalo}

# Cout par personne !
df_revalo <- df_revalo %>% mutate(
  COUT_REVALO_IND = (im_smic - IM) * point_indice * EQTP,
  COUT_REVALO_IND_NEW = (im_smic - IM_NEW) * point_indice * EQTP,
  COUT_REVALO_S_BRUT = S_BRUT * (im_smic/IM - 1),
  COUT_REVALO_S_BRUT_NEW = S_BRUT_NEW * (im_smic/IM_NEW - 1)
)

```


```{r agregat_grade_im_ech}
  
df_grade_im_ech <- df_revalo %>% group_by(GRADE, ECH, IM) %>%
  summarise(GRADE_NEW = first(GRADE_NEW),
            GRILLE_NEW = first(GRILLE_NEW),
            GRILLE = first(GRILLE),
            ECH_NEW = first(ECH_NEW),
            IM_NEW = first(IM_NEW), 
            Nb = n(),
            EQTP = sum(EQTP),
            SUM_IND = sum(IM), 
            COUT_REVALO_IND = sum(COUT_REVALO_IND),
            COUT_REVALO_IND_NEW = sum(COUT_REVALO_IND_NEW),
            S_BRUT = sum(S_BRUT), 
            S_BRUT_NEW = sum(S_BRUT_NEW),
            COUT_REVALO_S_BRUT = sum(COUT_REVALO_S_BRUT),
            COUT_REVALO_S_BRUT_NEW = sum(COUT_REVALO_S_BRUT_NEW),
            LIBELLE_NEW = first(LIBELLE_NEW), 
            CORPS_NEW = first(CORPS_NEW),
            FILIERE_NEW = first(FILIERE_NEW),
            .groups = "drop" ) %>% 
  mutate(SUM_IND_NEW = IM_NEW * Nb) %>% 
  arrange(desc(Nb))

# Réorganisation des colonnes
df_grade_im_ech <-  df_grade_im_ech %>% 
  select(GRADE, ECH, IM, GRILLE, GRADE_NEW, ECH_NEW, IM_NEW, GRILLE_NEW, 
         Nb, EQTP, SUM_IND, SUM_IND_NEW, COUT_REVALO_IND, COUT_REVALO_IND_NEW, 
         LIBELLE_NEW, CORPS_NEW, FILIERE_NEW, S_BRUT, S_BRUT_NEW, 
         COUT_REVALO_S_BRUT, COUT_REVALO_S_BRUT_NEW)


# Effectifs reclassement (sans suppression des IM>im_smic après reclassement)
df_grade_im_ech_all <- df_revalo_all %>% group_by(GRADE, ECH, IM) %>%
  summarise(GRILLE = first(GRILLE),
            GRADE_1 = first(GRADE_1),
            ECH_1 = first(ECH_1),
            IM_1 = first(IM_1), 
            GRADE_NEW = first(GRADE_NEW),
            ECH_NEW = first(ECH_NEW),
            IM_NEW = first(IM_NEW), 
            GRILLE_NEW = first(GRILLE_NEW),
            Nb = n(),
            EQTP = sum(EQTP),
            LIBELLE_NEW = first(LIBELLE_NEW), 
            CORPS_NEW = first(CORPS_NEW),
            FILIERE_NEW = first(FILIERE_NEW),
            .groups = "drop" ) %>%
  arrange(desc(Nb)) %>% 
  select(GRADE, ECH, IM, GRILLE, Nb, EQTP, GRADE_1, ECH_1, IM_1, 
         GRADE_NEW, ECH_NEW, IM_NEW, GRILLE_NEW)


```

```{r save_table_tith}
# Création du fichier
fic_xls <- sprintf("%s/Revalorisation_SMIC_%d_en_%d%s.xlsx", 
                  rep_local, im_smic, annee, 
                  ifelse(b_pre_reclass, "_pre_reclass_2021",""))

xlsx::write.xlsx(as.data.frame(df_grade_im_ech), fic_xls, 
                 sheetName ="TITH GRADE ECH IM<IM_SMIC", 
                 row.names = FALSE, 
                 append = FALSE)

xlsx::write.xlsx(as.data.frame(df_grade_im_ech_all), fic_xls, 
                 sheetName ="COMPAR GRADE ECH IM ALL",
                 row.names = FALSE, 
                 append = TRUE)


cat("Sauvegarde dans le fichier Excel :", fic_xls);

```

## Coût global

```{r cout_global}
df_cout_global <- df_revalo %>% 
  summarise(Nb = n(),
            EQTP = sum(EQTP),
            SUM_IND = sum(IM), 
            SUM_IND_NEW = sum(IM_NEW),
            COUT_REVALO_IND = sum(COUT_REVALO_IND),
            COUT_REVALO_IND_NEW = sum(COUT_REVALO_IND_NEW),
            S_BRUT = sum(S_BRUT), 
            S_BRUT_NEW = sum(S_BRUT_NEW),
            COUT_REVALO_S_BRUT = sum(COUT_REVALO_S_BRUT),
            COUT_REVALO_S_BRUT_NEW = sum(COUT_REVALO_S_BRUT_NEW)
            )

xlsx::write.xlsx(as.data.frame(df_cout_global), fic_xls, 
                 sheetName = "Cout global", 
                 row.names = FALSE, 
                 append = TRUE)

knitr::kable(df_cout_global, 
             caption = paste("Impact global sur les TITH de la revalorisation",
                             "du SMIC à l'IM", im_smic), 
             format.args = list(big.mark = " ") )

```


## Revalorisation par filière

```{r agregat_filiere}

df_cout_filiere <- df_revalo %>% group_by(FILIERE_NEW) %>% 
  summarise(Nb = n(),
            EQTP = sum(EQTP),
            SUM_IND = sum(IM), 
            SUM_IND_NEW = sum(IM_NEW),
            COUT_REVALO_IND = sum(COUT_REVALO_IND),
            COUT_REVALO_IND_NEW = sum(COUT_REVALO_IND_NEW),
            S_BRUT = sum(S_BRUT), 
            S_BRUT_NEW = sum(S_BRUT_NEW),
            COUT_REVALO_S_BRUT = sum(COUT_REVALO_S_BRUT),
            COUT_REVALO_S_BRUT_NEW = sum(COUT_REVALO_S_BRUT_NEW),
            .groups = "drop" ) %>% 
  arrange(desc(Nb))

                       
xlsx::write.xlsx(as.data.frame(df_cout_filiere), 
                 fic_xls, sheetName ="Cout par filière", 
                 row.names = FALSE, 
                 append = TRUE)

knitr::kable(df_cout_filiere, 
             caption = paste("Impact de la revalorisation du SMIC à l'IM", 
                             im_smic, "par grade"), 
             digits = 0,
             format.args = list(big.mark = " ") )

```


## Revalorisation par corps

```{r agregat_corps}
df_cout_corps <- df_revalo %>% group_by(CORPS_NEW) %>% 
  summarise(FILIERE_NEW = first(FILIERE_NEW),
            Nb = n(),
            EQTP = sum(EQTP),
            SUM_IND = sum(IM), 
            SUM_IND_NEW = sum(IM_NEW),
            COUT_REVALO_IND = sum(COUT_REVALO_IND),
            COUT_REVALO_IND_NEW = sum(COUT_REVALO_IND_NEW),
            S_BRUT = sum(S_BRUT), 
            S_BRUT_NEW = sum(S_BRUT_NEW),
            COUT_REVALO_S_BRUT = sum(COUT_REVALO_S_BRUT),
            COUT_REVALO_S_BRUT_NEW = sum(COUT_REVALO_S_BRUT_NEW),
            .groups = "drop" ) %>% 
  arrange(desc(Nb))

                       
xlsx::write.xlsx(as.data.frame(df_cout_corps),
                 fic_xls, sheetName ="Cout par corps", 
                 row.names = FALSE, 
                 append = TRUE)

knitr::kable(df_cout_corps,  
             caption = paste("Impact de la revalorisation du SMIC à l'IM", 
                             im_smic, "par corps"), 
             digits = 0,
             format.args = list(big.mark = " ") )

```


## Revalorisation par grade

```{r agregat_grade}

df_cout_grade <- df_revalo %>% group_by(GRADE_NEW) %>% 
  summarise(GRILLE_NEW = first(GRILLE_NEW), 
            GRADE = first(GRADE),
            GRILLE = first(GRILLE), 
            Nb = n(), 
            EQTP = sum(EQTP),
            SUM_IND = sum(IM), 
            SUM_IND_NEW = sum(IM_NEW),
            COUT_REVALO_IND = sum(COUT_REVALO_IND),
            COUT_REVALO_IND_NEW = sum(COUT_REVALO_IND_NEW),
            S_BRUT = sum(S_BRUT), 
            S_BRUT_NEW = sum(S_BRUT_NEW),
            COUT_REVALO_S_BRUT = sum(COUT_REVALO_S_BRUT),
            COUT_REVALO_S_BRUT_NEW = sum(COUT_REVALO_S_BRUT_NEW),
            CORPS_NEW = first(CORPS_NEW),
            FILIERE_NEW = first(FILIERE_NEW)
            ) %>% 
  arrange(desc(Nb))


xlsx::write.xlsx(as.data.frame(df_cout_grade),
                 fic_xls, sheetName = paste("Cout par grade", annee_avant_evol), 
                 row.names = FALSE, 
                 append = TRUE)


knitr::kable(df_cout_grade,  
             caption = paste("Impact de la revalorisation du SMIC à l'IM", 
                             im_smic, "par grade", annee_avant_evol), 
             digits = 0,
             format.args = list(big.mark = " ") )

```



# Revalorisation des contractuels

## Principe et sélection des données

Nous investiguons ici s'il est possible d'estimer le coût de la revalorisation 
pour les contractuels (STATUT_CONTRAT = "CONT"). 

La sélection des personnes dont le salaire brut hors prime est inférieur au
SMIC est effectué par comparaison au nouveau SMIC brut annuel pondéré par 
l'EQTP de l'agent : 

`(S_BRUT-PRIMES) < smic_brut_new * EQTP`

Nous voyons que parmi les agents sélectionnés, si la majorité sont des 
aides-soignants et ouvriers de catégorie C, une partie sont des agents qui ne
sont certainement pas payés au SMIC, et dont une partie des déclarations 
n'est pas sans doute pas bonne (EQTP/Nb d'heures déclarés, ...). 
Aussi, nous excluons également les agents de catégorie "A" du calcul. 


```{r select_cont}
# SMIC BRUT = Indice smic * valeur du Pt annuel
smic_brut_new <- im_smic * point_indice
# En 2022 : 
# 01/08/2022	1678.95
# 01/05/2022	1645.58
# 01/01/2022	1603.12
smic_brut_actuel <- 1678.95 * 12

df_cont <- df %>% filter(STATUT_CONTRAT=="CONT") %>% 
  mutate(S_BRUT_HP = S_BRUT - PRIMES, 
         CATEGORIE = objBDD$getCategorie(l_grades = GRADE)$CATEGORIE) %>% 
  filter(S_BRUT_HP < (smic_brut_new * EQTP) & CATEGORIE != "A") %>% 
  arrange(ID)
  

nb_all <- nrow(df_cont)

# Filtre sur les DOM
df_cont <- df_cont %>% 
  filter(!(substr(DEPCOM_FONCTION,1,3) %in% 
             c("975", "976", "977", "978")) &
           substr(DEPCOM_FONCTION,1,2)  != "99")

is_inf_actuel <- df_cont$S_BRUT_HP < smic_brut_actuel * df_cont$EQTP

# Recalage au SMIC à la valeur actuelle
df_cont$S_BRUT_HP[is_inf_actuel] <- 
  smic_brut_actuel * df_cont$EQTP[is_inf_actuel]
  
df_infos_effectifs <- data.frame(
  "SMIC_brut_annuel_actuel" = smic_brut_actuel,
  "Nouveau_SMIC_brut" = smic_brut_new,
  "Nb_contractuels_à_revaloriser" = nrow(df_cont),
  "Nb_IM_inf_SMIC_actuel_recalés" = sum(is_inf_actuel), 
  "Nb_DOM_exclus" = nb_all - nrow(df_cont)
  )

knitr::kable(df_infos_effectifs, 
             caption = paste("Nombre de contractuels impactés par",
                             "la revalorisation, données", annee),
             col.names = str_replace_all(names(df_infos_effectifs), "_", " "), 
             format.args = list(big.mark = " "))


```

## Calage des grades `r annee` au `r date_avant_evol`

Nous appliquons le reclassement des grades de l'année initiale pour pouvoir 
ensuite analyser le coût par grade, corps et filières mis à jour à la date
d'évolution du SMIC. 

```{r cout_cont}
# Calcul du nouveau S_BRUT sur la base du SMIC
df_cont$S_BRUT_HP_NEW <- smic_brut_new * df_cont$EQTP
df_cont$COUT_REVALO <- df_cont$S_BRUT_HP_NEW - df_cont$S_BRUT_HP

# Recalage des grades en 2023
# Recopie des agents pour mise en forme
l_revalo <- list()

if (b_pre_reclass==TRUE) {
  # Test en reclassant les agents 2021 selon les évolutions 2021 ...
  cat("Reclassement préalable des données 2021 sur la nomemclature de 2021")
  l_revalo[[as.character(annee)]] <- 
    smash::majReclassement(df_cont, 2021, majsbrut = FALSE)
} else {
  # Recopie des Données SIASP année initiale
  l_revalo[[as.character(annee)]] <- df_cont
}

# Reclassement 
for (an in seq(annee+1, annee_avant_evol)) {
  strAn <- as.character(an)
  strAnPrec <- as.character(an-1)
  calTime <- ifelse(an==annee_avant_evol, date_avant_evol, an)
  
  l_revalo[[strAn]]  <- smash::majReclassement(l_revalo[[strAnPrec]],
                                               calTime, majsbrut = FALSE)
  
}

df_cont$GRADE_NEW <- l_revalo[[strAn]]$GRADE

df_infos_grades_new <- objBDD$getInfoGrades(df_cont$GRADE_NEW, fill_NA = T)
df_cont$FILIERE_NEW <- df_infos_grades_new$FILIERE
df_cont$CORPS_NEW <- df_infos_grades_new$CORPS
df_cont$LIBELLE_NEW <- df_infos_grades_new$LIBELLE

# Agrégat par grade
df_cont_grade <- df_cont %>% 
  group_by(GRADE) %>%
  summarise(Nb = n(),
            EQTP = sum(EQTP),
            GRADE_NEW = first(GRADE_NEW),
            S_BRUT_HP = sum(S_BRUT_HP),
            S_BRUT_HP_NEW = sum(S_BRUT_HP_NEW),
            COUT_REVALO = sum(COUT_REVALO),
            LIBELLE_NEW = first(LIBELLE_NEW),
            CORPS_NEW = first(CORPS_NEW),
            FILIERE_NEW = first(FILIERE_NEW)
            )

```

## Coût global


```{r cout_global_cont}
df_cout_global_cont <- df_cont %>% 
  summarise(Nb = n(),
            EQTP = round(sum(EQTP)),
            S_BRUT_HP = sum(S_BRUT_HP), 
            S_BRUT_HP_NEW = sum(S_BRUT_HP_NEW),
            COUT_REVALO_S_BRUT = sum(COUT_REVALO),
            )

knitr::kable(
  df_cout_global_cont, 
  caption = paste("Coût global de la revalorisation",
                  "du SMIC su les Contractuel"),
  format.args = list(big.mark = " ") )

```
## Détail des coûts

Le détail des coûts par GRADE, CORPS et FILIERE n'est pas donné ici mais 
pourra être fait dans la feuille Excel "CONT GRADE" des coûts par grade 
de la revalorisation des contractuels.

```{r save_cont_xls}

xlsx::write.xlsx(as.data.frame(df_cont_grade), fic_xls, 
                 sheetName ="CONT GRADE", 
                 row.names = FALSE, 
                 append = TRUE)

# cat("Sauvegarde des résultats contractuels par GRADE, fichier Excel :\n -> ", 
#  fic_xls, "\n")

df_cout_corps_cont <- df_cont %>% group_by(CORPS_NEW) %>% 
  summarise(FILIERE_NEW = first(FILIERE_NEW),
            Nb = n(),
            EQTP = sum(EQTP),
            S_BRUT_HP = sum(S_BRUT_HP), 
            S_BRUT_HP_NEW = sum(S_BRUT_HP_NEW),
            COUT_REVALO_S_BRUT = sum(COUT_REVALO),
            .groups = "drop" ) %>% 
  arrange(desc(Nb))


knitr::kable(df_cout_corps_cont,  
             caption = paste("Impact de la revalorisation du SMIC",
                             "pour les contractuels", "par corps"), 
             digits = 0,
             format.args = list(big.mark = " ") )



```


# Evolution du SMIC et des grilles C

Nous comparons ici une projection des évolutions du SMIC 
par régression linéaire avec les évolutions de rémunération qu'aurait un 
agent sur les différentes grilles de catégorie C.

```{r smic_vs_grilles}
# Valeurs du SMIC depuis 2006
df_smic <- data.frame(
date_evol = c("01/08/2022","01/05/2022","01/01/2022",
              "01/01/2021","01/01/2020","01/01/2019","01/01/2018",
   "01/01/2017","01/01/2016","01/01/2015","01/01/2014",
   "01/01/2013","01/07/2012", "01/01/2012","01/12/2011",
   "01/01/2011","01/01/2010", "01/07/2009","01/07/2008",
   "01/05/2008","01/07/2007", "01/07/2006"), 
brut_mensuel = c(1678.95, 1645.58, 1603.12, 1554.58, 1539.42,
                 1521.22, 1498.47, 1480.27,
                 1466.62,1457.52,1445.38,1430.22,1425.67,
                 1398.37,1393.82,1365.00,1343.77,1337.70,1321.02,
                 1308.88,1280.07,1254.28)) %>% 
  mutate(date_evol = as.Date(date_evol, "%d/%m/%Y"))

# Régression linéaire sur le SMIC
lm_smic <- lm(brut_mensuel ~ date_evol, data= df_smic)

df_smic_proj <- data.frame(dates = as.Date(c("2006-07-01", "2022-01-01", "2042-01-01"))) %>% 
                             mutate(estim = (lm_smic$coefficient[2]*as.numeric(dates) + 
                                      lm_smic$coefficient[1])) %>% 
                             mutate(estim_ind = estim / point_indice * 12)

# Estimation / projection du SMIC
fig1 <- plot_ly(df_smic) %>% 
  add_trace(x = ~date_evol, y = ~brut_mensuel,
            type = "scatter", mode = "lines+markers", 
            line = list(shape = "hv"), 
            name = "Valeurs du SMIC") %>% 
  add_trace(x = df_smic_proj$dates, y = df_smic_proj$estim,
            type = "scatter", mode = "lines+markers", 
            line = list(shape = "linear"), 
            name = "Régression linéaire et projection") %>% 
  layout(title = "Historique des valeurs du SMIC et projection linéaire à 20 ans",
         yaxis = list(title = "Brut mensuel, €"), 
         xaxis = list(title = ""))



# Comparaison Grille de rémunération C
fig2 <- smash::plotGrille("C_Echelle_C1", b_show = F)
fig2 <- smash::plotGrille("C_Echelle_C2", b_show = F, fig = fig2) 
fig2 <- smash::plotGrille("C_Echelle_C3", b_show = F, fig = fig2) %>% 
  add_trace(x = c(0,20), 
            y = round(df_smic_proj$estim_ind[2:3]), 
            text = c("SMIC ind. 2022", "SMIC ind. 2042"),
            type = "scatter", mode = "lines+markers", 
            line = list(shape = "linear"), 
            name = "Projection SMIC indiciaire") %>% 
  layout(title = "Comparaison grilles C et projection SMIC à 20 ans",
         yaxis = list(title = "Indice majoré"), 
         xaxis = list(title = "Durée des échelons, années"))


htmltools::tagList(list(f1 = fig1, f2 = fig2))
```

