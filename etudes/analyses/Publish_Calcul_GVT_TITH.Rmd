---
title: "Calcul du GVT pour les titulaires"
subtitle: "Données SIASP `r params$anneeDeb`-`r params$anneeFin`, champ `r params$champ`"
author: "DREES/OSAM/BPS - Équipe SMASH"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  pdf_document:
    toc: true
    number_sections: true
  html_document:
    toc: true
    toc_float: true
    number_sections: true
    theme: united
    highlight: tango
params:
  anneeDeb:
    label: "Année de départ"
    value: 2010
    input: slider
    min: 2010
    max: 2020
    step: 1
  anneeFin:
    label: "Année de fin"
    value: 2021
    input: slider
    min: 2011
    max: 2021
    step: 1
  champ:
    label: "Champ des données"
    value: "hop"
    input: select
    choices: ["hop", "esms", "fph"]
  b_agrege:
    label: "Données agrégées par poste"
    value: TRUE
  rep_save:
    label: "Sous-répertoire de sauvegarde, défaut vide (~/Documents)"
    value: "~"

# indent: false 
# mainfont: Arial
# monofont: Arial
# fontsize: 12pt
geometry: "left=2cm,right=2cm,top=2cm,bottom=2cm"

# abstract: \singlespacing BLABLABLA.
keyword: "SIASP"
---


```{r generate_page, eval=FALSE, include=FALSE}
# Exécuter cette cellule pour générer le document PDF (accès à I: nécessaire)

# Paramètres passés au markdown
anneeDeb <- 2009
anneeFin <- 2021
b_agrege <- TRUE
rep_save <- "~/GVT_2023" # get_timestamp(b_fichier = TRUE)

for (champ in c("hop", "esms")) {

rmd_file <- "Publish_Calcul_GVT_TITH"
out_format <- "html" # "pdf"
  out_local <-  sprintf("%s_%s_%s%d-%d_primes.%s", rmd_file, 
                        champ, ifelse(b_agrege, "agreg_",""), anneeDeb, anneeFin, out_format)
  rmarkdown::render(paste(rmd_file,".Rmd", sep = ""),
                    output_format = paste(out_format, "_document", sep=""),
                    output_dir = rep_save,
                    output_file = out_local, params = list(anneeDeb = anneeDeb,
                                                           anneeFin = anneeFin,
                                                           champ = champ,
                                                           b_agrege = b_agrege, 
                                                           rep_save = rep_save))
  
}


# # Copie le dossier sur I
# tryCatch( {
#     chemin_dist <- file.path("I:/BPS/_Emplois et revenus",
#                              "Projet SMASH/05_ReponsesALaDemande",
#                              "GVT_ONDAM2023")
#     
#     cat("Copie du dossier :",dir_local,
#         "\n -> dossier :", chemin_dist, "\n")
#     dir.create(file.path(chemin_dist, rep_save))
#     file.copy(dir_local, chemin_dist, recursive = TRUE)
#     },
#     error = function(e) {
#       message( paste("sauvegarde distante impossible, dossier :", chemin_dist) )
#     }
#   )
```

```{r knit_opts, echo=FALSE}
# Pour ne pas avoir de bloc de code : 
knitr::opts_chunk$set(echo = FALSE)
```


# Introduction 

Ce document calcule les éléments du GVT sur les données SIASP de 2009 à 2020. 
Il s'agit d'un document de travail dont les chiffres n'ont pas vocation à être
publiés mais utilisés en interne pour les différentes directions, notamment 
pour les travaux sur l'élaboration de l'ONDAM.

Le champs des données analysées dans ce rapport sont les personnels titulaires 
de la FPH rattachés à des établissements publiques de santés (et leurs activités 
dans les ESMS rattachés).

> Ce rapport est généré automatiquement à partir d'un script Rmd avec le langage
> R. Il utilise le package `smash` et contient des informations assez verbeuses
> de logs des expériences menées afin de mieux suivre les étapes du calcul. 

```{r imports, message=FALSE, include=FALSE}
library(dplyr)
library(plotly)
# Chargement package SMASH, dev local (vérifier le chemin)
devtools::load_all("../../../smash-r-pkg")

```


```{r chargement_donnees, echo=FALSE, warning=FALSE}
# Récupération des paramètres du markdown
anneeDeb <- params$anneeDeb
anneeFin <- params$anneeFin
champ    <- params$champ
b_agrege <- params$b_agrege
rep_save <- params$rep_save
# Création du dossier de destination
if (!dir.exists(file.path("~",rep_save))) {
  dir.create(file.path("~",rep_save))
}

# Initialisation des données
l_pop <- list() # Tout le monde par année

# Liste des colonnes utiles (pour alléger les traitements)
l_cols_select <- toupper(c("age","anc_echelon","anc_fph","anc_grille","av_nat","brut_social",
                   "cat_ech","champ_31_12","cnracl_assiet_cotis","cnracl_pat","coef_sal_eqtp",
                   "corps","crds","csg","csreg4_bis","dat_debut","dat_fin","depcom_fonction",
                   "depcom_residence","duree_contrat","duree_poste","ech","ech_rank", "n3",
                   "emp_champ","eqtp","etab", "fnal", "grade","grade_ech", "grille", "hop",
                   "ID_EMPLOYEUR", "siren", "id_nir20","IM","indemn_resid","indice", 
                   "iq_nir","lib_emploi_1","lib_spe",
                   "motif_entree","motif_sortie","n","nature_b","nb_heures_remun",
                   "nb_heures_travail",
                   "nbi","net_fiscal","nomenc_grade","parcours","poste_princ_an","pr_tot",
                   "pr_tot_cal","primes","quotite","recupech","region","rem_hsup_exo",
                   "retraite_compl_pat","s_brut","sexe","sft","statut_contrat","tib",
                   "tibr","vsmt_transp", "prime_covid"))

for (an in seq(anneeDeb, anneeFin)) {
  strAn <- as.character(an)
  if (an>=2020) {
    # Sélection prime COVID
    l_pop[[strAn]] <- chargerFST(champ, an, b_agrege = TRUE)
    cols_presentes <- intersect(names(l_pop[[strAn]]), l_cols_select)
    l_pop[[strAn]] <- l_pop[[strAn]][, cols_presentes] #[l_pop[[strAn]]$GRADE == '2154', cols_presentes]
    
  } else {
      l_pop[[strAn]] <- chargerFST(champ, an, b_agrege = TRUE)
  }
}

v_annees <- names(l_pop)
strAnDeb <- v_annees[1]
strAnFin <- v_annees[length(v_annees)]

```

# Évolution des rémunérations : GVT positif indiciaire, effets collectifs

## Calcul

Pour comparer l'évolution d'une année sur l'autre, les personnes d'intérêt sont 
sélectionnées sur les critères suivants :

- le même id_nir doit être présent sur les deux années consécutives
- on ne filtre que sur le poste occupé par l'agent qui soit son poste 
principal (poste_princ_an="P" sur chaque année)
- on demande à ce qu'il soit bien présent sur les 24 mois au cours des deux 
années (duree_poste=365 sur chaque année)
- on demande aussi à ce que le temps de travail eqtp_arr soit le même 
les deux années de suite pour être comparable (par exemple, s'il est en 
mi-temps, ça doit être le cas les 2 années : eqtp_arr=round(eqtp,0.01), 
on arrondit l'EQTP au centième pour ne pas avoir de pb de comparaison d'égalité
- enfin, on rajoute la condition que l'employeur reste le même sur les 
deux années consécutives : à partir de la variable emp_siren.

L'outil `SMASH` fournit différentes fonctions permettant de simuler 
l'avancement indiciaire seul hors réglementation. 
Pour cela, il suffit d'appliquer la fonction `majIndiceMajore()` qui va 
calculer les évolutions d'indice entre l'année `n-1` et l'année `n` en 
s'affranchissant des évolutions réglementaires.
Il permet également de suivre les évolutions de nomenclature et de grilles 
indiciaires à partir de 2010. 


```{r table_gvt_positif, message=TRUE, warning=FALSE}

# Initialisation du tableau résultat la première année
df_GVT <- smash::calculer_GVT_positif(l_pop, v_annees[2])
cols_gvt <- names(df_GVT)


# Fenêtre glissante pour les années disponibles
for (an_s in v_annees[-1:-2]) {
  df_GVT <- rbind(df_GVT, 
                  smash::calculer_GVT_positif(l_pop, an_s)
                  )
}
```

## Tableau de synthèse

```{r print_table_gvt_pos}
# On enlève la colonne catégorie
df_GVT$categorie <- NULL
noms_cols <- c("An N",
               "Effectifs présents N-1/N",
               "Part des constants, en %",
               "EQTP An N",
               "Montant ind. N-1, €",
               "Montant ind. N, €",
               "Montant ind. contrefactuel N",
               "GVT pos. mes. indiv.",
               "GVT pos. mes. collect.",
               "GVT pos.",
               "Évolution Primes, %",
               "Part prime N-1, %",
               "Part prime N, %")

knitr::kable(df_GVT, 
             caption = sprintf("Calcul du GVT positif entre %s et %s", 
                               v_annees[2], v_annees[length(v_annees)]),
             col.names = noms_cols
             )
```

## Visualisation

```{r plot_GVT_pos}
# Visualisation 
fig <- smash::plot_GVT_positif(df_GVT)

htmltools::tagList(list(fig))
```

# Analyse par catégorie (A,B,C)

## Calcul

On reprend les calculs précédents mais en filtrant sur les catégories "A", "B"
et "C".

```{r gvt_pos_categorie, message=TRUE, warning=FALSE}
# Initialisation du tableau résultat la première année
df_GVT_cat <- data.frame(matrix(ncol= length(cols_gvt),nrow=0, dimnames=list(NULL, cols_gvt)))

# Fenêtre glissante pour les années disponibles
for (an_s in v_annees[-1]) {
  for (categorie in c("A", "B", "C")) {
    df_GVT_cat <- rbind(df_GVT_cat, 
                        calculer_GVT_positif(l_pop, an_s, 
                                             filtre_cat = categorie)
    )
  }
}
```

## Tableau de synthèse

La prime COVID versée en 2020 au titulaires n'est pas prise en compte dans les 
calculs. Le montant "prime covid" versé à l'ensemble des titulaires en 2020 
s'élève à `r round(sum((l_pop[["2020"]] %>% filter(STATUT_CONTRAT=="TITH"))$PRIME_COVID)/1e6, digits=1)` 
millions €.

```{r table_gvt_pos_categorie}

noms_cols_cat <- c("An N",
               "Catégorie",
               "Effectifs présents N-1/N",
               "Part des constants, en %",
               "EQTP An N",
               "Montant ind. N-1, €",
               "Montant ind. N, €",
               "Montant ind. contrefactuel N",
               "GVT pos. mes. indiv.",
               "GVT pos. mes. collect.",
               "GVT pos.", 
               "Évolution Primes, %",
               "Part prime N-1, %",
               "Part prime N, %")

knitr::kable(df_GVT_cat, 
             caption = sprintf("Calcul du GVT positif par catégorie entre %s et %s", 
                               v_annees[2], v_annees[length(v_annees)]),
             col.names = noms_cols_cat
             )

```


## Visualisation

```{r plot_GVT_pos_cat}
# Mise en forme des données

# Séparation mesures individuelles / collectives / Primes
l_effets <- c( "Évolution primes", 
              "Mes. collectives",
              "GVT pos. ind.")

# Indiv
df_GVT_cat_indiv <- df_GVT_cat[, c("an_N", "categorie", "GVT_pos_ctf")]
df_GVT_cat_indiv <- df_GVT_cat_indiv %>% rename(GVT_pos = GVT_pos_ctf)
# Ajout d'une ligne ensemble 
df_GVT_cat_indiv <- rbind(df_GVT_cat_indiv, 
                          data.frame( an_N = df_GVT$an_N, 
                                      categorie = rep("Ens", nrow(df_GVT)), 
                                      GVT_pos = df_GVT$GVT_pos_ctf)
                          )
df_GVT_cat_indiv$Effets <- rep(l_effets[3], nrow(df_GVT_cat_indiv))

# Collectif
df_GVT_cat_collec <- df_GVT_cat[, c("an_N", "categorie", "GVT_pos_obs")]
df_GVT_cat_collec <- df_GVT_cat_collec %>% rename(GVT_pos = GVT_pos_obs)
# Ajout d'une ligne ensemble 
df_GVT_cat_collec <- rbind(df_GVT_cat_collec, 
                          data.frame( an_N = df_GVT$an_N, 
                                      categorie = rep("Ens", nrow(df_GVT)), 
                                      GVT_pos = df_GVT$GVT_pos_obs)
                          )
df_GVT_cat_collec$Effets <- rep(l_effets[2], nrow(df_GVT_cat_collec))

# Primes
df_GVT_cat_primes <- df_GVT_cat[, c("an_N", "categorie", "evol_primes_pct")]
df_GVT_cat_primes <- df_GVT_cat_primes %>% rename(GVT_pos = evol_primes_pct)
# Ajout d'une ligne ensemble
df_GVT_cat_primes <- rbind(df_GVT_cat_primes,
                          data.frame( an_N = df_GVT$an_N,
                                      categorie = rep("Ens", nrow(df_GVT)),
                                      GVT_pos = df_GVT$evol_primes_pct)
                          )
df_GVT_cat_primes$Effets <- rep(l_effets[1], nrow(df_GVT_cat_primes))

# Concaténation pour visualisation
df_GVT_cat_visu <- rbind(
  df_GVT_cat_primes, 
  df_GVT_cat_collec,
  df_GVT_cat_indiv )

df_GVT_cat_visu$Effets <- factor(df_GVT_cat_visu$Effets,
                                 levels = l_effets)
# Visualisation 
col = c("#9ecae1","#4292c6","#08519c")
p <- ggplot(df_GVT_cat_visu, 
            aes(x = categorie, 
                y = GVT_pos, fill = Effets)
            ) + 
  geom_bar(stat = 'identity', position = 'stack') + 
  scale_fill_manual(values = col) +
  xlab("") + ylab("Effets d'évolution, %") +
  facet_grid(Effets ~ an_N, scales="free_y") + 
  theme_bw() + theme(legend.position = "none") # + scale_fill_brewer(palette="Blues")

ggplotly(p)

```

# Sauvegarde Excel
 
Les fichiers sont sauvés en local dans le dossier utilisateur "Documents/" puis
recopiés sur le serveur de la DREES dans le dossier du projet, sur `I:`.
 
```{r save_tableaux}
# Sauvegarde en XLSX
fic_xls <- sprintf("%s/GVT_positif_indiciaire_primes_TITH%s_%d-%d.xlsx", 
                   path.expand(rep_save), ifelse(b_agrege, "_agreg",""), 
                   anneeDeb, anneeFin)

if (file.exists(fic_xls)) {
  owb <- openxlsx::loadWorkbook(fic_xls)
} else {
  owb <- openxlsx::createWorkbook()
}


# Données générales
# Choix du data.frame et nommage des colonnes
df_GVT_xl <- df_GVT
names(df_GVT_xl) <- noms_cols

feuil <- paste("GVT positif TITH", toupper(champ))
if (feuil %in% names(owb)) { openxlsx::removeWorksheet(owb, feuil) }
openxlsx::addWorksheet(owb, feuil)
openxlsx::writeData(owb, feuil, df_GVT_xl, rowNames = FALSE)

# Données par catégorie
# Choix du data.frame et nommage des colonnes
df_GVT_cat_xl <- df_GVT_cat
names(df_GVT_cat_xl) <- noms_cols_cat

feuil <- paste("GVT positif TITH", toupper(champ), "catégorie")
if (feuil %in% names(owb)) { openxlsx::removeWorksheet(owb, feuil) }
openxlsx::addWorksheet(owb, feuil)
openxlsx::writeData(owb, feuil, df_GVT_cat_xl, rowNames = FALSE)

# Sauvegarde
openxlsx::saveWorkbook(owb, fic_xls, overwrite = TRUE)
cat("Sauvegarde Excel :", fic_xls, "\n")

```
