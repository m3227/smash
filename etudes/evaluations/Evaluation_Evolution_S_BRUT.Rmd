---
title: "Comparaison des évolutions de salaires bruts SIASP et simulées - `r params$anneeDebut`-`r params$anneeFin`"
author: "BPS DREES - `r stringr::str_to_title(stringr::str_replace(Sys.getenv('username'), '\\.', ' '))`"
date: "`r format(Sys.time(), '%d %B, %Y')`"
output:
  pdf_document:
    toc: true
    number_sections: true
  html_document:
    toc: true
    toc_float: true
    number_sections: true
    theme: united
    highlight: tango
  rmdformats::downcute:
    toc_depth: 5
    number_sections: true
    theme: united
    highlight: tango
    
params:
  anneeDebut:
    label: "Année"
    value: 2015
    input: slider
    min: 2015
    max: 2018
    step: 1
  anneeFin:
    label: "Année"
    value: 2020
    input: slider
    min: 2016
    max: 2020
    step: 1
  champ:
    label: "Champ des données"
    value: "hop"
    input: select
    choices: ["hop", "fph"]
  rep_out:
    label: "Répertoire de sauvergarde du document et tableaux"
    value: "~/Evaluations_SMASH_temp"

geometry: "left=2cm,right=2cm,top=2cm,bottom=2cm"
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = FALSE)
```


```{r librairies, include=FALSE}
## Chargement des données 
Sys.setenv(R_CONFIG_ACTIVE = Sys.getenv('username'))
config <- config::get()
chemin <- config$path_siasp

# Chargement package Smash dev
devtools::load_all(config$path_smash)

library(flextable)
library(reactable)

# Récupération paramètre année (entête Rmd)
anneeDebut <- params$anneeDebut
anneeFin <- params$anneeFin
champ <- params$champ
rep_out <- params$rep_out

v_annees <- as.character(seq(anneeDebut, anneeFin))

# Base de données réglementaire
objBDD <- GrillesIndiciaires$new()

# Fichier Excel pour sauver les tableaux générés
fic_xls <- file.path(rep_out, 
                     sprintf("Evaluation_Evolution_S_BRUT_%s_%d-%d.xlsx", 
                             champ, anneeDebut, anneeFin))

```

# Présentation 

Ce script cherche à évaluer avec quelle précision sont estimées les évolutions 
de salaire dans les simulations SMASH. Pour cela, nous comparons sur 
différentes durées de simulation l'évolution des salaires bruts dans les données 
SIASP et l'évolution des mêmes personnes mises en entrée du simulateur.

```{r generate_page, eval=FALSE, include=FALSE}
# Exécuter cette cellule pour générer le document (copie sur I: en option)
# Vérifier également le chemin relatif vers le package SMASH, ci-dessus

ch <- "hop"
rmd_file <- "Evaluation_Evolution_S_BRUT"
rep_out <- "~/Evaluations_SMASH"
out_format <- "html" 

# Liste des périodes à tester
# Liste des périodes à tester
l_periodes <-  list(c(2015,2020))
  # c(2012, 2016), c(2014, 2018), c(2016, 2020),
  #                  c(2012, 2020), c(2017, 2021), c(2010, 2021))


for (idx in seq(length(l_periodes))) {
  anDeb <- l_periodes[[idx]][1]
  anFin <- l_periodes[[idx]][2]
  
  out_local <-  sprintf("%s-%s-%d-%d.%s", 
                        rmd_file, ch, anDeb, anFin, out_format)
  
  # Génération au format demandé
  rmarkdown::render(sprintf("%s.Rmd", rmd_file),
                    output_format = ifelse(out_format=="html", 
                                       "rmdformats::robobook", 
                                       paste(out_format, "_document", sep="")), 
                    output_dir = rep_out,
                    output_file = out_local, 
                    params = list(anneeDebut = anDeb, 
                                  anneeFin = anFin, 
                                  champ = ch,
                                  rep_out = rep_out)
                    )
  
}

# Ouverture du Dossier : 
browseURL(path.expand(rep_out))

```

# Chargement des données

## Chargement population SIASP

Les données SIASP chargées sont mises au format de sortie du simulateur, soit 
une liste de data.frame des populations de chaque année disponible.

- champ des données (HOP ou FPH) : `r toupper(champ)`
- Années : `r anneeDebut` à `r anneeFin`

Pour ces comparaisons, on ne garde que les agents dont l'EQTP est 
supérieur à 0.08 (~1 mois). De plus, on ne garde qu'une ligne par agent 
celle de S_BRUT max.


```{r chargement_donnees, message=FALSE, warning=FALSE, include=FALSE}
popSIASP <- list()
v_annees <- as.character(seq(anneeDebut, anneeFin))

# Suppression des années hors période d'intérêt
for (an in v_annees) {
  popSIASP[[an]] <- getEchantillonSIASP(champ, as.numeric(an), 0, b_agrege = F)
}


# Filtre des agents TITH (1 ligne par agent)
# Agrégat par ID en gardant S_BRUT max :
# arrange() tri par S_brut décroissant et distinct() garde la 1ère ligne
for (an in v_annees) {
  popSIASP[[an]] <- popSIASP[[an]] %>% 
    filter(STATUT=="FPH" & EQTP>0.08) %>% 
    arrange(desc(S_BRUT)) %>% dplyr::distinct(ID, .keep_all = T)
}

```

## Sélection des présents

On ne garde que les agents présents sur l'ensemble de la période considérée, 
`r anneeDebut`-`r anneeFin`, afin d'observer leur évolution de salaire brut. 

Des filtres pourront être appliqués par la suite sur les personnes restant dans 
leur grade.

```{r selection_presents, include=FALSE}

# ID des personnes présentes sur la période considérée
v_id <- unique(popSIASP[[v_annees[1]]]$ID)
for (an in v_annees[-1]) {
  v_id <- intersect(v_id, unique(popSIASP[[an]]$ID))
}

# Population d'intérêt
for (an in v_annees) {
  # On récupère les ID sélectionnées
  popSIASP[[an]] <- popSIASP[[an]] %>% filter(ID %in% v_id) %>% arrange(ID)
}

```

Nombre de personnes sélectionnées pour la période : **`r length(v_id)`**.

# Simulation des évolutions de salaire

Nous lançons maintenant une simulation SMASH à partir de la population 
sélectionnée `r anneeDebut`.

Il n'est pas nécessaire de simuler les entrants et sortants car nous observons 
uniquement les évolutions sur les agents présents sur la période. 
Les étapes de mise à jour annuelle dans la simulation sont donc :

1. Âge des agents
2. Ancienneté FPH
3. Ancienneté dans l'échelon et éventuellement passage à l'échelon supérieur
4. Reclassements de l'année
5. Promotions de Grade
5. Primes et salaires brut


```{r simul_smash, echo=FALSE}
dir_model <- "~/data/modeles"
l_modeles <- smash::lister_modeles(anneeDebut, champ, dir_model)

  
l_maj <- c(majAge,
           majAncienneteFPH,
           majAncienneteEchelon,
           function(df, calTime) majReclassement(df, calTime, 
                                                 verbose = 1, 
                                                 majsbrut = TRUE),
           function(df, calTime) 
             majEvolutionGradeIM(df, calTime,
                                 fic_model = l_modeles$transitionGradeIM, 
                                 b_promo_seul = TRUE),
           # Primes
           function(df, calTime) majPrimes(df, calTime, l_modeles$primes),
           # Cont, MEDI
           # Mise à jour annuelle du salaire brut linéairement pente moyenne observée
           ModeleEvolSalaire$new(l_modeles$evolution_salaires, 
                                 c("CONT", "MEDI"),
                                 objBDD = objBDD)
           )

popSimul <- lancer_simulation(popSIASP[[v_annees[1]]], 
                              anneeDebut, anneeFin,
                              lFcnsCalculs = NULL,
                              lFcnsMaJ = l_maj,
                              list_corps = NULL )

# Normalement ok, mais on retrie quand même sur les ID
for (an in names(popSimul)) {
 popSimul[[an]] <- popSimul[[an]] %>% arrange(ID)
}

```


# Analyses

## Écarts de salaire brut total

Pour la comparaison, on ne garde que les agents qui n'ont pas changé de grade
sur la période considérée sauf changement de nomenclature des grades.

Par exemple, un aide-soignant de 3001 en 2015 qui est encore aide-soignant en 
2020, cette fois-ci sur le grade équivalent 3021 (reclassement en 2017) sera 
comptabilisé.

```{r fcn_comparaison, include=FALSE}

#' Calcul des écarts de S_BRUT entre données SIASP et Simulation
#'
#' Le calcul porte sur les agents  qui n'ont pas changé de grade
#' sur la période considérée, au changement de nomenclature près.
#' 
#' @param popObs données observées SIASP 
#' @param popSimu données simulées SMASH
#' @param anDeb année de début (pour le calage des grades)
#' @param anFin année de fin pour la comparaison 
#'
#' @return data.frame des différences filtrées
analyser_ecarts_sbrut <- function(popObs, popSimu, anDeb, anFin) {
  
  strAnDeb <- as.character(anDeb)
  strAnFin <- as.character(anFin)
  
  # Recherche dans SIASP ceux qui n'ont pas changé de GRADE
  # au changement de nomenclature près
  v_grades_new <- popObs[[strAnDeb]]$GRADE
  for (an in seq(anDeb+1, anFin)) {
    v_grades_new <- majGrades(v_grades_new, an)
  }
  
  is_grade_fixe <- popObs[[strAnFin]]$GRADE == v_grades_new
  
  # Data.frame des différences filtrées
  df_diff <- 
    data.frame( 
      S_BRUT_SIASP = popObs[[strAnFin]]$S_BRUT[is_grade_fixe],
      S_BRUT_SIASP_EQ1 = popObs[[strAnFin]]$S_BRUT[is_grade_fixe] /
                         popObs[[strAnFin]]$EQTP[is_grade_fixe],
      
      S_BRUT_SMASH = popSimu[[strAnFin]]$S_BRUT[is_grade_fixe],
      S_BRUT_SMASH_EQ1 = popSimu[[strAnFin]]$S_BRUT[is_grade_fixe] /
                         popSimu[[strAnFin]]$EQTP[is_grade_fixe],
      
      DIFF_BRUT_EQ1 = (popObs[[strAnFin]]$S_BRUT[is_grade_fixe] /
                         popObs[[strAnFin]]$EQTP[is_grade_fixe]) - 
        (popSimu[[strAnFin]]$S_BRUT[is_grade_fixe] /
           popSimu[[strAnFin]]$EQTP[is_grade_fixe]),
      
      DIFF_BRUT = popObs[[strAnFin]]$S_BRUT[is_grade_fixe] - 
        popSimu[[strAnFin]]$S_BRUT[is_grade_fixe],
      
      GRADE = popObs[[strAnFin]]$GRADE[is_grade_fixe],
      STATUT_CONTRAT = popObs[[strAnFin]]$STATUT_CONTRAT[is_grade_fixe],
      ID = popObs[[strAnFin]]$ID[is_grade_fixe]
    )


  # cat(" -", nrow(df_diff), "personnes sont restées sur leur grade entre",
  #   anDeb, "et", anFin, "\n\n")
  
  return(df_diff)
}

# Calcul des différences de salaire brut pour ces personnes 
l_diff <- list()
df_comp_global <- NULL
df_comp_grade_fixe <- NULL
for (an in seq(anneeDebut+1, anneeFin)) {
  strAn <- as.character(an)
  l_diff[[strAn]] <- analyser_ecarts_sbrut(popSIASP, popSimul, 
                                                      anneeDebut, an)
  # Comparaison Globale Sur les tous les présents pour la période
  df_comp_global <- rbind(
    df_comp_global, 
    data.frame(An = an, 
               NB = nrow(popSIASP[[strAn]]),
               S_BRUT_SIASP = sum(popSIASP[[strAn]]$S_BRUT),
               S_BRUT_SMASH = sum(popSimul[[strAn]]$S_BRUT)
               ) %>% 
      mutate(DIFF_BRUT_TOTAL = S_BRUT_SIASP - S_BRUT_SMASH, 
             PCT_DIFF_BRUT_TOTAL = round(100*(S_BRUT_SIASP - S_BRUT_SMASH)/S_BRUT_SIASP, 
                                         digits = 2)) %>% 
      select(An,
             NB,
             S_BRUT_SIASP, 
             S_BRUT_SMASH, 
             DIFF_BRUT_TOTAL, 
             PCT_DIFF_BRUT_TOTAL
             )
    )
  
  # Comparaison à grade fixe
  df_comp_grade_fixe <-  rbind(
    df_comp_grade_fixe,
    data.frame(An = an, 
               NB_GRADE_FIXE = nrow(l_diff[[strAn]]),
               S_BRUT_SIASP = sum(l_diff[[strAn]]$S_BRUT_SIASP),
               S_BRUT_SMASH = sum(l_diff[[strAn]]$S_BRUT_SMASH),
               S_BRUT_SIASP_EQ1 =sum(l_diff[[strAn]]$S_BRUT_SIASP_EQ1),
               DIFF_BRUT_EQ1_GRADE_FIXE = sum(l_diff[[strAn]]$DIFF_BRUT_EQ1),
               DIFF_BRUT_GRADE_FIXE = sum(l_diff[[strAn]]$DIFF_BRUT)
               ) %>% 
      mutate(PCT_DIFF_BRUT_GRADE_FIXE = 
               round(100*DIFF_BRUT_GRADE_FIXE/S_BRUT_SIASP, digits = 2),
             PCT_DIFF_BRUT_EQ1_GRADE_FIXE =
               round(100*DIFF_BRUT_EQ1_GRADE_FIXE/S_BRUT_SIASP_EQ1, digits = 2)) %>% 
      select(An,
             NB_GRADE_FIXE, 
             S_BRUT_SIASP, 
             S_BRUT_SMASH, 
             DIFF_BRUT_GRADE_FIXE, 
             PCT_DIFF_BRUT_GRADE_FIXE,
             S_BRUT_SIASP_EQ1, 
             DIFF_BRUT_EQ1_GRADE_FIXE,
             PCT_DIFF_BRUT_EQ1_GRADE_FIXE 
             )
  )
}

```


```{r table_diff_global, echo=FALSE}
names(df_comp_global) <- c("Année",
                           "Effectifs", 
                           "S_BRUT données SIASP", 
                           "S_BRUT simul. SMASH",
                           "Diff. S_BRUT global", 
                           " % écart S_BRUT global")

names(df_comp_grade_fixe) <- c("Année",
                               "Effectifs grade fixe", 
                               "S_BRUT grade fixe SIASP",
                               "S_BRUT grade fixe SMASH",
                               "Diff. S_BRUT grade fixe", 
                               " % écart S_BRUT grade fixe", 
                               "S_BRUT EQTP=1 grade fixe SIASP",
                               "Diff. S_BRUT EQTP=1 grade fixe", 
                               " % écart S_BRUT EQTP=1 grade fixe")



# Sauvegarde vers Excel Création du fichier
xlsx::write.xlsx(df_comp_global, fic_xls, 
                 sheetName ="Diff S_BRUT global", 
                 row.names = FALSE, 
                 append = FALSE)

# Sauvegarde vers Excel Création du fichier
xlsx::write.xlsx(df_comp_grade_fixe, fic_xls, 
                 sheetName ="Diff S_BRUT grade fixe", 
                 row.names = FALSE, 
                 append = FALSE)


knitr::kable(
  df_comp_global,
  caption = "Différences de salaire brut données Vs. Simulation")


knitr::kable(
  df_comp_grade_fixe,
  caption = "Différences de salaire brut données Vs. Simulation à grade fixe")



```

## Analyses pour une année cible

```{r count_grades, echo=FALSE}

anDiff <- anneeFin-1 # anneeFin
strAnDiff <- as.character(anDiff)
df_diff <- l_diff[[strAnDiff]]

df_count_grades <- df_diff %>% count(GRADE, sort = T, name = "Total")

nb_aff <- 20
df_count_aff <- df_count_grades %>% top_n(nb_aff, Total) %>% 
               mutate(LIBELLE = (objBDD$getInfoGrades(GRADE))$LIBELLE)

df_count_aff <- rbind(
  df_count_aff, 
  data.frame(GRADE = "Autres", 
             Total = sum(df_count_grades$Total[(nb_aff+1):nrow(df_count_grades)]),
             LIBELLE = paste(nrow(df_count_grades)-nb_aff, "Grades non affichés")
  )
)

# Sauvegarde vers Excel Création du fichier
xlsx::write.xlsx(rbind(
  df_count_aff, 
  data.frame(GRADE = "Total", 
             Total = sum(df_count_grades$Total),
             LIBELLE = paste0("Agents restés sur le même grade entre ",
                              anneeDebut, " et ", anDiff)
             )
  ), fic_xls, 
  sheetName = paste0("GRADES FIXES ", anneeDebut, "-", anDiff), 
  row.names = FALSE,
  append = TRUE)

```

On observe ci-dessous les distributions (boxplot) des différences de salaire brut 
entre les données SIASP de `r anDiff` et des données simulées avec SMASH en 
`r anDiff` à partir des données `r anneeDebut`.


```{r visu_hist_diffS_BRUT, echo=FALSE}

df_diff_main <- df_diff %>% 
  filter(GRADE %in% df_count_grades$GRADE[1:6]) %>% 
  mutate(GRADE = factor(GRADE))
# levels(df_diff_main$GRADE) = seq(1:6)

htmltools::tagList(
  list(
    # Table HTML(+titre)
    htmltools::tags$h4(paste0("Répartition par GRADE des ", nrow(df_diff), 
                              " agents restés sur le même grade entre ",
                              anneeDebut, " et ", anDiff)),
    reactable(df_count_aff, 
              columns = list(
                GRADE = colDef(minWidth = 50, name = "Grade"),
                Total = colDef(minWidth = 50),
                LIBELLE = colDef(minWidth = 350, name = "Libellé") ),
              highlight = TRUE, minRows = 10
              ),
    
    plot_ly(df_diff_main, 
            y = ~DIFF_BRUT, color = ~GRADE, type = "box", 
            boxpoints = FALSE) %>%
      layout(title = paste0("Histogrammes des écarts S_BRUT SIASP-SMASH en ", 
                            anDiff, " (simul. ", anneeDebut, "-", anneeFin, ")"),
             yaxis = list(title = 'Diff S_BRUT'),
             xaxis = list(type = "category")) ,
    
    plot_ly(df_diff_main, 
            y = ~DIFF_BRUT_EQ1, color = ~GRADE, type = "box", 
            boxpoints = FALSE) %>%
      layout(title = paste0("Histogrammes écarts S_BRUT/EQTP ",
                            "SIASP-SMASH en ", anDiff, 
                            " simul. ", anneeDebut, "-", anneeFin, ")"),
             yaxis = list(title = 'Diff S_BRUT_EQ1'),
             xaxis = list(type = "category"))     
    )
)



```

## Par statut de contrat

Nous affichons les différences ci-dessous les boxplot des différences de salaire
brut `s_brut_siasp - s_brut_simul` par statut de contrat, uniquement pour les
principaux grades du statut, après avoir rappelé le nombre de personnes 
présentes dans le grade. 

Comment lire ce graphique : Une boîte dont la médiane est au-dessus de 0 indique
que le salaire brut est sous-estimé par SMASH, à l'inverse une boîte située 
au-dessous de l'axe 0 indique que les salaires simulés pour l'année considérée 
de la simulation sont supérieurs aux salaires observés dans les données SIASP.


```{r compar_brut_statut, echo=FALSE}
l_figs <- list()
for (statut in c("TITH", "CONT", "MEDI")) {
  
  df_count_grades <- df_diff %>% filter(STATUT_CONTRAT==statut) %>% 
    count(GRADE, sort = T, name = "Total")
  
  df_diff_main <- df_diff %>% filter(STATUT_CONTRAT==statut) %>% 
    filter(GRADE %in% df_count_grades$GRADE[1:6]) %>% 
    mutate(GRADE = factor(GRADE))

  
  nb_aff <- 9
  df_count_aff <- df_count_grades %>% top_n(nb_aff, Total) %>% 
               mutate(LIBELLE = (objBDD$getInfoGrades(GRADE))$LIBELLE)

  df_count_aff <- rbind(
    df_count_aff, 
    data.frame(GRADE = "Autres", 
               Total = sum(df_count_grades$Total[(nb_aff+1):nrow(df_count_grades)]),
               LIBELLE = paste(nrow(df_count_grades)-nb_aff, "Grades non affichés")
               )
)

  # Titre Table HTML
  l_figs[[paste0("h_", statut)]] <- 
    htmltools::tags$h4(statut, paste("Principaux Grades, statut", statut))
  
  # Table HTML  
  l_figs[[paste0("tbl_", statut)]] <- 
    reactable(df_count_aff, 
              columns = list(
                GRADE = colDef(minWidth = 50, name = "Grade"),
                Total = colDef(minWidth = 50),
                LIBELLE = colDef(minWidth = 350, name = "Libellé") ),
              highlight = TRUE, minRows = 10
              ) 
  
  # Saut html
  l_figs[[paste0("s_", statut)]] <- htmltools::tags$p(" ")

  # Figure histogramme différence par grade (Boxplot)
  l_figs[[statut]] <- plot_ly(df_diff_main, 
          y = ~DIFF_BRUT, color = ~GRADE, type = "box", 
          boxpoints = FALSE) %>%
    layout(title = paste("Différence de Salaires bruts",statut, 
                         " SIASP - SMASH en ", anDiff,
                       "\nSimulation ", anneeDebut, "-", anneeFin, 
                       ", 6 grades les plus représentés"),
           yaxis = list(title = 'Diff S_BRUT'),
           xaxis = list(type = "category")) 
  }

htmltools::tagList(l_figs)

```


## Analyse des différences

Afin de mieux comprendre les grands écarts de salaire et d'IM, 
nous comparons au niveau individuel les évolutions dans les données et les 
évolutions simulées par SMASH pour quelques individus. Pour cela nous avons 
trié les agents par écart de salaire maximal entre les données et les 
simulations.

Une raison identifiée des écarts de S_BRUT semble être un écart d'EQTP, liée à
un départ en retraite. L'année du départ en retraite, l'EQTP diminue tandis 
que le salaire brut augmente. Lorsqu'il est ramené à un EQTP de 1 pour faire la 
comparaison, l'écart explose. 

Sur ces observations, c'est la variation d'EQTP qui semble la cause principale de 
l'écart de S_BRUT, car la différence est ramenée sur 1.


```{r details_outliers_s_brut, echo=FALSE}
l_tables <- list()

for (statut in c("TITH", "CONT", "MEDI")) {
  
  df_count_grades <- df_diff %>% filter(STATUT_CONTRAT==statut) %>% 
    count(GRADE, sort = T, name = "Total")
  
  df_diff_main <- df_diff %>% filter(STATUT_CONTRAT==statut) %>% 
    filter(GRADE %in% df_count_grades$GRADE[1:6]) %>% 
    mutate(GRADE = factor(GRADE))


  df_diff_outliers <- df_diff_main %>% arrange(desc(abs(DIFF_BRUT)))

l_cols <- c("ID", "AGE", "GRADE", "IM", "ECH", "IS_SORTANT",
            "ANCIENNETE_ECH", "ANCIENNETE_FPH", "EQTP", "S_BRUT", "PRIMES")

v_ids <- df_diff_outliers$ID[1:10]


for (an in v_annees) {
  if (an==v_annees[1]) {
      df_extrait_siasp <- popSIASP[[an]] %>% 
        select(all_of(l_cols)) %>% 
        filter(ID %in% v_ids) %>% mutate(AN = as.numeric(an))
      df_extrait_smash <- popSimul[[an]] %>% 
        select(all_of(l_cols)) %>% 
        filter(ID %in% v_ids) %>% mutate(AN = as.numeric(an))
    
  } else {
    df_extrait_siasp <- rbind(
      df_extrait_siasp, popSIASP[[an]] %>% 
        select(all_of(l_cols)) %>% 
        filter(ID %in% v_ids) %>% mutate(AN = as.numeric(an))
      )
    df_extrait_smash <- rbind(
      df_extrait_smash, popSimul[[an]] %>% 
        select(all_of(l_cols)) %>% 
        filter(ID %in% v_ids) %>% mutate(AN = as.numeric(an))
      )
  }
}

df_extrait_siasp <- df_extrait_siasp %>% arrange(ID) %>% 
  rename(ANC_ECH = ANCIENNETE_ECH, 
         ANC_FPH = ANCIENNETE_FPH)
df_extrait_smash <- df_extrait_smash %>% arrange(ID) %>% 
  rename(GRADE.Simu = GRADE, IM.Simu = IM, ECH.Simu = ECH,
         EQTP.Simu = EQTP,
         S_BRUT.Simu = S_BRUT,
         PRIMES.Simu = PRIMES,
         ANC_ECH.Simu = ANCIENNETE_ECH, 
         ANC_FPH.Simu = ANCIENNETE_FPH)

df_compar <- cbind(df_extrait_siasp, df_extrait_smash %>% 
                     select(GRADE.Simu, IM.Simu, EQTP.Simu, ECH.Simu,
                            ANC_ECH.Simu, ANC_FPH.Simu, S_BRUT.Simu, 
                            PRIMES.Simu)
                   ) %>% 
  select(AN, ID, AGE, GRADE, IM, ECH, GRADE.Simu, IM.Simu, ECH.Simu, 
         ANC_ECH, ANC_FPH, ANC_ECH.Simu, ANC_FPH.Simu, EQTP, EQTP.Simu, 
         S_BRUT, S_BRUT.Simu, PRIMES, PRIMES.Simu, IS_SORTANT) %>% 
  mutate(DIFF_S_BRUT = S_BRUT - S_BRUT.Simu,
         DIFF_S_BRUT_NORM = S_BRUT/EQTP - S_BRUT.Simu/EQTP.Simu)


  # Titre Table HTML
  l_tables[[paste0("h_", statut)]] <- 
    htmltools::tags$h4(paste("Comparaison SIASP/Simu smash d'outliers",
                             statut, "pour les évolutions de S_BRUT", anDiff))
  
  # Table HTML  
  l_tables[[paste0("tbl_", statut)]] <- 
    reactable(df_compar,
              highlight = TRUE, defaultPageSize = length(popSIASP)
              )


  # Sauvegarde vers Excel
  xlsx::write.xlsx(df_compar, fic_xls, 
                   sheetName = paste("Diff S_BRUT outliers", statut, anDiff), 
                   row.names = FALSE, 
                   append = TRUE)
  
}

htmltools::tagList(l_tables)

```



## Analyse par grade

### Grades Infirmiers

La figure précédente montre un écart important d'indice majoré pour les grades 
des infirmiers DE.

- `2154` : Grade 1 ISGS Infirmier en soins généraux (DE)
- `2164` : Grade 2 ISGS Infirmier en soins généraux (DE)


```{r evol_salaire_grade_an}


plot_evol_sbrut <- function(pop, gradeInit, str_titre) {
  df_plot <- NULL
  grade <- gradeInit
  for(strAn in names(pop)) {
    an <- as.numeric(strAn)
    grade <- majGrades(grade, an)
    
    df_plot <- rbind(
      df_plot, 
      pop[[strAn]] %>%
      filter(GRADE == grade & EQTP>0.1) %>% 
      select(ID, GRADE, STATUT_CONTRAT, IM, S_BRUT, ECH, EQTP) %>%
      mutate(AN = as.numeric(strAn), 
             S_BRUT_NORM = S_BRUT/EQTP)
      
    )
  }
  
  df_plot$AN <- factor(df_plot$AN)
  
  p <- plot_ly(df_plot, y = ~S_BRUT_NORM, color = ~AN, 
               type = "box", boxpoints = FALSE) %>%
      layout(title =str_titre,
             yaxis = list(title = 'S_BRUT'),
             xaxis = list(type = "Années")) 
  return(p)
}

htmltools::tagList(
  list(
    plot_evol_sbrut(popSIASP, "2154", 
                    "Évolution des S_BRUT/EQTP SIASP, grade 2154"),
    plot_evol_sbrut(popSimul, "2154", 
                    "Évolution des S_BRUT/EQTP SMASH, grade 2154")
  )
)

```


```{r diff_grade_infirmier, echo=FALSE}
anDiff <- 2019
statut <- "TITH"
if (anDiff < 2020) {
  v_grades <- c("2154") #  "2164")
} else {
  v_grades <- c("2176") #, "2177")
}


# Filtre les différences le salaire
l_cols <- c("ID", "AGE", "GRADE", "IM", "ECH", "IS_SORTANT",
            "ANCIENNETE_ECH", "ANCIENNETE_FPH", "EQTP", "S_BRUT", "PRIMES")

v_ids_inf_ko <- (df_diff %>% filter(STATUT_CONTRAT==statut & 
                                      GRADE %in% v_grades) %>%
                   arrange(desc(abs(DIFF_BRUT))) %>% top_n(10) )$ID

if (length(v_ids_inf_ko)>0) {
  # Extrait des lignes des ids sélectionnées
  for (an in v_annees) {
    if (an==v_annees[1]) {
        df_extrait_siasp <- popSIASP[[an]] %>% 
          select(all_of(l_cols)) %>% 
          filter(ID %in% v_ids_inf_ko) %>% mutate(AN = as.numeric(an))
        df_extrait_smash <- popSimul[[an]] %>% 
          select(all_of(l_cols)) %>% 
          filter(ID %in% v_ids_inf_ko) %>% mutate(AN = as.numeric(an))
      
    } else {
      df_extrait_siasp <- rbind(
        df_extrait_siasp, popSIASP[[an]] %>% 
          select(all_of(l_cols)) %>% 
          filter(ID %in% v_ids_inf_ko) %>% mutate(AN = as.numeric(an))
        )
      df_extrait_smash <- rbind(
        df_extrait_smash, popSimul[[an]] %>% 
          select(all_of(l_cols)) %>% 
          filter(ID %in% v_ids_inf_ko) %>% mutate(AN = as.numeric(an))
        )
    }
  }

  df_extrait_siasp <- df_extrait_siasp %>% arrange(ID) %>% 
    rename(ANC_ECH = ANCIENNETE_ECH, 
          ANC_FPH = ANCIENNETE_FPH)
  df_extrait_smash <- df_extrait_smash %>% arrange(ID) %>% 
    rename(GRADE.Simu = GRADE, IM.Simu = IM, ECH.Simu = ECH,
          EQTP.Simu = EQTP,
          S_BRUT.Simu = S_BRUT,
          PRIMES.Simu = PRIMES,
          ANC_ECH.Simu = ANCIENNETE_ECH, 
          ANC_FPH.Simu = ANCIENNETE_FPH)

  df_compar <- cbind(
    df_extrait_siasp, 
    df_extrait_smash %>% 
      select(GRADE.Simu, IM.Simu, EQTP.Simu, ECH.Simu,
            ANC_ECH.Simu, ANC_FPH.Simu, S_BRUT.Simu, PRIMES.Simu)) %>% 
    select(AN,ID,AGE,GRADE,IM,ECH,AGE,GRADE.Simu,IM.Simu,ECH.Simu, 
          ANC_ECH, ANC_FPH, ANC_ECH.Simu, ANC_FPH.Simu, EQTP, EQTP.Simu,  
          S_BRUT, S_BRUT.Simu, PRIMES, PRIMES.Simu, IS_SORTANT) %>% 
    mutate(DIFF_S_BRUT = S_BRUT - S_BRUT.Simu,
          DIFF_S_BRUT_NORM = S_BRUT/EQTP - S_BRUT.Simu/EQTP.Simu)

  htmltools::tagList(
    list(
    htmltools::tags$h4(paste("Comparaison SIASP/SMASH d'outliers",
                              "pour les évolutions de SBRUT,", statut, ",", 
                            anDiff, "grades", paste(v_grades, collapse = " "))),
    reactable(df_compar,
              highlight = TRUE, minRows = length(popSIASP) )
    ))

  # Sauvegarde vers Excel
  xlsx::write.xlsx(df_compar, fic_xls, 
                  sheetName = paste("Diff S_BRUT", statut, anDiff, 
                                    "grades", paste(v_grades, collapse = " ")), 
                  row.names = FALSE, 
                  append = TRUE)

} else {
  htmltools::tags$h4(
    paste("Aucun agent trouvé pour la comparaison SIASP/SMASH d'outliers",
          "pour les évolutions de SBRUT,", statut, ",", 
          anDiff, "grades", paste(v_grades, collapse = " "))
    )
}
```



# Sauvegarde Excel

Les tableaux de ce document sont également sauvés dans un fichier Excel situé 
dans le même dossier que cette page HTML :

[`r basename(fic_xls)`]("`r fic_xls`")

