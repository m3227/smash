# Lecture et intégration des grilles de reclassement dans la BD ----


library(xlsx)
library(dplyr)

# Charge le package SMASH pour accès à la BD
BD <- GrillesIndiciaires$new()
df_grilles <- BD$getInfoGrilles(BD$searchGrille("PARA")$GRADE, "2022-01-01") 

write.table(df_grilles, 
            file=paste0('~/data/grilles/Cadre_param', f,'.csv'), 
            quote = FALSE, sep = ";")

# Fichier des infos reclassement fourni par la DGOS
fic_reclass <- '~/data/2022_03_07_Modalites_reclassement.xlsx'

# Listing des feuilles
wb <- loadWorkbook(fic_reclass)
l_feuilles <- names(getSheets(wb))
rm(wb)

xl <- list()
for (f in l_feuilles) {
  xl[[f]] <- read.xlsx(fic_reclass, sheetName = f)
}
