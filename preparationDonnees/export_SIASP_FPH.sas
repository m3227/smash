/* Projet Smash - 
 * Script de sélection de colonnes dans les données SIASP 
 * pour import dans R.
 */

SIGNON serv;
%let serv=PRO075001APP366;
/* Ouverture du dossier Panel DADS 2018 */
Rsubmit; 
libname salaires "\\PRO075001APP366\DREESSASCOMMUN$\BPS\SMASH\Salaires"; 
libname effectif "\\PRO075001APP366\DREESSASCOMMUN$\BPS\SMASH\Effectifs"; /* 8 lettres max ... */
endRsubmit;
 
libname salaires slibref=salaires server=serv; 
libname effectif slibref=effectif server=serv;

/** Choix des colonnes 
id_nir, DAT_DEBUT, DAT_FIN, AGE, APET, DUREE_CONTRAT, id_employeur, ID_PERIODE_PRI, INDICE, LIB_EMPLOI_1, NIC, POS_ACTIVITE, 
NB_HEURES_REMUN, NB_HEURES_TRAVAIL, duree_poste, EQTP, QUOTITE, RC, RSS, SEXE, SIREN, STATUT_CONTRAT, ST, NH, N, 
INFO_SUP1, EMP_CHAMP, EMP_SIREN, EMP_NIC, EMP_CJ, EMP_APET, 
depcom_fonction, Iact, AV_NAT, CNRACL_PAT, CNRACL_SAL, CRDS, CSG, FNAL, HSUP_DEDUC_PAT,
HSUP_REM_HSUP01_EXO, HSUP_REM_JSUP_EXO, INDEMN_RESID, NBI, PENSION_CIVILE, PRIMES_INIT, REM_HSUP_EXO, VSMT_TRANSP, 
RETRAITE_COMPL_PAT, RETRAITE_COMPL_SAL, S_BRUT, S_NET, TOT_COT_EMP, SFT, TOT_COT_SAL, TRMT_PRINC_INIT,
HSUP_NBHSUP01_EXO, HSUP_NBJSUP_EXO, CJ, enseignement, PRIMES_CORR, TRMT_PRINC_CORR, poids, 
nature_b, cat, N2, N3, ca, grade_fil, grade_fil_rank, corps
*/

/**
ERROR: The following columns were not found in the contributing tables: corps_deux, HSUP_REM_HCOM_EXO, 
       corps_trois, csreg4_bis, csreg5_bis, deb_nir, dep, HSUP_DEDUC_SAL, HSUP_NBHCOM_EXO,
       HSUP_REM_HCOM_EXO, region, secteur_final, siret, s_brut_k, s_net_k.
*/

/* Salaires :
fph2015_v8_c16.sas7bdat 
fph2016_v9_c17.sas7bdat 
fph2017_v7_c17.sas7bdat 
fph2017_v9_c18.sas7bdat 
fph2018_v7_c18.sas7bdat 
fph2018_v13_c19.sas7bdat 
fph2019_v6_c19.sas7bdat 
fph2019_v8_c20.sas7bdat 
fph2020_v3_c20.sas7bdat 

Effectifs :
fph_fpe_2015_c16.sas7bdat, 
fph_fpe_2016_c17.sas7bdat, 
fph_fpe_2017_c17.sas7bdat,
fph_fpe_2017_c18.sas7bdat,
fph_fpe_2018_c18.sas7bdat, 
fph_fpe_2018_c19.sas7bdat, 
fph_fpe_2019_c19.sas7bdat,
fph_fpe_2019_c20.sas7bdat,
fph_fpe_2020_c20.sas7bdat,

*/


/* Salaires : fph2015_v8_c16 */
/* ERROR: The following columns were not found in the contributing tables: enseignement,
       PRIMES_CORR, PRIMES_INIT, TRMT_PRINC_CORR, TRMT_PRINC_INIT.
 */

Rsubmit;

PROC SQL FEEDBACK;
	CREATE TABLE fph2015_v8_c16_selection AS
 	SELECT id_nir, DAT_DEBUT, DAT_FIN, AGE, APET, DUREE_CONTRAT, GRADE, id_employeur, ID_PERIODE_PRI, INDICE, LIB_EMPLOI_1, NIC, POS_ACTIVITE, 
		NB_HEURES_REMUN, NB_HEURES_TRAVAIL, duree_poste, EQTP, QUOTITE, RC, RSS, SEXE, SIREN, STATUT_CONTRAT, ST, NH, N, 
		INFO_SUP1, EMP_CHAMP, EMP_SIREN, EMP_NIC, EMP_CJ, EMP_APET, 
		depcom_fonction, Iact, AV_NAT, CNRACL_PAT, CNRACL_SAL, CRDS, CSG, FNAL, HSUP_DEDUC_PAT,
		HSUP_REM_HSUP01_EXO, HSUP_REM_JSUP_EXO, INDEMN_RESID, NBI, PENSION_CIVILE, REM_HSUP_EXO, VSMT_TRANSP, 
		RETRAITE_COMPL_PAT, RETRAITE_COMPL_SAL, S_BRUT, S_NET, TOT_COT_EMP, SFT, TOT_COT_SAL, poids,
		HSUP_NBHSUP01_EXO, HSUP_NBJSUP_EXO, CJ, /* enseignement, PRIMES_INIT, PRIMES_CORR, TRMT_PRINC_CORR, TRMT_PRINC_INIT, */
		nature_b, cat, N2, N3, ca, grade_fil, grade_fil_rank, corps
 	FROM salaires.fph2015_v8_c16
 	WHERE emp_champ="FPH";
RUN;


proc copy in=work
	out=salaires; 
	select fph2015_v8_c16_selection;
RUN;

endRsubmit;

/* Salaires : FPH2016_V9_C17 /  FPH2017_V7_C17 */
/* ERROR: The following columns were not found in the contributing tables: enseignement,
          PRIMES_CORR, PRIMES_INIT, TRMT_PRINC_CORR, TRMT_PRINC_INIT. */

Rsubmit;
PROC SQL FEEDBACK;
	CREATE TABLE fph2016_v9_c17_selection AS
 	SELECT id_nir, DAT_DEBUT, DAT_FIN, AGE, APET, DUREE_CONTRAT, GRADE, id_employeur, ID_PERIODE_PRI, INDICE, LIB_EMPLOI_1, NIC, POS_ACTIVITE, 
		NB_HEURES_REMUN, NB_HEURES_TRAVAIL, duree_poste, EQTP, QUOTITE, RC, RSS, SEXE, SIREN, STATUT_CONTRAT, ST, NH, N, 
		INFO_SUP1, EMP_CHAMP, EMP_SIREN, EMP_NIC, EMP_CJ, EMP_APET, 
		depcom_fonction, Iact, AV_NAT, CNRACL_PAT, CNRACL_SAL, CRDS, CSG, FNAL, HSUP_DEDUC_PAT,
		HSUP_REM_HSUP01_EXO, HSUP_REM_JSUP_EXO, INDEMN_RESID, NBI, PENSION_CIVILE, REM_HSUP_EXO, VSMT_TRANSP, 
		RETRAITE_COMPL_PAT, RETRAITE_COMPL_SAL, S_BRUT, S_NET, TOT_COT_EMP, SFT, TOT_COT_SAL, poids,
		HSUP_NBHSUP01_EXO, HSUP_NBJSUP_EXO, CJ,  /* enseignement, PRIMES_INIT, PRIMES_CORR, TRMT_PRINC_CORR, TRMT_PRINC_INIT, */
		nature_b, cat, N2, N3, ca, grade_fil, grade_fil_rank, corps
 	FROM salaires.fph2016_v9_c17
 	WHERE emp_champ="FPH";
RUN;


proc copy in=work
	out=salaires; 
	select fph2016_v9_c17_selection;
RUN;


PROC SQL FEEDBACK;
	CREATE TABLE fph2017_v7_c17_selection AS
 	SELECT id_nir, DAT_DEBUT, DAT_FIN, AGE, APET, DUREE_CONTRAT, GRADE, id_employeur, ID_PERIODE_PRI, INDICE, LIB_EMPLOI_1, NIC, POS_ACTIVITE, 
		NB_HEURES_REMUN, NB_HEURES_TRAVAIL, duree_poste, EQTP, QUOTITE, RC, RSS, SEXE, SIREN, STATUT_CONTRAT, ST, NH, N, 
		INFO_SUP1, EMP_CHAMP, EMP_SIREN, EMP_NIC, EMP_CJ, EMP_APET, 
		depcom_fonction, Iact, AV_NAT, CNRACL_PAT, CNRACL_SAL, CRDS, CSG, FNAL, HSUP_DEDUC_PAT,
		HSUP_REM_HSUP01_EXO, HSUP_REM_JSUP_EXO, INDEMN_RESID, NBI, PENSION_CIVILE, REM_HSUP_EXO, VSMT_TRANSP, 
		RETRAITE_COMPL_PAT, RETRAITE_COMPL_SAL, S_BRUT, S_NET, TOT_COT_EMP, SFT, TOT_COT_SAL, poids,
		HSUP_NBHSUP01_EXO, HSUP_NBJSUP_EXO, CJ,  /* enseignement, PRIMES_INIT, PRIMES_CORR, TRMT_PRINC_CORR, TRMT_PRINC_INIT, */
		nature_b, cat, N2, N3, ca, grade_fil, grade_fil_rank, corps
 	FROM salaires.fph2017_v7_c17
 	WHERE emp_champ="FPH";
RUN;

proc copy in=work
	out=salaires; 
	select fph2017_v7_c17_selection;
RUN;

endRsubmit;


/* SALAIRES.FPH2017_V9_C18 /  SALAIRES.FPH2018_V7_C18 */
/*   ERROR: The following columns were not found in the contributing tables: 
       PRIMES_CORR, PRIMES_INIT, TRMT_PRINC_CORR, TRMT_PRINC_INIT. */

Rsubmit;

PROC SQL FEEDBACK;
	CREATE TABLE fph2017_v9_c18_selection AS
 	SELECT id_nir, DAT_DEBUT, DAT_FIN, AGE, APET, DUREE_CONTRAT, GRADE, id_employeur, ID_PERIODE_PRI, INDICE, LIB_EMPLOI_1, NIC, POS_ACTIVITE, 
		NB_HEURES_REMUN, NB_HEURES_TRAVAIL, duree_poste, EQTP, QUOTITE, RC, RSS, SEXE, SIREN, STATUT_CONTRAT, ST, NH, N, 
		INFO_SUP1, EMP_CHAMP, EMP_SIREN, EMP_NIC, EMP_CJ, EMP_APET, 
		depcom_fonction, Iact, AV_NAT, CNRACL_PAT, CNRACL_SAL, CRDS, CSG, FNAL, HSUP_DEDUC_PAT,
		HSUP_REM_HSUP01_EXO, HSUP_REM_JSUP_EXO, INDEMN_RESID, NBI, PENSION_CIVILE, REM_HSUP_EXO, VSMT_TRANSP, 
		RETRAITE_COMPL_PAT, RETRAITE_COMPL_SAL, S_BRUT, S_NET, TOT_COT_EMP, SFT, TOT_COT_SAL, poids,
		HSUP_NBHSUP01_EXO, HSUP_NBJSUP_EXO, CJ,  enseignement, /* PRIMES_INIT, PRIMES_CORR, TRMT_PRINC_CORR, TRMT_PRINC_INIT, */ 
		nature_b, cat, N2, N3, ca, grade_fil, grade_fil_rank, corps
 	FROM salaires.fph2017_v9_c18
 	WHERE emp_champ="FPH";
RUN;


proc copy in=work
	out=salaires; 
	select fph2017_v9_c18_selection;
RUN;


PROC SQL FEEDBACK;
	CREATE TABLE fph2018_v7_c18_selection AS
 	SELECT id_nir, DAT_DEBUT, DAT_FIN, AGE, APET, DUREE_CONTRAT, GRADE, id_employeur, ID_PERIODE_PRI, INDICE, LIB_EMPLOI_1, NIC, POS_ACTIVITE, 
		NB_HEURES_REMUN, NB_HEURES_TRAVAIL, duree_poste, EQTP, QUOTITE, RC, RSS, SEXE, SIREN, STATUT_CONTRAT, ST, NH, N, 
		INFO_SUP1, EMP_CHAMP, EMP_SIREN, EMP_NIC, EMP_CJ, EMP_APET, 
		depcom_fonction, Iact, AV_NAT, CNRACL_PAT, CNRACL_SAL, CRDS, CSG, FNAL, HSUP_DEDUC_PAT,
		HSUP_REM_HSUP01_EXO, HSUP_REM_JSUP_EXO, INDEMN_RESID, NBI, PENSION_CIVILE, REM_HSUP_EXO, VSMT_TRANSP, 
		RETRAITE_COMPL_PAT, RETRAITE_COMPL_SAL, S_BRUT, S_NET, TOT_COT_EMP, SFT, TOT_COT_SAL, poids,
		HSUP_NBHSUP01_EXO, HSUP_NBJSUP_EXO, CJ,  enseignement, /* PRIMES_INIT, PRIMES_CORR, TRMT_PRINC_CORR, TRMT_PRINC_INIT, */
		nature_b, cat, N2, N3, ca, grade_fil, grade_fil_rank, corps
 	FROM salaires.fph2018_v7_c18
 	WHERE emp_champ="FPH";
RUN;

proc copy in=work
	out=salaires; 
	select fph2018_v7_c18_selection;
RUN;


endRsubmit;


/* Salaires : fph2018_v13_c19 / fph2019_v6_c19 */

Rsubmit;

PROC SQL FEEDBACK;
	CREATE TABLE fph2018_v13_c19_selection AS
 	SELECT id_nir, DAT_DEBUT, DAT_FIN, AGE, APET, DUREE_CONTRAT, GRADE, id_employeur, ID_PERIODE_PRI, INDICE, LIB_EMPLOI_1, NIC, POS_ACTIVITE, 
		NB_HEURES_REMUN, NB_HEURES_TRAVAIL, duree_poste, EQTP, QUOTITE, RC, RSS, SEXE, SIREN, STATUT_CONTRAT, ST, NH, N, 
		INFO_SUP1, EMP_CHAMP, EMP_SIREN, EMP_NIC, EMP_CJ, EMP_APET, 
		depcom_fonction, Iact, AV_NAT, CNRACL_PAT, CNRACL_SAL, CRDS, CSG, FNAL, HSUP_DEDUC_PAT,
		HSUP_REM_HSUP01_EXO, HSUP_REM_JSUP_EXO, INDEMN_RESID, NBI, PENSION_CIVILE, REM_HSUP_EXO, VSMT_TRANSP, 
		RETRAITE_COMPL_PAT, RETRAITE_COMPL_SAL, S_BRUT, S_NET, TOT_COT_EMP, SFT, TOT_COT_SAL, poids,
		HSUP_NBHSUP01_EXO, HSUP_NBJSUP_EXO, CJ,  enseignement, PRIMES_INIT, PRIMES_CORR, TRMT_PRINC_CORR, TRMT_PRINC_INIT, 
		nature_b, cat, N2, N3, ca, grade_fil, grade_fil_rank, corps
 	FROM salaires.fph2018_v13_c19
 	WHERE emp_champ="FPH";
RUN;

proc copy in=work
	out=salaires; 
	select fph2018_v13_c19_selection;
RUN;

PROC SQL FEEDBACK;
	CREATE TABLE fph2019_v6_c19_selection AS
 	SELECT id_nir, DAT_DEBUT, DAT_FIN, AGE, APET, DUREE_CONTRAT, GRADE, id_employeur, ID_PERIODE_PRI, INDICE, LIB_EMPLOI_1, NIC, POS_ACTIVITE, 
		NB_HEURES_REMUN, NB_HEURES_TRAVAIL, duree_poste, EQTP, QUOTITE, RC, RSS, SEXE, SIREN, STATUT_CONTRAT, ST, NH, N, 
		INFO_SUP1, EMP_CHAMP, EMP_SIREN, EMP_NIC, EMP_CJ, EMP_APET, 
		depcom_fonction, Iact, AV_NAT, CNRACL_PAT, CNRACL_SAL, CRDS, CSG, FNAL, HSUP_DEDUC_PAT,
		HSUP_REM_HSUP01_EXO, HSUP_REM_JSUP_EXO, INDEMN_RESID, NBI, PENSION_CIVILE, REM_HSUP_EXO, VSMT_TRANSP, 
		RETRAITE_COMPL_PAT, RETRAITE_COMPL_SAL, S_BRUT, S_NET, TOT_COT_EMP, SFT, TOT_COT_SAL, poids,
		HSUP_NBHSUP01_EXO, HSUP_NBJSUP_EXO, CJ,  enseignement, PRIMES_INIT, PRIMES_CORR, TRMT_PRINC_CORR, TRMT_PRINC_INIT, 
		nature_b, cat, N2, N3, ca, grade_fil, grade_fil_rank, corps
 	FROM salaires.fph2019_v6_c19
 	WHERE emp_champ="FPH";
RUN;

proc copy in=work
	out=salaires; 
	select fph2019_v6_c19_selection;
RUN;

endRsubmit;


/* fph2019_v8_c20 / fph2020_v3_c20 */

Rsubmit;


PROC SQL FEEDBACK;
	CREATE TABLE fph2019_v8_c20_selection AS
 	SELECT id_nir, DAT_DEBUT, DAT_FIN, AGE, APET, DUREE_CONTRAT, GRADE, id_employeur, ID_PERIODE_PRI, INDICE, LIB_EMPLOI_1, NIC, POS_ACTIVITE, 
		NB_HEURES_REMUN, NB_HEURES_TRAVAIL, duree_poste, EQTP, QUOTITE, RC, RSS, SEXE, SIREN, STATUT_CONTRAT, ST, NH, N, 
		INFO_SUP1, EMP_CHAMP, EMP_SIREN, EMP_NIC, EMP_CJ, EMP_APET, 
		depcom_fonction, Iact, AV_NAT, CNRACL_PAT, CNRACL_SAL, CRDS, CSG, FNAL, HSUP_DEDUC_PAT,
		HSUP_REM_HSUP01_EXO, HSUP_REM_JSUP_EXO, INDEMN_RESID, NBI, PENSION_CIVILE, REM_HSUP_EXO, VSMT_TRANSP, 
		RETRAITE_COMPL_PAT, RETRAITE_COMPL_SAL, S_BRUT, S_NET, TOT_COT_EMP, SFT, TOT_COT_SAL, poids,
		HSUP_NBHSUP01_EXO, HSUP_NBJSUP_EXO, CJ,  enseignement, PRIMES_INIT, PRIMES_CORR, TRMT_PRINC_CORR, TRMT_PRINC_INIT, 
		nature_b, cat, N2, N3, ca, grade_fil, grade_fil_rank, corps
 	FROM salaires.fph2019_v8_c20
 	WHERE emp_champ="FPH";
RUN;


proc copy in=work
	out=salaires; 
	select fph2019_v8_c20_selection;
RUN;


PROC SQL FEEDBACK;
	CREATE TABLE fph2020_v3_c20_selection AS
 	SELECT id_nir, DAT_DEBUT, DAT_FIN, AGE, APET, DUREE_CONTRAT, GRADE, id_employeur, ID_PERIODE_PRI, INDICE, LIB_EMPLOI_1, NIC, POS_ACTIVITE, 
		NB_HEURES_REMUN, NB_HEURES_TRAVAIL, duree_poste, EQTP, QUOTITE, RC, RSS, SEXE, SIREN, STATUT_CONTRAT, ST, NH, N, 
		INFO_SUP1, EMP_CHAMP, EMP_SIREN, EMP_NIC, EMP_CJ, EMP_APET, 
		depcom_fonction, Iact, AV_NAT, CNRACL_PAT, CNRACL_SAL, CRDS, CSG, FNAL, HSUP_DEDUC_PAT,
		HSUP_REM_HSUP01_EXO, HSUP_REM_JSUP_EXO, INDEMN_RESID, NBI, PENSION_CIVILE, REM_HSUP_EXO, VSMT_TRANSP, 
		RETRAITE_COMPL_PAT, RETRAITE_COMPL_SAL, S_BRUT, S_NET, TOT_COT_EMP, SFT, TOT_COT_SAL, poids,
		HSUP_NBHSUP01_EXO, HSUP_NBJSUP_EXO, CJ,  enseignement, /* PRIMES_INIT, PRIMES_CORR, TRMT_PRINC_CORR, TRMT_PRINC_INIT, */
		nature_b, cat, N2, N3, ca, grade_fil, grade_fil_rank, corps
 	FROM salaires.fph2020_v3_c20
 	WHERE emp_champ="FPH";
RUN;

proc copy in=work
	out=salaires; 
	select fph2020_v3_c20_selection;
RUN;


endRsubmit;






/* Effectifs :
fph_fpe_2015_c16.sas7bdat, */


Rsubmit;

PROC SQL FEEDBACK;
	CREATE TABLE fph_2015_c16_eff_selection AS
 	SELECT id_nir, cat, cat_final, grade_fil, grade_fil_final, csreg4_bis, csreg4_bis_final, csreg5_bis, csreg5_bis_final, siret,  region
 	FROM effectif.fph_fpe_2015_c16
 	WHERE emp_champ="FPH";
RUN;

proc copy in=work
	out=effectif; 
	select fph_2015_c16_eff_selection;
RUN;

endRsubmit;


Rsubmit; 
/*fph_fpe_2016_c17.sas7bdat, fph_fpe_2017_c17.sas7bdat */
PROC SQL FEEDBACK;
	CREATE TABLE fph_2016_c17_eff_selection AS
 	SELECT id_nir, cat, cat_final, grade_fil, grade_fil_final, csreg4_bis, csreg4_bis_final, csreg5_bis, csreg5_bis_final, siret,  region
 	FROM effectif.fph_fpe_2016_c17
 	WHERE emp_champ="FPH";
RUN;
proc copy in=work
	out=effectif; 
	select fph_2016_c17_eff_selection;
RUN;
PROC SQL FEEDBACK;
	CREATE TABLE fph_2017_c17_eff_selection AS
 	SELECT id_nir, cat, cat_final, grade_fil, grade_fil_final, csreg4_bis, csreg4_bis_final, csreg5_bis, csreg5_bis_final, siret,  region
 	FROM effectif.fph_fpe_2017_c17
 	WHERE emp_champ="FPH";
RUN;
proc copy in=work
	out=effectif; 
	select fph_2017_c17_eff_selection;
RUN;
endRsubmit;

Rsubmit; 
/*fph_fpe_2017_c18.sas7bdat,fph_fpe_2018_c18.sas7bdat, */
PROC SQL FEEDBACK;
	CREATE TABLE fph_2017_c18_eff_selection AS
 	SELECT id_nir, cat, cat_final, grade_fil, grade_fil_final, csreg4_bis, csreg4_bis_final, csreg5_bis, csreg5_bis_final, siret,  region
 	FROM effectif.fph_fpe_2017_c18
 	WHERE emp_champ="FPH";
RUN;
proc copy in=work
	out=effectif; 
	select fph_2017_c18_eff_selection;
RUN;
PROC SQL FEEDBACK;
	CREATE TABLE fph_2018_c18_eff_selection AS
 	SELECT id_nir, cat, cat_final, grade_fil, grade_fil_final, csreg4_bis, csreg4_bis_final, csreg5_bis, csreg5_bis_final, siret,  region
 	FROM effectif.fph_fpe_2018_c18
 	WHERE emp_champ="FPH";
RUN;
proc copy in=work
	out=effectif; 
	select fph_2018_c18_eff_selection;
RUN;
endRsubmit;

Rsubmit; 
/*fph_fpe_2018_c19.sas7bdat, fph_fpe_2019_c19.sas7bdat */
PROC SQL FEEDBACK;
	CREATE TABLE fph_2018_c19_eff_selection AS
 	SELECT id_nir, cat, cat_final, grade_fil, grade_fil_final, csreg4_bis, csreg4_bis_final, csreg5_bis, csreg5_bis_final, siret,  region
 	FROM effectif.fph_fpe_2018_c19
 	WHERE emp_champ="FPH";
RUN;
proc copy in=work
	out=effectif; 
	select fph_2018_c19_eff_selection;
RUN;
PROC SQL FEEDBACK;
	CREATE TABLE fph_2019_c19_eff_selection AS
 	SELECT id_nir, cat, cat_final, grade_fil, grade_fil_final, csreg4_bis, csreg4_bis_final, csreg5_bis, csreg5_bis_final, siret,  region
 	FROM effectif.fph_fpe_2019_c19
 	WHERE emp_champ="FPH";
RUN;
proc copy in=work
	out=effectif; 
	select fph_2019_c19_eff_selection;
RUN;
endRsubmit;

Rsubmit; 
/*fph_fpe_2019_c20.sas7bdat,fph_fpe_2020_c20.sas7bdat, */

PROC SQL FEEDBACK;
	CREATE TABLE fph_2019_c20_eff_selection AS
 	SELECT id_nir, cat, cat_final, grade_fil, grade_fil_final, csreg4_bis, csreg4_bis_final, csreg5_bis, csreg5_bis_final, siret,  region
 	FROM effectif.fph_fpe_2019_c20
 	WHERE emp_champ="FPH";
RUN;
proc copy in=work
	out=effectif; 
	select fph_2019_c20_eff_selection;
RUN;
PROC SQL FEEDBACK;
	CREATE TABLE fph_2020_c20_eff_selection AS
 	SELECT id_nir, cat, cat_final, grade_fil, grade_fil_final, csreg4_bis, csreg4_bis_final, csreg5_bis, csreg5_bis_final, siret,  region
 	FROM effectif.fph_fpe_2020_c20
 	WHERE emp_champ="FPH";
RUN;
proc copy in=work
	out=effectif; 
	select fph_2020_c20_eff_selection;
RUN;
endRsubmit;
