
# popSimul avec les données SIASP réelles ----
# Création de l'équivalent du résultat de simulation SMASH à partir 
# des données réelles : 
# Le fichier créé va permettre de comparer des résultats de simulations 
# avec les données réelles.

# source('preparationDonnees/creationPopSimulSIASP.R', encoding = 'UTF-8', echo = FALSE)
library(dplyr)
library(fst)
devtools::load_all("./../smash-r-pkg/")


## Paramètres ----
# Récupération paramètre année (entête Rmd)
anneeDebut <- 2009
anneeFin <- 2021
champ <- "hop" # "fph" #  


#b_agrege <- TRUE # FALSE #
# path_siasp <- "T:/BPS/SMASH/DonneesSIASP/Tables_FST"
path_siasp = "~/data/DonneesSIASP/Tables_FST"

for (b_agrege in c(TRUE, FALSE)) {
  
  # Flag pour garder les OUT sur l'ensemble des années
  # TRUE : OUT conservés sur toutes les années
  # FALSE : OUT présents uniquement 1 an après leur sortie
  # b_out <- FALSE # TRUE # 
  for (b_out in c(TRUE, FALSE)) {
    

    # La sortie est sauvée en RDS (1 liste ne peut être sauvée en FST)
    fic_pop <- sprintf('popSiasp_%s_%d-%d%s%s.rds', 
                       champ, anneeDebut, anneeFin, 
                       ifelse(b_out, "_allOUT",""),
                       ifelse(b_agrege, "_agrege",""))
    
    ## Chargement données ----
    v_annees <- seq(anneeDebut, anneeFin)
    popSIASP <- list()
    for (an in v_annees) {
      strAn <- as.character(an)
      cat(' - chargement données', an, "\n")
    
      ## charge les variables pour la simulation ----
      popSIASP[[strAn]] <- getEchantillonSIASP(champ, an, 
                                               num_pop = 0,
                                               list_corps = NULL, 
                                               b_agrege = b_agrege)
      ## Ajout cout_employeur ----
      popSIASP[[strAn]]$cout_employeur <- popSIASP[[strAn]]$S_BRUT + 
        popSIASP[[strAn]]$TOT_COT_EMP
    
    
      ## Création des sortants à partir de anneeDebut+1 ----
      # Avec le GRADE "OUT" (pour les visus)
      if (an>anneeDebut) {
        strAnPrec <- as.character(an-1)
        ### Sél° sortants an précédent ----
        if (b_out) {
          ### Avec reconduction des OUT ----
          popPrec <- popSIASP[[strAnPrec]] %>% 
            dplyr::filter(IS_SORTANT==T | STATUT=="OUT")
          
          ### Enlève ceux qui sont présents cette année (retour OUT->FPH)
          popPrec <- popPrec %>% filter(!(ID %in% unique(popSIASP[[strAn]]$ID)))
        } else {
          ### Sans reconduct° des OUT ----
          # Ajoute juste les sortants de l'année préc comme OUT
          popPrec <- popSIASP[[strAnPrec]] %>% dplyr::filter(IS_SORTANT==T)
        }
    
        nPrec <- nrow(popPrec)
        v_zeros <- rep(0, nPrec)
        
        cat("  -> ajout des sortants an précédent", 
            ifelse(b_out,"avec", "sans"),"reconduct° des OUT :", 
            nPrec, "personnes\n")
        
        popSIASP[[strAn]] <- rbind(popSIASP[[strAn]], 
          data.frame(ID               = popPrec$ID,
                     AGE              = popPrec$AGE +1 ,
                     SEXE             = popPrec$SEXE,
                     STATUT           = rep("OUT", nPrec),
                     STATUT_CONTRAT   = rep("OUT", nPrec),
                     DUREE_CONTRAT    = popPrec$DUREE_CONTRAT,
                     INFO             = popPrec$INFO,
                     CORPS            = rep("OUT", nPrec),
                     GRADE            = rep("OUT", nPrec),
                     ECH              = v_zeros,
                     IM               = v_zeros,
                     CTI              = v_zeros,
                     NBI              = v_zeros,
                     QUOTITE          = v_zeros,
                     EQTP             = v_zeros,
                     ANCIENNETE_ECH   = v_zeros,
                     ANCIENNETE_FPH   = v_zeros,
                     S_BRUT           = v_zeros,
                     S_NET            = v_zeros,
                     PRIMES           = v_zeros,
                     TOT_COT_EMP      = v_zeros,
                     TOT_COT_SAL      = v_zeros,
                     SIREN            = v_zeros,
                     REGION           = popPrec$REGION,
                     DEPCOM_RESIDENCE = popPrec$DEPCOM_RESIDENCE,
                     DEPCOM_FONCTION  = popPrec$DEPCOM_FONCTION,
                     IS_ENTRANT       = rep(F, nPrec),
                     IS_SORTANT       = rep(F, nPrec),
                     cout_employeur   = v_zeros
                     )
        )
      }
      
    }
    
    # Sauvegardes ---- 
    # Sauvegarde de la population au format micro-simulation
    chemin_local <- "~/data/DonneesSIASP"
    chemin_dist <- "T:/BPS/SMASH/DonneesSIASP/"
    
    # Sauvegarde des modèles en local et en distant
    cat("Sauvegarde du fichier :\n",fic_pop,"\n")
    
    tryCatch( {
        cat("Sauvegarde en local, dossier :", chemin_local)
        saveRDS(popSIASP, file.path(chemin_local, fic_pop))
      },
      error = function(e) {
        message( paste("sauvegarde en local impossible, dossier :", chemin_local) )
      }
    )

  }
}

# tryCatch( {
#     cat("Sauvegarde distante, dossier :", chemin_dist)
#     file.copy(file.path(chemin_local, fic_pop), chemin_dist, overwrite = TRUE)
#   },
#   error = function(e) {
#     message( paste("sauvegarde distante impossible, dossier :", chemin_dist) )
#   }
# )

