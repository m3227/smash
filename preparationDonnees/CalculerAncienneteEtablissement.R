# Script de calcul de l'ancienneté dans l'établissement ----
# Ajoute une variable ANC_SIREN qui comptabilise l'ancienneté des agents par 
# établissement (au sens du SIREN)

library(dplyr)
library(fst)
devtools::load_all("../smash-r-pkg/")


chemin_local <- "~/data/DonneesSIASP/Tables_FST/" 
chemin_dist <- "T:/BPS/SMASH/DonneesSIASP/Tables_FST" 

cat(paste("Début calcul des anciennetés établissement :", get_timestamp(), "\n"))

# Parcours des données ----
anDeb <- 2009
anFin <- 2021

for (champ in c("hop", "fph")) { # "fph")) { #
  
  for (an in seq(anDeb, anFin)) {

    strAn <- as.character(an)

    # Chargement (ex: "fph2011_selection.fst")
    df <- chargerFST(champ, an, b_selection = TRUE) %>% 
      mutate(IDX = row_number())
    
    if (an == anDeb) {
      # Première année : on recopie ANC_FPH
      df$ANC_SIREN <- NA # df$ANC_FPH/365
      df$ANC_SIREN_TRUNC <- df$ANC_FPH/365
      
    } else {
      # Autres Années : différents cas 
      # 1 : non présents l'année d'avant (entrants) : ANC_SIREN = ANC_FPH
      is_entrant <- !(df$ID_NIR20 %in% unique(df_anPrec$ID_NIR20))
      df$ANC_SIREN[is_entrant] <- df$ANC_FPH[is_entrant]/365
      df$ANC_SIREN_TRUNC[is_entrant] <- df$ANC_FPH[is_entrant]/365
      
      # 2: présents/présents compare le SIREN de l'année précédente
      df_compar <- inner_join(df %>% select(IDX, ID_NIR20, SIREN), 
                             df_anPrec %>% select(IDX, ID_NIR20, SIREN), 
                             by.x = c("ID_NIR20", "SIREN"))
      
      # Pour ceux trouvés dans le inner_join : Ajoute un an à l'ancienneté
      df$ANC_SIREN[df_compar$IDX.x] <- df_anPrec$ANC_SIREN[df_compar$IDX.y] + 1
      df$ANC_SIREN_TRUNC[df_compar$IDX.x] <- df_anPrec$ANC_SIREN_TRUNC[df_compar$IDX.y] + 1
      
      # Pour les autres réset -> ANC_SIREN = 0 ou 1 
      df$ANC_SIREN[setdiff(df$IDX, df_compar$IDX.x)] <- 0
      df$ANC_SIREN_TRUNC[setdiff(df$IDX, df_compar$IDX.x)] <- 0
    }


    # Sauvegarde local / distante
    fic <- file.path(chemin_local,sprintf("%s%d_selection.fst", champ, an))
    cat(" -> sauvegarde locale", fic, "\n")
    fst::write.fst(df %>% select(-IDX), path = fic, compress = 100)
    
    # Sauvegarde les données de l'année comme données de l'année précédente
    df_anPrec <- df
    
    
  }
}


cat(paste("Fin calcul des anciennetés établissement :", get_timestamp(), "\n"))

