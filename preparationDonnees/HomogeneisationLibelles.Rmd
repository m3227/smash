---
title: "Homogénéisation Libellés"
author: "BPS DREES - Gilles Gonon"
date: "11/03/2022"
output: pdf_document
---

```{r setup, include=FALSE}
knitr::opts_chunk$set(echo = TRUE)
library(stringi)
library(tokenizers)
library(stringdist)
library(dplyr)
# devtools::load_all("../../smash-r-pkg")

```

## Objectif

L'objectif de ce traitement est d'uniformiser les libellés des emplois 
dans les données SIASP pour vérifier et assurer une meilleure cohérence 
avec les grades. 

Pour cela nous cherchons à apparier les libellés remplis à la main dans les
données déclaratives SIASP à ceux des nomenclatures DGOS NEHPNM et NEHMED.

Nous chargeons les données SIASP, ainsi que les libellés de nomenclatures depuis 
la base de données des GrillesIndiciaires.

```{r chargement_donnees}
# chemin <- "~/data/DonneesSIASP"
chemin <- "T:/BPS/SMASH/DonneesSIASP"

annee <- 2020
donnees <- "fph" # "hop" #
df <- as.data.frame(readRDS(
  file.path(chemin, "Tables_RDS", paste(donnees, annee, "_selection.rds",
                        sep =""))))

# Libellés SIASP
df_lib <- df %>% count(LIB_EMPLOI_1, name="total", sort = T)

# Libellé nomenclature
df_nom <- read.csv("T:/BPS/SMASH/DonneesSIASP/libelles-pnm-medi.csv", sep = ";")
df_nom$LIBELLE <- toupper(stri_trans_general(df_nom$LIBELLE, "Latin-ASCII"))

# Libelles SIASP + NOMENCLATURE
# df_libelles <- read.csv(file.path(chemin, "libelle poste SIASP 2020.csv"))

```


## Prétraitement

Le prétraitement consiste en une série de régularisation réalisée en observant
les données (data.frame df_lib).

Notamment, il y a une uniformisation des classes et grades pour devenir

- CLASSE NORMALE : CLN
- CLASSE SUPERIEURE : CLS
- CLASSE EXCEPTIONNELLE : CLE
- CLASSE 1/2 : CL1/2
- GRADE 1/2/3 : GR1/2/3

C'est assez flexible et il est facile de rajouter des traitements pour améliorer
la préparation des données.

```{r pretraitement, echo=FALSE}
preparer_libelle <- function(libelles) {
  # Mise en forme des libellés
  # browser()
  # Suppression des inutiles (non informatif)
  libelles_out <- stri_replace_all_regex(libelles, "[,'_/-]", " ")
  libelles_out <- stri_replace_all_fixed(libelles_out, "-", " ")
  libelles_out <- stri_replace_all_regex(libelles_out, "( DES | DE | DU | D | EN )", " ")
  libelles_out <- stri_replace_all_fixed(libelles_out, ".", " ")
  libelles_out <- stri_replace_all_regex(libelles_out, "\\s{2,}", " ")
  libelles_out <- stri_replace_all_regex(libelles_out, "\\(|\\)", "")
  # Vire les nombres (e.g. dates nomenclature) 
  libelles_out <- stri_replace_all_regex(libelles_out, "\\d{2,}", "")
  
  
  # Reformulation des mots, avec des espaces
  libelles_out <- stri_replace_all_fixed(libelles_out, "AGT", "AGENT")
  libelles_out <- stri_replace_all_regex(libelles_out, "ADJ\\s*(CL|CAD|ADM)", "ADJ $1")
  libelles_out <- stri_replace_all_regex(libelles_out, "ADADMHOS", "ADJOINT ADM HOS")
  libelles_out <- stri_replace_all_regex(libelles_out, "ADMHOS", "ADM HOS")
  libelles_out <- stri_replace_all_regex(libelles_out, "SOCIOE", "SOCIO E")
  libelles_out <- stri_replace_all_regex(libelles_out, "MEDICOA", "MEDICO A")
  libelles_out <- stri_replace_all_regex(libelles_out, "HOSPITALOU", "HOSPITALO U")
  
  # Homogénéisation des grades
  libelles_out <- stri_replace_all_regex(libelles_out,
                                         "(1\\s*ER*E*\\s*GRA*D*E*)", "GR1")
  
  libelles_out <- stri_replace_all_regex(libelles_out,
                                         "GR*A*D*E*\\s*(\\d)", "GR$1")
  
  # Grade 2 et 3
  libelles_out <- stri_replace_all_regex(libelles_out,
                                         "(\\d)\\s*I*EM*E*\\s*GRA*D*E*", "GR$1")
  libelles_out <- stri_replace_all_regex(libelles_out, "2ND GRA*D*E*", "GR2")
  libelles_out <- stri_replace_all_regex(libelles_out, "(2|1)(ND|ER) GR*A*", "GR$1")
  libelles_out <- stri_replace_all_regex(libelles_out, "PREMIER GRADE", "GR1")
  libelles_out <- stri_replace_all_regex(libelles_out, "SECOND GRADE", "GR2")
  
  
  
  # Homogénéisation des classes
  libelles_out <- stri_replace_all_regex(libelles_out, 
                                         "(\\s|^)(\\d)\\s*CL(\\s|$)", "$1CL$2$3")
  
  libelles_out <- stri_replace_all_regex(libelles_out,
    "(\\s|^)(CLASSE|CLAS|CLA|CL)(\\s|$)", "$1CLASSE$3")
  
  
  libelles_out <- stri_replace_all_regex(libelles_out,
                                         "CLASSE\\s*SU*P*E*R*I*E*U*R*E*",  "CLS")
  libelles_out <- stri_replace_all_regex(libelles_out,
                                         "CLASSE\\s*NOR*M*A*L*E*", "CLN")
  libelles_out <- stri_replace_all_regex(libelles_out,
                                         "CLASSE\\s*EXC*E*P*T*I*O*N*N*E*L*L*E*",
                                         "CLE")
  libelles_out <- stri_replace_all_regex(libelles_out,
                                         "\\sCL*\\s([SNE])\\s|\\sCL*\\s([SNE])$",
                                         " CL$1 ")
  
  libelles_out <- stri_replace_all_regex(libelles_out,
                                         "(\\d)\\s*I*EM*E*\\s*CLASSE", "CL$1")
  libelles_out <- stri_replace_all_regex(libelles_out,
                                         "CL*A*S*S*E*\\s*(\\d)", "CL$1")
  libelles_out <- stri_replace_all_regex(libelles_out,
                                         "(\\d)\\s*CLASSE", "CL$1")
  libelles_out <- stri_replace_all_regex(libelles_out,
                                         "1\\s*ER*E*\\s*CLA*S*S*E*", "CL1")
  
  libelles_out <- stri_replace_all_regex(libelles_out,
                                         "(1|2)(RE|ME)*\\s*CLA*S*S*E*", "CL$1")
  libelles_out <- stri_replace_all_fixed(libelles_out, "PREMIERE CLASSE", "CL1")
  libelles_out <- stri_replace_all_fixed(libelles_out, "SECONDE CLASSE", "CL2")

  # Catégories
  libelles_out <- stri_replace_all_regex(libelles_out, "(CATEGORIE|CATEG|CAT)\\s*(A|B|C)", "CATEGORIE $2")


  # Expansion des abrévations de la + grande à la plus petite
  libelles_out <- stri_replace_all_regex(libelles_out,
                                         "(A\\s*S\\s*H\\s*Q|A\\s*H\\s*S\\s*Q)(\\s|$)",
                                         "AGENT SERVICE HOSPITALIERS QUALIFIE$2")
  
  libelles_out <- stri_replace_all_regex(libelles_out, "A\\s*S\\s*H(\\s|$)", 
                                         "AGENT SERVICE HOSPITALIERS$1")
  
  libelles_out <- stri_replace_all_regex(libelles_out, 
                                         "I\\s*A\\s*D\\s*E(\\s|$)",
                                         "INFIRMIER ANESTHESISTE$1")
  
  libelles_out <- stri_replace_all_regex(libelles_out, 
                                         "I\\s*B\\s*O\\s*D\\s*E(\\s|$)",
                                         "INFIRMIER BLOC OPERATOIRE$1")
  libelles_out <- stri_replace_all_regex(libelles_out, 
                                         "A\\s*A\\s*H(\\s|$)",
                                         "ATTACHE ADM HOSPITALIERE$1")
  libelles_out <- stri_replace_all_regex(libelles_out, 
                                         "A\\s*E\\s*Q(\\s|$)",
                                         "AGENT ENTRETIEN QUALIFIE$1")
  libelles_out <- stri_replace_all_regex(libelles_out, 
                                         "A\\s*H\\s*U(\\s|$)",
                                         "ASSISTANT HOSPITALIER UNIVERSITAIRE$1")
  libelles_out <- stri_replace_all_regex(libelles_out, 
                                         "A\\s*M\\s*P(\\s|$)",
                                         "AIDE MEDICO PSYCHOLOQIQUE$1")
  
  libelles_out <- stri_replace_all_regex(libelles_out, "A\\s*S(\\s|$)", 
                                         "AIDE SOIGNANT$1")
  
  libelles_out <- stri_replace_all_regex(libelles_out, "A\\s*D\\s*C\\s*H(\\s|$)", 
                                         "ADJOINT CADRE HOSPITALIER$1")

  libelles_out <- stri_replace_all_regex(libelles_out, "A\\s*R\\s*C(\\s|$)", 
                                         "ATTACHE RECHERCHE CLINIQUE$1")

  libelles_out <- stri_replace_all_regex(libelles_out, "A SOI", "AIDE SOI")
  
  # Reste à faire
  # APA, AVQ, ATEP, CAE, ADCH, ...
  
  # Suppressions 1ère lettre (classement logiciel)
  libelles_out <- stri_replace_all_regex(libelles_out, "^Z{1,2}\\s*", "")
  
  
  return(libelles_out)
}

# Ajoute une colonne pour comparer les résultats
df_lib$LIB_NORM <- preparer_libelle(df_lib$LIB_EMPLOI_1)

df_nom$LIB_NORM <- preparer_libelle(df_nom$LIBELLE)

```

## Calcul des distances entre grades 


### Préparation lemmatisation

Ici on ajoute une phase d'homogénéisation avant calcul des distances entre mots
en tronquant à 3 lettres maximum chacun des terme des libellés. 

```{r lemmatize, echo=FALSE}
f_sub <- function(x) {substr(x,1,3)}
f_lemmatize <- function(x, tri=F) { 
  if (tri) {
    y <- stri_join_list(sort(lapply(stri_split_fixed(x, " "), f_sub)), sep = " ")
  } else {
    y <- stri_join_list(lapply(stri_split_fixed(x, " "), f_sub), sep = " ")
  }
  return(y)
}

tri <- F # Flag pour trier les termes
df_lib$LIB_NORM_LEMMA <- f_lemmatize(df_lib$LIB_NORM, tri)
df_nom$LIB_NORM_LEMMA <- f_lemmatize(df_nom$LIB_NORM, tri)

count_df_libnorm <- df_lib %>% group_by(LIB_NORM_LEMMA) %>% 
  summarise(total = sum(total)) %>% arrange(desc(total))

```

### Calcul des distances

Dans un premier temps, l'appariement est réalise en calculant une distance 
de type [Similarité cosinus](https://fr.wikipedia.org/wiki/Similarit%C3%A9_cosinus) 
entre les textes.

L'appariement est loin d'être parfait, mais il permet déjà de fournir un aperçu 
sans technique de machine learning. 

```{r txt_dist}
l_libelles <- df_lib$LIB_NORM_LEMMA

# Colonnes de sortie : on stocke les N premiers résultats
df_assoc <- df_lib
df_assoc$LIB_NEH <- ""
df_assoc$LIB_NEH_LEM <- ""
df_assoc$score <- 0
df_assoc$score_2 <- 0
df_assoc$LIB_NEH_2 <- ""
df_assoc$LIB_NEH_LEM_2 <- ""

for ( idx in (1:length(l_libelles)) ) { # (1:2)) { # 
  # distance entre le corps et tous les libellés de nomenclature
  if (l_libelles[idx]=="") { next; }
  
  v_dist = stringdist(l_libelles[idx], df_nom$LIB_NORM_LEMMA, method = "cos")
  score.tri <- sort(v_dist, index.return = T)

  
  #if (score<=0.6) {
    df_assoc$LIB_NEH[idx]     <- df_nom$LIBELLE[score.tri$ix[1]]
    df_assoc$LIB_NEH_LEM[idx] <- df_nom$LIB_NORM_LEMMA[score.tri$ix[1]]
    df_assoc$score[idx]       <- score.tri$x[1]
    df_assoc$LIB_NEH_2[idx]     <- df_nom$LIBELLE[score.tri$ix[2]]
    df_assoc$LIB_NEH_LEM_2[idx] <- df_nom$LIB_NORM_LEMMA[score.tri$ix[2]]
    df_assoc$score_2[idx]       <- score.tri$x[2]
  #} else {
  #  df_assoc$grille_txt[idx] <-  NA
  #  df_assoc$dist_min[idx]   <-  score
  #}
}

```

On sauve les résultats
```{r resultats}
# print(df_assoc)
fic_assoc <- file.path(chemin, "association_libelle_siasp_nomenclature_cos.csv")
l_cols <- c("total", "LIB_EMPLOI_1", "LIB_NEH", "score", "LIB_NEH_2", "score_2", 
            "LIB_NORM", "LIB_NORM_LEMMA", "LIB_NEH_LEM", "LIB_NEH_LEM_2")   
write.table(df_assoc[,l_cols], file = fic_assoc, sep = ";", quote = F, row.names = F)

```

