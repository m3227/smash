# Création des sortants 2021
# 
devtools::load_all("../smash-r-pkg")

fic_id_sortants <- paste0("I:/BPS/_Emplois et revenus/Projet SMASH/", 
                          "05_ReponsesALaDemande/GVT_ONDAM_2024/id_nir20_sortants_2021_v6.xlsx")

df_id_sortants <- xlsx::read.xlsx(fic_id_sortants, "Sortants")
v_id_sortants <- df_id_sortants$id_nir20

annee <- 2021

# Ajout de sortants supplémentaire par catégorie ----
# On crée le fichier si besoin sinon on charge le fichier existant
fic_id_sortants_plus <- paste0("~/GVT_ONDAM_2024/id_nir20_sortants_2021_ajout_fph.xlsx")
if (file.exists(fic_id_sortants_plus)) {
  # Lecture du fichier déjà créé
  df_id_sortants_plus <- xlsx::read.xlsx(fic_id_sortants_plus, "Sortants")


} else {

  # Chargement des données, en excluant les sortants déjà identifiés et les entrants
  df_entrants <- chargerFST("fph", annee, b_entrants = T)
  df_fph <- chargerFST("fph", annee, b_agrege = T) %>%
    filter(!(ID_NIR20 %in% union(df_id_sortants$id_nir20,
                                 df_entrants$ID_NIR20))) %>%
    filter(DAT_FIN < as.POSIXlt("2021-12-31", tz = "UTC") &
             EQTP > 0 & S_BRUT > 0 &
             POSTE_PRINC_AN=="P" & STATUT_CONTRAT %in% c("TITH", "CONT"))
  
  # df_hop <- chargerFST("hop", annee, b_agrege = T) %>%
  #   filter(!(ID_NIR20 %in% union(df_id_sortants$id_nir20,
  #                                df_entrants$ID_NIR20))) %>%
  #   filter(DAT_FIN < as.POSIXlt("2021-12-31", tz = "UTC") &
  #            EQTP > 0 & S_BRUT > 0 &
  #            POSTE_PRINC_AN=="P" & STATUT_CONTRAT %in% c("TITH", "CONT"))
  # 
  # df_esms <- chargerFST("esms", annee, b_agrege = T) %>%
  #   filter(!(ID_NIR20 %in% union(df_id_sortants$id_nir20,
  #                                df_entrants$ID_NIR20))) %>%
  #   filter(DAT_FIN < as.POSIXlt("2021-12-31", tz = "UTC") &
  #            EQTP > 0 & S_BRUT > 0 &
  #            POSTE_PRINC_AN=="P" & STATUT_CONTRAT %in% c("TITH", "CONT"))

  # TIRAGE aléatoires : n fph
  v_ids_fph <- sample(df_fph$ID_NIR20, 6600)
  
  # v_ids_hop <- sample(df_hop$ID_NIR20, 2400)
  # v_ids_esms <- sample(df_esms$ID_NIR20, 6300)

  df_id_sortants_plus <- data.frame(id_nir20 = v_ids_fph) # c(v_ids_hop, v_ids_esms))

  # Sauvegarde du fichier Excel
  xlsx::write.xlsx(df_id_sortants_plus, fic_id_sortants_plus,
                   sheetName = "Sortants",
                   row.names = FALSE)

}

# Concaténation des 2 listes
v_id_sortants <- c(df_id_sortants$id_nir20,
                   df_id_sortants_plus$id_nir20)

chemin <- "~/data/DonneesSIASP/FST"



cat("Création des sortants", annee, "\n")

for (champ in c("fph")) { # "hop", "esms", "siasp", "fph")) { # 
  
  for (b in c(T,F)) {
    # Soit _selection, soit _agrege
    b_agr <- b
    b_sel <- !b
    
    fic_sortants <- sprintf("%s_sortants%d%s%s.fst", champ,
                            annee,
                            ifelse(b_sel, "_selection", ""),
                            ifelse(b_agr, "_agrege", "") )
    # Chargement 
    df <- chargerFST(champ, annee, b_selection = b_sel, b_agrege = b_agr)
    
    # Sélection des sortants
    df_out <- df %>% filter(ID_NIR20 %in% v_id_sortants)
    
    fst::write.fst(df_out, file.path(chemin, fic_sortants))
    cat(" - ", fic_sortants, "\n")
    
  }
  
}

browseURL(chemin)
